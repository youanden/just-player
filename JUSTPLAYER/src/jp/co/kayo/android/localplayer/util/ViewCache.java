package jp.co.kayo.android.localplayer.util;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import jp.co.kayo.android.localplayer.appwidget.ColorSet;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioAlbum;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioFavorite;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.service.EasySSLSocketFactory;
import jp.co.kayo.android.localplayer.util.bean.FavoriteInfo;

public class ViewCache extends Fragment {

    private static final int MAX_QUEUE = 10;
    private static final int EVT_ADDTASK = 1000;
    private static File sRootdir = new File(
            Environment.getExternalStorageDirectory(),
            "data/jp.co.kayo.android.localplayer/cache/.albumart/");
    private static final int READ_BUF = 1024 * 5;

    private ExecutorService executor = Executors.newFixedThreadPool(MAX_QUEUE);
    private static HashMap<Integer, SoftReference<Bitmap>> mBitmaps = new HashMap<Integer, SoftReference<Bitmap>>();
    private boolean hasDesotoyd = false;
    private int mCurrentPos = -1;
    private long mCurrentId = -1;
    private long mPrefetchId = -1;
    private long mMax = 0;
    private volatile SchemeRegistry mSchemeRegistry;
    private volatile ThreadSafeClientConnManager clientConnectionManager = null;
    private long mPos = 0;
    private BasicHttpParams mParams = new BasicHttpParams();
    private final int CONNECTION_TIMEOUT = 10000;
    private final int SOCKET_TIMEOUT = 10000;
    private final int SOCK_BUFSIZE = 4096;

    private ViewCache mViewcache;
    private ColorSet mColorset = new ColorSet();;

    public ViewCache() {
        super();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Funcs.disableConnectionReuseIfNecessary();
        Funcs.enableHttpResponseCache(activity);
    }

    public static Bitmap getImage(Integer key) {
        if (mBitmaps.containsKey(key)) {
            SoftReference<Bitmap> ref = mBitmaps.get(key);
            if (ref != null) {
                return ref.get();
            }
        }
        return null;
    }

    public static void setImage(Integer key, Bitmap image) {
        mBitmaps.put(key, new SoftReference<Bitmap>(image));
    }

    public static boolean hasImage(Integer key) {
        return mBitmaps.containsKey(key);
    }

    public static void clearImage() {
        mBitmaps.clear();
    }

    private SharedPreferences mPref;
    
    private Handler mHandler = new MyHandler(this);
    private static class MyHandler extends Handler{
        WeakReference<ViewCache> refCache;
        MyHandler(ViewCache v){
            refCache = new WeakReference<ViewCache>(v);
        }
        
        @Override
        public void handleMessage(Message msg) {
            ViewCache v = refCache.get();
            if(v!=null){
                if (msg.what == EVT_ADDTASK) {
                    GetImage task = (GetImage) msg.obj;
                    if (!v.executor.isTerminated() && !v.executor.isShutdown()) {
                        v.executor.execute(task);
                    }
                }
            }
        }
    };

    synchronized SchemeRegistry getRegistry() {
        if (mSchemeRegistry == null) {
            mSchemeRegistry = new SchemeRegistry();
            // http scheme
            mSchemeRegistry.register(new Scheme("http", PlainSocketFactory
                    .getSocketFactory(), 80));
            // https scheme
            mSchemeRegistry.register(new Scheme("https",
                    new EasySSLSocketFactory(), 443));

            HttpConnectionParams.setSocketBufferSize(mParams, SOCK_BUFSIZE); // ソケットバッファサイズ
                                                                             // 4KB
            HttpConnectionParams.setSoTimeout(mParams, SOCKET_TIMEOUT); // ソケット通信タイムアウト20秒
            HttpConnectionParams.setConnectionTimeout(mParams, SOCKET_TIMEOUT); // HTTP通信タイムアウト20秒
            HttpProtocolParams.setContentCharset(mParams, "UTF-8"); // 文字コードをUTF-8と明示
            HttpProtocolParams.setVersion(mParams, HttpVersion.HTTP_1_1);

            CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
            BasicHttpContext context = new BasicHttpContext();
            context.setAttribute("http.auth.credentials-provider",
                    credentialsProvider);
        }
        return mSchemeRegistry;
    }

    synchronized ThreadSafeClientConnManager getClientConnManager() {
        if (clientConnectionManager == null) {
            clientConnectionManager = new ThreadSafeClientConnManager(mParams,
                    getRegistry());
        }

        return clientConnectionManager;
    }

    public static File createAlbumArtFile(Integer key) {
        File file = new File(sRootdir, key + ".jpg");
        return file;
    }

    private class GetImage implements Runnable {
        String src;
        Integer key;
        boolean manageflg;
        int fitsize;
        ImageObserver observer;

        public GetImage(String src, Integer key, ImageObserver observer,
                int fitsize) {
            this.src = src;
            this.key = key;
            this.observer = observer;
            this.fitsize = fitsize;
            this.manageflg = true;
        }

        public GetImage(String src, Integer key, ImageObserver observer,
                int fitsize, boolean manageflg) {
            this.src = src;
            this.key = key;
            this.observer = observer;
            this.fitsize = fitsize;
            this.manageflg = manageflg;
        }

        private File getImage(String path) {
            HttpURLConnection connection = null;
            InputStream is = null;
            OutputStream os = null;
            DefaultHttpClient httpClient = null;
            File file = null;
            try {
                file = src != null ? createAlbumArtFile(key) : null;
                if (!file.exists()) {
                    if (!ContentsUtils.isSDCard(mPref)
                            && path.startsWith("http")) {
                        if (Build.VERSION.SDK_INT <= 8) {
                            HttpGet request = new HttpGet(path);
                            httpClient = new DefaultHttpClient(
                                    getClientConnManager(), mParams);
                            HttpResponse resp = httpClient.execute(request);
                            if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                                is = resp.getEntity().getContent();
                            }
                        } else {
                            // is = new URL(path).openStream();
                            connection = (HttpURLConnection) new URL(path)
                                    .openConnection();
                            connection.setRequestMethod("GET");
                            connection.setConnectTimeout(CONNECTION_TIMEOUT);
                            connection.setReadTimeout(CONNECTION_TIMEOUT);
                            connection.connect();
                            int responseCode = connection.getResponseCode();

                            // クライアントにHTTPレスポンスを書きこむ
                            if (responseCode == 200 || responseCode == 206
                                    || responseCode == 203) {
                                is = connection.getInputStream();
                            }
                        }
                    } else {
                        is = openStream(Uri.parse(path));
                    }
                    if (is != null) {
                        File parent = file.getParentFile();
                        if (!parent.exists()) {
                            parent.mkdirs();
                        }
                        os = new FileOutputStream(file);

                        byte[] buf = new byte[READ_BUF];
                        int numbytes = 0;
                        boolean hasread = false;
                        while ((numbytes = is.read(buf, 0, READ_BUF)) != -1) {
                            os.write(buf, 0, numbytes);
                            hasread = true;
                        }
                        if (hasread) {
                            return file;
                        } else {
                            file.delete();
                        }
                    }
                    return null;
                } else {
                    return file;
                }
            } catch (Exception e) {
                Logger.e("getBitmap path is " + path, e);
                if (file != null) {
                    file.delete();
                }
                return null;
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                        Logger.e("getImage", e);
                    }
                }
                if (connection != null) {
                    try {
                        connection.disconnect();
                    } catch (Exception e) {
                        Logger.e("connection.disconnect", e);
                    }
                    connection = null;
                }
                if (os != null) {
                    try {
                        os.close();
                    } catch (IOException e) {
                        Logger.e("getImage", e);
                    }
                }
                if (httpClient != null) {
                    httpClient.getConnectionManager().shutdown();
                }
            }
        }

        public InputStream openStream(Uri uri) {
            try {
                File f = new File(uri.toString());
                if (f.exists()) {
                    return new FileInputStream(f);
                }
            } catch (Exception e) {
            }

            return null;
        }

        @Override
        public void run() {
            Logger.d("load image src = " + src);
            File imagefile = getImage(src);
            if (imagefile != null && !isDestory()) {
                InputStream is = null;
                try {
                    // まずはオリジナルの縦と横のサイズを取得するため、
                    // 画像読み込みなし(inJustDecodeBounds = true)で画像を読み込む
                    BitmapFactory.Options opt = new BitmapFactory.Options();
                    opt.inJustDecodeBounds = true;
                    is = new FileInputStream(imagefile.getAbsolutePath());
                    if (is != null) {
                        BitmapFactory.decodeStream(is, null, opt);
                        try {
                            is.close();
                        } catch (IOException e) {
                        }
                        is = null;

                        // fitsizeに合わせてX方向とY方向それぞれのinSampleSizeを求める
                        int sample_x = 1 + (opt.outWidth / fitsize);
                        int sample_y = 1 + (opt.outHeight / fitsize);

                        // inSampleSizeを設定し実際に画像の読み込みを行う
                        opt.inSampleSize = Math.max(sample_x, sample_y);
                        opt.inJustDecodeBounds = false;
                        opt.inPurgeable = true;
                        opt.inPreferredConfig = Bitmap.Config.RGB_565;
                        is = new FileInputStream(imagefile.getAbsolutePath());
                        Bitmap bmp = BitmapFactory.decodeStream(is, null, opt);
                        if (bmp != null) {
                            if (manageflg) {
                                setImage(key, bmp);
                            }
                            observer.onLoadImage(bmp);
                        }
                    }
                } catch (FileNotFoundException e) {
                    Logger.e("getBitmap", e);
                } finally {
                    if (is != null) {
                        try {
                            is.close();
                        } catch (IOException e) {
                        }
                    }
                }
            }
        }

    }

    private String getAlbumArt(Context context, String album_key) {
        Cursor cur = null;
        try {
            if (album_key != null) {
                cur = context.getContentResolver().query(
                        MediaConsts.ALBUM_CONTENT_URI,
                        new String[] { AudioAlbum.ALBUM_ART },
                        AudioAlbum.ALBUM_KEY + " = ?",
                        new String[] { album_key }, null);
                if (cur != null && cur.moveToFirst()) {
                    return cur.getString(cur
                            .getColumnIndex(AudioAlbum.ALBUM_ART));
                }
            }
            return null;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    public FavoriteInfo getFavorite(Context context, long media_id, String type) {
        FavoriteInfo inf = new FavoriteInfo();
        Cursor cur = null;
        try {
            cur = context.getContentResolver().query(
                    MediaConsts.FAVORITE_CONTENT_URI,
                    new String[] { AudioFavorite._ID, AudioFavorite.POINT },
                    AudioFavorite.MEDIA_ID + " = ? and " + AudioFavorite.TYPE
                            + " = ?",
                    new String[] { Long.toString(media_id), type }, null);
            if (cur != null && cur.moveToFirst()) {
                long id = cur.getLong(cur.getColumnIndex(AudioFavorite._ID));
                int point = cur.getInt(cur.getColumnIndex(AudioFavorite.POINT));
                inf.media_id = id;
                inf.rating = point;
            }
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
        return inf;
    }

    /***
     * AlbumArtから画像を取得する
     * 
     * @param album
     * @param album_art
     * @return
     */
    public Bitmap getImage(final String album, final String artist,
            final String album_art, final ImageObserver observer) {
        final Integer key = Funcs.getAlbumKey(album, artist);
        if (album_art == null && mBitmaps != null) {
            // 画像取得のキューに追加する
            Bitmap bmp = getImage(key);
            if (bmp == null) {
                File file = createAlbumArtFile(key);
                if (file.exists()) {
                    executor.execute(new GetImage(file.getAbsolutePath(), key,
                            observer, SystemConsts.FIT_IMAGESIZE, true));
                }
            } else {
                // return bmp.ref();
                return bmp;
            }
            return null;
        } else if (!isDestory() && album_art.length() > 0 && mBitmaps != null) {
            Bitmap bmp = getImage(key);
            if (bmp == null && album_art != null) {
                if (!ContentsUtils.isSDCard(mPref)) {
                    AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... params) {
                            String t_artist = artist;
                            String t_album = album;
                            Context context = getActivity();
                            if (context != null) {
                                String url = null;
                                File file = album_art != null ? createAlbumArtFile(key)
                                        : null;
                                if (file == null || !file.exists()) {
                                    Cursor cur = null;
                                    try {
                                        cur = context
                                                .getContentResolver()
                                                .query(MediaConsts.AUTH_CONTENT_URI,
                                                        new String[] { MediaConsts.Auth.AUTH_URL },
                                                        MediaConsts.Auth.PARAM1
                                                                + " = ?",
                                                        new String[] { album_art },
                                                        null);
                                        if (cur != null && cur.moveToFirst()) {
                                            url = cur
                                                    .getString(cur
                                                            .getColumnIndex(MediaConsts.Auth.AUTH_URL));
                                        }
                                    } finally {
                                        if (cur != null) {
                                            cur.close();
                                        }
                                    }
                                } else {
                                    url = file.getAbsolutePath();
                                }
                                if (url != null && url.length() > 0) {
                                    // 画像取得のキューに追加する
                                    Message msg = Message.obtain();
                                    msg.what = EVT_ADDTASK;
                                    msg.obj = new GetImage(url, key, observer,
                                            SystemConsts.FIT_IMAGESIZE, true);
                                    mHandler.sendMessage(msg);
                                }
                            }
                            return null;
                        }
                    };
                    try {
                        task.execute((Void[]) null);
                    } catch (RejectedExecutionException e) {
                        // タスクがいっぱいなのでエラーが発生した
                        Logger.e("タスクがいっぱいなのでエラーが発生した", e);
                    }
                } else {
                    // 画像取得のキューに追加する
                    executor.execute(new GetImage(album_art, key, observer,
                            SystemConsts.FIT_IMAGESIZE, true));
                }
                return null;
            } else {
                return bmp;
            }
        }
        return null;
    }

    public void getUnManagerImage(Context context, String album, String artist,
            String album_key, ImageObserver observer, int fitsize) {
        Integer key = Funcs.getAlbumKey(album, artist);
        if (!isDestory()) {
            String album_art = getAlbumArt(context, album_key);
            try{
                if (album_art != null) {
                    // 画像取得のキューに追加する
                    executor.execute(new GetImage(album_art, key, observer,
                            fitsize, false));
                } else {
                    File file = createAlbumArtFile(key);
                    if (file.exists()) {
                        // 画像取得のキューに追加する
                        executor.execute(new GetImage(file.getAbsolutePath(), key,
                                observer, fitsize, false));
                    }
                }
            }catch(Exception e){
                Logger.e("メモリ不足によってスレッドを生成できない", e);
            }
        }
    }

    public ViewCache getViewcache() {
        return mViewcache;
    }

    synchronized boolean isDestory() {
        return hasDesotoyd;
    }

    public boolean isReady() {
        return getActivity() != null && mBitmaps != null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            mCurrentId = savedInstanceState.getLong("key.CurrentId");
            mPrefetchId = savedInstanceState.getLong("key.PrefetchId");
            mCurrentPos = savedInstanceState.getInt("key.CurrentPos");
        }

        
        mPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = new View(getActivity());
        return view;
    }
    
    

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mColorset.load(getActivity());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        executor.shutdown();

        if (clientConnectionManager != null) {
            clientConnectionManager.shutdown();
            clientConnectionManager = null;
        }

        if (mBitmaps != null) {
            setDestory();
            mBitmaps.clear();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putLong("key.CurrentId", mCurrentId);
        outState.putLong("key.PrefetchId", mPrefetchId);
        outState.putInt("key.CurrentPos", mCurrentPos);
    }

    public void releaseImage(String album, String artist) {
        /*
         * Integer key = Funcs.getAlbumKey(album, artist); if (!isDestory() &&
         * mBitmaps != null) { clearImage(); }
         */
    }

    synchronized void setDestory() {
        hasDesotoyd = true;
    }

    public boolean setPosition(int pos, long current, long prefetch) {
        if (pos != mCurrentPos || current != mCurrentId
                || prefetch != mPrefetchId) {
            mCurrentPos = pos;
            mCurrentId = current;
            mPrefetchId = prefetch;
            return true;
        }
        return false;
    }

    public void setViewcache(ViewCache viewcache) {
        mViewcache = viewcache;
    }

    public long getCurrentId() {
        return mCurrentId;
    }

    public long getMax() {
        return mMax;
    }

    public long getPos() {
        return mPos;
    }

    public long getPrefetchId() {
        return mPrefetchId;
    }

    public void progress(long pos, long max) {
        this.mPos = pos;
        this.mMax = max;
    }

    public void setCurrentId(long currentId) {
        mCurrentId = currentId;
    }

    public void setMax(long max) {
        mMax = max;
    }

    public int getCurrentPos() {
        return mCurrentPos;
    }

    public void setCurrentPos(int currentPos) {
        mCurrentPos = currentPos;
    }

    public void setPos(long pos) {
        mPos = pos;
    }

    public void setPrefetchId(long prefetchId) {
        mPrefetchId = prefetchId;
    }

    public void startProgress(long max) {
        this.mMax = max;
        this.mPos = 0;
    }

    public void stopProgress() {
        this.mMax = 0;
        this.mPos = 0;
    }

    public ColorSet getColorset() {
        return mColorset;
    }

    
}
