
package jp.co.kayo.android.localplayer.util;

import jp.co.kayo.android.localplayer.R;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.view.animation.Animation.AnimationListener;

public class AnimationHelper {

    public static void setViewToPlayback(View view, AnimationListener listner) {
        // 移動アニメーション
        TranslateAnimation trans = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.4F,
                Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, -0.8f);
        trans.setDuration(800);
        // 縮小アニメーション
        ScaleAnimation scale = new ScaleAnimation(
                1.0f, 0.0f,
                1.0f, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        scale.setStartOffset(100);
        scale.setDuration(400);

        // アニメーションセットを作成
        AnimationSet set = new AnimationSet(false);
        set.setFillAfter(false);
        set.setFillEnabled(true);
        set.addAnimation(scale);
        set.addAnimation(trans);
        set.setAnimationListener(listner);
        view.startAnimation(set);
    }
    
    
    public static void setFragmentToPlayBack(FragmentTransaction t){
        t.setCustomAnimations(R.anim.pull_list_in, R.anim.alpha_hide, R.anim.pull_list_in, R.anim.alpha_hide);
    }

}
