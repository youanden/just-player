package jp.co.kayo.android.localplayer.util;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;

import jp.co.kayo.android.localplayer.BaseActivity;
import jp.co.kayo.android.localplayer.adapter.PlaybackListViewAdapter;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.provider.DeviceContentProvider;
import jp.co.kayo.android.localplayer.service.IMediaPlayerService;
import jp.co.kayo.android.localplayer.service.MediaPlayerService;
import jp.co.kayo.android.localplayer.service.StreamCacherServer;
import jp.co.kayo.android.localplayer.util.bean.OrderInfo;

import android.annotation.TargetApi;
import android.content.ContentUris;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.provider.BaseColumns;

@TargetApi(14)
public class NfcHelper implements NfcAdapter.CreateNdefMessageCallback,
        NfcAdapter.OnNdefPushCompleteCallback {
    NfcAdapter mNfcAdapter;
    BaseActivity mActivity;
    Handler mHandler;
    SharedPreferences mPref;
    int mSendCount;
    boolean mCanSentList = false;

    public static class BeamMessage implements Serializable {
        public String title;
        public String artist;
        public String album;
        public long duration;
        public String url;

        public static ArrayList<BeamMessage> toObject(byte[] bufs) {
            ByteArrayInputStream bi = null;
            try {
                bi = new ByteArrayInputStream(bufs);
                ObjectInputStream oi = new ObjectInputStream(bi);
                ArrayList<BeamMessage> pp = (ArrayList<BeamMessage>) oi
                        .readObject();
                return pp;
            } catch (Exception e) {
                Logger.e(e.getMessage(), e);
            } finally {
                if (bi != null) {
                    try {
                        bi.close();
                    } catch (IOException e) {
                    }
                }
            }
            return null;
        }

        public static byte[] toBytes(ArrayList<BeamMessage> msg) {
            ByteArrayOutputStream bo = null;
            try {
                bo = new ByteArrayOutputStream();
                ObjectOutputStream oo = new ObjectOutputStream(bo);
                oo.writeObject(msg);
                oo.close();
                return bo.toByteArray();
            } catch (IOException e) {
                Logger.e(e.getMessage(), e);
            } finally {
                if (bo != null) {
                    try {
                        bo.close();
                    } catch (IOException e) {
                    }
                }
            }
            return null;
        }
    }

    public NfcHelper(BaseActivity activity, Handler handler) {
        mActivity = activity;
        mPref = PreferenceManager.getDefaultSharedPreferences(mActivity);
        mHandler = handler;
        try {
            mNfcAdapter = NfcAdapter.getDefaultAdapter(mActivity);
            if (mNfcAdapter != null) {
                // Register callback to set NDEF message
                mNfcAdapter.setNdefPushMessageCallback(this, mActivity);
                // Register callback to listen for message-sent success
                mNfcAdapter.setOnNdefPushCompleteCallback(this, mActivity);
            }
        } catch (Exception e) {

        }
    }
    private final String[] FETCH = new String[]{ AudioMedia._ID, AudioMedia.TITLE, AudioMedia.ARTIST, AudioMedia.ALBUM, AudioMedia.ALBUM_KEY, AudioMedia.DATA, AudioMedia.DURATION};

    @Override
    public NdefMessage createNdefMessage(NfcEvent event) {
        mSendCount = 0;
        mCanSentList = false;
        byte[] bufs = null;
        if(!ContentsUtils.isSDCard(mActivity)){
            //StreamSource
            IMediaPlayerService binder = mActivity.getBinder();
            ArrayList<BeamMessage> list = new ArrayList<BeamMessage>();
            long[] ids;
            try {
                ids = binder.getList();
    
                int pos = binder.getPosition();
                int max = getBeamSoungCount();
                int index = 0;
                while (index < ids.length && index < max) {
                    long id = ids[pos];
                    if(id>0){
                        Cursor cursor = null;
                        try{
                            cursor = mActivity.getContentResolver().query(ContentUris.withAppendedId(MediaConsts.MEDIA_CONTENT_URI, id), FETCH, null, null, null);
                        
                            if(cursor!=null && cursor.moveToFirst()){
                                BeamMessage msg = new BeamMessage();
                                msg.album = cursor.getString(cursor.getColumnIndex(AudioMedia.ALBUM));
                                msg.title = cursor.getString(cursor.getColumnIndex(AudioMedia.TITLE));
                                msg.artist = cursor.getString(cursor.getColumnIndex(AudioMedia.ARTIST));
                                msg.duration = cursor.getLong(cursor.getColumnIndex(AudioMedia.DURATION));
                                String data = cursor.getString(cursor.getColumnIndex(AudioMedia.DATA));
                                String uri = StreamCacherServer.getContentUri(mActivity, data);
                                if (uri != null && uri.startsWith("http")) {
                                    msg.url = uri;
                                    list.add(msg);
                                }
                                index++;
                            }
                        }
                        finally{
                            if(cursor!=null){
                                cursor.close();
                            }
                        }
                    }
                    pos++;
                    if (pos >= ids.length) {
                        pos = 0;
                    }
                }
            } catch (RemoteException e) {
            } catch (MalformedURLException e) {
            } finally {
                mSendCount = list.size();
            }
    
            mCanSentList = true;
    
            bufs = BeamMessage.toBytes(list);
            if (bufs != null) {
                NdefMessage msg = new NdefMessage(
                        new NdefRecord[] { 
                                createMimeRecord("application/jp.co.kayo.android.localplayer", bufs)
                                ,NdefRecord.createApplicationRecord("jp.co.kayo.android.localplayer") 
                                });
                return msg;
            }
        }else{
            //SDCard
            mCanSentList = true;
            mSendCount = 0;
        }

        bufs = BeamMessage.toBytes(new ArrayList<BeamMessage>());
        NdefMessage msg = new NdefMessage(new NdefRecord[] { 
                createMimeRecord("application/jp.co.kayo.android.localplayer", bufs)
                ,NdefRecord.createApplicationRecord("jp.co.kayo.android.localplayer")
                });
        return msg;
    }

    @Override
    public void onNdefPushComplete(NfcEvent event) {
        Message msg = mHandler.obtainMessage(SystemConsts.EVT_NDEFPUSHCOMPLETE);
        msg.obj = new Object[] { mCanSentList, mSendCount };
        mHandler.sendMessage(msg);
    }

    public ArrayList<BeamMessage> getNdefMessage(Intent intent) {
        if (intent != null) {
            if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {
                Parcelable[] msgs = intent
                        .getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
                if(msgs!=null && msgs.length>0){
                    NdefMessage msg = (NdefMessage) msgs[0];
                    byte[] bufs = msg.getRecords()[0].getPayload();
                    if (bufs != null) {
                        return BeamMessage.toObject(bufs);
                    }
                }
            }
        }
        return null;
    }

    private NdefRecord createMimeRecord(String mimeType, byte[] data) {
        byte[] mimeBytes = mimeType.getBytes(Charset.forName("US-ASCII"));
        NdefRecord mimeRecord = new NdefRecord(NdefRecord.TNF_MIME_MEDIA,
                mimeBytes, new byte[0], data);
        return mimeRecord;
    }

    private int getBeamSoungCount() {
        int type = Integer
                .parseInt(mPref.getString("key.beam_songnumber", "0"));
        switch (type) {
        case 0:
            return 5;
        case 1:
            return 10;
        case 2:
            return 15;
        case 3:
        default:
            return 20;
        }
    }
}
