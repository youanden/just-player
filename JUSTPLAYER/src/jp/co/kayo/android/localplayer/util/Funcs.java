
package jp.co.kayo.android.localplayer.util;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.appwidget.AppWidgetHelper;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioAlbum;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.service.IMediaPlayerService;
import jp.co.kayo.android.localplayer.service.IMediaPlayerServiceCallback;
import jp.co.kayo.android.localplayer.service.MediaPlayerService;
import jp.co.kayo.android.localplayer.service.StreamCacherServer;

import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.text.format.Time;
import android.util.DisplayMetrics;
import android.util.TimeFormatException;
import android.view.WindowManager;
import android.widget.Toast;

public class Funcs {
    public static int CACHE_STATE_OK = 1;
    public static int CACHE_STATE_NOT_LIMIT_SPACE = 0;
    public static int CACHE_STATE_DENGERUS = -1;
    public static int CACHE_STATE_DISABLE = -2;
    static SdCardAccessHelper sdHelper = new SdCardAccessHelper();

    public static void setResumeReloadFlag(SharedPreferences pref, boolean b) {
        Editor editor = pref.edit();
        if (b) {
            editor.putBoolean(SystemConsts.PREF_RELAOD, true);
        } else {
            editor.remove(SystemConsts.PREF_RELAOD);
        }
        editor.commit();
    }

    public static boolean isResumeReloadFlag(SharedPreferences pref) {
        return pref.getBoolean(SystemConsts.PREF_RELAOD, false);
    }

    public static boolean isNotEmpty(String s) {
        if (s != null && s.length() > 0) {
            return true;
        }
        return false;
    }

    public static byte[] getBytes(String s) {
        int slen = s.length();
        byte bytes[] = new byte[slen];
        for (int i = 0; i < slen; i++) {
            char c = s.charAt(i);
            if (c >= 0x100) {
                // Byte列になっていない
                return null;
            }
            bytes[i] = (byte) c;
        }
        return bytes;
    }

    public static List<ResolveInfo> getShare(Context context) {
        Intent intent = new Intent("jp.co.kayo.android.localplayer.share.MAIN", null);
        intent.addCategory(Intent.CATEGORY_DEFAULT);

        ArrayList<ResolveInfo> ret = new ArrayList<ResolveInfo>();
        PackageManager pm = context.getPackageManager();
        return pm.queryIntentActivities(intent, 0);
    }

    public static String getEncodingString(String s, String enc)
            throws UnsupportedEncodingException {
        byte[] bufs = getBytes(s);
        if (bufs != null) {
            return new String(bufs, enc);
        }
        else {
            return s;
        }
    }

    // 外部メモリーが使えるかを返す
    public static boolean canUseExternalMemory() {
        String state = Environment.getExternalStorageState();
        return state.equals(Environment.MEDIA_MOUNTED);
    }

    // 外部メモリーが使える場合に外部メモリーのマウントされているパスを取得する
    public static File getExternalMemoryMoutedPath() {
        if (canUseExternalMemory()) {
            return Environment.getExternalStorageDirectory();
        }
        return null;
    }

    // 外部メモリーの利用可能容量（空きメモリーサイズ)を取得する
    public static long getExteranlMemoryAvailableSize() {
        long size = -1;

        if (canUseExternalMemory()) {
            File exmemPath = getExternalMemoryMoutedPath();
            if (exmemPath != null) {
                StatFs fs = new StatFs(exmemPath.getPath());

                long bkSize = fs.getBlockSize();
                long avaBlocks = fs.getAvailableBlocks();

                size = bkSize * avaBlocks;
            }
        }
        return size;
    }

    // 外部メモリーの総容量(トータルサイズ)を取得する
    public static long getExteranlMemorySize() {
        long size = -1;

        if (canUseExternalMemory()) {
            File exmemPath = getExternalMemoryMoutedPath();
            if (exmemPath != null) {
                StatFs fs = new StatFs(exmemPath.getPath());

                long bkSize = fs.getBlockSize();
                long bkCount = fs.getBlockCount();

                size = bkSize * bkCount;
            }
        }
        return size;
    }

    // 内部メモリーの利用可能容量（空きメモリーサイズ)を取得する
    public static long getInternalMemoryAvailableSize() {
        long size = -1;

        File internalMemPath = Environment.getDataDirectory();
        if (internalMemPath != null) {
            StatFs fs = new StatFs(internalMemPath.getPath());

            long blockSize = fs.getBlockSize();
            long availableBlockSize = fs.getAvailableBlocks();

            size = blockSize * availableBlockSize;
        }
        return size;
    }

    // 内部メモリーの総容量(トータルサイズ)を取得する
    public static long getInternalMemorySize() {
        long size = -1;

        File internalMemPath = Environment.getDataDirectory();
        if (internalMemPath != null) {
            StatFs fs = new StatFs(internalMemPath.getPath());

            long bkSize = fs.getBlockSize();
            long bkCount = fs.getBlockCount();

            size = bkSize * bkCount;
        }
        return size;
    }

    
    /***
     * 1: fine 0: no limit space -1: cant use cache
     * 
     * @param totalsize
     * @return
     */
    public static int canUseCache(SharedPreferences mPref, long bytesize) {
        long maxCacheSizeM = Funcs.getCachSize(mPref);
        long maxCacheSizeByte = maxCacheSizeM * 1000000;
        long avrCacheSizeByte = getInternalMemoryAvailableSize(); //利用可能なキャッシュサイズ
        long cacheSizeByte = sdHelper.calcDirSize(SdCardAccessHelper.cachedMusicDir); //キャッシュフォルダの使用量
        long totalsizeByte = (bytesize + cacheSizeByte); //必要なキャッシュのサイズ
        if(maxCacheSizeM == -1 || maxCacheSizeByte > avrCacheSizeByte){
            maxCacheSizeByte = avrCacheSizeByte;
        }

        if (totalsizeByte <= maxCacheSizeByte) {
            return CACHE_STATE_OK;
        } else {
            if(maxCacheSizeM == 0){
                return CACHE_STATE_DISABLE;
            }
            else if(totalsizeByte > avrCacheSizeByte){
                return CACHE_STATE_DENGERUS;
            }else {
                return CACHE_STATE_NOT_LIMIT_SPACE;
            }
        }
    }

    public static long makeSubstansId(String name) {
        return (name.hashCode() & 0xffffffffL) * -1;
    }

    public static void enableHttpResponseCache(Context context) {
        try {
            long httpCacheSize = 10 * 1024 * 1024; // 10 MiB
            File httpCacheDir = new File(context.getCacheDir(), "http");
            Class.forName("android.net.http.HttpResponseCache")
                    .getMethod("install", File.class, long.class)
                    .invoke(null, httpCacheDir, httpCacheSize);
        } catch (Exception httpResponseCacheNotAvailable) {
        }
    }

    public static void disableConnectionReuseIfNecessary() {
        // HTTP connection reuse which was buggy pre-froyo
        if (Build.VERSION.SDK_INT <= 8) {
            System.setProperty("http.keepAlive", "false");
        }
    }

    public static String trimString(String s) {
        if (s != null && s.length() > 0) {
            return s.trim();
        } else {
            return "";
        }
    }

    public static int getInt(String s) {
        if (s != null && s.length() > 0) {
            try {
                return Integer.parseInt(s);
            } catch (Exception e) {
            }
        }
        return 0;
    }

    public static boolean isMedia(String fname, String[] exts) {
        int s1 = fname.lastIndexOf('.');
        if (s1 > 0) {
            String ext = fname.substring(s1);
            for (int i = 0; i < exts.length; i++) {
                if (ext.toLowerCase().equals(exts[i])) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isJPG(String fname) {
        int s1 = fname.lastIndexOf('.');
        if (s1 > 0) {
            String ext = fname.substring(s1);
            if (ext.toLowerCase().equals(".jpg")) {
                return true;
            }
        }
        return false;
    }

    // static Formatter _timefmt = new Formatter();
    /***
     * ミリ秒から0:00の形式で文字列を作成します
     * 
     * @param context
     * @param msecs
     * @return
     */
    public static String makeTimeString(long msecs) {
        if (msecs > 0) {
            long sec = msecs / 1000;
            // _timefmt.flush();
            return String.format("%1$2d:%2$02d",
                    new Object[] {
                            sec / 60, sec % 60
                    }).toString();
        } else {
            return "0:00";
        }
    }

    public static Integer getAlbumKey(String album, String artist) {
        if (album != null && artist != null) {
            return (album.trim() + artist.trim()).hashCode();
        } else if (album != null) {
            return (album.trim()).hashCode();
        } else if (artist != null) {
            return (artist.trim()).hashCode();
        }
        return new Integer(0);
    }

    public static String getTrack(int i) {
        return String.format("%1$02d", i);
    }

    public static long parseLong(String text) {
        if (text != null && text.length() > 0) {
            try {
                return Long.parseLong(text);
            } catch (Exception e) {

            }
        }
        return 0;
    }

    public static int parseInt(String text) {
        if (text != null && text.length() > 0) {
            try {
                return Integer.parseInt(text);
            } catch (Exception e) {

            }
        }
        return 0;
    }

    static SimpleDateFormat _fmt = new SimpleDateFormat(
            "yyyy-MM-dd'T'HH:mm:ssZ");

    public static long parseDate(String text) {
        if (text != null && text.length() > 0) {
            if (false) {
                // SimpleDateFormatよりもandroid.text.format.Timeのほうが早いらしい
                try {
                    Time andTime = new Time();
                    andTime.parse3339(text);
                    long parsed = andTime.toMillis(true);
                    return parsed;
                } catch (TimeFormatException e) {
                    Logger.e("parseDate, text=" + text, e);
                }
                return -1;
            } else {
                try {
                    // 2011-08-01T16:03:16+09:00
                    Date date = _fmt.parse(text);
                    return date.getTime();
                } catch (Exception e) {
                }
            }
        }
        return 0;
    }

    static final String BITLYAPISHORTEN = "http://api.bit.ly/v3/shorten?apiKey=";
    static final String BITLYAPIEXPAND = "http://api.bit.ly/v3/expand?apiKey=";
    static final String BITLYLOGIN = "&login=";
    static final String BITLYLONGURL = "&longUrl=";
    static final String BITLYHASH = "&hash=";
    static final String BITLYSHORTURL = "&shortUrl=";
    static final String BITLYFORMAT = "&format=txt";
    static final String BITLYURL = "http://bit.ly/";

    public static String urlShorterBitly(String s) {
        try {
            String targetURL = BITLYAPISHORTEN
                    + "R_b73da8bcaa024e308246dd17b270d23a" + BITLYLOGIN
                    + "yokmama" + BITLYFORMAT + BITLYLONGURL
                    + URLEncoder.encode(s, "UTF-8");

            URL url = new URL(targetURL);
            HttpURLConnection urlconn = (HttpURLConnection) url
                    .openConnection();
            urlconn.setRequestMethod("GET");
            urlconn.setInstanceFollowRedirects(false);
            urlconn.setRequestProperty("Connection", "close");
            urlconn.connect();

            if (urlconn.getResponseCode() == 200) {
                BufferedReader bin;
                bin = new BufferedReader(new InputStreamReader(
                        urlconn.getInputStream()));
                String line;
                if ((line = bin.readLine()) != null) {
                    return line;
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String urlShorterGoogle(String source) {
        String urlString = "https://www.googleapis.com/urlshortener/v1/url";
        try {
            URL url = new URL(urlString);
            URLConnection uc = url.openConnection();
            uc.setDoOutput(true); // POST可能にする

            uc.setRequestProperty("Content-Type", "application/json"); // ヘッダを設定
            OutputStream os = uc.getOutputStream();

            String postStr = "{\"longUrl\": \"" + source + "\"}"; // 短縮したいURLを設定
            PrintStream ps = new PrintStream(os);
            ps.print(postStr); // データをPOSTする
            ps.close();

            InputStream is = uc.getInputStream(); // POSTした結果を取得
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(is));
            StringBuilder json = new StringBuilder();
            String s;
            while ((s = reader.readLine()) != null) {
                json.append(s);
            }
            reader.close();
            if (json.length() > 0 && json.indexOf("id") != -1) {
                JSONObject obj = new JSONObject(json.toString());
                return obj.getString("id");
            }
        } catch (Exception e) {

        }

        return null;
    }

    public static void checkSDCard(Context context) {
        String status = Environment.getExternalStorageState();
        if (status.equalsIgnoreCase(Environment.MEDIA_MOUNTED)) {
            // 正常
            return;
        } else {
            Toast.makeText(context, context.getString(R.string.txt_sdcard_err),
                    Toast.LENGTH_LONG).show();
        }
    }

    public static long getHideTime(SharedPreferences pref) {
        int type = Integer.parseInt(pref.getString("key.control_hide", "1"));
        long time = 0L;
        if (type == 1) {
            time = 2000L;
        }
        if (type == 2) {
            time = 5000L;
        }
        if (type == 3) {
            time = 10000L;
        }
        return time;
    }

    /**
     * AndroidManifestからVersionNameを取得
     * 
     * @param prefix
     * @param context
     * @return
     */
    public static String getVersionNumber(String prefix, Context context) {
        String versionName = prefix;
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(context.getPackageName(),
                    PackageManager.GET_META_DATA);
            versionName += info.versionName;
        } catch (NameNotFoundException e) {
            versionName += "0";
        }
        return versionName;
    }

    public static long getCachSize(SharedPreferences pref) {
        /*
         * <item>0</item> <item>104857600</item> <item>209715200</item>
         * <item>524288000</item> <item>1073741824</item>
         * <item>2147483648</item> <item>5368709120</item>
         * <item>10737418240</item> <item>21474836480</item> <item>-1</item>
         */
        int size = Funcs.parseInt(pref.getString(SystemConsts.KEY_CACHE_SIZE,
                "4"));
        if (size == 0) {
            return 0;
        } else if (size == 1) {
            return 100;
        } else if (size == 2) {
            return 200;
        } else if (size == 3) {
            return 500;
        } else if (size == 4) {
            return 1024;
        } else if (size == 5) {
            return 2048;
        } else if (size == 6) {
            return 5120;
        } else if (size == 7) {
            return 10240;
        } else if (size == 8) {
            return 20480;
        } else if (size == 9) {
            return -1;
        } else {
            return 2147;
        }
    }

    @TargetApi(10)
    public static Bitmap getAlbumArt(Context context, long mediaId, int defaultIcon,
            Hashtable<String, String> tbl1, Hashtable<String, String> tbl2) {
        ContentsUtils.getMedia(context, new String[] {
                AudioMedia.ALBUM, AudioMedia.ALBUM_KEY,
                AudioMedia.ARTIST, AudioMedia.TITLE,
                AudioMedia.DATA, AudioMedia.DURATION
        }, mediaId, tbl1);

        ContentsUtils.getAlbum(context, new String[] {
                AudioAlbum._ID, AudioAlbum.ALBUM, AudioAlbum.ARTIST,
                AudioAlbum.ALBUM_ART
        }, tbl1.get(AudioMedia.ALBUM_KEY), tbl2);

        int artSize = context.getResources().getDimensionPixelSize(R.dimen.albumart_size);

        // MediaArt
        if (Build.VERSION.SDK_INT >= 10) {
            try {
                String fname = tbl1.get(AudioMedia.DATA);
                Logger.d("fname=" + fname);
                if (ContentsUtils.isSDCard(context)) {
                    if (fname != null && fname.length() > 0) {
                        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                        mmr.setDataSource(fname);
                        byte[] data = mmr.getEmbeddedPicture();
                        if (data != null) {
                            return loadBitmap(data, artSize);
                        }
                    }
                }
                else {
                    File cacheFile = StreamCacherServer.getCacheFile(context, fname);
                    if (cacheFile != null && cacheFile.exists()) {
                        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                        mmr.setDataSource(cacheFile.toString());
                        byte[] data = mmr.getEmbeddedPicture();
                        if (data != null) {
                            return loadBitmap(data, artSize);
                        }
                    }
                }
            } catch (Exception e) {
                Logger.e("getEmbeddedPicture", e);
            }
        }

        // AlbumArt
        Integer key = AppWidgetHelper
                .getAlbumKey(tbl2.get(AudioAlbum.ALBUM),
                        tbl2.get(AudioAlbum.ARTIST));
        File file = AppWidgetHelper.createAlbumArtFile(key);
        if (file != null && file.exists()) {
            return loadBitmap(file, artSize);
        }
        if (defaultIcon != -1) {
            return BitmapFactory.decodeResource(context.getResources(), defaultIcon);
        }
        else {
            return null;
        }
    }

    public static Bitmap loadBitmap(byte[] data, int fitsize) {
        Bitmap bmp = null;
        try {
            // 読み込む画像へのUriがある場合は読み込む
            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(data, 0, data.length, opts);

            // fitsizeに合わせてX方向とY方向それぞれのinSampleSizeを求める
            int sample_x = 1 + (opts.outWidth / fitsize);
            int sample_y = 1 + (opts.outHeight / fitsize);
            opts.inSampleSize = Math.max(sample_x, sample_y);
            opts.inJustDecodeBounds = false;
            bmp = BitmapFactory.decodeByteArray(data, 0, data.length, opts);
            return bmp;
        } catch (Exception e) {
            Logger.e("loadBitmap", e);
        } finally {
        }
        return null;
    }

    public static Bitmap loadBitmap(File file, int fitsize) {
        Bitmap bmp = null;
        InputStream in = null;
        try {
            in = new FileInputStream(file);
            // 読み込む画像へのUriがある場合は読み込む
            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, opts);
            in.close();
            in = new FileInputStream(file);
            // fitsizeに合わせてX方向とY方向それぞれのinSampleSizeを求める
            int sample_x = 1 + (opts.outWidth / fitsize);
            int sample_y = 1 + (opts.outHeight / fitsize);
            opts.inSampleSize = Math.max(sample_x, sample_y);
            opts.inJustDecodeBounds = false;
            bmp = BitmapFactory.decodeStream(in, null, opts);
            return bmp;
        } catch (Exception e) {
            Logger.e("loadBitmap", e);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
        }
        return null;
    }

    public static Bitmap loadBitmap(Context context, Uri uri, int fitsize) {
        InputStream is = null;
        Bitmap bmp = null;
        try {
            // 読み込む画像へのUriがある場合は読み込む
            if (uri != null) {
                // まずはオリジナルの縦と横のサイズを取得するため、
                // 画像読み込みなし(inJustDecodeBounds = true)で画像を読み込む
                is = context.getContentResolver().openInputStream(uri);
                BitmapFactory.Options opts = new BitmapFactory.Options();
                opts.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(is, null, opts);
                is.close();
                is = null;

                // fitsizeに合わせてX方向とY方向それぞれのinSampleSizeを求める
                int sample_x = 1 + (opts.outWidth / fitsize);
                int sample_y = 1 + (opts.outHeight / fitsize);

                is = context.getContentResolver().openInputStream(uri);
                opts.inSampleSize = Math.max(sample_x, sample_y);
                opts.inJustDecodeBounds = false;
                bmp = BitmapFactory.decodeStream(is, null, opts);
                return bmp;
            }
        } catch (Exception e) {
            Logger.e("loadBitmap", e);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                }
            }
        }
        return null;
    }

    public static IMediaPlayerServiceCallback sService = null;
    private static HashMap<Context, ServiceBinder> sConnectionMap = new HashMap<Context, ServiceBinder>();

    public static class ServiceToken {
        ContextWrapper mWrappedContext;

        ServiceToken(ContextWrapper context) {
            mWrappedContext = context;
        }
    }

    private static class ServiceBinder implements ServiceConnection {
        ServiceConnection mCallback;

        ServiceBinder(ServiceConnection callback) {
            mCallback = callback;
        }

        public void onServiceConnected(ComponentName className, android.os.IBinder service) {
            sService = IMediaPlayerServiceCallback.Stub.asInterface(service);
            if (mCallback != null) {
                mCallback.onServiceConnected(className, service);
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            if (mCallback != null) {
                mCallback.onServiceDisconnected(className);
            }
            sService = null;
        }
    }

    public static ComponentName startService(Activity context, String action) {
        Activity realActivity = context.getParent();
        if (realActivity == null) {
            realActivity = context;
        }
        ContextWrapper cw = new ContextWrapper(realActivity);
        Intent i = new Intent(cw, MediaPlayerService.class);
        if (action != null) {
            i.setAction(action);
        }
        return cw.startService(i);
    }

    public static ServiceToken bindToService(Activity context, ServiceConnection callback) {
        Activity realActivity = context.getParent();
        if (realActivity == null) {
            realActivity = context;
        }
        ContextWrapper cw = new ContextWrapper(realActivity);
        cw.startService(new Intent(cw, MediaPlayerService.class));
        ServiceBinder sb = new ServiceBinder(callback);
        if (cw.bindService((new Intent()).setClass(cw, MediaPlayerService.class), sb, 0)) {
            sConnectionMap.put(cw, sb);
            return new ServiceToken(cw);
        }
        return null;
    }

    public static void unbindFromService(ServiceToken token) {
        if (token == null) {
            return;
        }
        ContextWrapper cw = token.mWrappedContext;
        ServiceBinder sb = sConnectionMap.remove(cw);
        if (sb == null) {
            return;
        }
        cw.unbindService(sb);
        if (sConnectionMap.isEmpty()) {
            sService = null;
        }
    }

    /**
     * pxからdipへの変換処理
     */
    public static float px2Dip(Context context, float pixel) {
        float dip = 0;
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(metrics);
        dip = metrics.scaledDensity * pixel;
        return dip;
    }

}
