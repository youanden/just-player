package jp.co.kayo.android.localplayer;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 *      This program is free software; you can redistribute it and/or modify it under
 *      the terms of the GNU General Public License as published by the Free Software
 *      Foundation; either version 2 of the License, or (at your option) any later
 *      version.
 *      
 *      This program is distributed in the hope that it will be useful, but WITHOUT
 *      ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *      FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *      details.
 *      
 *      You should have received a copy of the GNU General Public License along with
 *      this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *      Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

import jp.co.kayo.android.localplayer.BillingService.RequestPurchase;
import jp.co.kayo.android.localplayer.BillingService.RestoreTransactions;
import jp.co.kayo.android.localplayer.Consts.PurchaseState;
import jp.co.kayo.android.localplayer.Consts.ResponseCode;
import jp.co.kayo.android.localplayer.util.Logger;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.widget.Toast;

public class DungeonsPurchaseObserver extends PurchaseObserver {
    private BillingService mBillingService;
    private static final String DB_INITIALIZED = "db_initialized";

    public DungeonsPurchaseObserver(Activity activity,
            BillingService billingService, Handler handler) {
        super(activity, handler);
        this.mBillingService = billingService;
    }

    protected void restoreDatabase() {
        SharedPreferences prefs = getActivit().getPreferences(
                Context.MODE_PRIVATE);
        boolean initialized = prefs.getBoolean(DB_INITIALIZED, false);
        if (!initialized) {
            mBillingService.restoreTransactions();
            Toast.makeText(getActivit(), R.string.restoring_transactions,
                    Toast.LENGTH_SHORT).show();
        }
    }

    protected void logProductActivity(String product, String activity) {
        SpannableStringBuilder contents = new SpannableStringBuilder();
        contents.append(Html.fromHtml("<b>" + product + "</b>: "));
        contents.append(activity);
        prependLogEntry(contents);
    }

    protected void prependLogEntry(CharSequence cs) {
        StringBuilder contents = new StringBuilder(cs);
        contents.append("¥n");
        Logger.d(contents.toString());
    }

    @Override
    public void onBillingSupported(boolean supported) {
        if (Consts.DEBUG) {
            Logger.i("supported: " + supported);
        }
    }

    @Override
    public void onPurchaseStateChange(PurchaseState purchaseState,
            String itemId, int quantity, long purchaseTime,
            String developerPayload) {
        if (Consts.DEBUG) {
            Logger.i("onPurchaseStateChange() itemId: " + itemId + " "
                    + purchaseState);
        }

        if (developerPayload == null) {
            logProductActivity(itemId, purchaseState.toString());
        } else {
            logProductActivity(itemId, purchaseState + "¥n¥t"
                    + developerPayload);
        }

        if (purchaseState == PurchaseState.PURCHASED) {
            // mOwnedItems.add(itemId);
        }
    }

    @Override
    public void onRequestPurchaseResponse(RequestPurchase request,
            ResponseCode responseCode) {
        if (Consts.DEBUG) {
            Logger.d(request.mProductId + ": " + responseCode);
        }
        if (responseCode == ResponseCode.RESULT_OK) {
            if (Consts.DEBUG) {
                Logger.i("purchase was successfully sent to server");
            }
            logProductActivity(request.mProductId, "sending purchase request");
        } else if (responseCode == ResponseCode.RESULT_USER_CANCELED) {
            if (Consts.DEBUG) {
                Logger.i("user canceled purchase");
            }
            logProductActivity(request.mProductId, "dismissed purchase dialog");
        } else {
            if (Consts.DEBUG) {
                Logger.i("purchase failed");
            }
            logProductActivity(request.mProductId, "request purchase returned "
                    + responseCode);
        }
    }

    @Override
    public void onRestoreTransactionsResponse(RestoreTransactions request,
            ResponseCode responseCode) {
        if (responseCode == ResponseCode.RESULT_OK) {
            if (Consts.DEBUG) {
                Logger.d("completed RestoreTransactions request");
            }
            // Update the shared preferences so that we don't perform
            // a RestoreTransactions again.
            SharedPreferences prefs = getActivit().getPreferences(
                    Context.MODE_PRIVATE);
            SharedPreferences.Editor edit = prefs.edit();
            edit.putBoolean(DB_INITIALIZED, true);
            edit.commit();
        } else {
            if (Consts.DEBUG) {
                Logger.d("RestoreTransactions error: " + responseCode);
            }
        }
    }
}