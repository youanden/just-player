package jp.co.kayo.android.localplayer.fx;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import jp.co.kayo.android.localplayer.service.JukeBox;
import jp.co.kayo.android.localplayer.util.Logger;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.MediaPlayer;
import android.media.audiofx.BassBoost;
import android.media.audiofx.Equalizer;
import android.media.audiofx.PresetReverb;
import android.preference.PreferenceManager;

@TargetApi(9)
public class AudioFx {
    short minEQLevel;
    short maxEQLevel;
    short numBands;
    Equalizer mEqualizer;
    PresetReverb mPresetReverb;
    BassBoost mBassBoost;
    private JukeBox jukebox;

    public AudioFx(JukeBox JukeBox) throws Exception {
        this.jukebox = JukeBox;

    }

    public void save() {
        Editor editor = jukebox.getPreferences().edit();

        if (mEqualizer != null) {
            short preset = mEqualizer.getCurrentPreset();
            editor.putInt("eq.Preset", preset);
            int num = mEqualizer.getNumberOfBands();
            for (int i = 0; i < num; i++) {
                editor.putInt("eq.bandlevel" + i,
                        mEqualizer.getBandLevel((short) i));
            }
            editor.putBoolean("eq.enabled", mEqualizer.getEnabled());
        }

        if (mPresetReverb != null) {
            int preset = mPresetReverb.getPreset();
            editor.putInt("rv.preset", preset);
            editor.putBoolean("rv.enabled", mPresetReverb.getEnabled());
        }

        if (mBassBoost != null) {
            editor.putInt("bs.strength", mBassBoost.getRoundedStrength());
            editor.putBoolean("bs.enabled", mBassBoost.getEnabled());
        }

        editor.commit();
    }

    public void load() {
        if(mEqualizer == null){
            mEqualizer = new Equalizer(0, jukebox.getMediaPlayer().getAudioSessionId());
            numBands = mEqualizer.getNumberOfBands();
            if (mEqualizer.getBandLevelRange() != null) {
                minEQLevel = mEqualizer.getBandLevelRange()[0];
                maxEQLevel = mEqualizer.getBandLevelRange()[1];
            } else {
                mEqualizer.release();
                mEqualizer = null;
            }
        }
        if(mPresetReverb == null){
            mPresetReverb = new PresetReverb(0, jukebox.getMediaPlayer().getAudioSessionId());
        }
        if(mBassBoost == null){
            mBassBoost = new BassBoost(0, jukebox.getMediaPlayer().getAudioSessionId());
        }
        
        if (mEqualizer != null) {
            setEqEnabled(jukebox.getPreferences().getBoolean("eq.enabled", false));
            //jukebox.getMediaPlayer().attachAuxEffect(mEqualizer.getId());

            int preset = jukebox.getPreferences().getInt("eq.Preset", -1);
            if (preset != -1) {
                mEqualizer.usePreset((short) preset);
            } else {
                int num = mEqualizer.getNumberOfBands();
                int lim = maxEQLevel + 1;
                for (int i = 0; i < num; i++) {
                    int level = jukebox.getPreferences().getInt("eq.bandlevel" + i, lim);
                    if (level != lim) {
                        mEqualizer.setBandLevel((short) i, (short) level);
                    }
                }
            }
        }

        if (mPresetReverb != null) {
            setRvEnabled(jukebox.getPreferences().getBoolean("rv.enabled", false));
            //jukebox.getMediaPlayer().attachAuxEffect(mPresetReverb.getId());
            int preset = jukebox.getPreferences().getInt("rv.preset", PresetReverb.PRESET_NONE);
            mPresetReverb.setPreset((short) preset);
        }

        if (mBassBoost != null) {
            setBsEnabled(jukebox.getPreferences().getBoolean("bs.enabled", false));
            //jukebox.getMediaPlayer().attachAuxEffect(mBassBoost.getId());
            int strength = jukebox.getPreferences().getInt("bs.strength", 0);
            if (strength >= 0 || strength <= 1000) {
                mBassBoost.setStrength((short) strength);
            }
        }
        
    }

    public short getNumberOfBands() {
        return numBands;
    }

    public short getMinEQLevel() {
        return minEQLevel;
    }

    public short getMaxEQLevel() {
        return maxEQLevel;
    }

    public void setEqEnabled(boolean enabled) {
        if (mEqualizer != null) {
            mEqualizer.setEnabled(enabled);
        }
    }

    public void setRvEnabled(boolean enabled) {
        if (mPresetReverb != null) {
            mPresetReverb.setEnabled(enabled);
        }
    }

    public void setBsEnabled(boolean enabled) {
        if (mBassBoost != null) {
            mBassBoost.setEnabled(enabled);
        }
    }

    public boolean getEqEnabled() {
        if (mEqualizer != null) {
            try{
                return mEqualizer.getEnabled();
            }catch(Exception e){}
        }
        return false;
    }

    public boolean getRvEnabled() {
        if (mPresetReverb != null) {
            try{
                return mPresetReverb.getEnabled();
            }catch(Exception e){}
        }
        return false;
    }

    public boolean getBsEnabled() {
        if (mBassBoost != null) {
            try{
                return mBassBoost.getEnabled();
            }catch(Exception e){}
        }
        return false;
    }

    public short getNumberOfPresets() {
        if (mEqualizer != null) {
            try{
                return mEqualizer.getNumberOfPresets();
            }catch(Exception e){}
        }
        return -1;
    }

    public String getPresetName(short n) {
        if (mEqualizer != null) {
            try{
                return mEqualizer.getPresetName(n);
            }catch(Exception e){}
        }
        return null;
    }

    public short getBandLevel(short band) {
        if (mEqualizer != null) {
            try{
                return mEqualizer.getBandLevel(band);
            }catch(Exception e){}
        }
        return -1;
    }

    public int getCenterFreq(short band) {
        if (mEqualizer != null) {
            try{
                return mEqualizer.getCenterFreq(band);
            }catch(Exception e){}
        }
        return -1;
    }

    public short getCurrentPreset() {
        if (mEqualizer != null) {
            try{
                return mEqualizer.getCurrentPreset();
            }catch(Exception e){}
        }
        return -1;
    }

    public void usePreset(short preset) {
        if (mEqualizer != null && preset >= 0) {
            try{
                mEqualizer.usePreset(preset);
            }catch(Exception e){}
        }
    }

    public void setBandLevel(short band, short level) {
        if (mEqualizer != null) {
            try{
                mEqualizer.setBandLevel(band, level);
            }catch(Exception e){}
        }
    }

    public short getRvPreset() {
        if(mPresetReverb!=null){
            try{
                return mPresetReverb.getPreset();
            }catch(Exception e){}
        }
        return -1;
    }

    public void setRvPreset(short preset) {
        if (mPresetReverb != null) {
            try{
                mPresetReverb.setPreset(preset);
            }catch(Exception e){}
        }
    }

    public short getStrength() {
        if (mBassBoost != null) {
            try{
                return mBassBoost.getRoundedStrength();
            }catch(Exception e){}
        }
        return -1;
    }

    public void setStrength(short strength) {
        if (mBassBoost != null) {
            try{
                mBassBoost.setStrength(strength);
            }catch(Exception e){}
        }
    }

    public void release() {
        if (mEqualizer != null) {
            mEqualizer.release();
            mEqualizer = null;
        }

        if (mPresetReverb != null) {
            mPresetReverb.release();
            mPresetReverb = null;
        }

        if (mBassBoost != null) {
            mBassBoost.release();
            mBassBoost = null;
        }
    }
}
