
package jp.co.kayo.android.localplayer.menu;

import android.content.Context;
import android.view.View;

import android.view.ActionProvider;
import android.widget.ShareActionProvider;

public class BaseActionProvider extends ShareActionProvider {
    Context context;

    public BaseActionProvider(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    public View onCreateActionView() {
        return null;
    }
    
    @Override
    public boolean hasSubMenu() {
        return true;
    }
}
