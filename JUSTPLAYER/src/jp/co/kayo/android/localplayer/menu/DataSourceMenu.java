
package jp.co.kayo.android.localplayer.menu;

import java.util.ArrayList;
import java.util.List;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.provider.DeviceContentProvider;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.SubMenu;

public class DataSourceMenu extends BaseActionProvider implements OnMenuItemClickListener {
    SharedPreferences pref;
    List<ProviderInfo> dslist;
    String contenturi;
    Activity activity;
    Handler handler;

    public DataSourceMenu(Context context) {
        super(context);
        this.pref = (SharedPreferences) PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setActivity(Activity activity, Handler handler) {
        this.activity = activity;
        this.handler = handler;
    }

    @Override
    public void onPrepareSubMenu(SubMenu subMenu) {
        subMenu.clear();
        dslist = ContentsUtils.getDataSouce(context);
        PackageManager pm = context.getPackageManager();
        contenturi = pref.getString(SystemConsts.PREF_CONTENTURI,
                DeviceContentProvider.MEDIA_AUTHORITY);
        if (contenturi.equals(DeviceContentProvider.MEDIA_AUTHORITY)) {
            subMenu.addSubMenu(R.id.mnu_ds, R.string.lb_sdcard, 0,
                    context.getString(R.string.lb_sdcard) + " *");
        } else {
            subMenu.addSubMenu(R.id.mnu_ds, R.string.lb_sdcard, 0,
                    context.getString(R.string.lb_sdcard));
        }
        for (int i = 0; i < dslist.size(); i++) {
            ProviderInfo inf = dslist.get(i);
            if (inf.authority.equals(contenturi)) {
                subMenu.addSubMenu(R.id.mnu_ds, R.string.lb_sdcard + (i + 1), i + 1,
                        inf.loadLabel(pm).toString() + " *");
            } else {
                subMenu.addSubMenu(R.id.mnu_ds, R.string.lb_sdcard + (i + 1), i + 1,
                        inf.loadLabel(pm).toString());
            }
        }
        subMenu.addSubMenu(R.string.lb_conf_add_ds_title, R.string.lb_sdcard + (dslist.size() + 2),
                dslist.size() + 2, context.getString(R.string.lb_conf_add_ds_title));

        for (int i = 0; i < subMenu.size(); ++i) {
            subMenu.getItem(i).setOnMenuItemClickListener(this);
        }
    }
    
    @Override
    public boolean onPerformDefaultAction() {
        if (Build.VERSION.SDK_INT < 11) {
            ArrayList<String> menuItems = new ArrayList<String>();
            dslist = ContentsUtils.getDataSouce(context);
            PackageManager pm = context.getPackageManager();
            contenturi = pref.getString(SystemConsts.PREF_CONTENTURI,
                    DeviceContentProvider.MEDIA_AUTHORITY);
            if (contenturi.equals(DeviceContentProvider.MEDIA_AUTHORITY)) {
                menuItems.add(context.getString(R.string.lb_sdcard) + " *");
            } else {
                menuItems.add(context.getString(R.string.lb_sdcard));
            }
            for (int i = 0; i < dslist.size(); i++) {
                ProviderInfo inf = dslist.get(i);
                if (inf.authority.equals(contenturi)) {
                    menuItems.add(inf.loadLabel(pm).toString() + " *");
                } else {
                    menuItems.add(inf.loadLabel(pm).toString());
                }
            }
            menuItems.add(context.getString(R.string.lb_conf_add_ds_title));

            final CharSequence[] items = menuItems.toArray(new CharSequence[menuItems.size()]);
            new AlertDialog.Builder(context)
                    .setTitle(context.getString(R.string.mnu_ds))
                    .setItems(items, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            doItemSelect(R.string.lb_sdcard+item, items[item].toString());
                        }
                    })
                    .show();
        }
        return super.onPerformDefaultAction();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        int itemid = item.getItemId();
        doItemSelect(itemid, item.getTitle().toString());
        return true;
    }
    
    private void doItemSelect(int itemid, String item_name){
        String selecturl = null;
        boolean calledActivity = false;
        if (item_name.startsWith(context.getString(R.string.lb_sdcard))) {
            // SDCard
            selecturl = DeviceContentProvider.MEDIA_AUTHORITY;
        } else if (item_name.startsWith(context.getString(R.string.lb_conf_add_ds_title))) {
            Uri uri = Uri
                    .parse("market://search?q=jp.co.kayo.android.localplayer.ds");
            Intent it = new Intent(Intent.ACTION_VIEW, uri);
            context.startActivity(it);
        } else {
            // Ampcaheまたはその他
            int index = itemid - R.string.lb_sdcard - 1;
            selecturl = dslist.get(index).authority;

            boolean checkSetting = false;
            if (selecturl.equals(contenturi)) {
                // 同じURLなら設定画面を呼び出す
                checkSetting = true;
            } else {
                // PINGを飛ばしてだめなら設定画面を呼び出す
                Cursor cursor = null;
                try {
                    Uri uri = Uri.parse("content://"
                            + selecturl + "/config/ping");
                    cursor = context.getContentResolver().query(uri,
                            null, "force", null, null);
                    if (cursor == null) {
                        checkSetting = true;
                    }
                } finally {
                    if (cursor != null) {
                        cursor.close();
                    }
                }
            }
            if (checkSetting) {
                Intent it = new Intent();
                it.setClassName(
                        dslist.get(index).packageName,
                        dslist.get(index).packageName
                                + ".MainActivity");
                activity.startActivityForResult(it, SystemConsts.REQUEST_DSCHANGED);
                calledActivity = true;
            }
        }

        if (selecturl != null && !selecturl.equals(contenturi)) {
            Editor editor = pref.edit();
            editor.putString(SystemConsts.PREF_CONTENTURI,
                    selecturl);
            editor.commit();
            if (!calledActivity) {
                handler.sendEmptyMessage(SystemConsts.EVT_DSCHANFED);
            }
        }
    }

}
