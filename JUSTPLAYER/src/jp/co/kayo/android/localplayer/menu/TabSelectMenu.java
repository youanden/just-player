package jp.co.kayo.android.localplayer.menu;

import java.util.ArrayList;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.provider.DeviceContentProvider;

import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.SubMenu;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.os.Build;
import android.os.Handler;

public class TabSelectMenu extends BaseActionProvider implements OnMenuItemClickListener {
    Handler handler;

    public TabSelectMenu(Context context) {
        super(context);
    }
    
    public void setHandler(Handler handler) {
        this.handler = handler;
    }
    
    @Override
    public void onPrepareSubMenu(SubMenu subMenu) {
        subMenu.clear();
        
        subMenu.addSubMenu(R.id.mnu_select_tab, SystemConsts.TAG_ALBUM_GRID, 0, context.getString(R.string.lb_tab_albums));
        subMenu.addSubMenu(R.id.mnu_select_tab, SystemConsts.TAG_ALBUM_LIST, 1, context.getString(R.string.lb_tab_albums_list));
        subMenu.addSubMenu(R.id.mnu_select_tab, SystemConsts.TAG_ARTIST_LIST, 2, context.getString(R.string.lb_tab_artist));
        subMenu.addSubMenu(R.id.mnu_select_tab, SystemConsts.TAG_MEDIA, 3, context.getString(R.string.lb_tab_media));
        subMenu.addSubMenu(R.id.mnu_select_tab, SystemConsts.TAG_PLAYLIST, 4, context.getString(R.string.lb_tab_playlist));
        subMenu.addSubMenu(R.id.mnu_select_tab, SystemConsts.TAG_FOLDER, 5, context.getString(R.string.lb_tab_folder));
        subMenu.addSubMenu(R.id.mnu_select_tab, SystemConsts.TAG_GENRES, 6, context.getString(R.string.lb_tab_genres));
        subMenu.addSubMenu(R.id.mnu_select_tab, SystemConsts.TAG_VIDEO, 7, context.getString(R.string.lb_tab_videos));
        
        for (int i = 0; i < subMenu.size(); ++i) {
            subMenu.getItem(i).setOnMenuItemClickListener(this);
        }
    }
    
    @Override
    public boolean onPerformDefaultAction() {
        if (Build.VERSION.SDK_INT < 11) {
            ArrayList<String> menuItems = new ArrayList<String>();
            menuItems.add(context.getString(R.string.lb_tab_albums));
            menuItems.add(context.getString(R.string.lb_tab_albums_list));
            menuItems.add(context.getString(R.string.lb_tab_artist));
            menuItems.add(context.getString(R.string.lb_tab_media));
            menuItems.add(context.getString(R.string.lb_tab_playlist));
            menuItems.add(context.getString(R.string.lb_tab_folder));
            menuItems.add(context.getString(R.string.lb_tab_genres));
            menuItems.add(context.getString(R.string.lb_tab_videos));

            final CharSequence[] items = menuItems.toArray(new CharSequence[menuItems.size()]);
            new AlertDialog.Builder(context)
                    .setTitle(context.getString(R.string.lb_tab_title))
                    .setItems(items, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            doItemSelect(SystemConsts.TAG_ALBUM_GRID+item, items[item].toString());
                        }
                    })
                    .show();
        }
        return super.onPerformDefaultAction();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        int itemid = item.getItemId();
        doItemSelect(itemid, item.getTitle().toString());
        return true;
    }

    private void doItemSelect(int itemid, String item_name){
        handler.sendMessage(handler.obtainMessage(SystemConsts.EVT_SELECT_VIEW, itemid));
    }
}
