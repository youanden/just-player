
package jp.co.kayo.android.localplayer.menu;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.provider.DeviceContentProvider;
import jp.co.kayo.android.localplayer.service.IMediaPlayerService;
import jp.co.kayo.android.localplayer.service.StreamCacherServer;
import jp.co.kayo.android.localplayer.util.Funcs;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;

import android.view.MenuItem;
import android.view.SubMenu;
import android.view.MenuItem.OnMenuItemClickListener;

public class ShareMenu extends BaseActionProvider implements OnMenuItemClickListener {
    IMediaPlayerService binder;
    List<ResolveInfo> resolves;

    public ShareMenu(Context context) {
        super(context);
    }

    public void setBinder(IMediaPlayerService binder) {
        this.binder = binder;
    }

    @Override
    public void onPrepareSubMenu(SubMenu subMenu) {
        subMenu.clear();

        PackageManager pm = context.getPackageManager();
        resolves = Funcs.getShare(context);
        for (int i = 0; i < resolves.size(); i++) {
            ResolveInfo inf = resolves.get(i);
            CharSequence labelSeq = inf.loadLabel(pm);
            CharSequence label = labelSeq != null
                    ? labelSeq
                    : inf.activityInfo.name;

            subMenu.addSubMenu(R.id.mnu_share, i, i, label);
        }
        subMenu.addSubMenu(R.id.mnu_share, resolves.size(), resolves.size(),
                context.getString(R.string.lb_conf_add_share_title));

        for (int i = 0; i < subMenu.size(); ++i) {
            subMenu.getItem(i).setOnMenuItemClickListener(this);
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        int itemid = item.getItemId();
        doItemSelect(itemid, item.getTitle().toString());
        return true;
    }

    @Override
    public boolean onPerformDefaultAction() {
        if (Build.VERSION.SDK_INT < 11) {
            ArrayList<String> menuItems = new ArrayList<String>();
            
            
            PackageManager pm = context.getPackageManager();
            resolves = Funcs.getShare(context);
            for (int i = 0; i < resolves.size(); i++) {
                ResolveInfo inf = resolves.get(i);
                CharSequence labelSeq = inf.loadLabel(pm);
                CharSequence label = labelSeq != null
                        ? labelSeq
                        : inf.activityInfo.name;

                menuItems.add(label.toString());
            }
            menuItems.add(context.getString(R.string.lb_conf_add_share_title));


            final CharSequence[] items = menuItems.toArray(new CharSequence[menuItems.size()]);
            new AlertDialog.Builder(context)
                    .setTitle(context.getString(R.string.mnu_ds))
                    .setItems(items, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            doItemSelect(item, items[item].toString());
                        }
                    })
                    .show();
        }
        return super.onPerformDefaultAction();
    }

    
    private void doItemSelect(int itemid, String item_name){
        if (item_name.equals(context.getString(R.string.lb_conf_add_share_title))) {
            Uri uri = Uri
                    .parse("market://search?q=jp.co.kayo.android.localplayer.share");
            Intent it = new Intent(Intent.ACTION_VIEW, uri);
            context.startActivity(it);
        }
        else {
            if (binder != null) {
                String title = null;
                String artist = null;
                String album = null;
                long duration = 0;
                String data = null;
                try {
                    long media_id = binder.getMediaId();
                    if (media_id > 0) {
                        // 自分の曲
                        final Hashtable<String, String> tbl = ContentsUtils.getMedia(
                                context, new String[] {
                                        AudioMedia.ALBUM,
                                        AudioMedia.ALBUM_KEY, AudioMedia.ARTIST,
                                        AudioMedia.TITLE, AudioMedia.DURATION, AudioMedia.DATA
                                }, media_id);
                        if (tbl != null) {
                            album = tbl.get(AudioMedia.ALBUM);
                            artist = tbl.get(AudioMedia.ARTIST);
                            title = tbl.get(AudioMedia.TITLE);
                            duration = Funcs.parseLong(tbl.get(AudioMedia.DURATION));
                            data = StreamCacherServer.getContentUri(context,
                                    tbl.get(AudioMedia.DATA));
                        }
                    } else {
                        // NFCで共有した曲
                        int pos = binder.getPosition();
                        if (pos >= 0 && binder.getCount() > pos) {
                            String[] values = binder.getMediaD(pos);
                            if (values != null) {
                                title = values[0];
                                album = values[1];
                                artist = values[2];
                                duration = Funcs.parseLong(values[3]);
                            }
                        }
                    }

                    if (Funcs.isNotEmpty(album) && Funcs.isNotEmpty(title)) {
                        Intent it = new Intent();
                        it.setClassName(
                                resolves.get(itemid).activityInfo.packageName,
                                resolves.get(itemid).activityInfo.packageName
                                        + ".MainActivity");
                        it.putExtra("album", album);
                        it.putExtra("artist", artist);
                        it.putExtra("title", title);
                        it.putExtra("duration", duration);
                        it.putExtra("data", data);
                        context.startActivity(it);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
