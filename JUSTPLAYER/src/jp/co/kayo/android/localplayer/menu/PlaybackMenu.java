package jp.co.kayo.android.localplayer.menu;

import java.util.ArrayList;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.provider.DeviceContentProvider;

import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.SubMenu;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.os.Build;
import android.os.Handler;

public class PlaybackMenu extends BaseActionProvider implements OnMenuItemClickListener {
    Handler handler;

    public PlaybackMenu(Context context) {
        super(context);
    }
    
    public void setHandler(Handler handler) {
        this.handler = handler;
    }
    
    @Override
    public void onPrepareSubMenu(SubMenu subMenu) {
        subMenu.clear();
        
        subMenu.addSubMenu(R.id.mnu_playback_tab, R.id.mnu_playback_tab+0, 0, context.getString(R.string.lb_clear_playback));
        subMenu.addSubMenu(R.id.mnu_playback_tab, R.id.mnu_playback_tab+1, 1, context.getString(R.string.lb_save_playback));
        subMenu.addSubMenu(R.id.mnu_playback_tab, R.id.mnu_playback_tab+2, 2, context.getString(R.string.lb_load_playback));
        
        for (int i = 0; i < subMenu.size(); ++i) {
            subMenu.getItem(i).setOnMenuItemClickListener(this);
        }
    }
    
    @Override
    public boolean onPerformDefaultAction() {
        if (Build.VERSION.SDK_INT < 11) {
            ArrayList<String> menuItems = new ArrayList<String>();
            menuItems.add(context.getString(R.string.lb_clear_playback));
            menuItems.add(context.getString(R.string.lb_save_playback));
            menuItems.add(context.getString(R.string.lb_load_playback));

            final CharSequence[] items = menuItems.toArray(new CharSequence[menuItems.size()]);
            new AlertDialog.Builder(context)
                    .setTitle(context.getString(R.string.lb_title_playback))
                    .setItems(items, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            doItemSelect(R.id.mnu_playback_tab+item, items[item].toString());
                        }
                    })
                    .show();
        }
        return super.onPerformDefaultAction();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        int itemid = item.getItemId();
        doItemSelect(itemid, item.getTitle().toString());
        return true;
    }

    private void doItemSelect(int itemid, String item_name){
        if (item_name.startsWith(context.getString(R.string.lb_clear_playback))) {
            handler.sendEmptyMessage(SystemConsts.EVT_SELECT_PLAYBACK_CLEAR);
        }
        else if (item_name.startsWith(context.getString(R.string.lb_save_playback))) {
            handler.sendEmptyMessage(SystemConsts.EVT_SELECT_PLAYBACK_SAVE);
        }
        else if (item_name.startsWith(context.getString(R.string.lb_load_playback))) {
            handler.sendEmptyMessage(SystemConsts.EVT_SELECT_PLAYBACK_LOAD);
        }
    }
}
