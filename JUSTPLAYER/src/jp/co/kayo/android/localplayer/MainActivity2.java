
package jp.co.kayo.android.localplayer;

import java.io.ObjectInputStream.GetField;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;

import jp.co.kayo.android.localplayer.MainActivity2.MyTabsAdapter.TabInfo;
import jp.co.kayo.android.localplayer.appwidget.ColorSet;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.core.ContentManager;
import jp.co.kayo.android.localplayer.core.ContextMenuFragment;
import jp.co.kayo.android.localplayer.core.IProgressView;
import jp.co.kayo.android.localplayer.dialog.OpenPlaylistDialog;
import jp.co.kayo.android.localplayer.dialog.SavePlaylistDialog;
import jp.co.kayo.android.localplayer.fragment.ControlFragment;
import jp.co.kayo.android.localplayer.fragment.MainFragment;
import jp.co.kayo.android.localplayer.fragment.PlaybackListViewFragment;
import jp.co.kayo.android.localplayer.menu.DataSourceMenu;
import jp.co.kayo.android.localplayer.menu.PlaybackMenu;
import jp.co.kayo.android.localplayer.menu.ShareMenu;
import jp.co.kayo.android.localplayer.menu.TabSelectMenu;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.provider.LocalSuggestionProvider;
import jp.co.kayo.android.localplayer.service.IMediaPlayerService;
import jp.co.kayo.android.localplayer.service.IMediaPlayerServiceCallback;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.StrictHelper;
import jp.co.kayo.android.localplayer.util.ThemeHelper;
import jp.co.kayo.android.localplayer.util.ViewCache;
import jp.co.kayo.android.localplayer.util.Funcs.ServiceToken;
import jp.co.kayo.android.localplayer.util.NfcHelper.BeamMessage;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.nfc.NfcAdapter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.provider.SearchRecentSuggestions;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.text.TextUtils.TruncateAt;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;
import android.widget.Toast;

@TargetApi(14)
public class MainActivity2 extends BaseActivity implements OnQueryTextListener {
    SharedPreferences mPref;
    ViewPager mViewPager;
    private BillingService mBillingService = null;
    DungeonsPurchaseObserver mDungeonsPurchaseObserver;
    private ViewCache mViewCache;
    public MyTabsAdapter mTabsAdapter;
    Handler mHandler = new MyHandler(this);
    private Intent oneIntent;
    private ServiceToken mToken;
    private SearchView mSearchView;

    private static class MyHandler extends Handler {
        WeakReference<MainActivity2> ref;

        MyHandler(MainActivity2 r) {
            ref = new WeakReference<MainActivity2>(r);
        }

        @Override
        public void handleMessage(Message msg) {
            MainActivity2 main = ref.get();
            if (main != null) {
                if (msg.what == SystemConsts.EVT_DSCHANFED) {
                    try {
                        IMediaPlayerService binder = main.getBinder();
                        if (binder != null) {
                            binder.setContentsKey(null);
                            binder.stop();
                            //binder.clear();
                            binder.reload();
                        }
                    } catch (RemoteException e) {
                    }
                    
                    if(main.getSupportFragmentManager().getBackStackEntryCount()>0){
                        main.getSupportFragmentManager().popBackStack();
                        Funcs.setResumeReloadFlag(main.mPref, true);
                    }else{
                        for (int i = 0; i < main.mTabsAdapter.getCount(); i++) {
                            ContentManager mgr = (ContentManager) main.mTabsAdapter.getFragment(i);
                            if (mgr != null) {
                                mgr.reload();
                            }
                        }   
                    }
                }
                else if (msg.what == SystemConsts.EVT_SELECT_PLAYBACK_CLEAR) {
                    IMediaPlayerService binder = main.getBinder();
                    if (binder != null) {
                        try {
                            binder.setContentsKey(null);
                            binder.clear();
                        } catch (RemoteException e) {
                        }
                    }
                }
                else if (msg.what == SystemConsts.EVT_SELECT_PLAYBACK_SAVE) {
                    IMediaPlayerService binder = main.getBinder();
                    if (binder != null) {
                        long[] ids;
                        try {
                            ids = binder.getList();
                            if (ids != null && ids.length > 0) {
                                SavePlaylistDialog dlg = new SavePlaylistDialog();
                                Bundle b = new Bundle();
                                b.putLongArray("playlist", ids);
                                dlg.setArguments(b);
                                dlg.show(main.getSupportFragmentManager(), SystemConsts.TAG_RATING_DLG);
                            }
                        } catch (RemoteException e) {
                            Logger.e("mnu_save_playlist", e);
                        } finally {

                        }
                    }
                }
                else if (msg.what == SystemConsts.EVT_SELECT_PLAYBACK_LOAD) {
                    OpenPlaylistDialog dlg = new OpenPlaylistDialog();
                    dlg.show(main.getSupportFragmentManager(), "playlist.dlg");
                }
                else if (msg.what == SystemConsts.EVT_SELECT_VIEW) {
                    int itemid = (Integer)msg.obj;
                    MainFragment f = (MainFragment) main.mTabsAdapter.getFragment(0);
                    String tabname = f.selectView(itemid);
                    if (tabname != null) {
                        Tab tab = main.getSupportActionBar().getTabAt(0);
                        main.mTabsAdapter.setTabText(tab, tabname);
                    }
                }

            }
        }
    }

    IMediaPlayerServiceCallback.Stub mCallback = new IMediaPlayerServiceCallback.Stub() {
        ControlFragment control;

        ControlFragment getControll() {
            if (control == null) {
                FragmentManager m = getSupportFragmentManager();
                control = (ControlFragment) m
                        .findFragmentByTag(SystemConsts.TAG_CONTROL);
            }
            return control;
        }

        @Override
        public void updateView(final boolean updatelist) throws RemoteException {
            Logger.d("updateView=" + updatelist);
            // もし、コントロール部分が表示されているならリストを更新してあげて
            if (mHandler != null) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        // AlbumList,ArtistList,Song等
                        ControlFragment control = getControll();
                        // 再生中の曲の更新
                        if (mBinder != null) {
                            try {
                                mViewCache.setPosition(mBinder.getPosition(), mBinder.getMediaId(),
                                        mBinder.getPrefetchId());
                            } catch (RemoteException e) {
                            }
                        }

                        // 更新処理
                        if (control != null) {
                            control.updateView();
                        }

                        for (int i = 0; i < mTabsAdapter.getCount(); i++) {
                            ContentManager f = (ContentManager) mTabsAdapter.getFragment(i);
                            if (f != null) {
                                if (updatelist && f instanceof PlaybackListViewFragment) {
                                    f.reload();
                                } else {
                                    f.changedMedia();
                                }
                            }
                        }
                    }
                });
            }
        }

        @Override
        public void updateList() throws RemoteException {
            // もし、再生中のフラグメントが表示されているならリストを更新してあげて
        }

        @Override
        public void onBufferingUpdate(int percent) throws RemoteException {
            // バッファリング中のプログレスバーですよぉ
            ControlFragment f = getControll();
            if (f != null) {
                f.onBufferingUpdate(percent);
            }
        }

        @Override
        public void startProgress(long max) throws RemoteException {
            long media_id = mBinder.getMediaId();
            int pos = mBinder.getPosition();
            long pref_id = mBinder.getPrefetchId();
            mViewCache.setPosition(pos, media_id, pref_id);
            Fragment current = getCurrentFragment();
            if (current != null && current instanceof IProgressView) {
                ((IProgressView) current).startProgress(max);
            }
        }

        @Override
        public void stopProgress() throws RemoteException {
            Fragment current = getCurrentFragment();
            if (current != null && current instanceof IProgressView) {
                ((IProgressView) current).stopProgress();
            }
        }

        @Override
        public void progress(long pos, long max) throws RemoteException {
            Fragment current = getCurrentFragment();
            if (current != null && current instanceof IProgressView) {
                ((IProgressView) current).progress(pos, max);
            }
        }

        @Override
        public void close() throws RemoteException {
            MainActivity2.this.finish();
        }
    };

    ContentObserver mAlbumContentObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
        }
    };

    ContentObserver mArtistContentObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
        }
    };

    ContentObserver mMediaContentObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SystemConsts.REQUEST_TAGEDIT) {
            // 変更を通知
            for (int i = 0; i < mTabsAdapter.getCount(); i++) {
                ContentManager f = (ContentManager) mTabsAdapter.getFragment(i);
                if (f != null) {
                    f.reload();
                }
            }
        } else if (requestCode == SystemConsts.REQUEST_DSCHANGED) {
            mHandler.sendEmptyMessage(SystemConsts.EVT_DSCHANFED);
        } else if (requestCode == SystemConsts.REQUEST_ALBUMART) {
            if (data != null) {
                String album_key = data.getStringExtra("album_key");
                if (album_key != null) {
                    ContentsUtils.reloadAlbumArt(this, album_key);
                    // 変更を通知
                    for (int i = 0; i < mTabsAdapter.getCount(); i++) {
                        ContentManager f = (ContentManager) mTabsAdapter.getFragment(i);
                        if (f != null) {
                            f.reload();
                        }
                    }
                }
            }
        }
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        new ThemeHelper().selectTheme(this);
        StrictHelper.registStrictMode();
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);

        mPref = PreferenceManager.getDefaultSharedPreferences(this);

        setContentView(R.layout.main_tab);

        hideProgressBar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(
                getSupportFragmentManager().getBackStackEntryCount() > 0);
        getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        getSupportActionBar().setSubtitle(getString(R.string.hello));

        getWindow().setSoftInputMode(
                LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        // SDCardChec
        Funcs.checkSDCard(this);

        // Audio Volume
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        // ViewCache
        mViewCache = (ViewCache) getSupportFragmentManager().findFragmentByTag(
                SystemConsts.TAG_CACHE);

        // setup
        mViewPager = (ViewPager) findViewById(R.id.pager);

        mTabsAdapter = new MyTabsAdapter(this, getSupportActionBar(), mViewPager);

        ActionBar.Tab maintab = getSupportActionBar().newTab().setText(
                getString(R.string.lb_tab_albums));
        mTabsAdapter.addTab(maintab, MainFragment.class, null);

        ActionBar.Tab playtab = getSupportActionBar().newTab().setText(
                getString(R.string.lb_tab_order));
        mTabsAdapter.addTab(playtab, PlaybackListViewFragment.class, null);

        getSupportFragmentManager().addOnBackStackChangedListener(new OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                getSupportActionBar().setDisplayHomeAsUpEnabled(
                        getSupportFragmentManager().getBackStackEntryCount() > 0);

                MainFragment f = (MainFragment) mTabsAdapter.getFragment(0);
                String tabname = f.getName(MainActivity2.this);
                Tab tab = getSupportActionBar().getTabAt(0);
                mTabsAdapter.setTabText(tab, tabname);
                
                if (Funcs.isResumeReloadFlag(mPref)) {
                    Funcs.setResumeReloadFlag(mPref, false);
                    for (int i = 0; i < mTabsAdapter.getCount(); i++) {
                        ContentManager mgr = (ContentManager) mTabsAdapter.getFragment(i);
                        if (mgr != null) {
                            mgr.reload();
                        }
                    }
                }
            }
        });

        // ContentResolver
        getContentResolver().registerContentObserver(
                MediaConsts.ALBUM_CONTENT_URI, true, mAlbumContentObserver);
        getContentResolver().registerContentObserver(
                MediaConsts.ARTIST_CONTENT_URI, true, mArtistContentObserver);
        getContentResolver().registerContentObserver(
                MediaConsts.MEDIA_CONTENT_URI, true, mMediaContentObserver);

        Intent intent = getIntent();
        if (intent == null
                || intent.getAction() == null
                || !intent.getAction()
                        .equals(SystemConsts.MAIN_ACITON_SHOWHOMW)) {
            // 初回起動か確認
            boolean isFirst = mPref.getBoolean("is_first", true);
            if (isFirst) {
                Editor editor = mPref.edit();
                editor.putBoolean("is_first", false);
                editor.commit();
                // まず、既にアプリを購入済みか確認する
                mBillingService = new BillingService();
                mDungeonsPurchaseObserver = new DungeonsPurchaseObserver(this,
                        mBillingService, new Handler());
                ResponseHandler.register(mDungeonsPurchaseObserver);
                mBillingService.setContext(this);
                mBillingService.restoreTransactions();

            }
        }

        if (savedInstanceState != null) {
            int index = savedInstanceState.getInt("index");
            selectTab(index);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        if (procIntent(intent) == false) {
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        procIntent(intent);
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            doSearchWithIntent(intent);
        } else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            if (intent.getFlags() == Intent.FLAG_ACTIVITY_NEW_TASK) {
                doSearchWithIntent(intent);
            }
        }
    }

    @Override
    public void onServiceConnected(IMediaPlayerService binder) {
        if (oneIntent != null) {
            revieveNdef(oneIntent);
        }
        for (int i = 0; i < mTabsAdapter.getCount(); i++) {
            ContentManager f = (ContentManager) mTabsAdapter.getFragment(i);
            if (f != null) {
                if (f instanceof PlaybackListViewFragment) {
                    f.reload();
                }
                else {
                    f.changedMedia();
                }
            }
        }
    }

    public ArrayList<Tab> getPlaybackTab() {
        ArrayList<Tab> tabs = new ArrayList<Tab>();
        for (int i = 0; i < getSupportActionBar().getTabCount(); i++) {
            Tab tab = getSupportActionBar().getTabAt(i);
            TabInfo info = (TabInfo) tab.getTag();
            if (info != null && info.tag.equals(getString(R.string.lb_tab_order))) {
                tabs.add(tab);
            }
        }
        return tabs;
    }

    private boolean procIntent(Intent intent) {
        if (intent != null) {
            if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {
                if (revieveNdef(intent)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean revieveNdef(Intent intent) {
        if (getNFCHelper() != null) {
            IMediaPlayerService binder = getBinder();
            if (binder != null) {
                oneIntent = null;
                try {
                    ArrayList<BeamMessage> list = getNFCHelper()
                            .getNdefMessage(intent);
                    if (list != null) {
                        try {
                            String cntkey = SystemConsts.CONTENTSKEY_PLAYLIST
                                    + System.currentTimeMillis();
                            try {
                                binder.setContentsKey(cntkey);
                                binder.lockUpdateToPlay();
                                binder.clearcut();
                                for (int i = 0; i < list.size(); i++) {
                                    BeamMessage msg = list.get(i);
                                    try {
                                        binder.addMediaD(0, msg.duration,
                                                msg.title, msg.album,
                                                msg.artist, msg.url);
                                    } catch (RemoteException e) {
                                    }
                                }
                            } finally {
                                binder.play();
                            }
                        } catch (RemoteException e) {
                        }
                        return true;
                    }
                } finally {
                    ArrayList<Tab> tabs = getPlaybackTab();
                    if (tabs.size() > 0) {
                        getSupportActionBar().selectTab(tabs.get(0));
                    }
                }
            }
        }
        oneIntent = intent;
        return false;
    }

    public void doSearchWithIntent(Intent intent) {
        String queryString = intent.getStringExtra(SearchManager.QUERY);
        SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
                LocalSuggestionProvider.AUTHORITY, LocalSuggestionProvider.DATABASE_MODE_QUERIES);
        suggestions.saveRecentQuery(queryString, null);

        MainFragment f = (MainFragment) mTabsAdapter.getFragment(0);
        if (f != null) {
            f.doSearchQuery(queryString);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("index", getSupportActionBar()
                .getSelectedNavigationIndex());
    }

    @Override
    protected void onResume() {
        super.onResume();
        // 音楽サービスとの接続
        mToken = Funcs.bindToService(this, mConnection);
    }

    @Override
    protected void onPause() {
        if (mToken != null) {
            Funcs.unbindFromService(mToken);
            mToken = null;
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {

        getContentResolver().unregisterContentObserver(mAlbumContentObserver);
        getContentResolver().unregisterContentObserver(mArtistContentObserver);
        getContentResolver().unregisterContentObserver(mMediaContentObserver);

        if (mDungeonsPurchaseObserver != null) {
            ResponseHandler.unregister(mDungeonsPurchaseObserver);
        }
        if (mBillingService != null) {
            mBillingService.unbind();
        }

        ViewCache.clearImage();

        super.onDestroy();
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                Fragment fragment = getCurrentFragment();
                if (fragment != null) {
                    if (fragment instanceof ContextMenuFragment) {
                        ContextMenuFragment f = (ContextMenuFragment) fragment;
                        if (f.onBackPressed()) {
                            return true;
                        }
                    }
                }

                SharedPreferences pref = PreferenceManager
                        .getDefaultSharedPreferences(this);
                boolean exitcheck = pref.getBoolean("key.exitcheck", false);
                if (exitcheck) {
                    // ダイアログの表示
                    AlertDialog.Builder ad = new AlertDialog.Builder(this);
                    ad.setMessage(getString(R.string.alert_exit_msg));
                    ad.setPositiveButton(getString(R.string.lb_yes),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                        int whichButton) {
                                    // OKならActivity終了
                                    finish();
                                }
                            });
                    ad.setNegativeButton(getString(R.string.lb_no),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                        int whichButton) {
                                }
                            });
                    ad.create();
                    ad.show();
                    return true;
                } else {
                    return super.dispatchKeyEvent(event);
                }
            }
        }
        return super.dispatchKeyEvent(event);
    }

    MenuItem mSearchItem;

    private void setupSearchView(MenuItem searchItem) {
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        if (searchManager != null) {
            mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }
        mSearchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Logger.d("onQueryTextSubmit");

        String queryString = newText;
        SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
                LocalSuggestionProvider.AUTHORITY, LocalSuggestionProvider.DATABASE_MODE_QUERIES);
        suggestions.saveRecentQuery(queryString, null);

        MainFragment f = (MainFragment) mTabsAdapter.getFragment(0);
        if (f != null) {
            f.doSearchQuery(queryString);
        }

        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Logger.d("onQueryTextSubmit");
        mSearchItem.collapseActionView();
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.main_menu_items, menu);

        mSearchItem = menu.findItem(R.id.mnu_search);
        mSearchView = (SearchView) mSearchItem.getActionView();
        if (Build.VERSION.SDK_INT >= 8) {
            setupSearchView(mSearchItem);
        }
        
        MenuItem mnu_playback_tab = menu.findItem(R.id.mnu_playback_tab);
        if(Build.VERSION.SDK_INT <= 10){
            mnu_playback_tab.getSubMenu().clear();
        }
        
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (menu == null) {
            return super.onPrepareOptionsMenu(menu);
        }

        MenuItem mnu_ds = menu.findItem(R.id.mnu_ds);
        DataSourceMenu ds_provider = (DataSourceMenu) mnu_ds.getActionProvider();
        if (ds_provider != null) {
            ds_provider.setActivity(this, mHandler);
        }

        MenuItem mnu_share = menu.findItem(R.id.mnu_share);
        ShareMenu share_provider = (ShareMenu) mnu_share.getActionProvider();
        if (share_provider != null) {
            share_provider.setBinder(getBinder());
        }
        
        MenuItem mnu_playback_tab = menu.findItem(R.id.mnu_playback_tab);
        PlaybackMenu playback_provider = (PlaybackMenu) mnu_playback_tab.getActionProvider();
        if (playback_provider != null) {
            playback_provider.setHandler(mHandler);
        }
        
        MenuItem mnu_select_tab = menu.findItem(R.id.mnu_select_tab);
        TabSelectMenu tab_provider = (TabSelectMenu) mnu_select_tab.getActionProvider();
        if (tab_provider != null) {
            tab_provider.setHandler(mHandler);
        }
        
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        try {
            switch (item.getItemId()) {
                case android.R.id.home: {
                    getSupportFragmentManager().popBackStack();
                }
                    break;
                case R.id.mnu_config: {
                    // 設定画面
                    Intent intent = new Intent(this, ConfigActivity.class);
                    startActivityForResult(intent, SystemConsts.REQUEST_CONFIG);
                }
                    break;
                case R.id.mnu_search: {
                    onSearchRequested();
                }
                    break;
                case R.id.mnu_playback_tab:{
                    if(Build.VERSION.SDK_INT <= 10){
                        
                        return false;
                    }
                }break;
                default: {
                }
            }
            return super.onOptionsItemSelected(item);
        } finally {
            // Google Analyticsにトラックイベントを送信
        }
    }

    @Override
    IMediaPlayerServiceCallback getCallBack() {
        return mCallback;
    }

    @Override
    ViewCache getViewCache() {
        return mViewCache;
    }

    @Override
    Handler getHandler() {
        return mHandler;
    }

    public static class MyTabsAdapter extends FragmentPagerAdapter implements
            ViewPager.OnPageChangeListener, ActionBar.TabListener, OnClickListener,
            OnLongClickListener {
        private final MainActivity2 mContext;
        private final ActionBar mActionBar;
        private final ViewPager mViewPager;
        private final ArrayList<TabInfo> mTabs = new ArrayList<TabInfo>();
        private ColorSet mColorset;

        static final class TabInfo {
            private final String tag;
            private final Class<?> clss;
            private final Bundle args;
            String fragment_tag;

            TabInfo(String _tag, Class<?> _class, Bundle _args) {
                tag = _tag;
                clss = _class;
                args = _args;
            }
        }

        public MyTabsAdapter(MainActivity2 activity, ActionBar actionBar,
                ViewPager pager) {
            super(activity.getSupportFragmentManager());
            mContext = activity;
            mActionBar = actionBar;
            mViewPager = pager;
            mViewPager.setAdapter(this);
            mViewPager.setOnPageChangeListener(this);
            mColorset = new ColorSet();
            mColorset.load(activity);
        }

        public void addTab(ActionBar.Tab tab, Class<?> clss, Bundle args) {
            String tag = tab.getText().toString();
            TabInfo info = new TabInfo(tag, clss, args);
            mTabs.add(info);
            tab.setTag(info);
            String tabname = null;
            Fragment f = mContext.getSupportFragmentManager().findFragmentByTag(info.fragment_tag);
            if (f != null && f instanceof ContentManager) {
                tabname = ((ContentManager) f).getName(mContext);
            }

            View view = createTabView(mContext, tabname != null ? tabname : tab.getText()
                    .toString());
            view.setTag(tab);
            tab.setCustomView(view);
            mActionBar.addTab(tab.setTabListener(this));
            notifyDataSetChanged();
        }

        public void setTabText(ActionBar.Tab tab, String text) {
            TextView view = (TextView) tab.getCustomView();
            if (view != null) {
                view.setText(text);
                notifyDataSetChanged();
            }
        }

        private View createTabView(Context context, String text) {
            if (text != null) {
                TextView textView = new TextView(context, null, android.R.attr.actionBarTabTextStyle);
                //TextView textView = new TextView(context, null, R.attr.actionBarTabTextStyle);
                textView.setEllipsize(TruncateAt.END);
                LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT,
                        LayoutParams.WRAP_CONTENT);
                lp.gravity = Gravity.CENTER_VERTICAL;
                textView.setLayoutParams(lp);
                textView.setGravity(Gravity.CENTER_VERTICAL);
                textView.setText(text);
                int color = mColorset.getColor(ColorSet.KEY_ACTIONBAR_TAB_COLOR);
                if (color != -1) {
                    textView.setTextColor(color);
                }

                textView.setOnClickListener(this);
                textView.setOnLongClickListener(this);

                return textView;
            }
            else {
                return null;
            }
        }

        public final ArrayList<TabInfo> getTabs() {
            return mTabs;
        }

        public String findFragmentTag(String tag) {
            for (TabInfo info : mTabs) {
                if (info.tag.equals(tag)) {
                    return info.fragment_tag;
                }
            }
            return null;
        }

        @Override
        public int getCount() {
            return mTabs.size();
        }

        @Override
        public Fragment getItem(int position) {
            TabInfo info = mTabs.get(position);
            Fragment fragment = Fragment.instantiate(mContext, info.clss.getName(),
                    info.args);
            return fragment;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container,
                    position);
            if (position < mTabs.size()) {
                TabInfo info = mTabs.get(position);
                info.fragment_tag = fragment.getTag();
            }
            return fragment;
        }

        public Fragment getFragment(int position) {
            Tab tab = mActionBar.getTabAt(position);
            TabInfo info = (TabInfo) tab.getTag();
            if (info != null) {
                return mContext.getSupportFragmentManager().findFragmentByTag(
                        info.fragment_tag);
            }
            return null;
        }

        @Override
        public void onPageScrolled(int position, float positionOffset,
                int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            mContext.selectTab(position);
            ContentManager cmr = (ContentManager) getFragment(position);
            if (cmr != null) {
                cmr.changedMedia();
                FragmentManager m = mContext.getSupportFragmentManager();
                ControlFragment control = (ControlFragment) m
                        .findFragmentByTag(SystemConsts.TAG_CONTROL);
                if (control != null && control.getView() != null) {
                    control.showControl(false);
                }
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }

        @Override
        public void onTabReselected(Tab tab, android.app.FragmentTransaction ft) {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void onTabSelected(Tab tab, android.app.FragmentTransaction ft) {
            mViewPager.setCurrentItem(tab.getPosition());
            
        }

        @Override
        public void onTabUnselected(Tab tab, android.app.FragmentTransaction ft) {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void onClick(View v) {
            Tab tab = mActionBar.getSelectedTab();
            View currelntView = tab.getCustomView();
            if (currelntView == v) {
                Logger.d("ReSelect Click!");
                if (tab.getPosition() == 0) {
                    Fragment f = mContext.mTabsAdapter.getFragment(0);
                    if (f instanceof ContextMenuFragment) {
                        String tabname = ((ContextMenuFragment) f).selectSort();
                        if (tabname != null) {
                            mContext.mTabsAdapter.setTabText(tab, tabname);
                        }
                    }
                }
            } else {
                Tab viewTab = (Tab) v.getTag();
                if (viewTab != null) {
                    mActionBar.selectTab(viewTab);
                }
            }
        }

        @Override
        public boolean onLongClick(View v) {
            Tab tab = mActionBar.getSelectedTab();
            View currentView = tab.getCustomView();
            if (currentView == v) {
                Logger.d("ReSelect Long Click!");
                if (tab.getPosition() == 0) {
                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                    final CharSequence[] selectViews = {
                            mContext.getString(R.string.lb_tab_albums),
                            mContext.getString(R.string.lb_tab_albums_list),
                            mContext.getString(R.string.lb_tab_artist),
                            mContext.getString(R.string.lb_tab_media),
                            mContext.getString(R.string.lb_tab_playlist),
                            mContext.getString(R.string.lb_tab_folder),
                            mContext.getString(R.string.lb_tab_genres),
                            mContext.getString(R.string.lb_tab_videos),
                    };
                    alertDialogBuilder.setTitle(mContext.getString(R.string.lb_tab_title));
                    alertDialogBuilder.setItems(selectViews, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            int id = SystemConsts.TAG_ALBUM_GRID + which;

                            MainFragment f = (MainFragment) mContext.mTabsAdapter.getFragment(0);
                            String tabname = f.selectView(id);
                            if (tabname != null) {
                                Tab tab = mContext.getSupportActionBar().getTabAt(0);
                                mContext.mTabsAdapter.setTabText(tab, tabname);
                            }

                        }
                    });

                    // ダイアログを表示
                    alertDialogBuilder.create().show();

                }
            }
            return true;
        }

    }
}
