package jp.co.kayo.android.localplayer;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 *      This program is free software; you can redistribute it and/or modify it under
 *      the terms of the GNU General Public License as published by the Free Software
 *      Foundation; either version 2 of the License, or (at your option) any later
 *      version.
 *      
 *      This program is distributed in the hope that it will be useful, but WITHOUT
 *      ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *      FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *      details.
 *      
 *      You should have received a copy of the GNU General Public License along with
 *      this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *      Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

import java.util.ArrayList;
import java.util.List;

import co.jp.kayo.android.adview.AdViewPreferenceActivity;

import shoozhoo.libandrotranslation.TranslationListActivity;

import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.service.IMediaPlayerService;
import jp.co.kayo.android.localplayer.service.MediaPlayerService;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.StrictHelper;
import jp.co.kayo.android.localplayer.util.ThemeHelper;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceCategory;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceActivity;
import android.widget.Toast;

public class ConfigActivity extends AdViewPreferenceActivity implements
        OnPreferenceChangeListener, OnPreferenceClickListener {
    private static final String DEVELOPER_ADDRESS = "yokmama@kayosystem.com";

    List<ProviderInfo> mDsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        new ThemeHelper().selectTheme(this);
        StrictHelper.registStrictMode();
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref);
        getListView().setBackgroundColor(Color.TRANSPARENT);
        getListView().setCacheColorHint(Color.TRANSPARENT);

        Preference theme_pref = findPreference("key.theme");
        Preference blog_pref = findPreference("key.blog");
        Preference translation = findPreference("key.translation");

        blog_pref.setOnPreferenceClickListener(this);
        translation.setOnPreferenceClickListener(this);

        theme_pref.setOnPreferenceChangeListener(this);

        // data souce
        PreferenceCategory key_datasource = (PreferenceCategory) findPreference("key.datasource");

        PackageManager pm = this.getPackageManager();
        mDsList = ContentsUtils.getDataSouce(this);
        if (mDsList != null && mDsList.size() > 0) {
            for (ProviderInfo inf : mDsList) {
                Preference pref = new Preference(this);
                pref.setKey(inf.authority);
                pref.setTitle(inf.loadLabel(pm));
                pref.setOnPreferenceClickListener(this);

                key_datasource.addPreference(pref);
            }
        }

        Preference pref = new Preference(this);
        pref.setTitle(getString(R.string.lb_conf_add_ds_title));
        pref.setKey("ds.searchmarket");
        pref.setSummary(getString(R.string.lb_conf_add_ds_summary));
        pref.setOnPreferenceClickListener(this);
        key_datasource.addPreference(pref);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        Toast.makeText(this, getString(R.string.txt_pref_changed_notice),
                Toast.LENGTH_SHORT).show();
        return true;
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        if ("key.translation".equals(preference.getKey())) {
            // 翻訳
            translation();
        } else if ("ds.searchmarket".equals(preference.getKey())) {
            Uri uri = Uri
                    .parse("market://search?q=jp.co.kayo.android.localplayer.ds");
            Intent it = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(it);
        } else {
            String key = preference.getKey();
            for (ProviderInfo inf : mDsList) {
                if (inf.authority.equals(key)) {
                    Intent it = new Intent();
                    it.setClassName(inf.packageName, inf.packageName
                            + ".MainActivity");
                    startActivity(it);

                    break;
                }
            }
        }
        return false;
    }

    private void translation() {
        String email = DEVELOPER_ADDRESS;
        String version = Funcs.getVersionNumber("ver", this);
        String aboutApp = getString(R.string.app_name) + "[" + version + "]"
                + "\n";

        Intent intent = new Intent(this, TranslationListActivity.class);
        intent.putExtra(TranslationListActivity.INTENT_EXTRA_MAIL_TO, email);
        intent.putExtra(TranslationListActivity.INTENT_EXTRA_MAIL_APPNAME,
                aboutApp);

        ArrayList<String> ignores = new ArrayList<String>();
        // 翻訳を無視するリソースがある場合は追加
        // ignores.add("xxxx");
        intent.putExtra(TranslationListActivity.INTENT_EXTRA_IGNORE_PATTERNS,
                ignores);
        startActivity(intent);
    }
}
