package jp.co.kayo.android.localplayer.appwidget;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.service.MediaPlayerService;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.ThemeHelper;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.Paint.Style;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class FontColorPreference extends Activity implements OnClickListener {

    ColorSet colorset;
    
    TextView getTextView(int id){
        return (TextView)findViewById(id);
    }
    
    Button getButton(int id){
        return (Button)findViewById(id);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        new ThemeHelper().selectTheme(this);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fontcolorpref);

        colorset = new ColorSet();
        colorset.load(this);

        colorset.setColor(ColorSet.KEY_DEFAULT_PRI_COLOR, getTextView(R.id.textDefaultPriColor));
        colorset.setColor(ColorSet.KEY_DEFAULT_SEC_COLOR, getTextView(R.id.textDefaultSecColor));
        colorset.setColor(ColorSet.KEY_NOTIFI_PRI_COLOR, getTextView(R.id.textNotificationPriColor));
        colorset.setColor(ColorSet.KEY_NOTIFI_SEC_COLOR, getTextView(R.id.textNotificationSecColor));
//        colorset.setColor(ColorSet.KEY_ACTIONBAR_PRI_COLOR, getTextView(R.id.textActionbarPriColor));
//        colorset.setColor(ColorSet.KEY_ACTIONBAR_SEC_COLOR, getTextView(R.id.textActionbarSecColor));
        colorset.setColor(ColorSet.KEY_ACTIONBAR_TAB_COLOR, getTextView(R.id.textActionbarTabColor));

        getButton(R.id.btnDefaultPriColor).setText(colorset.getColor(ColorSet.KEY_DEFAULT_PRI_COLOR)!=-1?getColorString(colorset.getColor(ColorSet.KEY_DEFAULT_PRI_COLOR)): "default");
        getButton(R.id.btnDefaultSecColor).setText(colorset.getColor(ColorSet.KEY_DEFAULT_SEC_COLOR)!=-1?getColorString(colorset.getColor(ColorSet.KEY_DEFAULT_SEC_COLOR)): "default");
        getButton(R.id.btnNotificationPriColor).setText(colorset.getColor(ColorSet.KEY_NOTIFI_PRI_COLOR)!=-1?getColorString(colorset.getColor(ColorSet.KEY_NOTIFI_PRI_COLOR)): "default");
        getButton(R.id.btnNotificationSecColor).setText(colorset.getColor(ColorSet.KEY_NOTIFI_SEC_COLOR)!=-1?getColorString(colorset.getColor(ColorSet.KEY_NOTIFI_SEC_COLOR)): "default");
        getButton(R.id.btnActionbarTabColor).setText(colorset.getColor(ColorSet.KEY_ACTIONBAR_PRI_COLOR)!=-1?getColorString(colorset.getColor(ColorSet.KEY_ACTIONBAR_PRI_COLOR)): "default");
        getButton(R.id.btnActionbarTabColor).setText(colorset.getColor(ColorSet.KEY_ACTIONBAR_SEC_COLOR)!=-1?getColorString(colorset.getColor(ColorSet.KEY_ACTIONBAR_SEC_COLOR)): "default");
        getButton(R.id.btnActionbarTabColor).setText(colorset.getColor(ColorSet.KEY_ACTIONBAR_TAB_COLOR)!=-1?getColorString(colorset.getColor(ColorSet.KEY_ACTIONBAR_TAB_COLOR)): "default");


        getButton(R.id.btnDefaultPriColor).setOnClickListener(this);
        getButton(R.id.btnDefaultSecColor).setOnClickListener(this);
        getButton(R.id.btnNotificationPriColor).setOnClickListener(this);
        getButton(R.id.btnNotificationSecColor).setOnClickListener(this);
//        getButton(R.id.btnActionbarPriColor).setOnClickListener(this);
//        getButton(R.id.btnActionbarSecColor).setOnClickListener(this);
        getButton(R.id.btnActionbarTabColor).setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logger.d("resultCode=" + resultCode);
        if (data != null) {
            int color = (int) data.getExtras().getInt(Intent.ACTION_PICK);
            if(color!=-1){
                if(requestCode == R.id.btnDefaultPriColor){
                    colorset.putColor(this, ColorSet.KEY_DEFAULT_PRI_COLOR, color);
                    getTextView(R.id.textDefaultPriColor).setTextColor(color);
                    getButton(R.id.btnDefaultPriColor).setText(getColorString(color));
                }
                else if(requestCode == R.id.btnDefaultSecColor){
                    colorset.putColor(this, ColorSet.KEY_DEFAULT_SEC_COLOR, color);
                    getTextView(R.id.textDefaultSecColor).setTextColor(color);
                    getButton(R.id.btnDefaultSecColor).setText(getColorString(color));
                }
                else if(requestCode == R.id.btnNotificationPriColor){
                    colorset.putColor(this, ColorSet.KEY_NOTIFI_PRI_COLOR, color);
                    getTextView(R.id.textNotificationPriColor).setTextColor(color);
                    getButton(R.id.btnNotificationPriColor).setText(getColorString(color));
                }
                else if(requestCode == R.id.btnNotificationSecColor){
                    colorset.putColor(this, ColorSet.KEY_NOTIFI_SEC_COLOR, color);
                    getTextView(R.id.textNotificationSecColor).setTextColor(color);
                    getButton(R.id.btnNotificationSecColor).setText(getColorString(color));
                }
                /*else if(requestCode == R.id.btnActionbarPriColor){
                    colorset.putColor(this, ColorSet.KEY_ACTIONBAR_PRI_COLOR, color);
                    getTextView(R.id.textActionbarPriColor).setTextColor(color);
                    getButton(R.id.btnActionbarPriColor).setText(getColorString(color));
                }
                else if(requestCode == R.id.btnActionbarSecColor){
                    colorset.putColor(this, ColorSet.KEY_ACTIONBAR_SEC_COLOR, color);
                    getTextView(R.id.textActionbarSecColor).setTextColor(color);
                    getButton(R.id.btnActionbarSecColor).setText(getColorString(color));
                }*/
                else if(requestCode == R.id.btnActionbarTabColor){
                    colorset.putColor(this, ColorSet.KEY_ACTIONBAR_TAB_COLOR, color);
                    getTextView(R.id.textActionbarTabColor).setTextColor(color);
                    getButton(R.id.btnActionbarTabColor).setText(getColorString(color));
                }
            }
            else{
                if(requestCode == R.id.btnDefaultPriColor){
                    colorset.removeColor(this, ColorSet.KEY_DEFAULT_PRI_COLOR);
                    getTextView(R.id.textDefaultPriColor).setTextColor(getResources().getColor(android.R.color.primary_text_dark));
                    getButton(R.id.btnDefaultPriColor).setText("default");
                }
                else if(requestCode == R.id.btnDefaultSecColor){
                    colorset.removeColor(this, ColorSet.KEY_DEFAULT_SEC_COLOR);
                    getTextView(R.id.textDefaultSecColor).setTextColor(getResources().getColor(android.R.color.secondary_text_dark));
                    getButton(R.id.btnDefaultSecColor).setText("default");
                }
                else if(requestCode == R.id.btnNotificationPriColor){
                    colorset.removeColor(this, ColorSet.KEY_NOTIFI_PRI_COLOR);
                    getTextView(R.id.textNotificationPriColor).setTextColor(getResources().getColor(android.R.color.primary_text_dark));
                    getButton(R.id.btnNotificationPriColor).setText("default");
                }
                else if(requestCode == R.id.btnNotificationSecColor){
                    colorset.removeColor(this, ColorSet.KEY_NOTIFI_SEC_COLOR);
                    getTextView(R.id.textNotificationSecColor).setTextColor(getResources().getColor(android.R.color.secondary_text_dark));
                    getButton(R.id.btnNotificationSecColor).setText("default");
                }
                /*else if(requestCode == R.id.btnActionbarPriColor){
                    colorset.removeColor(this, ColorSet.KEY_ACTIONBAR_PRI_COLOR);
                    getTextView(R.id.textActionbarPriColor).setTextColor(getResources().getColor(android.R.color.secondary_text_dark));
                    getButton(R.id.btnActionbarPriColor).setText("default");
                }
                else if(requestCode == R.id.btnActionbarSecColor){
                    colorset.removeColor(this, ColorSet.KEY_ACTIONBAR_SEC_COLOR);
                    getTextView(R.id.textActionbarSecColor).setTextColor(getResources().getColor(android.R.color.secondary_text_dark));
                    getButton(R.id.btnActionbarSecColor).setText("default");
                }*/
                else if(requestCode == R.id.btnActionbarTabColor){
                    colorset.removeColor(this, ColorSet.KEY_ACTIONBAR_TAB_COLOR);
                    getTextView(R.id.textActionbarTabColor).setTextColor(getResources().getColor(android.R.color.secondary_text_dark));
                    getButton(R.id.btnActionbarTabColor).setText("default");
                }
            }
        }
    }

    public String getColorString(int icolor) {
        int r = Color.red(icolor);
        int g = Color.green(icolor);
        int b = Color.blue(icolor);
        return "#" + toHexString(r, 2) + toHexString(g, 2) + toHexString(b, 2);
    }

    public String toHexString(int value, int n) {
        String s = Integer.toHexString(value);
        StringBuilder sb = new StringBuilder();
        for (int i = s.length(); i < n; i++) {
            sb.append("0");
        }
        sb.append(s);
        return sb.toString().toUpperCase();
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this,
                ColorPickerDialog.class);
        startActivityForResult(intent, v.getId());
        
    }
}
