package jp.co.kayo.android.localplayer.consts;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;

public interface MediaConsts {
    public static final String AUTHORITY = "jp.co.kayo.android.localplayer";
    public static final String CONTENT_AUTHORITY_SLASH = "content://"
            + AUTHORITY + "/";

    // MEDIA URI
    public static final Uri MEDIA_CONTENT_URI = Uri
            .parse(CONTENT_AUTHORITY_SLASH + "audio/media");
    public static final Uri ALBUM_CONTENT_URI = Uri
            .parse(CONTENT_AUTHORITY_SLASH + "audio/albums");
    public static final Uri ARTIST_CONTENT_URI = Uri
            .parse(CONTENT_AUTHORITY_SLASH + "audio/artist");
    public static final Uri PLAYLIST_CONTENT_URI = Uri
            .parse(CONTENT_AUTHORITY_SLASH + "audio/playlist");
    public static final Uri PLAYLIST_MEMBER_CONTENT_URI = Uri
            .parse(CONTENT_AUTHORITY_SLASH + "audio/playlistmember");
    public static final Uri FAVORITE_CONTENT_URI = Uri
            .parse(CONTENT_AUTHORITY_SLASH + "audio/favorite");
    public static final Uri VIDEO_CONTENT_URI = Uri
            .parse(CONTENT_AUTHORITY_SLASH + "video/media");
    public static final Uri PLAYBACK_CONTENT_URI = Uri
            .parse(CONTENT_AUTHORITY_SLASH + "order/audio");
    public static final Uri FOLDER_CONTENT_URI = Uri
            .parse(CONTENT_AUTHORITY_SLASH + "audio/file");
    // public static final Uri DOWNLOAD_CONTENT_URI =
    // Uri.parse(CONTENT_AUTHORITY_SLASH + "download/media");
    public static final Uri GENRES_CONTENT_URI = Uri
            .parse(CONTENT_AUTHORITY_SLASH + "audio/genres");
    public static final Uri GENRES_MEMBER_CONTENT_URI = Uri
            .parse(CONTENT_AUTHORITY_SLASH + "audio/genresmember");
    public static final Uri AUTH_CONTENT_URI = Uri
            .parse(CONTENT_AUTHORITY_SLASH + "config/auth");
    public static final Uri URL_CONTENT_URI = Uri.parse(CONTENT_AUTHORITY_SLASH
            + "config/url");
    public static final Uri PING_CONTENT_URI = Uri
            .parse(CONTENT_AUTHORITY_SLASH + "config/ping");
    public static final Uri CLEAR_CONTENT_URI = Uri
            .parse(CONTENT_AUTHORITY_SLASH + "config/clear");
    public static final Uri RESET_CONTENT_URI = Uri
            .parse(CONTENT_AUTHORITY_SLASH + "config/reset");

    public static final int CODE_MEDIA = 0;
    public static final int CODE_MEDIA_ID = 1;
    public static final int CODE_ALBUMS = 2;
    public static final int CODE_ALBUMS_ID = 3;
    public static final int CODE_ARTIST = 4;
    public static final int CODE_ARTIST_ID = 5;
    public static final int CODE_PLAYLIST = 6;
    public static final int CODE_PLAYLIST_ID = 7;
    public static final int AUDIO_ALBUMART = 8;
    public static final int AUDIO_ALBUMART_ID = 9;
    public static final int AUDIO_ALBUMART_FILE_ID = 10;
    public static final int CODE_PLAYLISTMEMBER = 11;
    public static final int CODE_PLAYLISTMEMBER_ID = 12;
    public static final int CODE_FAVORITE = 13;
    public static final int CODE_FAVORITE_ID = 14;
    public static final int CODE_VIDEO = 15;
    public static final int CODE_VIDEO_ID = 16;
    public static final int CODE_ORDER_AUDIO = 17;
    public static final int CODE_ORDER_AUDIO_ID = 18;
    // public static final int CODE_DOWNLOAD = 19;
    // public static final int CODE_DOWNLOAD_ID = 20;
    public static final int CODE_GENRES = 19;
    public static final int CODE_GENRES_ID = 20;
    public static final int CODE_GENRESMEMBER = 21;
    public static final int CODE_GENRESMEMBER_ID = 22;
    public static final int CODE_CLEAR = 23;
    public static final int CODE_RESET = 24;
    public static final int CODE_AUTH = 25;
    public static final int CODE_PING = 26;
    public static final int CODE_FILE = 27;
    public static final int CODE_FILE_ID = 28;
    public static final int CODE_PREF = 30;

    public abstract static interface Media {
        public static final String _ID = BaseColumns._ID;
        public static final String DATA = MediaColumns.DATA;
        public static final String SIZE = MediaColumns.SIZE;
        public static final String DISPLAY_NAME = MediaColumns.DISPLAY_NAME;
        public static final String TITLE = MediaColumns.TITLE;
        public static final String DATE_ADDED = MediaColumns.DATE_ADDED;
        public static final String DATE_MODIFIED = MediaColumns.DATE_MODIFIED;
        public static final String MIME_TYPE = MediaColumns.MIME_TYPE;
        public static final String ENCODING = "encoding";
    }

    // Media
    public static interface AudioMedia extends Media {
        public static final String TITLE = MediaStore.Audio.Media.TITLE;
        public static final String MEDIA_KEY = "media_key";
        public static final String TITLE_KEY = MediaStore.Audio.Media.TITLE_KEY;
        public static final String DURATION = MediaStore.Audio.Media.DURATION;
        // public static final String BOOKMARK =
        // MediaStore.Audio.Media.BOOKMARK;
        public static final String ARTIST_ID = MediaStore.Audio.Media.ARTIST_ID;
        public static final String ARTIST = MediaStore.Audio.Media.ARTIST;
        public static final String ARTIST_KEY = MediaStore.Audio.Media.ARTIST_KEY;
        public static final String COMPOSER = MediaStore.Audio.Media.COMPOSER;
        public static final String ALBUM_ID = MediaStore.Audio.Media.ALBUM_ID;
        public static final String ALBUM = MediaStore.Audio.Media.ALBUM;
        public static final String ALBUM_KEY = MediaStore.Audio.Media.ALBUM_KEY;
        // public static final String ALBUM_ART =
        // MediaStore.Audio.Media.ALBUM_ART;
        public static final String TRACK = MediaStore.Audio.Media.TRACK;
        public static final String YEAR = MediaStore.Audio.Media.YEAR;
        public static final String IS_MUSIC = MediaStore.Audio.Media.IS_MUSIC;
        // public static final String IS_PODCAST =
        // MediaStore.Audio.Media.IS_PODCAST;
        public static final String IS_RINGTONE = MediaStore.Audio.Media.IS_RINGTONE;
        public static final String IS_ALARM = MediaStore.Audio.Media.IS_ALARM;
        public static final String IS_NOTIFICATION = MediaStore.Audio.Media.IS_NOTIFICATION;
        public static final String FAVORITE_POINT = TableConsts.FAVORITE_POINT;
        public static final String AUDIO_CACHE_FILE = TableConsts.AUDIO_CACHE_FILE;
    }

    public static interface AudioAlbum extends Media {
        public static final String ALBUM_ID = MediaStore.Audio.Albums.ALBUM_ID;
        public static final String ALBUM = MediaStore.Audio.Albums.ALBUM;
        public static final String ARTIST = MediaStore.Audio.Albums.ARTIST;
        public static final String NUMBER_OF_SONGS = MediaStore.Audio.Albums.NUMBER_OF_SONGS;
        public static final String NUMBER_OF_SONGS_FOR_ARTIST = MediaStore.Audio.Albums.NUMBER_OF_SONGS_FOR_ARTIST;
        public static final String FIRST_YEAR = MediaStore.Audio.Albums.FIRST_YEAR;
        public static final String LAST_YEAR = MediaStore.Audio.Albums.LAST_YEAR;
        public static final String ALBUM_KEY = MediaStore.Audio.Albums.ALBUM_KEY;
        public static final String ALBUM_ART = MediaStore.Audio.Albums.ALBUM_ART;
        public static final String FAVORITE_POINT = TableConsts.FAVORITE_POINT;
        public static final String ALBUM_INIT_FLG = TableConsts.ALBUM_INIT_FLG;

    }

    public static interface AudioArtist extends Media {
        public static final String ARTIST = MediaStore.Audio.Artists.ARTIST;
        public static final String ARTIST_KEY = MediaStore.Audio.Artists.ARTIST_KEY;
        public static final String NUMBER_OF_ALBUMS = MediaStore.Audio.Artists.NUMBER_OF_ALBUMS;
        public static final String NUMBER_OF_TRACKS = MediaStore.Audio.Artists.NUMBER_OF_TRACKS;
        public static final String FAVORITE_POINT = TableConsts.FAVORITE_POINT;
    }

    public static interface AudioPlaylist extends Media {
        public static final String NAME = MediaStore.Audio.Playlists.NAME;
        public static final String PLAYLIST_KEY = "playlist_key";
        public static final String DATA = MediaStore.Audio.Playlists.DATA;
        public static final String DATE_ADDED = MediaStore.Audio.Playlists.DATE_ADDED;
        public static final String DATE_MODIFIED = MediaStore.Audio.Playlists.DATE_MODIFIED;
    }

    public static interface AudioPlaylistMember extends AudioMedia {
        public static final String CONTENT_DIRECTORY = MediaStore.Audio.Playlists.Members.CONTENT_DIRECTORY;
        public static final String AUDIO_ID = MediaStore.Audio.Playlists.Members.AUDIO_ID;
        public static final String PLAYLIST_ID = MediaStore.Audio.Playlists.Members.PLAYLIST_ID;
        public static final String PLAY_ORDER = MediaStore.Audio.Playlists.Members.PLAY_ORDER;
    }

    public static interface AudioFavorite {
        public static final String _ID = BaseColumns._ID;
        public static final String MEDIA_ID = TableConsts.FAVORITE_ID;
        public static final String TYPE = TableConsts.FAVORITE_TYPE;
        public static final String POINT = TableConsts.FAVORITE_POINT;
    }

    public static interface AudioGenres extends Media {
        public static final String NAME = MediaStore.Audio.Genres.NAME;
        public static final String GENRES_KEY = "genres_key";
    }

    public static interface AudioGenresMember extends AudioMedia {
        public static final String CONTENT_DIRECTORY = MediaStore.Audio.Genres.Members.CONTENT_DIRECTORY;
        public static final String AUDIO_ID = MediaStore.Audio.Genres.Members.AUDIO_ID;
        public static final String GENRE_ID = MediaStore.Audio.Genres.Members.GENRE_ID;
        public static final String DEFAULT_SORT_ORDER = MediaStore.Audio.Genres.Members.DEFAULT_SORT_ORDER;
    }

    public static interface VideoMedia {
        public static final String _ID = BaseColumns._ID;
        public static final String MEDIA_KEY = "media_key";
        public static final String TITLE = MediaStore.Video.Media.TITLE;
        public static final String MIME_TYPE = MediaStore.Video.Media.MIME_TYPE;
        public static final String RESOLUTION = MediaStore.Video.Media.RESOLUTION;
        public static final String SIZE = MediaStore.Video.Media.SIZE;
        public static final String DURATION = MediaStore.Video.Media.DURATION;
        public static final String DATE_ADDED = MediaStore.Video.Media.DATE_ADDED;
        public static final String DATE_MODIFIED = MediaStore.Video.Media.DATE_MODIFIED;
        public static final String DATA = MediaStore.Video.Media.DATA;
    }

    public static interface FileMedia {
        public static final String _ID = BaseColumns._ID;
        public static final String TITLE = "title";
        public static final String TYPE = "type";
        public static final String DATE_MODIFIED = "date_modified";
        public static final String SIZE = "size";
        public static final String DATA = "data";
    }

    public static interface FileType {
        public static final String FOLDER = "folder";
        public static final String AUDIO = "audio";
        public static final String VIDEO = "video";
        public static final String ZIP = "zip";
        public static final String ZIPENTRY = "zipentry";
    }

    public static interface Auth {
        public static final String _ID = BaseColumns._ID;
        public static final String AUTH_KEY = "authkey";
        public static final String AUTH_URL = "authurl";
        public static final String PARAM1 = "param1";
        public static final String PARAM2 = "param2";
        public static final String PARAM3 = "param3";
        public static final String PARAM4 = "param4";
    }

    public static interface Session {
        public static final String _ID = BaseColumns._ID;
        public static final String SESSION_KEY = "session_key";
    }

    public static interface Url {
        public static final String _ID = BaseColumns._ID;
        public static final String PATH = "path";
    }
}
