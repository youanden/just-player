package jp.co.kayo.android.localplayer.consts;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.secret.Keys;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

public interface SystemConsts {
    public static final String DEFAULT_HOST = "";
    public static final String DEFAULT_USID = "";
    public static final String DEFAULT_PSWD = "";
    public static final int DEFAULT_INDEX = -1;
    public static final int DEFAULT_LIST_LIMIT = 50;
    public static final int DEFAULT_FETCH_LIMIT = 20;
    public static final String MAIN_ACITON_SHOWHOMW = "showhome";
    public static final String MAIN_ACITON_SHOWPLAYLIST = "showplaylist";
    public static final String MAIN_ACITON_SHOWSUBLIST = "showsublist";

    public static final String CONTENTSKEY_ALBUM = "Album.";
    public static final String CONTENTSKEY_ARTIST = "Artist.";
    public static final String CONTENTSKEY_ARTIST_TITLE = "ArtistTitle.";
    public static final String CONTENTSKEY_PLAYLIST = "Playlist.";
    public static final String CONTENTSKEY_MEDIA = "Media.";
    public static final String CONTENTSKEY_GENRES = "Genres.";
    public static final String CONTENTSKEY_PLAYALBUM = "PlayAlbum.";
    public static final String CONTENTSKEY_SEARCH = "Search.";
    public static final String CONTENTSKEY_FOLDER = "Folder.";

    public static final String PREF_CONTENTURI = "key_current_uri";
    public static final String PREF_REPEATFLG = "key_repeat_flg";
    public static final String PREF_SHUFFLEFLG = "key_shuffle_flg";
    public static final String PREF_REGISTDATE = "pref.registdate";
    public static final String PREF_FIRST_YEAR = "pref.first_year";
    public static final String PREF_LAST_YEAR = "pref.last_year";
    public static final String PREF_BRANKFILTER = "pref.brankfilter";
    public static final String PREF_LIMIT = "pref.limit";
    //public static final String PREF_FIRST_ANALYSIS = "pref.first.analysis";
    public static final String PREF_VISUALIZER_MODE = "pref.visualizer.mode";
    public static final String PREF_ROOTPATH = "pref.rootpath";
    public static final String PREF_RELAOD = "pref.reload";

    public static final boolean OrderHistoryDefaultValue = true;

    public static final String TAG_CACHE = "tag_cache";
    public static final String TAG_TITLE = "tag_title";

    public static final int TAG_ALBUM_GRID = R.id.mnu_select_tab;
    public static final int TAG_ALBUM_LIST = R.id.mnu_select_tab+1;
    public static final int TAG_ARTIST_LIST = R.id.mnu_select_tab+2;
    public static final int TAG_MEDIA = R.id.mnu_select_tab+3;
    public static final int TAG_PLAYLIST = R.id.mnu_select_tab+4;
    public static final int TAG_FOLDER = R.id.mnu_select_tab+5;
    public static final int TAG_GENRES = R.id.mnu_select_tab+6;
    public static final int TAG_VIDEO = R.id.mnu_select_tab+7;
    public static final int TAG_FAVORITE = R.id.mnu_select_tab+8;
    public static final int TAG_ALBUM_EXPAND = R.id.mnu_select_tab+9;
    
    public static final String TAG_SUBFRAGMENT = "tag_subfragment";

    public static final String VISIBLE_TAG_ALBUM = "visible.tag_album";
    public static final String VISIBLE_TAG_ARTIST = "visible.tag_artist";
    public static final String VISIBLE_TAG_MEDIA = "visible.tag_media";
    public static final String VISIBLE_TAG_GENRES = "visible.tag_genres";
    public static final String VISIBLE_TAG_FAVORITE = "visible.tag_favorite";
    public static final String VISIBLE_TAG_FOLDER = "visible.tag_folder";
    public static final String VISIBLE_TAG_VIDEO = "visible.tag_video";
    public static final String VISIBLE_TAG_PLAYLIST = "visible.tag_playlist";
    public static final String VISIBLE_TAG_ORDER = "visible.tag_order";

    public static final String VISIBLE_TAG_ALBUMART_VIEW = "visible.albumartview";
    public static final String VISIBLE_TAG_SONGS_VIEW = "visible.songsview";
    public static final String VISIBLE_TAG_VSUALIZER_VIEW = "visible.visualizerview";
    public static final String VISIBLE_TAG_WIKIPEDIA_VIEW = "visible.wikipediaview";
    public static final String VISIBLE_TAG_YOUTUBE_VIEW = "visible.youtubeview";
    public static final String VISIBLE_TAG_LYRICS_VIEW = "visible.lyricsview";

    public static final String TAG_CONTROL = "tag_control";
    public static final String TAG_CONTENTS = "tag_contents";
    public static final String TAG_RATING_DLG = "tag_rating_dlg";
    public static final String TAG_WIKIPEDIA_DLG = "tag_wikipedia_dlg";
    public static final String TAG_YOUTUBE_DLG = "tag_youtube_dlg";

    public static final int EVT_ADVIEW_VISIVLE = 1;
    public static final int EVT_ADVIEW_INVISIVLE = 2;
    public static final int EVT_PEOGRESS_VISIBLE = 3;
    public static final int EVT_PEOGRESS_START = 4;
    public static final int EVT_PEOGRESS_STOP = 5;
    public static final int EVT_PEOGRESS_MOVE = 6;
    public static final int EVT_PEOGRESS_SET = 7;
    public static final int EVT_PEOGRESS_TOAST = 8;
    public static final int EVT_LIST_SETCURSOR = 9;
    public static final int EVT_PEOGRESS_TITLE = 10;

    public static final int EVT_UPDATE_RATING = 100;
    public static final int EVT_NDEFPUSHCOMPLETE = 101;
    public static final int EVT_PROGRESS_SHOW = 102;
    public static final int EVT_PROGRESS_HIDE = 103;
    public static final int EVT_UPDATE_LISTVIEW = 104;
    public static final int EVT_UPDATE_ALBUMART = 105;

    public static final int EVT_SELECT_RATING = 1000;
    public static final int EVT_SELECT_PLAY = 1001;
    public static final int EVT_SELECT_ADD = 1002;
    public static final int EVT_SELECT_MORE = 1003;
    public static final int EVT_SELECT_ALBUMART = 1004;
    public static final int EVT_SELECT_CLEARCACHE = 1005;
    public static final int EVT_SELECT_DOWNLOAD = 1006;
    public static final int EVT_SELECT_LYRICS = 1007;
    public static final int EVT_SELECT_YOUTUBE = 1008;
    public static final int EVT_SELECT_LOVE = 1009;
    public static final int EVT_SELECT_BAN = 1010;
    public static final int EVT_SELECT_DEL = 1011;
    public static final int EVT_SELECT_OPENALBUM = 1012;
    public static final int EVT_SELECT_ARTIST = 1013;
    public static final int EVT_SELECT_EDIT = 1014;
    public static final int EVT_DSCHANFED = 1015;
    public static final int EVT_POPBACKSTACK = 1016;
    public static final int EVT_RELOAD = 1017;
    public static final int EVT_SELECT_PLAYBACK_CLEAR = 1018;
    public static final int EVT_SELECT_PLAYBACK_SAVE = 1019;
    public static final int EVT_SELECT_PLAYBACK_LOAD = 1020;
    public static final int EVT_SELECT_VIEW = 1021;

    final int ACT_NOTIFYDATASETCHANGED = 2001;
    final int ACT_ADDROW = 2002;
    final int ACT_SHOWPROGRESS = 2003;
    final int ACT_HIDEPROGRESS = 2004;

    public static final int FIT_IMAGESIZE = 64;//128;

    public static final int FLG_REPEAT_NO = 0;
    public static final int FLG_REPEAT_ONCE = 1;
    public static final int FLG_REPEAT_ALL = 2;
    public static final int FLG_SHUFFLE_NO = 0;
    public static final int FLG_SHUFFLE_YES = 1;

    public static final int REQ_OAUTH = 0;

    public static final String KEY_AUTO_VOLUMEAJUST = "key.autovolumeajust";
    public static final String KEY_FIRST_VOLUME = "key.firstVolume";
    public static final String KEY_DELETE_MUSIC = "key.cache.deleteMusic";
    public static final String KEY_DELETE_ALBUMART = "key.cache.deleteAlbumart";
    public static final String KEY_CACHE_SIZE = "key.cache.size";
    public static final String KEY_BEAM_SONGNUMBER = "key.beam_songnumber";
    public static final String KEY_BEAM_TYPE = "key.beam_type";

    public static final String PRM_ALBUMLIST = "albumlist";
    public static final String PRM_ARTISTLIST = "artistlist";
    public static final String PRM_GENRESLIST = "genreslist";
    public static final String PRM_PLAYLISTLIST = "playlistlist";

    public static final String KEY_LIST_SUBTYPE = "list.subtype";
    public static final String KEY_LIST_KEY = "list.positionkey";
    public static final String KEY_LIST_TITLE = "list.title";
    public static final String KEY_LIST_MAX = "list.max";
    public static final String KEY_PLAY_ALBUMKEY = "play.albumkey";
    public static final String KEY_PLAY_LOCK = "play.playlock";
    public static final String KEY_SOURCESTR = "key.sourcestring";
    public static final String KEY_SOURCEKEY = "key.sourcekey";
    public static final String KEY_EDITTYPE = "key.edittype";
    public static final String KEY_EDITKEY = "key.editkey";

    public static final int REQUEST_ALBUMART = 0;
    public static final int REQUEST_TAGEDIT = 1;
    public static final int REQUEST_CONFIG = 2;
    public static final int REQUEST_DSCHANGED = 3;


    public static final String LASTFM_LOVE = "fm.last.android.LOVE";
    public static final String LASTFM_BAN = "fm.last.android.BAN";
}
