package jp.co.kayo.android.localplayer.consts;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

public interface TableConsts {
    public static final String FAVORITE_TYPE_SONG = "song";
    public static final String FAVORITE_TYPE_ALBUM = "album";
    public static final String FAVORITE_TYPE_ARTIST = "artist";

    public static final String TBNAME_ALBUM = "album";
    public static final String ALBUM_DEL_FLG = "del_flg";
    public static final String ALBUM_INIT_FLG = "init_flg";

    public static final String TBNAME_ARTIST = "artist";
    public static final String ARTIST_DEL_FLG = "del_flg";
    public static final String ARTIST_INIT_FLG = "init_flg";

    public static final String TBNAME_PLAYLIST = "playlist";
    public static final String PLAYLIST_DEL_FLG = "del_flg";
    public static final String PLAYLIST_INIT_FLG = "init_flg";

    public static final String TBNAME_AUDIO = "audio";
    public static final String AUDIO_DEL_FLG = "del_flg";
    public static final String AUDIO_CACHE_FILE = "cache_file";

    public static final String TBNAME_PLAYLIST_AUDIO = "playlist_audio";

    public static final String TBNAME_VIDEO = "video";
    public static final String VIDEO_DEL_FLG = "del_flg";
    public static final String VIDEO_INIT_FLG = "init_flg";

    public static final String TBNAME_GENRES = "genres";
    public static final String GENRES_DEL_FLG = "del_flg";
    public static final String GENRES_INIT_FLG = "init_flg";

    public static final String TBNAME_GENRES_AUDIO = "genres_audio";

    public static final String TBNAME_FAVORITE = "favorite";
    public static final String FAVORITE_ID = "media_id";
    public static final String FAVORITE_TYPE = "type";
    public static final String FAVORITE_POINT = "point";

    public static final String TBNAME_PLAYBACK = "orderlist";
    public static final String PLAYBACK_URI = "uri";
    public static final String PLAYBACK_TITLE = "title";
    public static final String PLAYBACK_ARTIST = "artist";
    public static final String PLAYBACK_ALBUM = "album";
    public static final String PLAYBACK_MEDIA_ID = "media_id";
    public static final String PLAYBACK_ALBUM_KEY = "album_key";
    public static final String PLAYBACK_ARTIST_KEY = "artist_key";
    public static final String PLAYBACK_DURATION = "duration";
    public static final String PLAYBACK_DATA = "data";
    public static final String PLAYBACK_ORDER = "track";
    public static final String PLAYBACK_STATE = "state";

    public static final String TBNAME_DOWNLOAD = "download";
    public static final String DOWNLOAD_ID = "download_id";
    public static final String DOWNLOAD_MEDIA_ID = "media_id";
    public static final String DOWNLOAD_TITLE = "title";
    public static final String DOWNLOAD_LOCAL_URI = "local_uri";
    public static final String DOWNLOAD_REMOTE_URI = "remote_uri";
    public static final String DOWNLOAD_TYPE = "type";
    public static final String DOWNLOAD_STATUS = "status";

}
