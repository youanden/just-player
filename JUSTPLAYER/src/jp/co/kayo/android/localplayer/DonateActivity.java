package jp.co.kayo.android.localplayer;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 *      This program is free software; you can redistribute it and/or modify it under
 *      the terms of the GNU General Public License as published by the Free Software
 *      Foundation; either version 2 of the License, or (at your option) any later
 *      version.
 *      
 *      This program is distributed in the hope that it will be useful, but WITHOUT
 *      ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *      FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *      details.
 *      
 *      You should have received a copy of the GNU General Public License along with
 *      this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *      Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Locale;

import org.json.JSONObject;

import jp.co.kayo.android.localplayer.util.Logger;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Spinner;

public class DonateActivity extends Activity implements OnClickListener {

    private Spinner mSpinner;
    private Button mBtnBuy;

    private BillingService mBillingService;
    private String mDeveloperPayload = null;
    private MyDungeonsPurchaseObserver mDungeonsPurchaseObserver;
    private Handler mHandler;

    private String[] PRODUCT_ITEM = new String[] { "hideads1", "hideads2",
            "android.test.purchased" };

    private static final int DIALOG_CANNOT_CONNECT_ID = 1;
    private static final int DIALOG_BILLING_NOT_SUPPORTED_ID = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.donate_main);

        mBtnBuy = (Button) findViewById(R.id.btnBuy);
        mSpinner = (Spinner) findViewById(R.id.spinBilling);

        mBillingService = new BillingService();
        mHandler = new Handler();
        mDungeonsPurchaseObserver = new MyDungeonsPurchaseObserver(this,
                mBillingService, mHandler);
        mBillingService.setContext(this);

        ResponseHandler.register(mDungeonsPurchaseObserver);

        mBtnBuy.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        ResponseHandler.register(mDungeonsPurchaseObserver);
        updateBillingState();
    }

    /**
     * Called when this activity is no longer visible.
     */
    @Override
    protected void onStop() {
        super.onStop();
        ResponseHandler.unregister(mDungeonsPurchaseObserver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mBillingService.unbind();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        case DIALOG_CANNOT_CONNECT_ID:
            return createDialog(R.string.cannot_connect_title,
                    R.string.cannot_connect_message);
        case DIALOG_BILLING_NOT_SUPPORTED_ID:
            return createDialog(R.string.billing_not_supported_title,
                    R.string.billing_not_supported_message);
        default:
            return null;
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnBuy) {
            int pos = mSpinner.getSelectedItemPosition();
            String productId = PRODUCT_ITEM[pos];
            if (!mBillingService.requestPurchase(productId, mDeveloperPayload)) {
                showDialog(DIALOG_BILLING_NOT_SUPPORTED_ID);
            }
        }
    }

    private void updateBillingState() {
        PurchaseDatabase db = null;
        Cursor cur = null;
        try {
            db = new PurchaseDatabase(this);
            cur = db.queryAllPurchasedItems();
            if (cur != null && cur.moveToFirst()) {
                do {
                    String id = cur
                            .getString(cur
                                    .getColumnIndex(PurchaseDatabase.PURCHASED_PRODUCT_ID_COL));
                    int qt = cur
                            .getInt(cur
                                    .getColumnIndex(PurchaseDatabase.PURCHASED_QUANTITY_COL));
                    Logger.d(id + "=" + qt);
                } while (cur.moveToNext());
            }
        } finally {
            if (cur != null) {
                cur.close();
                cur = null;
            }
            if (db != null) {
                db.close();
            }
        }

    }

    private Dialog createDialog(int titleId, int messageId) {
        String helpUrl = replaceLanguageAndRegion(getString(R.string.help_url));
        if (Consts.DEBUG) {
            Logger.i(helpUrl);
        }
        final Uri helpUri = Uri.parse(helpUrl);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titleId)
                .setIcon(android.R.drawable.stat_sys_warning)
                .setMessage(messageId)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, null)
                .setNegativeButton(R.string.learn_more,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                Intent intent = new Intent(Intent.ACTION_VIEW,
                                        helpUri);
                                startActivity(intent);
                            }
                        });
        return builder.create();
    }

    private boolean checkSpporter(String mailid) {
        BufferedReader in = null;
        try {
            // URLクラスのインスタンスを生成
            URL url = new URL("http://justplayer-dev.appspot.com/check?id="
                    + URLEncoder.encode(mailid, "UTF-8"));

            // 接続します
            URLConnection con = url.openConnection();

            // 入力ストリームを取得
            in = new BufferedReader(new InputStreamReader(con.getInputStream(),
                    "UTF-8"));

            // 一行ずつ読み込みます
            StringBuffer json = new StringBuffer();
            String line = null;
            while ((line = in.readLine()) != null) {
                // 表示します
                json.append(line);
            }

            JSONObject jsonobj = new JSONObject(json.toString());
            boolean ret = jsonobj.getBoolean("result");
            return ret;
        } catch (Exception e) {
            Logger.e("check supporter", e);
        } finally {
            // 入力ストリームを閉じます
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
        }
        return false;
    }

    private String replaceLanguageAndRegion(String str) {
        // Substitute language and or region if present in string
        if (str.contains("%lang%") || str.contains("%region%")) {
            Locale locale = Locale.getDefault();
            str = str.replace("%lang%", locale.getLanguage().toLowerCase());
            str = str.replace("%region%", locale.getCountry().toLowerCase());
        }
        return str;
    }

    private class MyDungeonsPurchaseObserver extends DungeonsPurchaseObserver {
        public MyDungeonsPurchaseObserver(Activity activity,
                BillingService billingService, Handler handler) {
            super(activity, billingService, handler);
        }

        @Override
        public void onBillingSupported(boolean supported) {
            super.onBillingSupported(supported);
            if (supported) {
                restoreDatabase();
                mBtnBuy.setEnabled(true);
            } else {
                getActivit().showDialog(DIALOG_BILLING_NOT_SUPPORTED_ID);
            }
        }
    }
}
