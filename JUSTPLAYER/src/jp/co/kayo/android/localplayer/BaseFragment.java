package jp.co.kayo.android.localplayer;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 *      This program is free software; you can redistribute it and/or modify it under
 *      the terms of the GNU General Public License as published by the Free Software
 *      Foundation; either version 2 of the License, or (at your option) any later
 *      version.
 *      
 *      This program is distributed in the hope that it will be useful, but WITHOUT
 *      ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *      FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *      details.
 *      
 *      You should have received a copy of the GNU General Public License along with
 *      this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *      Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.core.NotifyHandler;
import jp.co.kayo.android.localplayer.dialog.LyricsDialog;
import jp.co.kayo.android.localplayer.dialog.WikipediaDialog;
import jp.co.kayo.android.localplayer.dialog.YoutubeSearchDialog;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;

public abstract class BaseFragment extends Fragment {

    protected NotifyHandler mHandler = new BaseNotifyHandler(this);
    
    private static class BaseNotifyHandler extends NotifyHandler{
        WeakReference<BaseFragment> ref;
        public BaseNotifyHandler(BaseFragment r) {
            ref = new WeakReference<BaseFragment>(r);
        }
        
        @Override
        public void handleMessage(Message msg) {
            BaseFragment f = ref.get();
            if(f!=null){
                switch (msg.what) {
                case SystemConsts.ACT_SHOWPROGRESS: {
                    f.showProgressBar();
                }
                    break;
                case SystemConsts.ACT_HIDEPROGRESS: {
                    f.hideProgressBar();
                }
                    break;
                case SystemConsts.ACT_NOTIFYDATASETCHANGED: {
                    f.datasetChanged();
                }
                    break;
                case SystemConsts.ACT_ADDROW: {
                    Object[] values = (Object[]) msg.obj;
                    f.addRow(values);
                }
                    break;
                case SystemConsts.EVT_SELECT_RATING:
                case SystemConsts.EVT_SELECT_PLAY:
                case SystemConsts.EVT_SELECT_ADD:
                case SystemConsts.EVT_SELECT_OPENALBUM:
                case SystemConsts.EVT_SELECT_MORE:
                case SystemConsts.EVT_SELECT_ALBUMART:
                case SystemConsts.EVT_SELECT_CLEARCACHE:
                case SystemConsts.EVT_SELECT_DOWNLOAD:
                case SystemConsts.EVT_SELECT_LYRICS:
                case SystemConsts.EVT_SELECT_YOUTUBE:
                case SystemConsts.EVT_SELECT_ARTIST:
                case SystemConsts.EVT_SELECT_LOVE:
                case SystemConsts.EVT_SELECT_BAN:
                case SystemConsts.EVT_SELECT_DEL:
                case SystemConsts.EVT_SELECT_EDIT: {
                    f.messageHandle(msg.what, (Integer) msg.obj);
                    f.hideMenu();
                }
                    break;
                default: {
                    super.handleMessage(msg);
                }
                }
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void showProgressBar() {
        if (getActivity() != null) {
            ((BaseActivity)getActivity()).showProgressBar();
        }
    }

    public void hideProgressBar() {
        if (getActivity() != null) {
            ((BaseActivity)getActivity()).hideProgressBar();
        }
    }

    abstract protected void hideMenu();

    protected void datasetChanged() {

    }

    protected void addRow(Object[] values) {

    }

    protected void messageHandle(int what, int selectedPosition) {

    }

    public void showInfoDialog(final String album, final String artist,
            final String title) {

        final ArrayList<String> list = new ArrayList<String>();
        if (artist != null)
            list.add(getString(R.string.txt_web_wikipedia));
        if (artist != null)
            list.add(getString(R.string.txt_web_youtube));
        if (artist != null && title != null)
            list.add(getString(R.string.txt_web_lyrics));

        if (list.size() > 1) {
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    getActivity());
            // alertDialogBuilder.setTitle("タイトル");
            alertDialogBuilder.setItems(
                    (String[]) list.toArray(new String[list.size()]),
                    new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            selectedItem(album, artist, title, list, which);
                        }
                    });
            alertDialogBuilder.create().show();
        } else if (list.size() > 0) {
            selectedItem(album, artist, title, list, 0);
        }
    }

    private void selectedItem(final String album, final String artist,
            final String title, ArrayList<String> list, int which) {
        if (list.get(which).equals(getString(R.string.txt_web_wikipedia))) {
            WikipediaDialog dlg = new WikipediaDialog();
            Bundle b = new Bundle();
            b.putString("artist", artist);
            dlg.setArguments(b);
            dlg.show(getFragmentManager(), SystemConsts.TAG_WIKIPEDIA_DLG);
        } else if (list.get(which).equals(getString(R.string.txt_web_youtube))) {
            YoutubeSearchDialog dlg = new YoutubeSearchDialog();
            Bundle b = new Bundle();
            if (title != null) {
                b.putString("title", title);
            }
            b.putString("artist", artist);
            dlg.setArguments(b);
            dlg.show(getFragmentManager(), SystemConsts.TAG_YOUTUBE_DLG);
        } else if (list.get(which).equals(getString(R.string.txt_web_lyrics))) {
            LyricsDialog dlg = new LyricsDialog();
            Bundle b = new Bundle();
            b.putString("title", title);
            b.putString("artist", artist);
            dlg.setArguments(b);
            dlg.show(getFragmentManager(), SystemConsts.TAG_WIKIPEDIA_DLG);
        }
    }
}
