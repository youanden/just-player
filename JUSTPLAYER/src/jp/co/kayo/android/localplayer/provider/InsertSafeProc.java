package jp.co.kayo.android.localplayer.provider;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.ArrayList;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public class InsertSafeProc implements Runnable {
    SQLiteDatabase db;
    String tbname;
    String[] projection;
    String key_field;
    String key;
    ArrayList<Object> values;

    public InsertSafeProc(SQLiteDatabase db, String tbname,
            String[] projection, String key_field, String key,
            ArrayList<Object> values) {

        this.db = db;
        this.tbname = tbname;
        this.projection = projection;
        this.key_field = key_field;
        this.key = key;
        this.values = values;
    }

    @Override
    public void run() {
        Cursor cur = db.query(tbname, new String[] { BaseColumns._ID, key },
                key_field + "=?", new String[] { key }, null, null, null);
        if (cur != null && cur.getCount() > 0) {
            /*
             * long id = cur.getLong(cur.getColumnIndex(BaseColumns._ID));
             * ContentValues values = new ContentValues(); db.update(tbname,
             * values, BaseColumns._ID +"=?", new String[]{Long.toString(id)});
             */
        } else {
            ContentValues val = new ContentValues(projection.length);
            for (int i = 0; i < projection.length; i++) {
                Object o = values.get(i);
                if (projection.equals(BaseColumns._ID)) {

                } else if (o instanceof String) {
                    val.put(projection[i], (String) o);
                } else if (o instanceof Integer) {
                    val.put(projection[i], (Integer) o);
                } else if (o instanceof Long) {
                    val.put(projection[i], (Long) o);
                } else if (o instanceof Boolean) {
                    val.put(projection[i], (Boolean) o);
                }
            }
            db.insert(tbname, null, val);
        }
    }

}
