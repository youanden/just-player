package jp.co.kayo.android.localplayer.provider;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.File;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import jp.co.kayo.android.localplayer.BaseActivity;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.appwidget.AppWidgetHelper;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioAlbum;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioArtist;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.consts.MediaConsts.FileMedia;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.service.IMediaPlayerService;
import jp.co.kayo.android.localplayer.service.StreamCacherServer;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.ViewCache;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.RemoteException;
import android.preference.PreferenceManager;

public class ContentsUtils {
    public static class AlbumInfo {
        public String album;
        public String album_art;
        public String album_key;
    }
    
    private static final String DS = "jp.co.kayo.android.localplayer.ds.";

    public static List<ProviderInfo> getDataSouce(Context context) {
        ArrayList<ProviderInfo> ret = new ArrayList<ProviderInfo>();
        PackageManager pm = context.getPackageManager();
        List<ProviderInfo> list = pm.queryContentProviders(null, 0, 0);

        for (ProviderInfo inf : list) {
            if (inf.authority.startsWith(DS)) {
                ret.add(inf);
            }
        }

        return ret;
    }
    
    public static void reloadAlbumArt(BaseActivity activity, String album_key){
        if (album_key != null) {
            Hashtable<String, String> tbl = ContentsUtils
                    .getAlbum(activity, new String[] {
                            AudioAlbum.ALBUM, AudioAlbum.ALBUM_KEY,
                            AudioAlbum.ARTIST
                    }, album_key);
            if (tbl != null) {
                ViewCache viewcache = (ViewCache) activity.getSupportFragmentManager()
                        .findFragmentByTag(SystemConsts.TAG_CACHE);
                if (viewcache != null) {
                    viewcache.releaseImage(
                            tbl.get(AudioAlbum.ALBUM),
                            tbl.get(AudioAlbum.ARTIST));
                }
            }
        }
    }
    
    public static void selectSource(final Context context, final SharedPreferences pref, final Handler handler) {
        final String contenturi = pref.getString(SystemConsts.PREF_CONTENTURI,
                DeviceContentProvider.MEDIA_AUTHORITY);

        final List<ProviderInfo> dslist = ContentsUtils.getDataSouce(context);
        PackageManager pm = context.getPackageManager();
        final ArrayList<String> items = new ArrayList<String>();
        if (contenturi.equals(DeviceContentProvider.MEDIA_AUTHORITY)) {
            items.add(context.getString(R.string.lb_sdcard) + " *");
        } else {
            items.add(context.getString(R.string.lb_sdcard));
        }
        for (ProviderInfo inf : dslist) {
            if (inf.authority.equals(contenturi)) {
                items.add(inf.loadLabel(pm) + " *");
            } else {
                items.add(inf.loadLabel(pm).toString());
            }
        }
        items.add(context.getString(R.string.lb_conf_add_ds_title));
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.lb_srcdialog));
        builder.setItems(items.toArray(new String[items.size()]),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        String selecturl = null;
                        String item_name = items.get(item);
                        if (item_name.indexOf(context.getString(R.string.lb_sdcard)) != -1) {
                            // SDCard
                            selecturl = DeviceContentProvider.MEDIA_AUTHORITY;
                        } else if (item_name
                                .equals(context.getString(R.string.lb_conf_add_ds_title))) {
                            Uri uri = Uri
                                    .parse("market://search?q=jp.co.kayo.android.localplayer.ds");
                            Intent it = new Intent(Intent.ACTION_VIEW, uri);
                            context.startActivity(it);
                        } else {
                            // Ampcaheまたはその他
                            selecturl = dslist.get(item - 1).authority;

                            boolean checkSetting = false;
                            if (selecturl.equals(contenturi)) {
                                // 同じURLなら設定画面を呼び出す
                                checkSetting = true;
                            } else {
                                // PINGを飛ばしてだめなら設定画面を呼び出す
                                Cursor cursor = null;
                                try {
                                    Uri uri = Uri.parse("content://"
                                            + selecturl + "/config/ping");
                                    cursor = context.getContentResolver().query(uri,
                                            null, null, null, null);
                                    if (cursor == null) {
                                        checkSetting = true;
                                    }
                                } finally {
                                    if (cursor != null) {
                                        cursor.close();
                                    }
                                }
                            }
                            if (checkSetting) {
                                Intent it = new Intent();
                                it.setClassName(
                                        dslist.get(item - 1).packageName,
                                        dslist.get(item - 1).packageName
                                                + ".MainActivity");
                                context.startActivity(it);
                            }
                        }

                        if (selecturl != null && !selecturl.equals(contenturi)) {
                            Editor editor = pref.edit();
                            editor.putString(SystemConsts.PREF_CONTENTURI,
                                    selecturl);
                            editor.commit();

                            handler.sendEmptyMessage(SystemConsts.EVT_DSCHANFED);
                        }
                    }
                });
        AlertDialog dlg = builder.create();
        dlg.show();
    }

    public static AlbumInfo getAlbumArt(Context context, String artist,
            Hashtable<String, AlbumInfo> tbl1, Hashtable<String, AlbumInfo> tbl2) {
        if (artist != null) {
            AlbumInfo inf = tbl1.get(artist);
            if (inf == null) {
                Cursor cur = null;
                Cursor cur2 = null;
                try {
                    inf = new AlbumInfo();
                    inf.album = "";

                    cur = context.getContentResolver().query(
                            MediaConsts.ALBUM_CONTENT_URI,
                            new String[] { AudioAlbum.ALBUM,
                                    AudioAlbum.ALBUM_ART },
                            AudioAlbum.ARTIST + " = ?",
                            new String[] { artist }, null);
                    if (cur != null && cur.moveToFirst()) {
                        do {
                            String album = cur
                                    .getString(cur
                                            .getColumnIndex(MediaConsts.AudioAlbum.ALBUM));
                            String album_art = cur
                                    .getString(cur
                                            .getColumnIndex(MediaConsts.AudioAlbum.ALBUM_ART));
                            if (album_art != null
                                    && album_art.trim().length() > 0) {
                                inf.album = album;
                                inf.album_art = album_art;
                                break;
                            }
                        } while (cur.moveToNext());
                    } else if (!isNoCacheAction(context)) {
                        cur2 = context.getContentResolver().query(
                                MediaConsts.MEDIA_CONTENT_URI,
                                new String[] { AudioMedia.ALBUM_KEY },
                                AudioMedia.ARTIST + " = ?",
                                new String[] { artist }, null);
                        if (cur2 != null && cur2.moveToFirst()) {
                            do {
                                String album_key = cur2
                                        .getString(cur2
                                                .getColumnIndex(MediaConsts.AudioMedia.ALBUM_KEY));
                                if (album_key != null
                                        && album_key.trim().length() > 0) {
                                    inf = getAlbumArtByAlbum(context,
                                            album_key, tbl2);
                                    break;
                                }
                            } while (cur2.moveToNext());
                        }
                    }
                } finally {
                    if (cur != null) {
                        cur.close();
                    }
                    if (cur2 != null) {
                        cur2.close();
                    }
                }
                if (inf != null) {
                    tbl1.put(artist, inf);
                }
            }
            return inf;
        } else {
            return null;
        }
    }

    public static AlbumInfo getAlbumArtByAlbum(Context context,
            String album_key, Hashtable<String, AlbumInfo> tbl) {
        AlbumInfo inf = tbl.get(album_key);
        if (inf == null) {
            Cursor cur = null;
            try {

                cur = context.getContentResolver()
                        .query(MediaConsts.ALBUM_CONTENT_URI,
                                new String[] { AudioAlbum.ALBUM,
                                        AudioAlbum.ALBUM_ART },
                                AudioAlbum.ALBUM_KEY + " = ?",
                                new String[] { album_key }, null);
                if (cur != null && cur.moveToFirst()) {
                    String album = cur.getString(cur
                            .getColumnIndex(MediaConsts.AudioAlbum.ALBUM));
                    String album_art = cur.getString(cur
                            .getColumnIndex(MediaConsts.AudioAlbum.ALBUM_ART));
                    if (album_art != null && album_art.trim().length() > 0) {
                        inf = new AlbumInfo();
                        inf.album = album;
                        inf.album_art = album_art;
                    }
                }
            } finally {
                if (cur != null) {
                    cur.close();
                }
                if (inf != null) {
                    tbl.put(album_key, inf);
                }
            }
        }
        return inf;
    }

    public static void addOrderMediaList(IMediaPlayerService binder, long id, String data) {
        try {
            binder.addMedia(id, data);
            binder.commit();
        } catch (RemoteException e) {
        }
    }

    public static Cursor getFileItemCursor(Context context, String data, String type){
        return context.getContentResolver()
                .query(ContentUris
                        .withAppendedId(
                                MediaConsts.FOLDER_CONTENT_URI,
                                0),
                        null,
                        MediaConsts.FileMedia.DATA
                                + " = ? AND "
                                + FileMedia.TYPE
                                + " = ?",
                        new String[] { data,
                                type }, null);
    }
    
    public static long addOrderFolder(IMediaPlayerService binder,
            Context context, String path, String sortOrder) {
        long ret = -1;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(
                    MediaConsts.FOLDER_CONTENT_URI, null,
                    FileMedia.DATA + " = ?",
                    new String[] { path }, sortOrder);
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    String data = cursor.getString(cursor
                            .getColumnIndex(FileMedia.DATA));
                    String type = cursor.getString(cursor
                            .getColumnIndex(FileMedia.TYPE));
                    
                    Cursor file_cur = getFileItemCursor(context, data, type);
                    if(file_cur!=null){
                        if(file_cur.moveToFirst()){
                            long media_id = file_cur
                                    .getLong(file_cur
                                            .getColumnIndex(AudioMedia._ID));
                            String media_data = file_cur
                                    .getString(file_cur
                                            .getColumnIndex(AudioMedia.DATA));
                            if (media_id > 0) {
                                binder.addMedia(media_id, media_data);
                            } else {
                                String title = file_cur
                                        .getString(file_cur
                                                .getColumnIndex(AudioMedia.TITLE));
                                String album = file_cur
                                        .getString(file_cur
                                                .getColumnIndex(AudioMedia.ALBUM));
                                String artist = file_cur
                                        .getString(file_cur
                                                .getColumnIndex(AudioMedia.ARTIST));
                                if (title == null) {
                                    title = context.getString(R.string.txt_unknown_title);
                                }
                                if (album == null) {
                                    album = context.getString(R.string.txt_unknown_album);
                                }
                                if (artist == null) {
                                    artist = context.getString(R.string.txt_unknown_artist);
                                }
                                binder.addMediaD(media_id, 0,
                                        title, album, artist,
                                        data);
                            }
                        }
                        file_cur.close();
                    }
                } while (cursor.moveToNext());
                binder.commit();
            }
        } catch (RemoteException e) {
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return ret;
    }

    public static boolean playMediaReplace(String contentkey,
            IMediaPlayerService binder, Cursor cursor, String cname1,
            String cname2, String cname3, int position) {
        try {
            if (!binder.setContentsKey(contentkey)) {
                // 既に設定中のリストだった
                int pos = binder.getPosition();
                if (pos != position) {// 同じ位置をクリックした
                    binder.lockUpdateToPlay();
                    try {
                        binder.stop();
                        binder.setPosition(position);
                    } finally {
                        binder.play();
                    }
                    return true;
                }
            } else {
                binder.lockUpdateToPlay();
                try {
                    binder.clear();
                    cursor.moveToFirst();
                    do {
                        long id = cursor.getLong(cursor.getColumnIndex(cname1));
                        String media_data = cursor
                                .getString(cursor
                                        .getColumnIndex(cname3));
                        binder.addMedia(id, media_data);
                    } while (cursor.moveToNext());
                    binder.setPosition(position);
                    cursor.moveToPosition(position);
                } finally {
                    binder.play();
                }
                return true;
            }
        } catch (RemoteException e) {
        }
        return false;
    }

    /***
     * 曲を即時再生でかつ履歴は残す
     * 
     * @param binder
     * @param id
     * @param duration
     * @param data
     */
    public static void playMedia(IMediaPlayerService binder, long id, String data) {
        try {
            String cntkey = SystemConsts.CONTENTSKEY_MEDIA + id;
            binder.setContentsKey(cntkey);
            binder.lockUpdateToPlay();
            try {
                binder.clearcut();
                binder.addMedia(id, data);
            } finally {
                binder.play();
            }
        } catch (RemoteException e) {
        }
    }

    public static long playGenres(IMediaPlayerService binder,
            BaseActivity activity, String genres_id) {
        long ret = -1;
        Cursor cursor = null;
        try {
            String cntkey = SystemConsts.CONTENTSKEY_GENRES + genres_id;
            cursor = activity.getContentResolver().query(
                    MediaConsts.GENRES_MEMBER_CONTENT_URI, null,
                    MediaConsts.AudioGenresMember.GENRE_ID + " = ?",
                    new String[] { genres_id },
                    MediaConsts.AudioGenresMember.DEFAULT_SORT_ORDER);
            if (cursor != null && cursor.moveToFirst()) {
                binder.setContentsKey(cntkey);
                binder.lockUpdateToPlay();
                try {
                    activity.showProgressBar();
                    binder.clearcut();
                    String audio_id_col = MediaConsts.AudioGenresMember._ID;
                    int audio_id_index = cursor
                            .getColumnIndex(MediaConsts.AudioGenresMember.AUDIO_ID);
                    if (audio_id_index != -1) {
                        audio_id_col = MediaConsts.AudioGenresMember.AUDIO_ID;
                    }

                    do {
                        long id = cursor.getLong(cursor
                                .getColumnIndex(audio_id_col));
                        String media_data = cursor
                                .getString(cursor
                                        .getColumnIndex(MediaConsts.AudioGenresMember.DATA));
                        if (ret == -1) {
                            ret = id;
                        }
                        binder.addMedia(id, media_data);
                    } while (cursor.moveToNext());
                } finally {
                    binder.play();
                    activity.hideProgressBar();
                }
            }
        } catch (RemoteException e) {
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return ret;
    }


    public static long playArtist(IMediaPlayerService binder,
            BaseActivity activity, String artist_key) {
        long ret = -1;
        Cursor cursor = null;
        try {
            String cntkey = SystemConsts.CONTENTSKEY_ARTIST + artist_key;
            cursor = activity.getContentResolver().query(
                    MediaConsts.MEDIA_CONTENT_URI,
                    new String[]{MediaConsts.AudioMedia._ID},
                    MediaConsts.AudioMedia.ARTIST_KEY + " = ?",
                    new String[] { artist_key },
                    MediaConsts.AudioMedia.ALBUM + ","
                            + MediaConsts.AudioMedia.TRACK);
            if (cursor != null && cursor.moveToFirst()) {
                binder.setContentsKey(cntkey);
                binder.lockUpdateToPlay();
                try {
                    activity.showProgressBar();
                    binder.clearcut();
                    do {
                        long id = cursor.getLong(cursor
                                .getColumnIndex(MediaConsts.AudioMedia._ID));
                        String media_data = cursor
                                .getString(cursor
                                        .getColumnIndex(MediaConsts.AudioMedia.DATA));
                        if (ret == -1) {
                            ret = id;
                        }
                        binder.addMedia(id, media_data);
                    } while (cursor.moveToNext());
                } finally {
                    activity.hideProgressBar();
                    binder.play();
                }
            }
        } catch (RemoteException e) {
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return ret;
    }

    public static long playArtistTitle(IMediaPlayerService binder,
            BaseActivity activity, String artist) {
        long ret = -1;
        Cursor cursor = null;
        try {
            String cntkey = SystemConsts.CONTENTSKEY_ARTIST_TITLE + artist;

            cursor = activity.getContentResolver().query(
                    MediaConsts.MEDIA_CONTENT_URI,
                    new String[]{MediaConsts.AudioMedia._ID},
                    MediaConsts.AudioMedia.ARTIST + " like '%' || ? || '%'",
                    new String[] { artist },
                    MediaConsts.AudioMedia.ALBUM + ","
                            + MediaConsts.AudioMedia.TRACK);
            if (cursor != null && cursor.moveToFirst()) {
                binder.setContentsKey(cntkey);
                binder.lockUpdateToPlay();
                try {
                    activity.showProgressBar();
                    binder.clearcut();
                    do {
                        long id = cursor.getLong(cursor
                                .getColumnIndex(MediaConsts.AudioMedia._ID));
                        String media_data = cursor
                                .getString(cursor
                                        .getColumnIndex(MediaConsts.AudioMedia.DATA));
                        if (ret == -1) {
                            ret = id;
                        }
                        binder.addMedia(id, media_data);
                    } while (cursor.moveToNext());
                } finally {
                    binder.play();
                    activity.hideProgressBar();
                }
            }
        } catch (RemoteException e) {
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return ret;
    }

    public static boolean isSDCard(Context context) {
        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(context);
        return isSDCard(pref);
    }

    public static boolean isSDCard(SharedPreferences pref) {
        String contenturi = pref.getString(SystemConsts.PREF_CONTENTURI,
                DeviceContentProvider.MEDIA_AUTHORITY);
        return contenturi.equals(DeviceContentProvider.MEDIA_AUTHORITY);
    }

    public static boolean isDropbox(SharedPreferences pref) {
        String contenturi = pref.getString(SystemConsts.PREF_CONTENTURI,
                DeviceContentProvider.MEDIA_AUTHORITY);
        return contenturi.equals(MediaConsts.AUTHORITY + ".ds.dropbox");
    }

    public static boolean isNoCacheAction(Context context) {
        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(context);
        return isNoCacheAction(pref);
    }

    public static boolean isNoCacheAction(SharedPreferences pref) {
        String contenturi = pref.getString(SystemConsts.PREF_CONTENTURI,
                DeviceContentProvider.MEDIA_AUTHORITY);
        return contenturi.equals(MediaConsts.AUTHORITY + ".ds.ampache");
    }

    public static boolean canDownload(SharedPreferences pref) {
        if (Build.VERSION.SDK_INT > 8) {
            if (!isSDCard(pref)) {
                return true;
            }
        }
        return false;
    }

    public static Hashtable<String, String> getMedia(Context contxt,
            String[] projection, long id) {
        return getMedia(contxt, projection, id, new Hashtable<String, String>());
    }
    
    public static Hashtable<String, String> getMedia(Context contxt,
            String[] projection, long id, Hashtable<String, String> tbl) {
        Cursor cur = null;
        try {
            if (id > 0) {
                String[] prj = projection;
                if (prj == null) {
                    prj = new String[] { AudioMedia.ALBUM,
                            AudioMedia.ALBUM_KEY, AudioMedia.ARTIST,
                            AudioMedia.TITLE, AudioMedia.DURATION };
                }
                cur = contxt.getContentResolver().query(
                        ContentUris.withAppendedId(
                                MediaConsts.MEDIA_CONTENT_URI, id), prj, null,
                        null, null);
                if (cur != null && cur.moveToFirst()) {
                    for (int i = 0; i < prj.length; i++) {
                        String value = cur
                                .getString(cur.getColumnIndex(prj[i]));
                        if (value != null) {
                            tbl.put(prj[i], value);
                        }
                    }
                }
            }

            return tbl;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }
    
    public static Hashtable<String, String> getMedia(Context contxt,
            String[] projection, String media_key) {
        return getMedia(contxt, projection, media_key, new Hashtable<String, String>());
    }

    public static Hashtable<String, String> getMedia(Context contxt,
            String[] projection, String media_key, Hashtable<String, String> tbl) {
        Cursor cur = null;
        try {
            if (media_key != null) {
                String[] prj = projection;
                if (prj == null) {
                    prj = new String[] { AudioMedia.ALBUM,
                            AudioMedia.ALBUM_KEY, AudioMedia.ARTIST,
                            AudioMedia.TITLE, AudioMedia.DURATION };
                }
                cur = contxt.getContentResolver().query(
                        MediaConsts.MEDIA_CONTENT_URI, prj,
                        MediaConsts.AudioMedia.MEDIA_KEY + " = ?",
                        new String[] { media_key }, null);
                if (cur != null && cur.moveToFirst()) {
                    for (int i = 0; i < prj.length; i++) {
                        String value = cur
                                .getString(cur.getColumnIndex(prj[i]));
                        if (value != null) {
                            tbl.put(prj[i], value);
                        }
                    }
                }
            }

            return tbl;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    public static Cursor getMediaFile(Context contxt, long id, String path) {
        return contxt.getContentResolver().query(
                ContentUris.withAppendedId(MediaConsts.FOLDER_CONTENT_URI, id),
                null, MediaConsts.FileMedia.DATA + " = ?",
                new String[] { path }, null);
    }

    public static Hashtable<String, String> getAlbum(Context contxt,
            String[] projection, String album_key) {
        Hashtable<String, String> tbl = new Hashtable<String, String>();
        return getAlbum(contxt, projection, album_key, tbl);
    }
    
    public static Hashtable<String, String> getAlbum(Context contxt,
            String[] projection, String album_key, Hashtable<String, String> tbl) {
        Cursor cur = null;
        try {
            if (album_key != null) {
                String[] prj = projection;
                if (prj == null) {
                    prj = new String[] { AudioAlbum.ALBUM,
                            AudioAlbum.ALBUM_KEY, AudioAlbum.ARTIST };
                }
                cur = contxt.getContentResolver().query(
                        MediaConsts.ALBUM_CONTENT_URI, prj,
                        AudioAlbum.ALBUM_KEY + "= ?",
                        new String[] { album_key }, null);
                if (cur != null && cur.moveToFirst()) {
                    for (int i = 0; i < prj.length; i++) {
                        String value = cur
                                .getString(cur.getColumnIndex(prj[i]));
                        if (value != null) {
                            tbl.put(prj[i], value);
                        }
                    }
                }
            }

            return tbl;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    public static Hashtable<String, String> getArtist(Context contxt,
            String[] projection, String artist_key) {
        Hashtable<String, String> tbl = new Hashtable<String, String>();
        Cursor cur = null;
        try {
            if (artist_key != null) {
                String[] prj = projection;
                if (prj == null) {
                    prj = new String[] { AudioArtist.ARTIST,
                            AudioArtist.ARTIST_KEY };
                }
                cur = contxt.getContentResolver().query(
                        MediaConsts.ARTIST_CONTENT_URI, prj,
                        AudioArtist.ARTIST_KEY + "= ?",
                        new String[] { artist_key }, null);
                if (cur != null && cur.moveToFirst()) {
                    for (int i = 0; i < prj.length; i++) {
                        String value = cur
                                .getString(cur.getColumnIndex(prj[i]));
                        if (value != null) {
                            tbl.put(prj[i], value);
                        }
                    }
                }
            }

            return tbl;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    public static String getAlbumKey(Context contxt, String album) {
        Cursor cur = null;
        try {
            cur = contxt.getContentResolver().query(
                    MediaConsts.ALBUM_CONTENT_URI,
                    new String[] { AudioAlbum.ALBUM_KEY },
                    AudioAlbum.ALBUM + " = ?", new String[] { album }, null);
            if (cur != null && cur.moveToFirst()) {
                String value = cur.getString(cur
                        .getColumnIndex(AudioAlbum.ALBUM_KEY));
                return value;
            }

            return null;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    public static Hashtable<String, String> getMediaInfoForFavorite(
            Context context, long media_id) {
        Hashtable<String, String> tbl = new Hashtable<String, String>();
        Cursor cur = null;
        try {
            cur = context.getContentResolver().query(
                    ContentUris.withAppendedId(MediaConsts.MEDIA_CONTENT_URI,
                            media_id),
                    new String[] { AudioMedia.ALBUM, AudioMedia.ARTIST,
                            AudioMedia.TITLE, AudioMedia.DURATION }, null,
                    null, null);
            if (cur != null && cur.moveToFirst()) {
                tbl.put(AudioMedia.ALBUM,
                        cur.getString(cur.getColumnIndex(AudioMedia.ALBUM)));
                tbl.put(AudioMedia.ARTIST,
                        cur.getString(cur.getColumnIndex(AudioMedia.ARTIST)));
                tbl.put(AudioMedia.DURATION,
                        cur.getString(cur.getColumnIndex(AudioMedia.DURATION)));
                tbl.put(AudioMedia.TITLE,
                        cur.getString(cur.getColumnIndex(AudioMedia.TITLE)));
                return tbl;
            }
            return null;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    public static String getArtistKey(Context contxt, String artist) {
        Cursor cur = null;
        try {
            cur = contxt.getContentResolver().query(
                    MediaConsts.ARTIST_CONTENT_URI,
                    new String[] { AudioArtist.ARTIST_KEY },
                    AudioArtist.ARTIST + " = ?", new String[] { artist }, null);
            if (cur != null && cur.moveToFirst()) {
                String value = cur.getString(cur
                        .getColumnIndex(AudioArtist.ARTIST_KEY));
                return value;
            }

            return null;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    public static void lastfmLove(Context context, String title, String artist,
            String album, String duration) {
        try {
            Intent i = new Intent(SystemConsts.LASTFM_LOVE);
            i.putExtra("artist", artist);
            i.putExtra("track", title);
            if (album != null)
                i.putExtra("album", album);
            i.putExtra("duration", duration);
            context.sendBroadcast(i);
        } catch (Exception e) {
            Logger.e("lastfm love", e);
        }
    }

    public static void lastfmBan(Context context, String title, String artist,
            String album, String duration) {
        try {
            Intent i = new Intent(SystemConsts.LASTFM_BAN);
            i.putExtra("artist", artist);
            i.putExtra("track", title);
            if (album != null)
                i.putExtra("album", album);
            i.putExtra("duration", duration);
            context.sendBroadcast(i);
        } catch (Exception e) {
            Logger.e("lastfm love", e);
        }
    }

    public static ArrayList<String> getAlbumsArtist(Context context,
            SharedPreferences pref, String artist, String artist_key,
            int nalbums) {
        Cursor cur = null;
        Cursor cur2 = null;
        ArrayList<String> albumkes = new ArrayList<String>();
        ArrayList<String> tbl = new ArrayList<String>();
        try {
            String where;
            String[] whereArgs;
            if (ContentsUtils.isNoCacheAction(pref)) {
                where = AudioArtist.ARTIST_KEY + " = ?";
                whereArgs = new String[] { artist_key };
            } else {
                where = AudioAlbum.ARTIST + " = ?";
                whereArgs = new String[] { artist };
            }

            cur = context.getContentResolver().query(
                    MediaConsts.ALBUM_CONTENT_URI,
                    new String[] { AudioMedia.ALBUM_KEY }, where, whereArgs,
                    null);
            if (cur != null && cur.moveToFirst()) {
                do {
                    String album_key = cur.getString(cur
                            .getColumnIndex(AudioAlbum.ALBUM_KEY));
                    albumkes.add(album_key);
                } while (cur.moveToNext());
            }

            // コンピレーションアルバムの可能性、曲から検索し該当するアルバムを追加する
            if (nalbums > 0 && albumkes.size() < nalbums) {
                cur2 = context.getContentResolver().query(
                        MediaConsts.MEDIA_CONTENT_URI,
                        new String[] { AudioMedia.ALBUM_KEY },
                        AudioArtist.ARTIST_KEY + " = ?",
                        new String[] { artist_key }, null);
                if (cur2 != null && cur2.moveToFirst()) {
                    do {
                        String album_key = cur2.getString(cur2
                                .getColumnIndex(AudioMedia.ALBUM_KEY));
                        if (tbl.indexOf(album_key) == -1) {
                            albumkes.add(album_key);
                        }
                    } while (cur2.moveToNext());
                }
            }

            return albumkes;
        } finally {
            if (cur != null) {
                cur.close();
            }
            if (cur2 != null) {
                cur2.close();
            }
        }
    }

    public static void clearAlbumCache(Context context, String albumKey) {
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(
                    MediaConsts.MEDIA_CONTENT_URI,
                    new String[] { MediaConsts.AudioMedia.DATA },
                    MediaConsts.AudioMedia.ALBUM_KEY + " = ?",
                    new String[] { albumKey }, MediaConsts.AudioMedia.TRACK);
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    String data = cursor.getString(cursor
                            .getColumnIndex(MediaConsts.AudioMedia.DATA));
                    File file = StreamCacherServer.getCacheFile(context, data);
                    if (file != null && file.exists()) {
                        file.delete();
                    }
                } while (cursor.moveToNext());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

    }

    public static void clearMediaCache(Context context, String data) {
        File file = StreamCacherServer.getCacheFile(context, data);
        if (file != null && file.exists()) {
            file.delete();
        }
    }
}
