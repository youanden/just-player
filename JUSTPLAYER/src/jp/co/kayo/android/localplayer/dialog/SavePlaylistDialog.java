package jp.co.kayo.android.localplayer.dialog;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioPlaylist;
import jp.co.kayo.android.localplayer.core.ContentManager;
import jp.co.kayo.android.localplayer.core.ServiceBinderHolder;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.service.IMediaPlayerService;
import jp.co.kayo.android.localplayer.util.ViewCache;
import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.content.ContentUris;
import android.content.ContentValues;
import android.support.v4.content.CursorLoader;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.support.v4.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;

public class SavePlaylistDialog extends DialogFragment implements
        LoaderCallbacks<Cursor>, OnItemClickListener {
    private SharedPreferences mPref;
    private ViewCache mViewcache;
    private PlaylistListAdapter mAdapter;
    long[] mPlaylist;
    EditText edit = null;
    private ListView mListView;
    DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.saveplaylistdialog, null, false);
        mListView = (ListView) view.findViewById(android.R.id.list);
        mListView.setOnItemClickListener(this);

        edit = (EditText) view.findViewById(R.id.EditText01);

        StringBuilder buf = new StringBuilder();
        Calendar cal = Calendar.getInstance();
        String date = dfm.format(cal.getTime());
        buf.append(getString(R.string.lb_newplaylist)).append(date);
        edit.setText(buf.toString());

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.lb_save_playback));
        builder.setPositiveButton(getString(R.string.lb_ok),
                new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        saveNew(edit.getText().toString());
                    }
                });
        builder.setNegativeButton(getString(R.string.lb_cancel), null);
        builder.setView(view);

        return builder.create();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mViewcache = (ViewCache) getFragmentManager().findFragmentByTag(
                SystemConsts.TAG_CACHE);
        Bundle args = getArguments();
        if (args != null) {
            mPlaylist = args.getLongArray("playlist");
        }
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getLoaderManager().initLoader(R.layout.saveplaylistdialog, null, this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getLoaderManager().destroyLoader(R.layout.saveplaylistdialog);
    }

    private void saveNew(String name) {
        ContentValues newplaylist = new ContentValues();
        long playlist_key = System.nanoTime();//外部でプレイリストを管理するシステムと整合をあわせるためダミー用のIDを作っておく
        newplaylist.put(AudioPlaylist.PLAYLIST_KEY, Long.toString(playlist_key));
        newplaylist.put(MediaStore.Audio.Playlists.NAME, name);
        // AllInsert
        Uri uri = getActivity().getContentResolver().insert(
                MediaConsts.PLAYLIST_CONTENT_URI, newplaylist);
        for (int i = 0; i < mPlaylist.length; i++) {
            long id = mPlaylist[i];
            ContentValues values = new ContentValues();
            values.put(MediaStore.Audio.Playlists.Members.AUDIO_ID, id);
            values.put(MediaStore.Audio.Playlists.Members.PLAYLIST_ID,
                    ContentUris.parseId(uri));
            values.put(MediaStore.Audio.Playlists.Members.PLAY_ORDER, i);
            getActivity().getContentResolver().insert(uri, values);
        }
        ContentManager mgr = (ContentManager)getFragmentManager().findFragmentById(R.id.fragment_main);
        if(mgr!=null){
            mgr.reload();
        }
    }

    private void replaceList(String name, long playlist_id) {
        Uri playlisturi = ContentUris.withAppendedId(MediaConsts.PLAYLIST_CONTENT_URI, playlist_id);
        // AllDelete
        getActivity().getContentResolver().delete(
                playlisturi,
                null,
                null);
        // AllInsert
        for (int i = 0; i < mPlaylist.length; i++) {
            long id = mPlaylist[i];
            ContentValues values = new ContentValues();
            values.put(MediaStore.Audio.Playlists.Members.AUDIO_ID, id);
            values.put(MediaStore.Audio.Playlists.Members.PLAYLIST_ID,
                    playlist_id);
            values.put(MediaStore.Audio.Playlists.Members.PLAY_ORDER, i);
            getActivity().getContentResolver().insert(playlisturi, values);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int position,
            long arg3) {
        Cursor cursor = (Cursor) mAdapter.getItem(position);
        if (cursor != null) {
            final String name = cursor.getString(cursor.getColumnIndex(AudioPlaylist.NAME));
            final long id = cursor.getLong(cursor.getColumnIndex(AudioPlaylist._ID));
            final IMediaPlayerService binder = getBinder();
            if (binder != null) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        getActivity());

                builder.setTitle(R.string.lb_confirm);
                builder.setMessage(getString(R.string.msg_replace_orderlist));
                builder.setPositiveButton(R.string.lb_ok,
                        new OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                replaceList(name, id);
                                SavePlaylistDialog.this.dismiss();
                            }
                        });
                builder.setNegativeButton(R.string.lb_cancel, null);
                builder.show();
            }
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (mAdapter == null) {
            mAdapter = new PlaylistListAdapter(getActivity(), null, mViewcache);
            mListView.setAdapter(mAdapter);
        } else {
            Cursor cur = mAdapter.swapCursor(null);
            if (cur != null) {
                cur.close();
            }
        }

        return new CursorLoader(getActivity(),
                MediaConsts.PLAYLIST_CONTENT_URI, null, null, null,
                MediaConsts.AudioPlaylist.DATE_ADDED);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (mAdapter != null) {
            Cursor cur = mAdapter.swapCursor(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> arg0) {
    }

    private IMediaPlayerService getBinder() {
        if (getActivity() instanceof ServiceBinderHolder) {
            return ((ServiceBinderHolder) getActivity()).getBinder();
        } else {
            return null;
        }
    }
}
