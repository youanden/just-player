package jp.co.kayo.android.localplayer.dialog;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.Hashtable;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.appwidget.AppWidgetHelper;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioPlaylistMember;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioPlaylist;
import jp.co.kayo.android.localplayer.core.ServiceBinderHolder;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.service.IMediaPlayerService;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.ViewCache;
import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.content.ContentUris;
import android.content.ContentValues;
import android.support.v4.content.CursorLoader;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.support.v4.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class AddPlaylistDialog extends DialogFragment implements
        LoaderCallbacks<Cursor>, OnItemClickListener {
    private SharedPreferences mPref;
    private ViewCache mViewcache;
    private PlaylistListAdapter mAdapter;
    long[] mPlaylist;
    private ListView mListView;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.playlist_grid_view, null, false);
        mListView = (ListView) view.findViewById(android.R.id.list);
        mListView.setOnItemClickListener(this);

        View footer = inflater.inflate(R.layout.addplaylist_footer, mListView, false);
        mListView.addHeaderView(footer);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.lb_addnewplaylist));
        builder.setNegativeButton(getString(R.string.lb_cancel), null);
        builder.setView(view);

        return builder.create();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mViewcache = (ViewCache) getFragmentManager().findFragmentByTag(
                SystemConsts.TAG_CACHE);
        Bundle args = getArguments();
        if (args != null) {
            mPlaylist = args.getLongArray("playlist");
        }
        
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getLoaderManager().initLoader(R.layout.playlist_grid_view, null, this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getLoaderManager().destroyLoader(R.layout.playlist_grid_view);
    }

    private void addPlayback(){
        IMediaPlayerService binder = getBinder();
        if(binder!=null){
            try{
                for (int i = 0; i < mPlaylist.length; i++) {
                    long id = mPlaylist[i];
                    Hashtable<String, String> tbl = ContentsUtils.getMedia(getActivity(), new String[]{AudioMedia.DATA}, id);
                    if(tbl!=null && tbl.size()>0){
                        binder.addMedia(id, tbl.get(AudioMedia.DATA));
                    }
                }
                binder.commit();
                Toast.makeText(getActivity(), getString(R.string.txt_action_addsong), Toast.LENGTH_SHORT).show();
            } catch (RemoteException e) {
                e.printStackTrace();
            }finally{
                
            }
        }
    }

    private void addPlaylist(long playlist_id) {
        Uri playlisturi = ContentUris.withAppendedId(MediaConsts.PLAYLIST_CONTENT_URI, playlist_id);
        int order = 0;
        // Count
        Cursor cursor = null;
        try{
            cursor = getActivity().getContentResolver().query(
                    ContentUris.withAppendedId(
                    MediaConsts.PLAYLIST_CONTENT_URI,
                    playlist_id), new String[]{AudioPlaylistMember._ID}, null, null, null);
            if(cursor!=null){
                order = cursor.getCount();
            }
        }
        finally{
            if(cursor!=null){
                cursor.close();
            }
        }
        // AllInsert
        for (int i = 0; i < mPlaylist.length; i++) {
            long id = mPlaylist[i];
            ContentValues values = new ContentValues();
            values.put(MediaStore.Audio.Playlists.Members.AUDIO_ID, id);
            values.put(MediaStore.Audio.Playlists.Members.PLAYLIST_ID,
                    playlist_id);
            values.put(MediaStore.Audio.Playlists.Members.PLAY_ORDER, order+i);
            getActivity().getContentResolver().insert(playlisturi, values);
        }
        Toast.makeText(getActivity(), getString(R.string.txt_action_addsong), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
            long arg3) {
        if(pos == 0){
            //header
            addPlayback();
        }
        else{
            int position = pos - 1;
            Cursor cursor = (Cursor) mAdapter.getItem(position);
            if (cursor != null) {
                long id = cursor.getLong(cursor.getColumnIndex(AudioPlaylist._ID));
                addPlaylist(id);
            }
        }
        AddPlaylistDialog.this.dismiss();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (mAdapter == null) {
            mAdapter = new PlaylistListAdapter(getActivity(), null, mViewcache);
            mListView.setAdapter(mAdapter);
        } else {
            Cursor cur = mAdapter.swapCursor(null);
            if (cur != null) {
                cur.close();
            }
        }

        return new CursorLoader(getActivity(),
                MediaConsts.PLAYLIST_CONTENT_URI, null, null, null,
                MediaConsts.AudioPlaylist.DATE_ADDED);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (mAdapter != null) {
            Cursor cur = mAdapter.swapCursor(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> arg0) {
    }

    private IMediaPlayerService getBinder() {
        if (getActivity() instanceof ServiceBinderHolder) {
            return ((ServiceBinderHolder) getActivity()).getBinder();
        } else {
            return null;
        }
    }
}
