package jp.co.kayo.android.localplayer.dialog;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.appwidget.AppWidgetHelper;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.core.ServiceBinderHolder;
import jp.co.kayo.android.localplayer.service.IMediaPlayerService;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.Loader;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class YoutubeSearchDialog extends DialogFragment implements
        LoaderCallbacks<List<YouTubeInfo>>, OnItemClickListener {
    private SharedPreferences mSetting;
    private ExecutorService mExecutor = Executors.newFixedThreadPool(5);
    private HashMap<Integer, Bitmap> mBitmaps = new HashMap<Integer, Bitmap>();
    private Boolean isShutdown = false;
    AsyncTask<Void, Void, Void> mTask;
    private String mArtist;
    private String mTitle;
    private ListView mListView;
    private YoutubeListAdapter mAdapter;

    private Handler mHandler = new MyHandler(this);
    
    private static class MyHandler extends Handler{
        WeakReference<YoutubeSearchDialog> refDialog;
        MyHandler(YoutubeSearchDialog dialog){
            refDialog = new WeakReference<YoutubeSearchDialog>(dialog);
        }
        
        @Override
        public void handleMessage(Message msg) {
            YoutubeSearchDialog dlg = refDialog.get();
            if(dlg!=null){
                Object[] obj = (Object[]) msg.obj;
                synchronized (dlg.mBitmaps) {
                    if (!dlg.isShutdown) {
                        dlg.mBitmaps.put(((String) obj[0]).hashCode(), (Bitmap) obj[1]);
                        dlg.mAdapter.notifyDataSetChanged();
                    }
                }
            }
        }
    };

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.youtube_list_view, null, false);

        if (getArguments() != null) {
            mArtist = getArguments().getString("artist");
            mTitle = getArguments().getString("title");
        }
        mListView = (ListView) view.findViewById(android.R.id.list);
        mListView.setOnItemClickListener(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setNegativeButton(getString(R.string.alert_dialog_close), null);
        builder.setView(view);

        return builder.create();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mTask != null) {
            mTask.cancel(true);
        }
        synchronized (mBitmaps) {
            for (Bitmap bmp : mBitmaps.values()) {
                bmp.recycle();
            }
            mBitmaps.clear();
            isShutdown = true;
        }
        mExecutor.shutdown();
        mExecutor = null;
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        mSetting = PreferenceManager.getDefaultSharedPreferences(getActivity());
        getLoaderManager().initLoader(R.layout.youtube_list_view, null, this);
    }

    public IMediaPlayerService getBinder() {
        if (getActivity() instanceof ServiceBinderHolder) {
            return ((ServiceBinderHolder) getActivity()).getBinder();
        } else {
            return null;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapter, View view, int position,
            long id) {
        try {
            YouTubeInfo item = (YouTubeInfo) mListView.getAdapter().getItem(
                    position);
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(item
                    .getUrl())));

            IMediaPlayerService binder = getBinder();
            if (binder != null) {
                int stat;
                try {
                    stat = binder.stat();
                    if ((stat & AppWidgetHelper.FLG_PLAY) > 0) {
                        binder.pause();
                    }
                } catch (RemoteException e) {
                }
            }
        } finally {
            // dismiss();
        }
    }

    @Override
    public Loader<List<YouTubeInfo>> onCreateLoader(int arg0, Bundle arg1) {
        if (mAdapter == null) {
            mAdapter = new YoutubeListAdapter(getActivity(),
                    new ArrayList<YouTubeInfo>());
            mListView.setAdapter(mAdapter);
        }
        int limit = Funcs.parseInt(mSetting.getString("PrefYoutubeListLimit",
                Integer.toString(SystemConsts.DEFAULT_FETCH_LIMIT)));
        return new YoutubeLoader(getActivity(), mArtist, mTitle, limit);
    }

    @Override
    public void onLoadFinished(Loader<List<YouTubeInfo>> loader,
            List<YouTubeInfo> data) {
        mAdapter.setData(data);
    }

    @Override
    public void onLoaderReset(Loader<List<YouTubeInfo>> arg0) {
        mAdapter = new YoutubeListAdapter(getActivity(),
                new ArrayList<YouTubeInfo>());
        mListView.setAdapter(mAdapter);
    }

    private class YoutubeListAdapter extends ArrayAdapter<YouTubeInfo> {
        LayoutInflater mInflater;
        DecimalFormat _dfmt = new DecimalFormat("00,000");
        List<YouTubeInfo> objects;

        public YoutubeListAdapter(Context context, List<YouTubeInfo> objects) {
            super(context, 0, objects);
            this.objects = objects;
            mInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.youtube_list_row,
                        parent, false);
                holder = new ViewHolder();
                holder.imgThumbnail = (ImageView) convertView
                        .findViewById(R.id.imgThumbnail);
                holder.imgProgress = (ProgressBar) convertView
                        .findViewById(R.id.imgProgress);
                holder.txtDuration = (TextView) convertView
                        .findViewById(R.id.txtDuration);
                holder.txtTitle = (TextView) convertView
                        .findViewById(R.id.txtTitle);
                holder.txtUploaded = (TextView) convertView
                        .findViewById(R.id.txtUploaded);
                holder.txtViewCount = (TextView) convertView
                        .findViewById(R.id.txtViewCount);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            YouTubeInfo item = getItem(position);
            Bitmap bmp = null;
            synchronized (mBitmaps) {
                bmp = mBitmaps.get(item.getThumbnail().hashCode());
            }
            if (bmp != null) {
                holder.imgThumbnail.setImageBitmap(bmp);
                holder.imgThumbnail.setVisibility(View.VISIBLE);
                holder.imgProgress.setVisibility(View.GONE);
            } else {
                mExecutor.execute(new GetImage(item.getThumbnail()));
                holder.imgThumbnail.setVisibility(View.GONE);
                holder.imgProgress.setVisibility(View.VISIBLE);
            }
            holder.txtDuration
                    .setText(Funcs.makeTimeString(item.getDuration()));
            holder.txtTitle.setText(item.getTitle());
            holder.txtUploaded.setText(android.text.format.DateFormat
                    .getDateFormat(getActivity()).format(item.getUploaded()));
            holder.txtViewCount.setText(String.format(
                    getString(R.string.fmt_video_play_count),
                    _dfmt.format(item.getViewCount())));

            return convertView;
        }

        public void setData(List<YouTubeInfo> collection) {
            clear();
            if (collection != null) {
                objects.addAll(collection);
                notifyDataSetChanged();
            }
        }

        class ViewHolder {
            ImageView imgThumbnail;
            ProgressBar imgProgress;
            TextView txtDuration;
            TextView txtTitle;
            TextView txtUploaded;
            TextView txtViewCount;
        }
    }

    private class GetImage implements Runnable {
        final int IMG_SIZE = 128;
        String mPath;

        GetImage(String path) {
            this.mPath = path;
        }

        @Override
        public void run() {
            InputStream is = null;
            Bitmap bmp = null;
            try {
                // 読み込む画像へのUriがある場合は読み込む
                if (mPath != null) {
                    is = new URL(mPath).openStream();
                    BitmapFactory.Options opts = new BitmapFactory.Options();
                    opts.inJustDecodeBounds = true;
                    BitmapFactory.decodeStream(is, null, opts);
                    is.close();
                    is = null;

                    int sample_x = 1 + (opts.outWidth / IMG_SIZE);
                    int sample_y = 1 + (opts.outHeight / IMG_SIZE);

                    is = new URL(mPath).openStream();
                    opts.inSampleSize = Math.max(sample_x, sample_y);
                    opts.inJustDecodeBounds = false;
                    bmp = BitmapFactory.decodeStream(is, null, opts);

                    Message msg = mHandler.obtainMessage();
                    msg.obj = new Object[] { mPath, bmp };
                    mHandler.sendMessage(msg);
                }
            } catch (Exception e) {
                Logger.e("Bitmap loading", e);
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (Exception e) {
                    }
                }
            }
        }

    }

    public static class YoutubeLoader extends
            AsyncTaskLoader<List<YouTubeInfo>> {
        String mArtist;
        String mTitle;
        int mLimit;
        ArrayList<YouTubeInfo> mList;
        SimpleDateFormat _fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        public YoutubeLoader(Context context, String artist, String title,
                int limit) {
            super(context);
            this.mArtist = artist;
            this.mTitle = title;
            this.mLimit = limit;
        }

        @Override
        public List<YouTubeInfo> loadInBackground() {
            return makeList();
        }

        private List<YouTubeInfo> makeList() {
            mList = new ArrayList<YouTubeInfo>();

            // 検索処理
            // http://gdata.youtube.com/feeds/api/videos?v=2&alt=jsonc&q=AKB48&max-results=10
            StringBuilder uri = new StringBuilder();

            HttpClient client = null;
            HttpGet get = null;
            HttpURLConnection connection = null;
            InputStream ris = null;
            try {
                uri.append("http://gdata.youtube.com/feeds/api/videos?v=2&alt=jsonc&q=");
                if (mArtist != null && mTitle != null) {
                    uri.append(URLEncoder.encode(mArtist, "UTF-8")).append("+")
                            .append(URLEncoder.encode(mTitle, "UTF-8"));
                } else if (mArtist != null) {
                    uri.append(URLEncoder.encode(mArtist, "UTF-8"));
                } else if (mTitle != null) {
                    uri.append(URLEncoder.encode(mTitle, "UTF-8"));
                }

                uri.append("&max-results=" + mLimit);

                if (true) {// uri.toString().startsWith("https")){
                    client = new DefaultHttpClient();
                    get = new HttpGet();

                    get.setURI(new URI(uri.toString()));
                    HttpResponse res = client.execute(get);
                    if (res.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                        addList(res.getEntity().getContent());
                    }
                } else {
                    connection = (HttpURLConnection) (new URL(uri.toString()))
                            .openConnection();
                    connection.setDoOutput(true);

                    connection.connect();

                    // ここから下はクライアントに対するHTTPレスポンスを作成
                    int responseCode = connection.getResponseCode();
                    // クライアントにHTTPレスポンスを書きこむ
                    if (responseCode == 200 || responseCode == 206
                            || responseCode == 203) {
                        ris = connection.getInputStream();
                        addList(ris);
                    }
                }

            } catch (URISyntaxException e) {
                Logger.e("Uri parse error", e);
            } catch (ClientProtocolException e) {
                Logger.e("client protocol error", e);
            } catch (JSONException e) {
                Logger.e("Json parse error", e);
            } catch (IOException e) {
                Logger.e("IO error", e);
            } catch (ParseException e) {
                Logger.e("long parse error", e);
            } finally {
                if (ris != null) {
                    try {
                        ris.close();
                    } catch (IOException e) {
                    }
                    ris = null;
                }
                if (connection != null) {
                    try {
                        connection.disconnect();
                    } catch (Exception e) {
                        Logger.e("connection.disconnect", e);
                    }
                    connection = null;
                }
                if (client != null) {
                    client.getConnectionManager().shutdown();
                }
            }

            return mList;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            if (mList == null) {
                forceLoad();
            } else {
                deliverResult(mList);
            }
        }

        private void addList(InputStream in) throws IOException, JSONException,
                ParseException {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    in, "UTF-8"));
            StringBuilder builder = new StringBuilder();
            for (String line = null; (line = reader.readLine()) != null;) {
                builder.append(line);
            }
            JSONTokener tokener = new JSONTokener(builder.toString());
            JSONObject jsonobj = new JSONObject(tokener);

            JSONObject data = jsonobj.getJSONObject("data");
            if (data != null && !data.isNull("items")) {
                JSONArray items = data.getJSONArray("items");
                if (items != null) {
                    for (int i = 0; i < items.length(); i++) {
                        JSONObject item = items.getJSONObject(i);
                        YouTubeInfo youtubeinfo = new YouTubeInfo();

                        JSONObject player = item.getJSONObject("player");
                        if (player != null) {
                            youtubeinfo.setUrl(player.getString("default"));
                            if (youtubeinfo.getUrl() == null) {
                                youtubeinfo.setUrl(player.getString("mobile"));
                            }
                        }

                        youtubeinfo.setTitle(item.getString("title"));
                        JSONObject thumbnail = item.getJSONObject("thumbnail");
                        String sq = null;
                        if (thumbnail != null) {
                            youtubeinfo.setThumbnail(thumbnail
                                    .getString("sqDefault"));
                            if (youtubeinfo.getThumbnail() == null) {
                                youtubeinfo.setThumbnail(thumbnail
                                        .getString("hqDefault"));
                            }
                        }
                        String str = item.getString("uploaded");
                        if (str != null) {
                            // 2010-09-08T04:15:24.000Z
                            youtubeinfo.setUploaded(_fmt.parse(str));
                        }
                        youtubeinfo.setViewCount(item.getLong("viewCount"));
                        youtubeinfo
                                .setDuration(item.getLong("duration") * 1000);

                        mList.add(youtubeinfo);
                    }
                }
            }
        }
    }

}
