package jp.co.kayo.android.localplayer.dialog;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import jp.co.kayo.android.localplayer.BaseActivity;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioFavorite;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.ViewCache;
import jp.co.kayo.android.localplayer.util.bean.FavoriteInfo;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import android.view.Window;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;

public class RatingDialog extends Dialog implements OnRatingBarChangeListener {
    RatingBar ratingbar;
    String mType;
    long mMediaId;
    ViewCache viewcache;
    Handler handler;
    BaseActivity owner;
    boolean hasChanged = false;

    public RatingDialog(Activity context, String type, long id, Handler handler) {
        super(context);
        owner = (BaseActivity) context;
        this.mType = type;
        this.mMediaId = id;
        this.handler = handler;
        // setStyle(STYLE_NO_TITLE, 0);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Logger.d("RatingDialog.onTouchEvent");
        if (hasChanged) {
            if (getWindow().getCurrentFocus() == null
                    || this.getWindow().getCurrentFocus().getId() != ratingbar
                            .getId()) {
                save();
                dismiss();
            }
        }
        return super.onTouchEvent(event);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        save();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rating_dialog);

        viewcache = (ViewCache) owner.getSupportFragmentManager()
                .findFragmentByTag(SystemConsts.TAG_CACHE);

        ratingbar = (RatingBar) findViewById(R.id.ratingBar1);
        loadFavorite();
        ratingbar.setOnRatingBarChangeListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        // TODO Auto-generated method stub
        Logger.d("Dialog dispatchTouchEvent");

        return super.dispatchTouchEvent(ev);
    }

    public void save() {
        // ratingがなしの場合は、Favoriteから曲を削除
        if (favorite != null && favorite.rating == 0) {
            ContentResolver cr = getContext().getContentResolver();
            cr.delete(MediaConsts.FAVORITE_CONTENT_URI,
                    TableConsts.FAVORITE_POINT + " = 0", null);
            // 通知
            if (handler != null) {
                Message msg = new Message();
                msg.what = SystemConsts.EVT_UPDATE_RATING;
                msg.obj = mMediaId;
                handler.sendMessage(msg);
            }
        }
        ratingbar = null;
        viewcache = null;
        handler = null;
    }

    private FavoriteInfo favorite = null;

    private void loadFavorite() {
        Cursor cur = null;
        try {
            cur = getContext().getContentResolver().query(
                    MediaConsts.FAVORITE_CONTENT_URI,
                    new String[] { AudioFavorite._ID, AudioFavorite.POINT },
                    AudioFavorite.MEDIA_ID + " = ? and " + AudioFavorite.TYPE
                            + " = ?",
                    new String[] { Long.toString(mMediaId), mType }, null);
            if (cur != null && cur.moveToFirst()) {
                long id = cur.getLong(cur.getColumnIndex(AudioFavorite._ID));
                int rating = cur
                        .getInt(cur.getColumnIndex(AudioFavorite.POINT));
                favorite = new FavoriteInfo();
                favorite.media_id = mMediaId;
                favorite.rating = rating;
                ratingbar.setRating(rating);
            }
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    @Override
    public void onRatingChanged(RatingBar ratingBar, float rating,
            boolean fromUser) {
        Logger.d("rating=" + rating);
        hasChanged = true;
        // 更新
        if (favorite != null) {
            ContentValues values = new ContentValues();
            values.put(AudioFavorite.POINT, (int) rating);
            getContext().getContentResolver().update(
                    MediaConsts.FAVORITE_CONTENT_URI,
                    values,
                    AudioFavorite.MEDIA_ID + " = ? AND " + AudioFavorite.TYPE
                            + " = ?",
                    new String[] { Long.toString(mMediaId), mType });
            favorite.rating = (int) rating;
        } else {
            Cursor cur = null;
            try {
                ContentValues values = new ContentValues();
                values.put(AudioFavorite.MEDIA_ID, mMediaId);
                values.put(AudioFavorite.POINT, (int) rating);
                values.put(AudioFavorite.TYPE, mType);
                getContext().getContentResolver().insert(
                        MediaConsts.FAVORITE_CONTENT_URI, values);

                favorite = new FavoriteInfo();
                favorite.media_id = mMediaId;
                favorite.rating = (int) rating;
            } finally {
                if (cur != null) {
                    cur.close();
                }
            }
        }
        // 通知
        getContext().getContentResolver().notifyChange(
                MediaConsts.MEDIA_CONTENT_URI, null);
        /*
         * if (handler != null) { Message msg = new Message(); msg.what =
         * SystemConsts.EVT_UPDATE_RATING; msg.obj = mMediaId;
         * handler.sendMessage(msg); }
         */
    }

}
