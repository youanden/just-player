package jp.co.kayo.android.localplayer.dialog;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import jp.co.kayo.android.localplayer.BaseActivity;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioPlaylist;
import jp.co.kayo.android.localplayer.core.ContentManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

public class NewPlaylistDialog extends DialogFragment {
    EditText edit = null;
    DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.newplaylistdialog, null, false);

        edit = (EditText) view.findViewById(R.id.EditText01);

        StringBuilder buf = new StringBuilder();
        Calendar cal = Calendar.getInstance();
        String date = dfm.format(cal.getTime());
        buf.append(getString(R.string.lb_newplaylist)).append(date);
        edit.setText(buf.toString());

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.lb_newnewplaylist));
        builder.setPositiveButton(getString(R.string.lb_ok),
                new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        saveNew(edit.getText().toString());
                    }
                });
        builder.setNegativeButton(getString(R.string.lb_cancel), null);
        builder.setView(view);

        return builder.create();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void saveNew(String name) {
        ContentValues newplaylist = new ContentValues();
        long playlist_key = System.nanoTime();//外部でプレイリストを管理するシステムと整合をあわせるためダミー用のIDを作っておく
        newplaylist.put(AudioPlaylist.PLAYLIST_KEY, Long.toString(playlist_key));
        newplaylist.put(MediaStore.Audio.Playlists.NAME, name);
        // AllInsert
        Uri uri = getActivity().getContentResolver().insert(
                MediaConsts.PLAYLIST_CONTENT_URI, newplaylist);
        
        ContentManager mgr = (ContentManager)getFragmentManager().findFragmentById(R.id.fragment_main);
        if(mgr!=null){
            mgr.reload();
        }
    }
}
