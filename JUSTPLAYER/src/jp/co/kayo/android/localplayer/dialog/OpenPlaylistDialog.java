
package jp.co.kayo.android.localplayer.dialog;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioPlaylist;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioPlaylistMember;
import jp.co.kayo.android.localplayer.core.NotifyHandler;
import jp.co.kayo.android.localplayer.core.ProgressDialogTask;
import jp.co.kayo.android.localplayer.core.ServiceBinderHolder;
import jp.co.kayo.android.localplayer.fragment.ControlFragment;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.service.IMediaPlayerService;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.ViewCache;
import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.content.ContentUris;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class OpenPlaylistDialog extends DialogFragment implements
        LoaderCallbacks<Cursor>, OnItemClickListener, OnScrollListener {
    private SharedPreferences mPref;
    private ViewCache mViewcache;
    private PlaylistListAdapter mAdapter;
    private ListView mListView;

    boolean isReadFooter;
    View mFooter = null;
    Runnable mTask = null;
    MatrixCursor mcur;
    AsyncTask<Void, Void, Void> loadtask = null;
    boolean getall = false;

    String[] fetchcols = new String[] {
            AudioPlaylist._ID, AudioPlaylist.NAME,
            AudioPlaylist.PLAYLIST_KEY, AudioPlaylist.DATE_ADDED,
            AudioPlaylist.DATE_MODIFIED
    };

    NotifyHandler mHandler = new MyNotifyHandler(this);

    private static class MyNotifyHandler extends NotifyHandler {
        WeakReference<OpenPlaylistDialog> ref;

        MyNotifyHandler(OpenPlaylistDialog r) {
            ref = new WeakReference<OpenPlaylistDialog>(r);
        }

        @Override
        public void handleMessage(Message msg) {
            OpenPlaylistDialog dlg = ref.get();
            if (dlg != null) {
                switch (msg.what) {
                    case SystemConsts.ACT_NOTIFYDATASETCHANGED: {
                        dlg.mAdapter.notifyDataSetChanged();
                    }
                        break;
                    case SystemConsts.ACT_ADDROW: {
                        Object[] values = (Object[]) msg.obj;
                        if (dlg.mcur != null) {
                            dlg.mcur.addRow(values);
                        }
                    }
                        break;
                    default: {
                        super.handleMessage(msg);
                    }
                }
            }
        }
    };

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.playlist_grid_view, null, false);
        mListView = (ListView) view.findViewById(android.R.id.list);
        mListView.setOnItemClickListener(this);
        mListView.setOnScrollListener(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.lb_load_playback));
        builder.setNegativeButton(getString(R.string.lb_cancel), null);
        builder.setView(view);

        return builder.create();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mViewcache = (ViewCache) getFragmentManager().findFragmentByTag(
                SystemConsts.TAG_CACHE);
        if (savedInstanceState != null) {
            getall = savedInstanceState.getBoolean("getall");
        }
        isReadFooter = ContentsUtils.isNoCacheAction(mPref);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("getall", getall);
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getLoaderManager().initLoader(R.layout.playlist_grid_view, null, this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getLoaderManager().destroyLoader(R.layout.playlist_grid_view);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (mAdapter == null) {
            mAdapter = new PlaylistListAdapter(getActivity(), null, mViewcache);
            mListView.setAdapter(mAdapter);
        } else {
            Cursor cur = mAdapter.swapCursor(null);
            if (cur != null) {
                cur.close();
            }
        }

        getall = false;
        hideFooter();
        return new CursorLoader(getActivity(),
                MediaConsts.PLAYLIST_CONTENT_URI, null, null, null,
                MediaConsts.AudioPlaylist.DATE_ADDED);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        try {
            showFooter();
            if (mAdapter != null) {
                if (ContentsUtils.isNoCacheAction(mPref)) {
                    if (data != null && !data.isClosed()) {
                        int count = data.getCount();
                        try {
                            mcur = new MatrixCursor(data.getColumnNames(),
                                    data.getCount());
                            if (count > 0) {
                                data.moveToFirst();
                                do {
                                    ArrayList<Object> values = new ArrayList<Object>();
                                    for (int i = 0; i < mcur.getColumnCount(); i++) {
                                        if (AudioPlaylist.DATE_ADDED
                                                .equals(mcur.getColumnName(i))) {
                                            values.add(data.getLong(i));
                                        } else {
                                            values.add(data.getString(i));
                                        }
                                    }
                                    mcur.addRow(values
                                            .toArray(new Object[values.size()]));
                                } while (data.moveToNext());
                                Cursor cur = mAdapter.swapCursor(mcur);
                            }

                        } finally {
                            if (count < SystemConsts.DEFAULT_FETCH_LIMIT) {
                                invisibleFooter();
                            }
                            if (data != null) {
                                data.close();
                            }
                        }
                    }
                } else {
                    Cursor cur = mAdapter.swapCursor(data);
                    invisibleFooter();
                }
            }
        } finally {
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> arg0) {
        showFooter();
        mcur = null;
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int position,
            long arg3) {
        Cursor cursor = (Cursor) mAdapter.getItem(position);
        if (cursor != null) {
            long playlistId = cursor.getLong(cursor.getColumnIndex(AudioPlaylist._ID));
            final IMediaPlayerService binder = getBinder();
            if (binder != null) {
                Cursor cur = null;
                try {
                    cur = getActivity().getContentResolver()
                            .query(
                                    ContentUris.withAppendedId(MediaConsts.PLAYLIST_CONTENT_URI,
                                            playlistId),
                                    null,
                                    null,
                                    null,
                                    AudioPlaylistMember.PLAY_ORDER);
                    if (cur != null && cur.getCount() > 0) {
                        ContentsUtils.playMediaReplace(
                                SystemConsts.CONTENTSKEY_PLAYLIST
                                        + playlistId, binder, cur,
                                AudioPlaylistMember.AUDIO_ID, AudioPlaylistMember.DURATION,
                                AudioPlaylistMember.DATA, 0);
                    }
                } finally {
                    if (cur != null) {
                        cur.close();
                    }
                    dismiss();
                }
            }
        }
    }

    private IMediaPlayerService getBinder() {
        if (getActivity() instanceof ServiceBinderHolder) {
            return ((ServiceBinderHolder) getActivity()).getBinder();
        } else {
            return null;
        }
    }

    private View getFooter() {
        if (mFooter == null) {
            mFooter = getActivity().getLayoutInflater().inflate(
                    R.layout.listview_footer, null);
        }
        return mFooter;
    }

    private void showFooter() {
        getFooter().findViewById(R.id.progress1).setVisibility(View.VISIBLE);
    }

    private void hideFooter() {
        getFooter().findViewById(R.id.progress1).setVisibility(View.GONE);
    }

    private void invisibleFooter() {
        getall = true;
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                hideFooter();
            }
        });
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
            int visibleItemCount, int totalItemCount) {
        if (mAdapter == null || loadtask != null) {
            return;
        }
        if (isReadFooter) {
            if (!getall && totalItemCount > 0
                    && totalItemCount == (firstVisibleItem + visibleItemCount)) {
                final int current = mAdapter.getCount();
                if (current > 0) {
                    loadtask = new AsyncTask<Void, Void, Void>() {

                        @Override
                        protected Void doInBackground(Void... params) {
                            Cursor cursor = null;
                            try {
                                cursor = getActivity()
                                        .getContentResolver()
                                        .query(MediaConsts.PLAYLIST_CONTENT_URI,
                                                fetchcols,
                                                "LIMIT ? AND OFFSET ?",
                                                new String[] {
                                                        Integer.toString(SystemConsts.DEFAULT_FETCH_LIMIT),
                                                        Long.toString(current)
                                                },
                                                null);
                                int count = cursor.getCount();
                                if (mcur != null && count > 0) {
                                    cursor.moveToFirst();
                                    do {
                                        ArrayList<Object> values = new ArrayList<Object>();
                                        for (int i = 0; i < mcur
                                                .getColumnCount(); i++) {
                                            String colname = mcur
                                                    .getColumnName(i);
                                            int colid = cursor
                                                    .getColumnIndex(colname);
                                            if (colid != -1) {
                                                if (AudioPlaylist.DATE_ADDED
                                                        .equals(colname)) {
                                                    values.add(cursor
                                                            .getLong(colid));
                                                } else {
                                                    values.add(cursor
                                                            .getString(colid));
                                                }
                                            }
                                        }
                                        mHandler.sendMessage(mHandler
                                                .obtainMessage(
                                                        SystemConsts.ACT_ADDROW,
                                                        values.toArray(new Object[values
                                                                .size()])));
                                    } while (cursor.moveToNext());

                                    if (count < SystemConsts.DEFAULT_FETCH_LIMIT) {
                                        invisibleFooter();
                                    }
                                } else {
                                    if (count < 1) {
                                        invisibleFooter();
                                    }
                                }
                            } finally {
                                if (cursor != null) {
                                    cursor.close();
                                }
                                mHandler.sendEmptyMessage(SystemConsts.ACT_NOTIFYDATASETCHANGED);
                                loadtask = null;
                            }

                            return null;
                        }
                    };
                    loadtask.execute((Void) null);
                }
            }
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        Logger.d("onScrollStateChanged:" + scrollState);
        long time = Funcs.getHideTime(mPref);
        if (time > 0) {
            if (scrollState == 1) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                if (getFragmentManager() != null) {
                    ControlFragment control = (ControlFragment) getFragmentManager()
                            .findFragmentByTag(SystemConsts.TAG_CONTROL);
                    if (control != null) {
                        control.hideControl(false);
                    }
                }
            } else if (scrollState == 0) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                mTask = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (getFragmentManager() != null) {
                                ControlFragment control = (ControlFragment) getFragmentManager()
                                        .findFragmentByTag(
                                                SystemConsts.TAG_CONTROL);
                                if (control != null) {
                                    control.showControl(false);
                                }
                            }
                        } finally {
                            mTask = null;
                        }
                    }
                };
                mHandler.postDelayed(mTask, time);
            }
        }
    }
}
