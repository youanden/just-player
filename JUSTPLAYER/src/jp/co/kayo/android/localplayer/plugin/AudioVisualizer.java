package jp.co.kayo.android.localplayer.plugin;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.EqualizerActivity.FrqInfo;
import jp.co.kayo.android.localplayer.EqualizerActivity.AttackTime;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

public class AudioVisualizer extends View {

    public enum ViewMode {
        FFT, WAVE, BMS
    }

    private int mFontSize = 24;
    // 表示モード
    private ViewMode mViewMode = ViewMode.FFT;

    // 波形の表示領域の矩形です
    private Rect mRect = null;

    // 波形の色です
    private Paint mForePaint1 = new Paint();
    private Paint mForePaint2 = new Paint();
    private Paint mForePaint3 = new Paint();
    private Paint mForePaint4 = new Paint();
    private Paint mForePaint5 = new Paint();
    private Paint mLavelPaint = new Paint();
    
    private Rect tmpRect = new Rect();

    private float[] mWavePoints = new float[1024 * 4];
    private byte[] mWaveBytes;

    private Context mContext;

    private String mLabelStringLevelMeter;
    private String mLabelStringWave;
    private String mLabelStringBms;
    private int mPowcolor;

    public AudioVisualizer(Context context) {
        super(context);
        init();
    }

    public AudioVisualizer(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
        init();
    }

    public AudioVisualizer(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();

        if (isInEditMode()) {
            Random rand = new Random();
            float[] bytes = new float[512];
            for (int i = 0; i < bytes.length; i++) {
                bytes[i] = (float) (rand.nextFloat() * 256);
            }
            updateVisualizer(bytes);
        }
    }

    private void init() {
        mDbs = null;
        mRect = null;

        // mForePaint.setStyle(Style.STROKE);
        mForePaint1.setStrokeWidth(1f);
        mForePaint1.setAntiAlias(true);
        mForePaint1.setColor(getResources().getColor(
                R.color.visual_normal_color));

        mForePaint2.setStrokeWidth(1f);
        mForePaint2.setAntiAlias(true);
        mForePaint2.setColor(getResources().getColor(R.color.visual_max_color));

        mForePaint3
                .setColor(getResources().getColor(R.color.visual_font_color));
        mForePaint3.setAntiAlias(true);

        // WAVE表示用
        mForePaint4.setColor(Color.MAGENTA);
        mForePaint4.setAntiAlias(true);
        mForePaint4.setStyle(Style.FILL_AND_STROKE);
        mForePaint4.setStrokeWidth(2f);

        // BMS表示用
        mForePaint5.setAntiAlias(true);
        mForePaint5.setStyle(Style.FILL_AND_STROKE);
        mForePaint5.setStrokeWidth(2f);

        // ラベル表示用
        mLavelPaint.setAntiAlias(true);
        mLavelPaint.setColor(Color.GREEN);
        mLavelPaint.setStyle(Style.FILL);
        mLavelPaint.setStrokeWidth(1f);
        mFontSize = mContext.getResources().getInteger(
                R.integer.visualizer_fontsize);
        mLavelPaint.setTextSize(mFontSize);

        mLabelStringLevelMeter = mContext.getResources().getString(
                R.string.visualizer_levelmeter);
        mLabelStringWave = mContext.getResources().getString(
                R.string.visualizer_wave);
        mLabelStringBms = mContext.getResources().getString(
                R.string.visualizer_bms);

        mPowcolor = Color.argb(160, 190, 190, 255);

        calcDimension();
    }

    public void calcDimension() {
        yn = getResources().getDimensionPixelSize(R.dimen.yn);
        xn = getResources().getDimensionPixelSize(R.dimen.xn);

        box = new byte[xn][yn];
        avrsf = new float[xn];
        maxsf = new float[xn];
        lastdtf = new long[xn];

        Arrays.fill(avrsf, 0);
        Arrays.fill(maxsf, 0);
        Arrays.fill(lastdtf, 0);

        mRect = null;
    }

    // int YN = 60; //値の分割数
    // int XN = 30;//周波数の分割数
    byte[][] box;

    // int bw, bh;

    int x_margin, y_margin;
    int ewidth, eheight;
    int bwidth, bheight;
    int dx, dy;
    int box_width, box_height;
    int yn = -1, xn = -1;
    int limit_yn = -1, limit_xn = -1;
    float[] mDbs;
    float[] avrsf;
    float[] maxsf;
    long[] lastdtf;

    private List<FrqInfo> mFrqInfoList;

    private void initView() {
        if (getWidth() > 0 && this.isShown()) {
            x_margin = getResources().getDimensionPixelSize(R.dimen.x_margin);
            y_margin = getResources().getDimensionPixelSize(R.dimen.y_margin);
            dx = getResources().getDimensionPixelSize(R.dimen.dx);
            dy = getResources().getDimensionPixelSize(R.dimen.dy);
            mRect = new Rect(0, 0, getWidth(), getHeight());
            ewidth = (getWidth() - x_margin * 2);
            eheight = (getHeight() - y_margin * 2);
            bwidth = ewidth / xn;
            bheight = eheight / yn;
            box_width = bwidth - dx * 2;
            box_height = bheight - dy * 2;
            if (box_height < 4) {
                box_height = 4;
            }
        }
    }

    public float calcAvgF(float[] bytes, int start, int end) {
        float sum = 0;
        int count = 1;
        for (int i = start; i < end; i++) {
            sum += bytes[i];
            count++;
        }
        return sum / count;
    }

    public void updateVisualizer(float[] dbs) {
        mDbs = dbs;
        int length = mDbs.length;
        int dt = length / xn;
        int s = 255 / yn;
        long now = System.currentTimeMillis();
        for (int i = 0; i < xn; i++) {
            int start = i * dt;
            int end = start + dt;
            // 平均を求める
            float avg = calcAvgF(mDbs, start, end);
            avrsf[i] = avg;
            // 最大値を更新する
            boolean update = false;
            if (maxsf[i] <= avg) {
                lastdtf[i] = now;
                maxsf[i] = avg;
                update = true;
            } else {
                long fade = now - lastdtf[i];
                if (fade > 2000) {
                    update = true;
                    maxsf[i] = avg;
                }
            }

            // int y = (int)((float)avg/s);

            for (int j = 0; j < yn; j++) {
                byte val = box[i][j];
                float range1 = j * s;
                float range2 = (j + 1) * s;
                if (range1 < avg) {
                    val = 1;
                } else if (range1 <= maxsf[i] && maxsf[i] < range2) {
                    val = 2;
                } else {
                    val = 0;
                }
                box[i][j] = val;
            }
        }

        mViewMode = ViewMode.FFT;
        invalidate();
    }

    public void updateVisualizerWave(byte[] bytes) {
        mWaveBytes = bytes;
        mViewMode = ViewMode.WAVE;
        invalidate();
    }

    public void updateVisualizerBms(List<FrqInfo> frqInfoList, long currenttime) {
        mFrqInfoList = frqInfoList;
        mCurrentTime = currenttime;
        mViewMode = ViewMode.BMS;
        invalidate();
    }

    ColorTemperture colortemp = new ColorTemperture();

    private long mCurrentTime;

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (mRect == null) {
            initView();
        }
        if (mRect != null) {
            if (mViewMode == ViewMode.FFT) {
                // 波形データがない場合は処理をしません
                if (mDbs == null) {
                    return;
                }

                // Fft表示
                drawFft(canvas);
                drawLabel(canvas, mLabelStringLevelMeter);
            } else if (mViewMode == ViewMode.WAVE) {
                // 波形データがない場合は処理をしません
                if (mWaveBytes == null) {
                    return;
                }
                // WAVE表示
                drawWave(canvas);
                drawLabel(canvas, mLabelStringWave);
            } else if (mViewMode == ViewMode.BMS) {
                // BMS表示
                drawBms(canvas);
                drawLabel(canvas, mLabelStringBms);
            }

        }
    }

    protected void drawFft(Canvas canvas) {
        // Logger.d("onDraw");
        // GridRowの描画
        int posX = x_margin;
        int posY = mRect.bottom - y_margin;
        // Logger.d("onDraw:("+mRect.left+","+mRect.top+")-("+mRect.right+","+mRect.bottom+")");
        float range = (22026 - 1096);
        int x1, y1, x2, y2, rx1, ry1, rx2, ry2;
        int color = Color.RED;
        Paint paint = new Paint();
        byte b;
        float dt, kelv;
        for (int i = 0; i < xn; i++) {
            x1 = posX + i * bwidth;
            // int x2 = x1+bwidth;

            for (int j = 0; j < yn; j++) {
                y2 = posY - j * bheight;
                y1 = y2 - bheight;

                rx1 = x1 + dx;
                ry1 = y1 + dy;
                rx2 = rx1 + box_width;
                ry2 = ry1 + box_height;
                // Logger.d("rx1:" + rx1 + " / " + "ry1:" + ry1 + " / " +
                // "rx2:" + rx2 + " / "
                // + "ry2:" + ry2);
                //Rect r = new Rect(rx1, ry1, rx2, ry2);
                tmpRect.left = rx1;
                tmpRect.top = ry1;
                tmpRect.right = rx2;
                tmpRect.bottom = ry2;
                b = box[i][j];
                // Logger.d("onDraw:"+b);
                if (b == 1) {
                    dt = range / (float) yn;
                    kelv = 22026 - ((dt * (float) j));
                    if (kelv > 1096) {
                        color = (int) colortemp.GetColor(
                                22026 - ((dt * (float) (j % yn))), 1.0f);
                    }
                    paint.setStrokeWidth(1f);
                    paint.setAntiAlias(true);
                    paint.setColor(0xFF000000 | color);
                    // canvas.drawLine(rx1, ry1, rx2, ry1, mForePaint1);
                    canvas.drawRect(tmpRect, paint);

                } else if (b == 2) {
                    // canvas.drawLine(rx1, ry1, rx2, ry1, mForePaint2);
                    canvas.drawRect(tmpRect, mForePaint2);
                } else {
                    // canvas.drawRect(r, mForePaint);
                }
            }
        }
    }

    private void drawWave(Canvas canvas) {
        int w = getWidth();
        int h = getHeight();

        for (int i = 0; i < mWaveBytes.length - 1; i++) {
            mWavePoints[i * 4] = w * i / (mWaveBytes.length - 1);
            mWavePoints[i * 4 + 1] = h / 2 + ((byte) (mWaveBytes[i] + 128))
                    * (h / 2) / 128;
            mWavePoints[i * 4 + 2] = w * (i + 1) / (mWaveBytes.length - 1);
            mWavePoints[i * 4 + 3] = h / 2 + ((byte) (mWaveBytes[i + 1] + 128))
                    * (h / 2) / 128;
        }

        canvas.drawLines(mWavePoints, mForePaint4);
    }

    public void drawBms(Canvas canvas) {
        if (mFrqInfoList == null) {
            return;
        }
        int w = getWidth();
        int h = getHeight();

        final long maxTimeLen = 1500; // ゲーム画面で見えている時間
        int numFrq = mFrqInfoList.size();

        int lanes = numFrq;
        if (lanes > 7) {
            lanes = 7; // 最大７レーン表示する
        }
        int gap = 20;
        float barWidth = (w - gap) / lanes - gap;
        int bottomMargin = 8;
        final int POW_SMALLOFFSET = 10;
        final int POW_LARGEOFFSET = 5;
        for (int i = 0; i < numFrq; i++) {
            int ix = i;
            if (ix >= lanes) {
                ix = lanes - 1;
            }
            FrqInfo info = mFrqInfoList.get(i);
            float x = gap + ((barWidth + gap) * ix); // 高音部まとめる
            // バーを表示
            int pos = 0;
            float bowy = 0;
            do {
                pos = getNextAttackTime(info, pos, mCurrentTime, maxTimeLen);

                if (pos < 0) {
                    break;
                }
                AttackTime at = info.mAttackTimelist.get(pos);
                float y = h * (float) (mCurrentTime - at.mTime)
                        / (float) maxTimeLen;
                if (y >= 0) {
                    if (y < h - bottomMargin - POW_SMALLOFFSET) {
                        int r = 0;
                        int g = 0;
                        int b = 0;
                        if (at.mValue >= 1.0) {
                            // 赤
                            r = 255;
                        } else if (at.mValue > 0.9) {
                            // 黄色
                            r = 255;
                            g = 255;
                        } else if (at.mValue > 0.8) {
                            // 緑
                            g = 255;
                        } else {
                            // 強い音ほど明るい色で表示
                            r = (int) (255 * at.mValue);
                            g = (int) (255 * at.mValue);
                            b = (int) (255 * at.mValue);
                        }
                        mForePaint5.setColor(Color.argb(255, r, g, b));
                        canvas.drawRect(x, y, x + barWidth, y + 5, mForePaint5);
                    } else {
                        // 底に到達
                        bowy = y;
                    }
                }
                pos++;

            } while (true);

            // 爆発を表示
            if (bowy != 0) {
                mForePaint5.setColor(mPowcolor);
                if (bowy >= h - bottomMargin - POW_LARGEOFFSET) {
                    canvas.drawCircle(x + barWidth / 2, h - bottomMargin - 5,
                            barWidth / 2, mForePaint5);
                } else if (bowy >= h - bottomMargin - POW_SMALLOFFSET) {
                    canvas.drawCircle(x + barWidth / 2, h - bottomMargin - 5,
                            barWidth / 3, mForePaint5);
                }
            }
        }
    }

    public void drawLabel(Canvas canvas, String s) {
        float w = mLavelPaint.measureText(s);
        float posx = getWidth() - w - mFontSize;
        canvas.drawText(s, posx, mFontSize + 10, mLavelPaint);
    }

    // 解析したデータを取得する
    public int getNextAttackTime(FrqInfo info, int startPos, long currentTime,
            long maxTimeLen) {
        int ret = -1;
        if (info != null) {
            int numAttack = info.mAttackTimelist.size();
            int pos0 = startPos > info.mPos ? startPos : info.mPos;
            for (int i = pos0; i < numAttack; i++) {
                AttackTime attack = info.mAttackTimelist.get(i);
                if (((currentTime - maxTimeLen) > attack.mTime)) {
                    info.mPos = i;
                } else if ((currentTime - maxTimeLen) <= attack.mTime) {
                    ret = i;
                    break;
                } else if (currentTime < attack.mTime) {
                    break;
                }
            }
        }
        return ret;
    }
}
