package jp.co.kayo.android.localplayer.core;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.ArrayList;

import jp.co.kayo.android.localplayer.util.Logger;

import android.content.Context;
import android.database.Cursor;
import android.widget.CursorTreeAdapter;
import android.widget.SectionIndexer;

public abstract class IndexTreeCursorAdapter extends CursorTreeAdapter
        implements SectionIndexer {
    protected ArrayList<IndexValue> indexSections = new ArrayList<IndexValue>();
    protected ArrayList<IndexValue> fastSections = new ArrayList<IndexValue>();
    String cName;
    protected String indexbar;
    boolean imageloadskip = false;

    public IndexTreeCursorAdapter(Context context, Cursor c,
            Boolean autoRequery, String cName) {
        super(c, context, autoRequery);
        this.cName = cName;
        indexSection(c);
    }

    @Override
    public void changeCursor(Cursor newCursor) {
        super.changeCursor(newCursor);
        indexSection(newCursor);
    }

    public void setImageLoadSkip(boolean b) {
        imageloadskip = b;
    }

    public void setCname(String cname) {
        this.cName = cname;
    }

    public String getCname() {
        return this.cName;
    }

    public int findPosition(char c) {
        int index = 0;
        for (IndexValue i : indexSections) {
            if (i.ch.equals(c)) {
                return i.pos;
            }
            index++;
        }
        return -1;
    }

    /**
     * インデックスを作る処理。
     * 
     * @param Cursor
     *            データベースへのカーソル
     */
    public void indexSection(Cursor c) {
        indexSections.clear();
        fastSections.clear();
        ArrayList<IndexValue> al = new ArrayList<IndexValue>();
        int index = 0;
        if (c != null && c.moveToFirst()) {
            do {
                String section = c.getString(c.getColumnIndex(getCname()));
                if (section != null && section.length() > 0) {
                    char ch = section.charAt(0);
                    IndexValue pt = get(al, ch);
                    if (pt == null) {
                        pt = new IndexValue();
                        pt.ch = ch;
                        pt.pos = index;
                        al.add(pt);
                    } else {
                        pt.count++;
                    }
                }
                index++;
            } while (c.moveToNext());
            c.moveToFirst();
        }

        for (IndexValue o : al) {
            fastSections.add(o);
        }
    }

    @Override
    public int getPositionForSection(int section) {
        if (section < fastSections.size()) {
            return fastSections.get(section).pos;
        } else {
            return -1;
        }
    }

    @Override
    public int getSectionForPosition(int position) {
        int start = 0;
        int end = 0;
        int max = fastSections.size();
        for (int i = 0; i < max; i++) {
            start = end;
            end = fastSections.get(i).pos;
            if (start <= position && end >= position) {
                return i;
            }
        }
        return 0;
    }

    @Override
    public Object[] getSections() {
        return fastSections.toArray(new IndexValue[0]);
    }

    IndexValue get(ArrayList<IndexValue> list, Character c) {
        for (IndexValue o : list) {
            if (o.ch.equals(c)) {
                return o;
            }
        }
        return null;
    }

    private String makeIndexBar(ArrayList<IndexValue> al, int limit) {
        Character befor = null;
        StringBuilder buf = new StringBuilder();
        boolean doublecheck = false;
        for (int i = 0; i < al.size(); i++) {
            IndexValue obj = al.get(i);
            Character ch = obj.ch;
            if (ch < 'A') {
                ch = '#';
            } else if ('Z' < ch && ch < 'a') {
                ch = '*';
            } else if ('z' < ch && ch < 127) {
                ch = '*';
            } else if (obj.count < limit) {
                if ((i + 1) < al.size()
                        && (al.get(i + 1).count < limit || doublecheck)) {
                    ch = '*';
                    if (!doublecheck) {
                        doublecheck = true;
                    } else {
                        doublecheck = false;
                    }
                } else {
                    doublecheck = false;
                }
            }
            if (befor == null) {
                indexSections.add(obj);
                buf.append(ch);
            } else if (!befor.equals(ch)) {
                // skip
            } else {
                indexSections.add(obj);
                buf.append(ch);
            }
            befor = ch;
        }

        return buf.toString();
    }

    public int searchListPosition(char key) {
        if (indexSections.size() == 0) {
            return -1;
        }
        if (key == '#') { // 記号
            return 0;
        } else {
            for (int i = 0; i < indexSections.size(); i++) {
                IndexValue c = indexSections.get(i);
                if (c.ch >= key) {
                    Logger.d("key = " + String.valueOf(key) + " " + "index ="
                            + Integer.toString(i) + " " + "Result = "
                            + String.valueOf(c));
                    return i;
                }
            }
        }
        return -1;
    }
}
