
package jp.co.kayo.android.localplayer.fragment;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.lang.ref.WeakReference;
import java.security.spec.MGF1ParameterSpec;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import jp.co.kayo.android.localplayer.AlbumartActivity;
import jp.co.kayo.android.localplayer.BaseActivity;
import jp.co.kayo.android.localplayer.BaseListFragment;
import jp.co.kayo.android.localplayer.EqualizerActivity;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.TabFragment;
import jp.co.kayo.android.localplayer.TagEditActivity;
import jp.co.kayo.android.localplayer.adapter.PlaybackListViewAdapter;
import jp.co.kayo.android.localplayer.appwidget.ColorSet;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioAlbum;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.core.ContentManager;
import jp.co.kayo.android.localplayer.core.ContextMenuFragment;
import jp.co.kayo.android.localplayer.core.FastOnTouchListener;
import jp.co.kayo.android.localplayer.core.IProgressView;
import jp.co.kayo.android.localplayer.core.ServiceBinderHolder;
import jp.co.kayo.android.localplayer.dialog.RatingDialog;
import jp.co.kayo.android.localplayer.menu.MediaContentActionCallback;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.provider.DeviceContentProvider;
import jp.co.kayo.android.localplayer.service.IMediaPlayerService;
import jp.co.kayo.android.localplayer.util.AnimationHelper;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.ImageObserver;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.ViewCache;
import jp.co.kayo.android.localplayer.util.bean.FavoriteInfo;
import jp.co.kayo.android.localplayer.util.bean.MediaData;
import jp.co.kayo.android.localplayer.util.bean.OrderInfo;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.CursorLoader;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.Loader;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnClickListener;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.util.TypedValue;
import android.view.LayoutInflater;

import android.view.ActionMode;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.ScaleAnimation;
import android.webkit.WebView.FindListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

@TargetApi(11)
public class PlaybackListViewFragment extends BaseListFragment implements
        ContentManager, ContextMenuFragment,
        OnItemLongClickListener, TabFragment, LoaderCallbacks<Cursor>,
        OnScrollListener, IProgressView, android.view.View.OnClickListener, OnLongClickListener {

    SharedPreferences mPref;
    PlaybackListViewAdapter mAdapter;
    private ViewCache mViewcache;
    Runnable mTask = null;
    private ActionMode mMode;
    Bitmap sourcebmp;
    ImageView imageAlbumArt;
    TextView txtTitle1;
    TextView txtTitle2;
    TextView txtTitle3;
    RatingBar ratingBar1;
    int imagesize;
    private String mTitle;
    private LinearLayout mPlayViewLayout;
    private LinearLayout mSongInfoLayout;
    private String mOrgTitle;
    private String mOrgArtist;
    private String mOrgAlbum;
    private int mTextHeight;

    private Handler myHandler = new UpdateMyHandler(this);

    private IMediaPlayerService getBinder() {
        if (getActivity() instanceof ServiceBinderHolder) {
            return ((ServiceBinderHolder) getActivity()).getBinder();
        } else {
            return null;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mViewcache = (ViewCache) getFragmentManager().findFragmentByTag(
                SystemConsts.TAG_CACHE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.playorder_grid_view, container,
                false);

        imageAlbumArt = (ImageView) root.findViewById(R.id.imageAlbumArt);
        txtTitle1 = (TextView) root.findViewById(R.id.txtTitle1);
        txtTitle2 = (TextView) root.findViewById(R.id.txtTitle2);
        txtTitle3 = (TextView) root.findViewById(R.id.txtTitle3);
        ratingBar1 = (RatingBar) root.findViewById(R.id.ratingBar1);

        imageAlbumArt.setOnClickListener(this);
        imageAlbumArt.setOnLongClickListener(this);
        View btnEq = root.findViewById(R.id.btnEq);

        ListView list = (ListView) root.findViewById(android.R.id.list);

        list.setOnItemLongClickListener(this);
        list.setOnScrollListener(this);

        if (btnEq != null) {
            if (Build.VERSION.SDK_INT >= 9) {
                btnEq.setOnClickListener(this);
            }
            else {
                btnEq.setVisibility(View.GONE);
            }
        }

        // 画面が縦向きならアルバムアートを拡大するためにアルバムアートレイアウトを取得
        Configuration config = getResources().getConfiguration();
        if (config.orientation == Configuration.ORIENTATION_PORTRAIT) {
            mPlayViewLayout = (LinearLayout) root
                    .findViewById(R.id.playViewLayout);
            mPlayViewLayout.setOnClickListener(this);
            mSongInfoLayout = (LinearLayout) root
                    .findViewById(R.id.songInfoLayout);
            txtTitle1.setTextSize(getResources().getDimension(R.dimen.scaleDownFontSize2));
            txtTitle2.setTextSize(getResources().getDimension(R.dimen.scaleDownFontSize2));
            txtTitle3.setTextSize(getResources().getDimension(R.dimen.scaleDownFontSize2));
        }

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        this.imagesize = getResources().getDimensionPixelSize(
                R.dimen.albumart_size);
        getLoaderManager().initLoader(R.layout.playorder_grid_view, null, this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        Logger.d("PlayOrder.onResume");
        if (getView() != null) {
            imageAlbumArt = (ImageView) getView().findViewById(R.id.imageAlbumArt);
            setAlbumImage();
        }

        // ヘッダーテキストの色を設定
        mViewcache.getColorset().setColor(ColorSet.KEY_DEFAULT_PRI_COLOR, txtTitle1);
        mViewcache.getColorset().setColor(ColorSet.KEY_DEFAULT_SEC_COLOR, txtTitle2);

    }

    @Override
    public void onPause() {
        // アルバムアートを縮小処理のタイマーが動いていたら停止
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
        if (imageAlbumArt != null) {
            imageAlbumArt.setImageBitmap(null);
            imageAlbumArt = null;
        }
        if (sourcebmp != null) {
            sourcebmp.recycle();
            sourcebmp = null;
        }
        super.onPause();
    }

    @Override
    public void reload() {
        getLoaderManager().restartLoader(R.layout.playorder_grid_view,
                null, this);
        if (mAdapter != null) {
            setAlbumImage();
        }
    }

    @Override
    public void changedMedia() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
            setAlbumImage();
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> arg0, View view,
            int position, long arg3) {
        Cursor selectedCursor = (Cursor) mAdapter.getItem(position);
        if (selectedCursor != null) {
            BaseActivity act = (BaseActivity) getActivity();
            mMode = act.startActionMode(new AnActionModeOfEpicProportions(
                    getActivity(), position, mHandler));
            TextView textView = (TextView) view.findViewById(R.id.text2);
            if (textView != null) {
                mTitle = textView.getText().toString();
            }
            else {
                mTitle = selectedCursor.getString(selectedCursor
                        .getColumnIndex(TableConsts.PLAYBACK_TITLE));
            }
            mMode.setTitle(mTitle);
            mMode.setSubtitle(getString(R.string.lb_tab_playlist));
            return true;
        }

        return false;
    }

    private final class AnActionModeOfEpicProportions extends
            MediaContentActionCallback {

        public AnActionModeOfEpicProportions(Context context,
                int selectedIndex, Handler handler) {
            super(context, selectedIndex, handler);
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mMode = null;
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {

            menu.add(getString(R.string.sub_mnu_remove))
                    .setIcon(R.drawable.ic_menu_delete)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.sub_mnu_rating))
                    .setIcon(R.drawable.ic_menu_star)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.sub_mnu_artist))
                    .setIcon(R.drawable.ic_menu_btn_artist)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.sub_mnu_album))
                    .setIcon(R.drawable.ic_menu_album)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.txt_web_more))
                    .setIcon(R.drawable.ic_menu_wikipedia)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.sub_mnu_edit))
                    .setIcon(R.drawable.ic_menu_strenc)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            boolean b = mPref.getBoolean("key.useLastFM", false);
            if (b) {
                menu.add(getString(R.string.sub_mnu_love)).setShowAsAction(
                        MenuItem.SHOW_AS_ACTION_IF_ROOM);
                menu.add(getString(R.string.sub_mnu_ban)).setShowAsAction(
                        MenuItem.SHOW_AS_ACTION_IF_ROOM);
            }

            if (!ContentsUtils.isSDCard(mPref)) {
                menu.add(getString(R.string.sub_mnu_clearcache))
                        .setIcon(R.drawable.ic_menu_refresh)
                        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
            }
            return true;
        }

    };
    
    private final String[] FETCH = new String[]{ AudioMedia._ID, AudioMedia.TITLE, AudioMedia.ARTIST, AudioMedia.ALBUM, AudioMedia.ALBUM_KEY, AudioMedia.DATA, AudioMedia.DURATION};
    private MediaData loadMediaItem(long id) {
        Cursor cursor = null;
        try{
            cursor = getActivity().getContentResolver().query(
                    ContentUris.withAppendedId(MediaConsts.MEDIA_CONTENT_URI, id), FETCH, null,
                    null, null);

            if(cursor.moveToFirst()){
                MediaData data = new MediaData(
                        0,
                        cursor.getLong(cursor.getColumnIndex(AudioMedia._ID)), 
                        MediaData.NOTPLAYED, 
                        cursor.getLong(cursor.getColumnIndex(AudioMedia.DURATION)), 
                        cursor.getString(cursor.getColumnIndex(AudioMedia.TITLE)), 
                        cursor.getString(cursor.getColumnIndex(AudioMedia.ALBUM)), 
                        cursor.getString(cursor.getColumnIndex(AudioMedia.ARTIST)), 
                        cursor.getString(cursor.getColumnIndex(AudioMedia.DATA)));
                return data;
            }
        }
        finally{
            if(cursor!=null){
                cursor.close();
            }
        }
        
        return null;
    }

    @Override
    protected void messageHandle(int what, final int selectedPosition) {
        Cursor selectedCursor = (Cursor) mAdapter.getItem(selectedPosition);
        if (selectedCursor != null) {
            long mediaId = selectedCursor.getLong(selectedCursor
                    .getColumnIndex(TableConsts.PLAYBACK_MEDIA_ID));
            
            MediaData data = loadMediaItem(mediaId);

            switch (what) {
                case SystemConsts.EVT_SELECT_RATING: {
                    if (mediaId >= 0) {
                        RatingDialog dlg = new RatingDialog(getActivity(),
                                TableConsts.FAVORITE_TYPE_SONG, mediaId, mHandler);
                        dlg.show();
                    }
                }
                    break;
                case SystemConsts.EVT_SELECT_ARTIST: {
                    String artist = data.getArtist();
                    FragmentTransaction t = getFragmentManager().beginTransaction();
                    AnimationHelper.setFragmentToPlayBack(t);
                    t.replace(R.id.fragment_main,
                            jp.co.kayo.android.localplayer.fragment.ArtistListFragment
                                    .createFragment(null, artist, -1));
                    t.addToBackStack(SystemConsts.TAG_SUBFRAGMENT);
                    t.commit();
                }
                    break;
                case SystemConsts.EVT_SELECT_DEL: {
                    String title = data.getTitle();
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            getActivity());

                    builder.setTitle(R.string.lb_confirm);
                    builder.setMessage(String.format(
                            getString(R.string.fmt_remove_orderlist), title));
                    builder.setPositiveButton(R.string.lb_ok,
                            new OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                        int which) {
                                    try {
                                        getBinder().setContentsKey(null);
                                        getBinder().remove(selectedPosition);
                                    } catch (RemoteException e) {
                                    }
                                }
                            });
                    builder.setNegativeButton(R.string.lb_cancel, null);
                    builder.show();
                }
                    break;
                case SystemConsts.EVT_SELECT_OPENALBUM: {
                    Hashtable<String, String> tbl = ContentsUtils.getMedia(
                            getActivity(), new String[] {
                                AudioMedia.ALBUM_KEY
                            },
                            mediaId);
                    String album_key = tbl.get(AudioMedia.ALBUM_KEY);
                    if (album_key != null) {
                        FragmentTransaction t = getFragmentManager().beginTransaction();
                        AnimationHelper.setFragmentToPlayBack(t);
                        t.replace(R.id.fragment_main, AlbumSongsFragment.createFragment(album_key));
                        t.addToBackStack(SystemConsts.TAG_SUBFRAGMENT);
                        t.commit();
                    }
                }
                    break;
                case SystemConsts.EVT_SELECT_MORE: {
                    String album = data.getAlbum();
                    String artist = data.getArtist();
                    String title = data.getTitle();
                    showInfoDialog(album, artist, title);
                }
                    break;
                case SystemConsts.EVT_SELECT_CLEARCACHE: {
                    String url = selectedCursor.getString(selectedCursor
                            .getColumnIndex(TableConsts.PLAYBACK_DATA));
                    ContentsUtils.clearMediaCache(getActivity(), url);
                    ContentValues values = new ContentValues();
                    values.put(TableConsts.AUDIO_CACHE_FILE, (String) null);
                    getActivity().getContentResolver().update(
                            ContentUris.withAppendedId(
                                    MediaConsts.MEDIA_CONTENT_URI, mediaId),
                            values, null, null);
                    datasetChanged();
                }
                    break;
                case SystemConsts.EVT_SELECT_LOVE: {
                    String album = data.getAlbum();
                    String artist = data.getArtist();
                    String title = data.getTitle();
                    long duration = data.getDuration();
                    ContentsUtils.lastfmLove(getActivity(), title, artist, album,
                            Long.toString(duration));
                }
                    break;
                case SystemConsts.EVT_SELECT_BAN: {
                    String album = data.getAlbum();
                    String artist = data.getArtist();
                    String title = data.getTitle();
                    long duration = data.getDuration();
                    ContentsUtils.lastfmBan(getActivity(), title, artist, album,
                            Long.toString(duration));
                }
                    break;
                case SystemConsts.EVT_SELECT_EDIT: {
                    // 文字化け修正
                    Intent intent = new Intent(getActivity(), TagEditActivity.class);
                    intent.putExtra(SystemConsts.KEY_EDITTYPE,
                            TagEditActivity.MEDIA);
                    intent.putExtra(SystemConsts.KEY_EDITKEY,
                            Long.toString(mediaId));
                    getActivity().startActivityForResult(intent, SystemConsts.REQUEST_TAGEDIT);
                }
                    break;
                default: {
                }
            }
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, final int position, long id) {
        final Cursor cursor = (Cursor) getListAdapter().getItem(position);
        final BaseActivity base = (BaseActivity) getActivity();
        final IMediaPlayerService binder = base.getBinder();
        final long media_id = cursor.getLong(cursor.getColumnIndex(TableConsts.PLAYBACK_MEDIA_ID));
        if (binder != null) {
            AsyncTask<Void, Void, Void> animTask = new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    try {
                        binder.stop();
                        binder.setPosition(position);
                        binder.play();

                        mViewcache.setCurrentPos(position);
                        mViewcache.setCurrentId(media_id);
                    } catch (RemoteException e) {
                    }
                    return null;
                }
            };
            animTask.execute();
        }
    }

    @Override
    public void release() {
        // TODO Auto-generated method stub

    }

    @Override
    public int getFragmentId() {
        return R.layout.playorder_grid_view;
    }

    String beforeSetting = null;

    public void setAlbumImage() {
        if (imageAlbumArt == null) {
            return;
        }
        final View myView = getView();
        final Context myContext = getActivity();
        if (myView == null || myContext == null) {
            return;
        }
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                String currentAlbumKey = null;
                long mediaId = mViewcache.getCurrentId();
                if (mediaId >= 0) {
                    View view = myView.findViewById(R.id.playViewLayout);
                    View btnEq = myView.findViewById(R.id.btnEq);
                    myHandler.sendMessage(myHandler.obtainMessage(EVT_SET_VISIBLE, view));
                    if (btnEq != null) {
                        myHandler.sendMessage(myHandler.obtainMessage(EVT_SET_VISIBLE, btnEq));
                    }

                    Hashtable<String, String> tbl = ContentsUtils.getMedia(myContext,
                            new String[] {
                                AudioMedia.ALBUM_KEY
                            }, mediaId);
                    if (tbl != null && tbl.size() > 0) {
                        currentAlbumKey = tbl.get(AudioMedia.ALBUM_KEY);
                        Hashtable<String, String> tbl1 = new Hashtable<String, String>();
                        Hashtable<String, String> tbl2 = new Hashtable<String, String>();
                        Bitmap bitmap = null;
                        // アルバム情報を取得
                        bitmap = Funcs.getAlbumArt(myContext, mediaId, -1, tbl1, tbl2);
                        if (bitmap != null) {
                            myHandler.sendMessage(myHandler.obtainMessage(EVT_SET_IMAGE, bitmap));
                        }
                        else if (tbl2.size() > 0) {
                            myHandler.sendMessage(myHandler.obtainMessage(EVT_SET_IMAGE, null));
                            mViewcache.getUnManagerImage(myContext, tbl2.get(AudioAlbum.ALBUM),
                                    tbl2.get(AudioAlbum.ARTIST), currentAlbumKey,
                                    new MyImageObserver(), imagesize);
                        }
                        // レーティングを設定
                        FavoriteInfo inf = mViewcache.getFavorite(myContext, mediaId,
                                TableConsts.FAVORITE_TYPE_SONG);
                        myHandler.sendMessage(myHandler.obtainMessage(EVT_SET_TEXT1,
                                tbl1.get(AudioAlbum.TITLE)));
                        myHandler.sendMessage(myHandler.obtainMessage(EVT_SET_TEXT2,
                                tbl1.get(AudioAlbum.ALBUM)));
                        myHandler.sendMessage(myHandler.obtainMessage(EVT_SET_TEXT3,
                                tbl1.get(AudioAlbum.ARTIST)));
                        myHandler.sendMessage(myHandler.obtainMessage(EVT_SET_RATING, inf));
                    }
                } else {
                    View view = myView.findViewById(R.id.playViewLayout);
                    View btnEq = myView.findViewById(R.id.btnEq);
                    myHandler.sendMessage(myHandler.obtainMessage(EVT_SET_GONE, view));
                    if (btnEq != null) {
                        myHandler.sendMessage(myHandler.obtainMessage(EVT_SET_GONE, btnEq));
                    }
                }
                return null;
            }

        };
        task.execute();
    }

    private void setBitmap(Bitmap bm) {
        if (imageAlbumArt == null) {
            if(bm!=null){
                bm.recycle();
                bm = null;
            }
            return;
        }
        if(sourcebmp!=null){
            sourcebmp.recycle();
            sourcebmp = null;
        }
        sourcebmp = bm;
        imageAlbumArt.setImageBitmap(bm);
        imageAlbumArt.requestLayout();
    }

    class MyImageObserver implements ImageObserver {

        @Override
        public void onLoadImage(final Bitmap bmp) {
            myHandler.sendMessage(myHandler.obtainMessage(EVT_SET_IMAGE, bmp));
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Logger.d("onCreateLoader:"+mAdapter);
        Uri orderuri = Uri.parse(DeviceContentProvider.MEDIA_CONTENT_AUTHORITY_SLASH
                + "order/audio");
        if (mAdapter == null) {
            mAdapter = new PlaybackListViewAdapter(getActivity(), null, mViewcache);
            getListView().setOnTouchListener(
                    new FastOnTouchListener(new Handler(), mAdapter));
            setListAdapter(mAdapter);
            mHandler.setAdapter(mAdapter);
        } else {
            Cursor cur = mAdapter.swapCursor(null);
            if (cur != null) {
                cur.close();
            }
        }

        showProgressBar();
        String cur_contenturi = mPref.getString(SystemConsts.PREF_CONTENTURI,
                DeviceContentProvider.MEDIA_AUTHORITY);
        return new CursorLoader(getActivity(), orderuri, null,
                TableConsts.PLAYBACK_URI + " = ?", new String[] {
                    cur_contenturi
                }, TableConsts.PLAYBACK_ORDER);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader,
            Cursor data) {
        Logger.d("onLoadFinished:"+mAdapter);
        try {
            if (mAdapter == null) {
                mAdapter = new PlaybackListViewAdapter(getActivity(), data, mViewcache);
                getListView().setOnTouchListener(
                        new FastOnTouchListener(new Handler(), mAdapter));
                setListAdapter(mAdapter);
                mHandler.setAdapter(mAdapter);
            }
            else if (mAdapter != null) {
                if (data == null || data.isClosed()) {
                    if (data == null)
                        mAdapter.swapCursor(null);
                    return;
                }
                Cursor cur = mAdapter.swapCursor(data);
            }
        } finally {
            hideProgressBar();
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        hideProgressBar();
    }

    @Override
    public void startProgress(final long max) {
        if (mAdapter != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mViewcache.startProgress(max);
                    IMediaPlayerService binder = getBinder();
                    if (binder != null) {
                        try {
                            mViewcache.setPrefetchId(binder.getPrefetchId());
                        } catch (RemoteException e) {
                        }
                    }
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public void stopProgress() {
        if (mAdapter != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mViewcache.stopProgress();
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public void progress(final long pos, final long max) {
        if (mAdapter != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mViewcache.progress(pos, max);
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
            int visibleItemCount, int totalItemCount) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        Logger.d("onScrollStateChanged:" + scrollState);
        long time = Funcs.getHideTime(mPref);
        if (time > 0) {
            if (scrollState == 1) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                if (getFragmentManager() != null) {
                    ControlFragment control = (ControlFragment) getFragmentManager()
                            .findFragmentByTag(SystemConsts.TAG_CONTROL);
                    if (control != null) {
                        control.hideControl(false);
                    }
                }
            } else if (scrollState == 0) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                mTask = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (getFragmentManager() != null) {
                                ControlFragment control = (ControlFragment) getFragmentManager()
                                        .findFragmentByTag(
                                                SystemConsts.TAG_CONTROL);
                                if (control != null) {
                                    control.showControl(false);
                                }
                            }
                        } finally {
                            mTask = null;
                        }
                    }
                };
                mHandler.postDelayed(mTask, time);
            }
        }
    }

    @Override
    public boolean onBackPressed() {
        if (mMode != null) {
            mMode.finish();
            mMode = null;
            return true;
        }
        return false;
    }

    @Override
    public String selectSort() {
        return null;
    }

    @Override
    protected void hideMenu() {
        if (mMode != null) {
            mMode.finish();
            mMode = null;
        }
    }

    @Override
    protected void datasetChanged() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void addRow(Object[] values) {
        // TODO Auto-generated method stub

    }

    private Timer mTimer = null;
    private boolean mAlbumartScale = false;
    private int mOrgLayoutSize;
    private int mScaleLayoutSize;

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnEq) {
            // イコライザー画面
            Intent intent = new Intent(getActivity(), EqualizerActivity.class);
            startActivity(intent);
        }
        else {
            Configuration config = getResources().getConfiguration();
            if (config.orientation == Configuration.ORIENTATION_PORTRAIT) {
                int id = v.getId();
                switch (id) {
                    case R.id.imageAlbumArt:
                        if (mTimer == null) {
                            if (mAlbumartScale == false) {
                                // アルバムアートの拡大
                                scaleUpAlbumArt();
                                mAlbumartScale = true;
                            } else {
                                // アルバムアートを元のサイズに戻す
                                scaleDownAlbumArt();
                                mAlbumartScale = false;
                            }
                        }
                        break;
                    case R.id.playViewLayout:
                        if (mTimer == null) {
                            if (mAlbumartScale == true) {
                                // アルバムアートを元のサイズに戻す
                                scaleDownAlbumArt();
                                mAlbumartScale = false;
                            }
                        }
                        break;
                }
            }
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if (v.getId() == R.id.imageAlbumArt) {
            long mediaId = mViewcache.getCurrentId();
            if (mediaId >= 0) {
                Intent i = new Intent(getActivity(), AlbumartActivity.class);
                i.putExtra("media_key", mediaId);
                getActivity().startActivityForResult(i, SystemConsts.REQUEST_ALBUMART);
                return true;
            }
        }
        return false;
    }

    /**
     * アルバムアートを拡大
     */
    private void scaleUpAlbumArt() {
        // 背景のCDジャケットを消す
        imageAlbumArt.setBackgroundColor(Color.argb(00, 00, 00, 00));

        // 拡大サイズを設定
        mOrgLayoutSize = mPlayViewLayout.getHeight();
        View controller = getActivity().findViewById(R.id.controlLinearLayout01);
        mScaleLayoutSize = getView().findViewById(R.id.mainPlayWrapperFrame).getHeight()
                - controller.getHeight();

        // 拡大アニメーション開始
        float toX = Float.valueOf(getString(R.string.scaleX));
        float toY = Float.valueOf(getString(R.string.scaleY));
        int pivotX = getResources().getInteger(R.integer.pivotX);
        int pivotY = getResources().getInteger(R.integer.pivotY);

        ScaleAnimation scale = new ScaleAnimation(1, toX, 1, toY, pivotX,
                pivotY);
        scale.setDuration(300);
        scale.setFillAfter(true);
        imageAlbumArt.startAnimation(scale);

        // アルバムアートの拡大に合わせてレイアウトの高さを変更
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, mScaleLayoutSize);
        mPlayViewLayout.setOrientation(LinearLayout.VERTICAL);
        mPlayViewLayout.setLayoutParams(params);

        // 曲情報をアルバムアートの下に移動
        TypedValue typedValue = new TypedValue();
        getActivity().getTheme().resolveAttribute(R.attr.albumBackground, typedValue, true);
        int resource_id = typedValue.resourceId;

        mSongInfoLayout.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL);
        float bottomDp = Funcs.px2Dip(getActivity(), 10);
        mSongInfoLayout.setPadding(0, 0, 0, (int) bottomDp);

        mTextHeight = txtTitle1.getHeight();
        txtTitle1.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
        txtTitle2.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
        txtTitle3.setVisibility(View.GONE);
        ratingBar1.setVisibility(View.VISIBLE);

        // テキスト情報を2行表示に変更
        mOrgTitle = txtTitle1.getText().toString();
        mOrgArtist = txtTitle2.getText().toString();
        mOrgAlbum = txtTitle3.getText().toString();
        txtTitle1.setText(mOrgTitle + " - " + mOrgAlbum);
        txtTitle2.setText(mOrgArtist);

        // font size
        txtTitle1.setTextSize(getResources().getDimension(R.dimen.scaleUpFontSize2));
        txtTitle2.setTextSize(getResources().getDimension(R.dimen.scaleUpFontSize2));
        txtTitle3.setTextSize(getResources().getDimension(R.dimen.scaleUpFontSize2));

        // ソングリストを隠す
        ListView listView = getListView();
        if (listView != null) {
            listView.setVisibility(View.INVISIBLE);
        }

        // 再生コントロールを表示状態にする
        ControlFragment control = (ControlFragment) getFragmentManager()
                .findFragmentByTag("tag_control");
        control.showControl(true);
    }

    /**
     * アルバムアートを縮小
     */
    private void scaleDownAlbumArt() {
        // 拡大サイズを設定
        mScaleLayoutSize = mOrgLayoutSize;

        // 縮小アニメーション開始
        float fromX = Float.valueOf(getResources().getString(R.string.scaleX));
        float fromY = Float.valueOf(getResources().getString(R.string.scaleY));
        int pivotX = getResources().getInteger(R.integer.pivotX);
        int pivotY = getResources().getInteger(R.integer.pivotY);
        ScaleAnimation scale = new ScaleAnimation(fromX, 1, fromY, 1, pivotX,
                pivotY);
        scale.setDuration(300);
        scale.setFillAfter(true);
        imageAlbumArt.startAnimation(scale);

        // アルバムアートの拡大に合わせてレイアウトの高さを変更
        if (mTimer == null) {
            mTimer = new Timer(true);
            final Handler handler = new Handler();
            mTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        public void run() {
                            // 背景のCDジャケットをセット
                            imageAlbumArt
                                    .setBackgroundResource(R.drawable.album_border_mini);

                            // アルバムアート表示領域を元に戻す
                            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                                    ViewGroup.LayoutParams.MATCH_PARENT,
                                    mOrgLayoutSize);
                            mPlayViewLayout
                                    .setOrientation(LinearLayout.HORIZONTAL);
                            mPlayViewLayout.setLayoutParams(params);

                            // font size
                            txtTitle1.setTextSize(getResources().getDimension(
                                    R.dimen.scaleDownFontSize2));
                            txtTitle2.setTextSize(getResources().getDimension(
                                    R.dimen.scaleDownFontSize2));
                            txtTitle3.setTextSize(getResources().getDimension(
                                    R.dimen.scaleDownFontSize2));

                            // 曲情報を元の位置に戻す
                            txtTitle1.setText(mOrgTitle);
                            txtTitle2.setText(mOrgArtist);
                            txtTitle3.setText(mOrgAlbum);
                            txtTitle1.setHeight(mTextHeight);
                            txtTitle2.setHeight(mTextHeight);
                            txtTitle3.setHeight(mTextHeight);
                            txtTitle1.setGravity(Gravity.BOTTOM);
                            txtTitle2.setGravity(Gravity.BOTTOM);
                            txtTitle3.setVisibility(View.VISIBLE);
                            mSongInfoLayout.setGravity(Gravity.LEFT | Gravity.TOP);
                            mSongInfoLayout.setPadding(0, 0, 0, 0);
                            ratingBar1.setVisibility(View.INVISIBLE);

                            // ソングリストを表示
                            ListView listView = getListView();
                            if (listView != null) {
                                listView.setVisibility(View.VISIBLE);
                            }

                            if (mTimer != null) {
                                mTimer.cancel();
                                mTimer = null;
                            }
                        }
                    });
                }
            }, 200, 1000);
        }
    }

    @Override
    public String getName(Context context) {
        return context.getString(R.string.lb_tab_order);
    }

    @Override
    public void doSearchQuery(String queryString) {
        // TODO Auto-generated method stub

    }

    static final int EVT_SET_TEXT1 = 100;
    static final int EVT_SET_TEXT2 = 101;
    static final int EVT_SET_TEXT3 = 102;
    static final int EVT_SET_RATING = 103;
    static final int EVT_SET_IMAGE = 104;
    static final int EVT_SET_VISIBLE = 105;
    static final int EVT_SET_GONE = 106;

    static class UpdateMyHandler extends Handler {
        WeakReference<PlaybackListViewFragment> ref;

        UpdateMyHandler(PlaybackListViewFragment f) {
            ref = new WeakReference<PlaybackListViewFragment>(f);
        }

        @Override
        public void handleMessage(Message msg) {
            PlaybackListViewFragment f = ref.get();
            if (f != null) {
                switch (msg.what) {
                    case EVT_SET_VISIBLE: {
                        View view = (View) msg.obj;
                        view.setVisibility(View.VISIBLE);
                    }
                        break;
                    case EVT_SET_GONE: {
                        View view = (View) msg.obj;
                        view.setVisibility(View.GONE);
                    }
                        break;
                    case EVT_SET_TEXT1: {
                        f.mOrgTitle = (String) msg.obj;
                        f.txtTitle1.setText(f.mOrgTitle);

                    }
                        break;
                    case EVT_SET_TEXT2: {
                        f.mOrgArtist = (String) msg.obj;
                        f.txtTitle2.setText(f.mOrgArtist);
                    }
                        break;
                    case EVT_SET_TEXT3: {
                        f.mOrgAlbum = (String) msg.obj;
                        f.txtTitle3.setText(f.mOrgAlbum);
                    }
                        break;
                    case EVT_SET_RATING: {
                        FavoriteInfo inf = (FavoriteInfo) msg.obj;
                        f.ratingBar1.setRating(inf.rating);
                    }
                        break;
                    case EVT_SET_IMAGE: {
                        f.setBitmap((Bitmap) msg.obj);
                    }
                        break;
                }
            }
        }
    }
}
