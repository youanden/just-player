
package jp.co.kayo.android.localplayer.fragment;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.ArrayList;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;

import jp.co.kayo.android.localplayer.BaseActivity;
import jp.co.kayo.android.localplayer.BaseListFragment;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.adapter.PlaylistListAdapter;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioArtist;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioPlaylist;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioPlaylistMember;
import jp.co.kayo.android.localplayer.core.ContentManager;
import jp.co.kayo.android.localplayer.core.ContextMenuFragment;
import jp.co.kayo.android.localplayer.dialog.NewPlaylistDialog;
import jp.co.kayo.android.localplayer.dialog.DeletePlaylistDialog.DeleteItems;
import jp.co.kayo.android.localplayer.fragment.ControlFragment.MyHandler;
import jp.co.kayo.android.localplayer.menu.MediaContentActionCallback;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.task.AsyncAddPlaylistTask;
import jp.co.kayo.android.localplayer.util.AnimationHelper;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.ViewCache;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.format.DateFormat;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;

public class PlaylistViewFragment extends BaseListFragment implements
        ContentManager, ContextMenuFragment, LoaderCallbacks<Cursor>, OnItemLongClickListener {
    private SharedPreferences mPref;
    private ViewCache mViewcache;
    private PlaylistListAdapter mAdapter;

    private ActionMode mMode;
    private String mQueryString;

    String[] fetchcols = new String[] {
            AudioPlaylist._ID, AudioPlaylist.NAME,
            AudioPlaylist.PLAYLIST_KEY, AudioPlaylist.DATE_ADDED,
            AudioPlaylist.DATE_MODIFIED
    };

    @Override
    protected void hideMenu() {
        if (mMode != null) {
            mMode.finish();
            mMode = null;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.playlist_grid_view, null, false);

        ListView list = (ListView) view
                .findViewById(android.R.id.list);
        list.setOnItemLongClickListener(this);
        list.addHeaderView(makeHeaderView());

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mViewcache = (ViewCache) getFragmentManager().findFragmentByTag(
                SystemConsts.TAG_CACHE);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getLoaderManager().initLoader(R.layout.playlist_grid_view, null, this);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getLoaderManager().destroyLoader(R.layout.playlist_grid_view);
    }
    
    public View makeHeaderView() {
        LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View countView = inflater.inflate(R.layout.newplaylist_header, null);
        
       
        return countView;
      }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String selection = null;
        String[] selectionArgs = null;
        if (mAdapter == null) {
            mAdapter = new PlaylistListAdapter(getActivity(), null, mViewcache);
            setListAdapter(mAdapter);
        } else {
            Cursor cur = mAdapter.swapCursor(null);
            if (cur != null) {
                cur.close();
            }
        }

        if (Funcs.isNotEmpty(mQueryString)) {
            selection = MediaConsts.AudioPlaylist.NAME + " like '%' || ? || '%'";
            selectionArgs = new String[] {
                    mQueryString
            };
        }

        showProgressBar();
        return new CursorLoader(getActivity(),
                MediaConsts.PLAYLIST_CONTENT_URI, null, selection, selectionArgs,
                MediaConsts.AudioPlaylist.DATE_ADDED);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        try {
            hideProgressBar();
            if (mAdapter != null) {
                if (data == null || data.isClosed()) {
                    if(data==null) mAdapter.swapCursor(null);
                    return;
                }
                Cursor cur = mAdapter.swapCursor(data);
                if (cur != null) {
                    cur.close();
                }
            }
        } finally {
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> arg0) {
        hideProgressBar();
    }

    @Override
    public void onListItemClick(ListView l, View v, int pos, long id) {
        if(pos > 0){
            int position = pos-1;
            Cursor cursor = (Cursor) getListAdapter().getItem(position);
            if (cursor != null) {
                long playlistId = cursor.getLong(cursor.getColumnIndex(AudioPlaylist._ID));
    
                FragmentTransaction t = getFragmentManager().beginTransaction();
                AnimationHelper.setFragmentToPlayBack(t);
                t.replace(R.id.fragment_main, PlaylistFragment.createFragment(playlistId));
                t.addToBackStack(SystemConsts.TAG_SUBFRAGMENT);
                t.commit();
            }
        }else{
            NewPlaylistDialog dlg = new NewPlaylistDialog();
            dlg.show(getFragmentManager(), "NewPlayDialog");
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> arg0, View view,
            int pos, long arg3) {
        if(pos > 0){
            int position = pos-1;
            Cursor selectedCursor = (Cursor) mAdapter.getItem(position);
            if (selectedCursor != null) {
                java.text.DateFormat format = DateFormat.getMediumDateFormat(getActivity());
                BaseActivity act = (BaseActivity) getActivity();
                long modified = selectedCursor.getLong(selectedCursor
                        .getColumnIndex(MediaConsts.AudioPlaylist.DATE_MODIFIED));
                mMode = act.startActionMode(new AnActionModeOfEpicProportions(
                        getActivity(), position, mHandler));
                mMode.setTitle(selectedCursor.getString(selectedCursor
                        .getColumnIndex(MediaConsts.AudioPlaylist.NAME)));
                mMode.setSubtitle(format.format(modified));
                return true;
            }
        }
        return false;
    }

    @Override
    public void reload() {
        getLoaderManager().restartLoader(R.layout.playlist_grid_view, null, this);
    }

    @Override
    public void release() {
    }

    @Override
    public void changedMedia() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public String getName(Context context) {
        return context.getString(R.string.lb_tab_playlist);
    }

    @Override
    public boolean onBackPressed() {
        if (mMode != null) {
            mMode.finish();
            mMode = null;
            return true;
        }
        return false;
    }

    @Override
    public String selectSort() {
        return null;
    }

    @Override
    public void doSearchQuery(String queryString) {
        boolean dirty = mQueryString == null || !mQueryString.equals(queryString);
        if (dirty) {
            mQueryString = queryString;
            reload();
        }
    }

    @Override
    protected void datasetChanged() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void addRow(Object[] values) {
    }

    @Override
    protected void messageHandle(int what, int selectedPosition) {
        Cursor selectedCursor = (Cursor) mAdapter.getItem(selectedPosition);
        if (selectedCursor != null) {
            switch (what) {
                case SystemConsts.EVT_SELECT_DEL: {
                    long playlistId = selectedCursor.getLong(selectedCursor
                            .getColumnIndex(AudioPlaylist._ID));
                    String name = selectedCursor.getString(selectedCursor
                            .getColumnIndex(MediaConsts.AudioPlaylist.NAME));
                    deletePlaylist(playlistId, name);
                }
                    break;
                case SystemConsts.EVT_SELECT_ADD: {
                    long playlistId = selectedCursor.getLong(selectedCursor
                            .getColumnIndex(AudioPlaylist._ID));
                    AsyncAddPlaylistTask task = new AsyncAddPlaylistTask(getActivity(),
                            getFragmentManager(), ContentUris.withAppendedId(
                                    MediaConsts.PLAYLIST_CONTENT_URI, playlistId),
                            AudioPlaylistMember.AUDIO_ID, null, null, AudioPlaylistMember.PLAY_ORDER);
                    task.execute();
                }
                    break;
            }
        }

    }

    private void deletePlaylist(final long playlistid, String title) {
        final ContentResolver resolver = getActivity().getContentResolver();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.lb_confirm));
        builder.setMessage(String.format(
                getString(R.string.fmt_remove_orderlist), title));
        builder.setPositiveButton(getString(R.string.lb_ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        resolver.delete(
                                ContentUris.withAppendedId(MediaConsts.PLAYLIST_CONTENT_URI,
                                        playlistid),
                                null, null);
                        resolver.delete(
                                MediaConsts.PLAYLIST_CONTENT_URI,
                                AudioPlaylist._ID + " = ?", new String[] {
                                    Long.toString(playlistid)
                                });
                        reload();
                    }
                });
        builder.setNegativeButton(getString(R.string.lb_cancel), null);
        builder.setCancelable(true);
        AlertDialog dlg = builder.create();
        dlg.show();
    }

    private final class AnActionModeOfEpicProportions extends
            MediaContentActionCallback {

        public AnActionModeOfEpicProportions(Context context,
                int selectedIndex, Handler handler) {
            super(context, selectedIndex, handler);
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mMode = null;
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {

            menu.add(getString(R.string.sub_mnu_order))
                    .setIcon(R.drawable.ic_menu_btn_add)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.sub_mnu_remove))
                    .setIcon(R.drawable.ic_menu_remove)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            return true;
        }

    };

}
