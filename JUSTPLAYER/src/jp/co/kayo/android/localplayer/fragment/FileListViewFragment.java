package jp.co.kayo.android.localplayer.fragment;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.File;

import jp.co.kayo.android.localplayer.BaseActivity;
import jp.co.kayo.android.localplayer.BaseListFragment;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.TabFragment;
import jp.co.kayo.android.localplayer.adapter.FileListViewAdapter;
import jp.co.kayo.android.localplayer.appwidget.AppWidgetHelper;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.consts.MediaConsts.FileMedia;
import jp.co.kayo.android.localplayer.consts.MediaConsts.FileType;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.core.ContentManager;
import jp.co.kayo.android.localplayer.core.FastOnTouchListener;
import jp.co.kayo.android.localplayer.core.ContextMenuFragment;
import jp.co.kayo.android.localplayer.core.ServiceBinderHolder;
import jp.co.kayo.android.localplayer.dialog.RatingDialog;
import jp.co.kayo.android.localplayer.menu.MediaContentActionCallback;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.service.IMediaPlayerService;
import jp.co.kayo.android.localplayer.service.StreamCacherServer;
import jp.co.kayo.android.localplayer.util.AnimationHelper;
import jp.co.kayo.android.localplayer.util.BaseAnimationListener;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.ViewCache;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class FileListViewFragment extends BaseListFragment implements
        ContentManager, OnItemLongClickListener, ContextMenuFragment,
        TabFragment, LoaderCallbacks<Cursor>, OnScrollListener {
    private ViewCache viewcache;
    private final String SORTKEY = "folderview.sort";
    SharedPreferences mPref;
    FileListViewAdapter mAdapter;
    String sort_cname;
    private ActionMode mMode;

    Runnable mTask = null;

    private IMediaPlayerService getBinder() {
        if (getActivity() instanceof ServiceBinderHolder) {
            return ((ServiceBinderHolder) getActivity()).getBinder();
        } else {
            return null;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        viewcache = (ViewCache) getFragmentManager().findFragmentByTag(
                SystemConsts.TAG_CACHE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.albumart_list_view, container,
                false);

        ListView list = (ListView) root
                .findViewById(android.R.id.list);
        list.setOnItemLongClickListener(this);
        list.setOnScrollListener(this);

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getLoaderManager().initLoader(getFragmentId(), null, this);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getLoaderManager().destroyLoader(getFragmentId());
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view,
            final int position, long id) {
        Cursor selectedCursor = (Cursor) getListAdapter().getItem(position);
        if (selectedCursor != null) {
            String type = selectedCursor.getString(selectedCursor
                    .getColumnIndex(FileMedia.TYPE));
            String title = selectedCursor.getString(selectedCursor
                    .getColumnIndex(FileMedia.TITLE));
            String data = selectedCursor.getString(selectedCursor
                    .getColumnIndex(FileMedia.DATA));
            if (type != null) {
                // MEDIA
                if (type.equals(FileType.FOLDER)|| type.equals(FileType.ZIP)) {
                    BaseActivity act = (BaseActivity) getActivity();
                    mMode = act.startActionMode(new AnActionModeOfEpicProportions(
                            getActivity(), position, mHandler));
                    mMode.setTitle(title);
                    mMode.setSubtitle(getString(R.string.lb_tab_folder));
                    return true;
                }
            }
        }
        return false;
    }

    private final class AnActionModeOfEpicProportions extends
            MediaContentActionCallback {

        public AnActionModeOfEpicProportions(Context context,
                int selectedIndex, Handler handler) {
            super(context, selectedIndex, handler);
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mMode = null;
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {

            menu.add(getString(R.string.sub_mnu_order))
            .setIcon(R.drawable.ic_menu_btn_add)
            .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
            
            return true;
        }

    };

    @Override
    protected void messageHandle(int what, int selectedPosition) {
        Cursor selectedCursor = (Cursor) mAdapter.getItem(selectedPosition);
        if (selectedCursor != null) {
            long mediaId = selectedCursor.getLong(selectedCursor
                    .getColumnIndex(FileMedia._ID));
            String type = selectedCursor.getString(selectedCursor
                    .getColumnIndex(FileMedia.TYPE));
            String data = selectedCursor.getString(selectedCursor
                    .getColumnIndex(FileMedia.DATA));

            switch (what) {
            case SystemConsts.EVT_SELECT_RATING: {
                RatingDialog dlg = new RatingDialog(getActivity(),
                        TableConsts.FAVORITE_TYPE_ALBUM, mediaId, mHandler);
                dlg.show();
            }
                break;
            case SystemConsts.EVT_SELECT_PLAY: {
                Toast.makeText(getActivity(),
                        getActivity().getString(R.string.txt_action_addalbum),
                        Toast.LENGTH_SHORT).show();
            }
                break;
            case SystemConsts.EVT_SELECT_ADD: {
                // フォルダ、ファイル単位で追加
                if(type.equals(FileType.FOLDER)|| type.equals(FileType.ZIP)){
                    ContentsUtils.addOrderFolder(getBinder(), getActivity(), data, getSort()[1]);

                    Toast.makeText(getActivity(),
                            getActivity().getString(R.string.txt_action_addalbum),
                            Toast.LENGTH_SHORT).show();
                }
            }
                break;
            case SystemConsts.EVT_SELECT_DOWNLOAD: {
                // フォルダ、ファイル単位でダウンロード
            }
                break;
            default: {
            }
            }
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, final int position, long id) {
        final Cursor cursor = (Cursor) getListAdapter().getItem(position);
        if (cursor != null) {
            // Folderならさらに掘る、そうでないなら再生する
            final String type = cursor.getString(cursor
                    .getColumnIndex(FileMedia.TYPE));
            final String title = cursor.getString(cursor
                    .getColumnIndex(FileMedia.TITLE));
            final String data = cursor.getString(cursor
                    .getColumnIndex(FileMedia.DATA));
            if (type != null) {
                // MEDIA
                if (type.equals(FileType.AUDIO)
                        || type.equals(FileType.ZIPENTRY)) {
                    final BaseActivity base = (BaseActivity)getActivity();
                    final IMediaPlayerService binder = base.getBinder();
                    AsyncTask<Void, Void, Void> animTask = new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... params) {
                            playMediaReplace(getActivity(), binder, cursor, type, title, data, position);
                            return null;
                        }
                    };
                    animTask.execute();
                    //AnimationHelper.setViewToPlayback(v, new BaseAnimationListener((BaseActivity)getActivity()) {});
                } else if (type.equals(FileType.VIDEO)) {
                    // VIDEO
                    if (data != null && data.length() > 0) {
                        String ext = "*";
                        int lastDot = data.lastIndexOf('.');
                        if (lastDot > 0) {
                            ext = data.substring(lastDot + 1);
                        }
                        String mime = "video/" + ext;

                        if (!ContentsUtils.isSDCard(mPref)) {
                            Cursor cur = null;
                            try {
                                String geturl = StreamCacherServer
                                        .getContentUri(getActivity(), data);
                                if (geturl != null) {
                                    Uri uri = Uri.parse(geturl);
                                    Intent intent = new Intent(
                                            Intent.ACTION_VIEW);
                                    intent.setDataAndType(uri, mime);
                                    startActivity(intent);

                                    // 動画再生前に音楽一時停止
                                    IMediaPlayerService binder = getBinder();
                                    if (binder != null) {
                                        int stat;
                                        try {
                                            stat = binder.stat();
                                            if ((stat & AppWidgetHelper.FLG_PLAY) > 0) {
                                                binder.pause();
                                            }
                                        } catch (RemoteException e) {
                                        }
                                    }
                                } else {
                                    // TODO URLの取得に失敗してエラー、データがおかしいおそれ
                                }
                            } catch (Exception e) {
                                String mes = String.format(
                                        getString(R.string.fmt_intent_error),
                                        mime);
                                Toast.makeText(this.getActivity(), mes,
                                        Toast.LENGTH_SHORT).show();
                            } finally {
                                if (cur != null) {
                                    cur.close();
                                }
                            }
                        } else {
                            File file = new File(data);
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(Uri.fromFile(file), mime);
                            try {
                                startActivity(intent);

                                // 動画再生前に音楽一時停止
                                IMediaPlayerService binder = getBinder();
                                if (binder != null) {
                                    try {
                                        binder.pause();
                                    } catch (RemoteException e) {
                                    }
                                }
                            } catch (Exception e) {
                                String mes = String.format(
                                        getString(R.string.fmt_intent_error),
                                        mime);
                                Toast.makeText(this.getActivity(), mes,
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putString("path", data);
                    getLoaderManager().restartLoader(getFragmentId(), bundle,
                            this);
                }
            }
        }
    }

    @Override
    public void reload() {
        getLoaderManager().restartLoader(getFragmentId(), null, this);
    }

    @Override
    public void changedMedia() {

    }

    @Override
    public void release() {
        // TODO Auto-generated method stub

    }
    
    private boolean playMediaReplace(Context context, IMediaPlayerService binder, Cursor cursor, String type,
            String title, String data, int position){
        try{
            binder.setContentsKey(SystemConsts.CONTENTSKEY_FOLDER
                    + System.currentTimeMillis());
            binder.lockUpdateToPlay();
            binder.clearcut();
            // cursor.moveToFirst();
            do {
                type = cursor.getString(cursor
                        .getColumnIndex(FileMedia.TYPE));
                if (type != null
                        && (type.equals(FileType.AUDIO) || type
                                .equals(FileType.ZIPENTRY))) {
                    long file_id = cursor.getLong(cursor
                            .getColumnIndex(FileMedia._ID));
                    title = cursor.getString(cursor
                            .getColumnIndex(FileMedia.TITLE));
                    data = cursor.getString(cursor
                            .getColumnIndex(FileMedia.DATA));
    
                    Cursor file_cur = null;
                    try {
                        file_cur = ContentsUtils.getFileItemCursor(context, data, type);
                        addMedia(binder, file_cur, title, data);
                    } finally {
                        if (file_cur != null) {
                            file_cur.close();
                        }
                    }
                }
            } while (cursor.moveToNext());
            binder.play();
            return true;
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        finally{
            cursor.moveToPosition(position);
        }
        return false;
    }
    
    private void addMedia(IMediaPlayerService binder, Cursor file_cur, String file_title, String file_data){
        if (file_cur != null
                && file_cur.moveToFirst()) {
            long media_id = file_cur
                    .getLong(file_cur
                            .getColumnIndex(AudioMedia._ID));
            String data = file_cur
                    .getString(file_cur
                            .getColumnIndex(AudioMedia.DATA));
            if (media_id > 0) {
                try {
                    binder.addMedia(media_id, data);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            } else {
                String title = file_cur
                        .getString(file_cur
                                .getColumnIndex(AudioMedia.TITLE));
                String album = file_cur
                        .getString(file_cur
                                .getColumnIndex(AudioMedia.ALBUM));
                String artist = file_cur
                        .getString(file_cur
                                .getColumnIndex(AudioMedia.ARTIST));
                if (title == null) {
                    title = file_title;
                }
                if (album == null) {
                    album = getString(R.string.txt_unknown_album);
                }
                if (artist == null) {
                    artist = getString(R.string.txt_unknown_artist);
                }
                try {
                    binder.addMediaD(media_id, 0,
                            title, album, artist,
                            file_data);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        } else {
            try {
                binder.addMediaD(
                        Funcs.makeSubstansId(file_data),
                        0, file_title, getString(R.string.txt_unknown_album),
                        getString(R.string.txt_unknown_artist), file_data);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String selectSort() {
        int sort = mPref.getInt(SORTKEY, 1);
        sort++;

        if (sort > 1) {
            sort = 0;
        }
        Editor editor = mPref.edit();
        editor.putInt(SORTKEY, sort);
        editor.commit();
        getLoaderManager().restartLoader(getFragmentId(), null, this);

        return getName(getActivity());
    }

    @Override
    public int getFragmentId() {
        return R.layout.albumart_list_view;
    }

    private String[] getSort() {
        int sort = mPref.getInt(SORTKEY, 1);
        String cname = FileMedia.TITLE;
        String sortOrder = FileMedia.TITLE + "," + FileMedia.TYPE;
        if (sort == 0) {
            sortOrder = FileMedia.DATE_MODIFIED + "," + FileMedia.TYPE;
        }

        return new String[] { cname, sortOrder };
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] sortname = getSort();
        if (mAdapter == null) {
            mAdapter = new FileListViewAdapter(getActivity(), null, viewcache,
                    sortname[0]);
            getListView().setOnTouchListener(
                    new FastOnTouchListener(new Handler(), mAdapter));
            setListAdapter(mAdapter);
            mHandler.setAdapter(mAdapter);
        } else {
            Cursor cur = mAdapter.swapCursor(null);
            if (cur != null) {
                cur.close();
            }
        }
        sort_cname = sortname[0];

        showProgressBar();
        String selection = null;
        String[] selectionArgs = null;
        if (args != null) {
            String path = args.getString("path");
            if (path != null) {
                selection = FileMedia.DATA + " = ?";
                selectionArgs = new String[] { path };
            }
        }

        return new CursorLoader(getActivity(), MediaConsts.FOLDER_CONTENT_URI,
                null, selection, selectionArgs, sortname[1]);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Logger.d("in onLoadFinished");
        try {
            if (mAdapter != null) {
                if (data == null || data.isClosed()) {
                    if(data==null) mAdapter.swapCursor(null);
                    return;
                }
                mAdapter.setCname(sort_cname);
                Cursor cur = mAdapter.swapCursor(data);
            }
        } finally {
            hideProgressBar();
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        hideProgressBar();
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
            int visibleItemCount, int totalItemCount) {
        // Logger.d("onScroll:"+firstVisibleItem+"/"+visibleItemCount+"/"+totalItemCount);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        Logger.d("onScrollStateChanged:" + scrollState);
        long time = Funcs.getHideTime(mPref);
        if (time > 0) {
            if (scrollState == 1) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                if (getFragmentManager() != null) {
                    ControlFragment control = (ControlFragment) getFragmentManager()
                            .findFragmentByTag(SystemConsts.TAG_CONTROL);
                    if (control != null) {
                        control.hideControl(false);
                    }
                }

            } else if (scrollState == 0) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                mTask = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (getFragmentManager() != null) {
                                ControlFragment control = (ControlFragment) getFragmentManager()
                                        .findFragmentByTag(
                                                SystemConsts.TAG_CONTROL);
                                if (control != null) {
                                    control.showControl(false);
                                }
                            }
                        } finally {
                            mTask = null;
                        }
                    }
                };
                mHandler.postDelayed(mTask, time);
            }
        }
    }

    @Override
    public boolean onBackPressed() {
        if (mMode != null) {
            mMode.finish();
            mMode = null;
            return true;
        }
        return false;
    }

    @Override
    protected void datasetChanged() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void addRow(Object[] values) {
    }

    @Override
    protected void hideMenu() {
        if (mMode != null) {
            mMode.finish();
            mMode = null;
        }
    }
    
    @Override
    public String getName(Context context) {
        SharedPreferences pref = mPref;
        if(pref == null){
            pref = PreferenceManager.getDefaultSharedPreferences(context);
        }
        int sort = pref.getInt(SORTKEY, 1);
        if(sort == 1)
            return context.getString(R.string.lb_tab_folder_date);
        else
            return context.getString(R.string.lb_tab_folder_title);
    }

    @Override
    public void doSearchQuery(String queryString) {
        // TODO Auto-generated method stub
        
    }
}
