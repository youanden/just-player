package jp.co.kayo.android.localplayer.fragment;

import android.app.ActionBar.Tab;

import jp.co.kayo.android.localplayer.BaseActivity;
import jp.co.kayo.android.localplayer.MainActivity2;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.core.ContentManager;
import jp.co.kayo.android.localplayer.core.ContextMenuFragment;
import jp.co.kayo.android.localplayer.util.AnimationHelper;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class MainFragment extends Fragment implements ContentManager, ContextMenuFragment {
    SharedPreferences mPref;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_main, null);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        
        
        Fragment f = getFragmentManager().findFragmentByTag("MAIN_FRAGMENT");
        if(f == null){
            String tabname = selectView(mPref.getInt("saveTab", R.id.mnu_select_tab));
            if(tabname!=null){
                MainActivity2 base = (MainActivity2)getActivity();
                Tab tab = base.getSupportActionBar().getTabAt(0);
                base.mTabsAdapter.setTabText(tab, tabname);
            }
        }
    }
    
    
    
    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        String name = getName(getActivity());
        if(name != null){
            MainActivity2 base = (MainActivity2)getActivity();
            Tab tab = base.getSupportActionBar().getTabAt(0);
            base.mTabsAdapter.setTabText(tab, name);
        }
    }

    public void saveTabPosition(int id){
        Editor editor = mPref.edit();
        editor.putInt("saveTab", id);
        editor.commit();
    }
    
    public String selectView(int id){
        Fragment f = null;
        
        if(id == SystemConsts.TAG_ALBUM_LIST){
            f = new AlbumListViewFragment();
        }
        else if(id == SystemConsts.TAG_ARTIST_LIST){
            f = new ArtistListViewFragment();
        }
        else if(id == SystemConsts.TAG_MEDIA){
            f = new MediaListViewFragment();
        }
        else if(id == SystemConsts.TAG_PLAYLIST){
            f = new PlaylistViewFragment();
        }
        else if(id == SystemConsts.TAG_FOLDER){
            f = new FileListViewFragment();
        }
        else if(id == SystemConsts.TAG_GENRES){
            f = new GenresListViewFragment();
        }
        else if(id == SystemConsts.TAG_VIDEO){
            f = new VideoListViewFragment();
        }
        else {//if(id == R.id.mnu_select_tab){
            f = new AlbumGridViewFragment();
        }
        
        if(f!=null){
            if(getFragmentManager().getBackStackEntryCount()>0){
                getFragmentManager().popBackStack();
            }
            FragmentTransaction t = getFragmentManager().beginTransaction();
            t.replace(R.id.fragment_main, f, "MAIN_FRAGMENT");
            t.commit();
            
            saveTabPosition(id);
            if(f instanceof ContentManager){
                return ((ContentManager)f).getName(getActivity());
            }
        }
        return null;
    }

    @Override
    public void reload() {
        ContentManager mgr = (ContentManager)getFragmentManager().findFragmentById(R.id.fragment_main);
        if(mgr!=null){
            Logger.d("Reload Class="+mgr.getClass().getName());
            mgr.reload();
        }
    }

    @Override
    public void changedMedia() {
        ContentManager mgr = (ContentManager)getFragmentManager().findFragmentById(R.id.fragment_main);
        if(mgr!=null){
            mgr.changedMedia();
        }
    }

    @Override
    public void release() {
        ContentManager mgr = (ContentManager)getFragmentManager().findFragmentById(R.id.fragment_main);
        if(mgr!=null){
            mgr.release();
        }
    }

    @Override
    public String getName(Context context) {
        ContentManager mgr = (ContentManager)getFragmentManager().findFragmentById(R.id.fragment_main);
        if(mgr!=null){
            return mgr.getName(context);
        }
        return null;
    }
    
    @Override
    public boolean onBackPressed() {
        Fragment f = getFragmentManager().findFragmentById(R.id.fragment_main);
        if(f instanceof ContextMenuFragment){
            return ((ContextMenuFragment)f).onBackPressed();
        }
        return false;
    }

    @Override
    public String selectSort() {
        Fragment f = getFragmentManager().findFragmentById(R.id.fragment_main);
        if(f instanceof ContextMenuFragment){
            return ((ContextMenuFragment)f).selectSort();
        }
        return null;
    }

    @Override
    public void doSearchQuery(String queryString) {
        Fragment f = getFragmentManager().findFragmentById(R.id.fragment_main);
        if(f instanceof ContextMenuFragment){
            ((ContextMenuFragment)f).doSearchQuery(queryString);
        }
    }
}
