package jp.co.kayo.android.localplayer.fragment;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.File;
import java.net.MalformedURLException;

import jp.co.kayo.android.localplayer.BaseActivity;
import jp.co.kayo.android.localplayer.BaseListFragment;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.TabFragment;
import jp.co.kayo.android.localplayer.adapter.VideoListViewAdapter;
import jp.co.kayo.android.localplayer.appwidget.AppWidgetHelper;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.VideoMedia;
import jp.co.kayo.android.localplayer.core.ContentManager;
import jp.co.kayo.android.localplayer.core.ContextMenuFragment;
import jp.co.kayo.android.localplayer.core.ServiceBinderHolder;
import jp.co.kayo.android.localplayer.dialog.YoutubeSearchDialog;
import jp.co.kayo.android.localplayer.menu.MediaContentActionCallback;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.provider.DownloadHelper;
import jp.co.kayo.android.localplayer.service.IMediaPlayerService;
import jp.co.kayo.android.localplayer.service.StreamCacherServer;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.ViewCache;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.content.Context;
import android.support.v4.content.CursorLoader;
import android.content.Intent;
import android.support.v4.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class VideoListViewFragment extends BaseListFragment implements
        ContentManager, OnItemLongClickListener, ContextMenuFragment,
        TabFragment, LoaderCallbacks<Cursor>, OnScrollListener {
    SharedPreferences mPref;
    private ViewCache mViewcache;
    VideoListViewAdapter mAdapter;
    String sort_cname;
    Runnable mTask = null;
    private ActionMode mMode;

    @Override
    public void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mViewcache = (ViewCache) getFragmentManager().findFragmentByTag(
                SystemConsts.TAG_CACHE);
    }

    private IMediaPlayerService getBinder() {
        if (getActivity() instanceof ServiceBinderHolder) {
            return ((ServiceBinderHolder) getActivity()).getBinder();
        } else {
            return null;
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getLoaderManager().initLoader(getFragmentId(), null, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater
                .inflate(R.layout.video_grid_view, container, false);

        ListView list = (ListView) root
                .findViewById(android.R.id.list);
        list.setOnItemLongClickListener(this);
        list.setOnScrollListener(this);

        return root;
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> arg0, View view,
            int position, long arg3) {
        Cursor selectedCursor = (Cursor) getListAdapter().getItem(position);
        if (selectedCursor != null) {
            BaseActivity act = (BaseActivity) getActivity();
            mMode = act.startActionMode(new AnActionModeOfEpicProportions(
                    getActivity(), position, mHandler));
            mMode.setTitle(selectedCursor.getString(selectedCursor
                    .getColumnIndex(VideoMedia.TITLE)));
            mMode.setSubtitle(getString(R.string.lb_tab_videos));
            return true;
        }

        return false;
    }

    private final class AnActionModeOfEpicProportions extends
            MediaContentActionCallback {
        public AnActionModeOfEpicProportions(Context context,
                int selectedIndex, Handler handler) {
            super(context, selectedIndex, handler);
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mMode = null;
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            if (ContentsUtils.canDownload(mPref)) {
                menu.add(getString(R.string.sub_mnu_download))
                        .setIcon(R.drawable.ic_menu_download)
                        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
                menu.add(getString(R.string.txt_web_more))
                        .setIcon(R.drawable.ic_menu_wikipedia)
                        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
                return true;
            }
            return true;
        }
    }

    @Override
    protected void messageHandle(int what, int selectedPosition) {
        Cursor selectedCursor = (Cursor) getListAdapter().getItem(
                selectedPosition);
        if (selectedCursor != null) {
            String title = selectedCursor.getString(selectedCursor
                    .getColumnIndex(VideoMedia.TITLE));
            String data = selectedCursor.getString(selectedCursor
                    .getColumnIndex(VideoMedia.DATA));
            String mime = selectedCursor.getString(selectedCursor
                    .getColumnIndex(VideoMedia.MIME_TYPE));
            switch (what) {
            case SystemConsts.EVT_SELECT_MORE: {
                showInfoDialog(null, title, null);
            }
                break;
            case SystemConsts.EVT_SELECT_YOUTUBE: {
                YoutubeSearchDialog dlg = new YoutubeSearchDialog();
                Bundle b = new Bundle();
                b.putString("title", title);
                dlg.setArguments(b);
                dlg.show(getFragmentManager(), SystemConsts.TAG_YOUTUBE_DLG);
            }
                break;
            case SystemConsts.EVT_SELECT_DOWNLOAD: {
                DownloadHelper helper = new DownloadHelper(getActivity());
                long id;
                try {
                    id = helper.enqueueVideo(title, mime, data);
                    helper.print(id);
                    if (id != -1) {
                        Toast.makeText(getActivity(),
                                getString(R.string.txt_action_download),
                                Toast.LENGTH_SHORT).show();
                    }
                } catch (MalformedURLException e) {
                    Logger.e("Create Uri failed.", e);
                }
            }
                break;
            default: {
            }
            }
        }
    }

    private String getVideoURL(String uri) throws MalformedURLException {
        String geturi = StreamCacherServer.getContentUri(getActivity(), uri);
        // CacheServerを利用できない(Videoはキャッシュ変換できない）
        if (geturi.indexOf("https:") != -1) {
            return geturi.replaceFirst("https:", "http:");
        } else {
            return geturi;
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Cursor cursor = (Cursor) getListAdapter().getItem(position);
        if (cursor != null) {
            String title = cursor.getString(cursor
                    .getColumnIndex(VideoMedia.TITLE));
            String data = cursor.getString(cursor
                    .getColumnIndex(VideoMedia.DATA));
            String mime = cursor.getString(cursor
                    .getColumnIndex(VideoMedia.MIME_TYPE));
            if (data != null && data.length() > 0) {
                if (!ContentsUtils.isSDCard(mPref)) {
                    Cursor cur = null;
                    try {
                        String geturl = getVideoURL(data);
                        if (geturl != null) {
                            Uri uri = Uri.parse(geturl);
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            if (title.endsWith(".mp4")) {
                                intent.setDataAndType(uri, "video/mp4");
                            } else if (mime != null) {
                                if (mime.endsWith("quicktime")) {
                                    intent.setDataAndType(uri, "video/mp4");
                                } else {
                                    intent.setDataAndType(uri, mime);
                                }
                            } else {
                                intent.setDataAndType(uri, "video/*");
                            }
                            startActivity(intent);

                            // 動画再生前に音楽一時停止
                            IMediaPlayerService binder = getBinder();
                            if (binder != null) {
                                int stat;
                                try {
                                    stat = binder.stat();
                                    if ((stat & AppWidgetHelper.FLG_PLAY) > 0) {
                                        binder.pause();
                                    }
                                } catch (RemoteException e) {
                                }
                            }
                        } else {
                            // TODO URLの取得に失敗してエラー、データがおかしいおそれ
                        }
                    } catch (Exception e) {
                        String mes = String.format(
                                getString(R.string.fmt_intent_error), mime);
                        Toast.makeText(this.getActivity(), mes,
                                Toast.LENGTH_SHORT).show();
                    } finally {
                        if (cur != null) {
                            cur.close();
                        }
                    }
                } else {
                    File file = new File(data);
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(file), mime);
                    try {
                        startActivity(intent);

                        // 動画再生前に音楽一時停止
                        IMediaPlayerService binder = getBinder();
                        if (binder != null) {
                            try {
                                binder.pause();
                            } catch (RemoteException e) {
                            }
                        }
                    } catch (Exception e) {
                        String mes = String.format(
                                getString(R.string.fmt_intent_error), mime);
                        Toast.makeText(this.getActivity(), mes,
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getLoaderManager().destroyLoader(getFragmentId());
    }

    @Override
    public void reload() {
        getLoaderManager().restartLoader(getFragmentId(), null, this);
    }

    @Override
    public void changedMedia() {

    }

    @Override
    public void release() {
        // TODO Auto-generated method stub

    }

    @Override
    public int getFragmentId() {
        return R.layout.video_grid_view;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Logger.d("in Loader<Cursor> onCreateLoader");
        String selection = null;
        String[] selectionArgs = null;
        String sortOrder = null;
        sortOrder = MediaConsts.VideoMedia.TITLE;

        if (mAdapter == null) {
            mAdapter = new VideoListViewAdapter(getActivity(), null, mViewcache);
            setListAdapter(mAdapter);
        } else {
            Cursor cur = mAdapter.swapCursor(null);
            if (cur != null) {
                cur.close();
            }
        }

        return new CursorLoader(getActivity(), MediaConsts.VIDEO_CONTENT_URI,
                null, selection, selectionArgs, sortOrder);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (mAdapter != null) {
            if (data == null || data.isClosed()) {
                if(data==null) mAdapter.swapCursor(null);
                return;
            }
            if (isVisible()) {
                getListView().setFastScrollEnabled(false);
            }
            Cursor cur = mAdapter.swapCursor(data);
            if (isVisible()) {
                getListView().setFastScrollEnabled(true);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
            int visibleItemCount, int totalItemCount) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        Logger.d("onScrollStateChanged:" + scrollState);
        long time = Funcs.getHideTime(mPref);
        if (time > 0) {
            if (scrollState == 1) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                if (getFragmentManager() != null) {
                    ControlFragment control = (ControlFragment) getFragmentManager()
                            .findFragmentByTag(SystemConsts.TAG_CONTROL);
                    if (control != null) {
                        control.hideControl(false);
                    }
                }
            } else if (scrollState == 0) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                mTask = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (getFragmentManager() != null) {
                                ControlFragment control = (ControlFragment) getFragmentManager()
                                        .findFragmentByTag(
                                                SystemConsts.TAG_CONTROL);
                                if (control != null) {
                                    control.showControl(false);
                                }
                            }
                        } finally {
                            mTask = null;
                        }
                    }
                };
                mHandler.postDelayed(mTask, time);
            }
        }
    }

    @Override
    public boolean onBackPressed() {
        if (mMode != null) {
            mMode.finish();
            mMode = null;
            return true;
        }
        return false;
    }

    @Override
    public String selectSort() {
        return null;
    }

    @Override
    protected void datasetChanged() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void addRow(Object[] values) {

    }

    @Override
    protected void hideMenu() {
        if (mMode != null) {
            mMode.finish();
            mMode = null;
        }
    }
    
    @Override
    public String getName(Context context) {
        return context.getString(R.string.lb_tab_videos);
    }

    @Override
    public void doSearchQuery(String queryString) {
        // TODO Auto-generated method stub
        
    }
}
