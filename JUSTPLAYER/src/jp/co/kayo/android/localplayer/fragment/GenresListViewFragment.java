package jp.co.kayo.android.localplayer.fragment;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.File;
import java.util.ArrayList;

import jp.co.kayo.android.localplayer.BaseActivity;
import jp.co.kayo.android.localplayer.BaseListFragment;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.TabFragment;
import jp.co.kayo.android.localplayer.adapter.GenresListViewAdapter;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioGenres;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.core.ContentManager;
import jp.co.kayo.android.localplayer.core.ContextMenuFragment;
import jp.co.kayo.android.localplayer.core.ServiceBinderHolder;
import jp.co.kayo.android.localplayer.menu.MediaContentActionCallback;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.service.IMediaPlayerService;
import jp.co.kayo.android.localplayer.service.StreamCacherServer;
import jp.co.kayo.android.localplayer.task.AsyncAddPlaylistTask;
import jp.co.kayo.android.localplayer.util.AnimationHelper;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.ViewCache;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.content.Context;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class GenresListViewFragment extends BaseListFragment implements
        ContentManager, OnItemLongClickListener, TabFragment,
        ContextMenuFragment, LoaderCallbacks<Cursor>, OnScrollListener {
    private SharedPreferences mPref;
    private ViewCache mViewcache;
    private GenresListViewAdapter mAdapter;
    private ActionMode mMode;

    boolean isReadFooter;
    Runnable mTask = null;
    MatrixCursor mcur;
    AsyncTask<Void, Void, Void> loadtask = null;
    boolean getall = false;
    private String mQueryString;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mViewcache = (ViewCache) getFragmentManager().findFragmentByTag(
                SystemConsts.TAG_CACHE);
        if (savedInstanceState != null) {
            getall = savedInstanceState.getBoolean("getall");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("getall", getall);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.genres_grid_view, container,
                false);

        ListView list = (ListView) root
                .findViewById(android.R.id.list);
        list.setOnItemLongClickListener(this);
        list.setOnScrollListener(this);
        isReadFooter = ContentsUtils.isNoCacheAction(mPref);

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getLoaderManager().initLoader(getFragmentId(), null, this);
    }

    @Override
    public void onPause() {
        super.onPause();
    }
    
    

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getLoaderManager().destroyLoader(getFragmentId());
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Cursor cursor = (Cursor) getListAdapter().getItem(position);
        if (cursor != null) {
            String genres_col = AudioGenres.GENRES_KEY;
            int col = cursor.getColumnIndex(genres_col);
            if (col == -1) {
                genres_col = AudioGenres._ID;
                col = cursor.getColumnIndex(genres_col);
            }
            String genres_id = cursor.getString(col);
            String genres_name = cursor.getString(cursor
                    .getColumnIndex(AudioGenres.NAME));
            if (genres_id != null) {

                FragmentTransaction t = getFragmentManager().beginTransaction();
                AnimationHelper.setFragmentToPlayBack(t);
                t.replace(R.id.fragment_main, jp.co.kayo.android.localplayer.fragment.GenresListFragment.createFragment(genres_id, genres_name));
                t.addToBackStack(SystemConsts.TAG_SUBFRAGMENT);
                t.commit();
            } else {
                Toast.makeText(getActivity(),
                        getString(R.string.txt_cannotopen_data),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> arg0, View view,
            int position, long arg3) {
        Cursor selectedCursor = (Cursor) getListAdapter().getItem(position);
        if (selectedCursor != null) {
            BaseActivity act = (BaseActivity) getActivity();
            mMode = act.startActionMode(new AnActionModeOfEpicProportions(
                    getActivity(), position, mHandler));
            mMode.setTitle(selectedCursor.getString(selectedCursor
                    .getColumnIndex(AudioGenres.NAME)));
            mMode.setSubtitle(getString(R.string.lb_tab_genres));
            return true;
        }

        return false;
    }

    private final class AnActionModeOfEpicProportions extends
            MediaContentActionCallback {
        public AnActionModeOfEpicProportions(Context context,
                int selectedIndex, Handler handler) {
            super(context, selectedIndex, handler);
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mMode = null;
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            menu.add(getString(R.string.sub_mnu_order))
                    .setIcon(R.drawable.ic_menu_btn_add)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
            if (!ContentsUtils.isSDCard(mPref)) {
                menu.add(getString(R.string.sub_mnu_clearcache))
                        .setIcon(R.drawable.ic_menu_delete)
                        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
            }
            return true;
        }
    }

    @Override
    protected void messageHandle(int what, int selectedPosition) {
        Cursor selectedCursor = (Cursor) mAdapter.getItem(selectedPosition);
        if (selectedCursor != null) {
            int col = selectedCursor.getColumnIndex(AudioGenres.GENRES_KEY);
            if (col == -1) {
                col = selectedCursor.getColumnIndex(AudioGenres._ID);
            }
            long mediaId = selectedCursor.getLong(selectedCursor
                    .getColumnIndex(AudioGenres._ID));
            String genresKey = selectedCursor.getString(col);

            switch (what) {
            case SystemConsts.EVT_SELECT_ADD: {
                String audio_id_col = MediaConsts.AudioGenresMember._ID;
                int audio_id_index = selectedCursor
                        .getColumnIndex(MediaConsts.AudioGenresMember.AUDIO_ID);
                if (audio_id_index != -1) {
                    audio_id_col = MediaConsts.AudioGenresMember.AUDIO_ID;
                }
                
                AsyncAddPlaylistTask task = new AsyncAddPlaylistTask(getActivity(),
                        getFragmentManager(), MediaConsts.GENRES_MEMBER_CONTENT_URI,
                        audio_id_col, MediaConsts.AudioGenresMember.GENRE_ID + " = ?",
                        new String[] { genresKey },
                        MediaConsts.AudioGenresMember.DEFAULT_SORT_ORDER);
                task.execute();
            }
                break;
            case SystemConsts.EVT_SELECT_CLEARCACHE: {
                Cursor cursor = null;
                try {
                    cursor = getActivity().getContentResolver().query(
                            MediaConsts.GENRES_MEMBER_CONTENT_URI, null,
                            MediaConsts.AudioGenresMember.GENRE_ID + " = ?",
                            new String[] { genresKey },
                            MediaConsts.AudioGenresMember.DEFAULT_SORT_ORDER);
                    if (cursor != null && cursor.moveToFirst()) {
                        do {
                            String title = cursor
                                    .getString(cursor
                                            .getColumnIndex(MediaConsts.AudioMedia.TITLE));
                            String data = cursor
                                    .getString(cursor
                                            .getColumnIndex(MediaConsts.AudioMedia.DATA));
                            File file = StreamCacherServer.getCacheFile(
                                    getActivity(), data);
                            if (file != null && file.exists()) {
                                file.delete();
                                Toast.makeText(
                                        getActivity(),
                                        getString(R.string.txt_action_deletecache)
                                                + "(" + title + ")",
                                        Toast.LENGTH_SHORT).show();
                                mAdapter.notifyDataSetChanged();
                            }
                        } while (cursor.moveToNext());
                    }
                } finally {
                    if (cursor != null) {
                        cursor.close();
                    }
                }
            }
                break;
            default: {
            }
            }
        }
    }

    @Override
    public void reload() {
        getLoaderManager().restartLoader(getFragmentId(), null, this);
    }

    @Override
    public void changedMedia() {

    }

    @Override
    public void release() {
        // TODO Auto-generated method stub

    }

    @Override
    public int getFragmentId() {
        return R.layout.genres_grid_view;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String selection = null;
        String[] selectionArgs = null;
        String sortOrder = null;
        sortOrder = AudioGenres.NAME;

        if (mAdapter == null) {
            mAdapter = new GenresListViewAdapter(getActivity(), mViewcache,
                    null);
            setListAdapter(mAdapter);
        } else {
            Cursor cur = mAdapter.swapCursor(null);
            if (cur != null) {
                cur.close();
            }
        }
        
        if(Funcs.isNotEmpty(mQueryString)){
            selection = MediaConsts.AudioGenres.NAME + " like '%' || ? || '%'";
            selectionArgs = new String[]{mQueryString};
        }
        
        getall = false;
        showProgressBar();
        return new CursorLoader(getActivity(), MediaConsts.GENRES_CONTENT_URI,
                null, selection, selectionArgs, sortOrder);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        try {
            hideProgressBar();
            if (mAdapter != null) {
                if (data == null || data.isClosed()) {
                    if(data==null) mAdapter.swapCursor(null);
                    invisibleProgressBar();
                    return;
                }
                if (ContentsUtils.isNoCacheAction(mPref)) {
                    int count = data.getCount();
                    try {
                        mcur = new MatrixCursor(data.getColumnNames(),
                                data.getCount());
                        if (count <= 0) {
                            invisibleProgressBar();
                        }
                        if (count > 0) {
                            data.moveToFirst();
                            do {
                                ArrayList<Object> values = new ArrayList<Object>();
                                for (int i = 0; i < mcur.getColumnCount(); i++) {
                                    values.add(data.getString(i));
                                }
                                mcur.addRow(values
                                        .toArray(new Object[values.size()]));
                            } while (data.moveToNext());
                            Cursor cur = mAdapter.swapCursor(mcur);
                        }

                    } finally {
                        if (count < SystemConsts.DEFAULT_FETCH_LIMIT) {
                            invisibleProgressBar();
                        }
                        if (data != null) {
                            data.close();
                        }
                    }
                } else {
                    Cursor cur = mAdapter.swapCursor(data);
                    invisibleProgressBar();
                }
            }
        } finally {
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        hideProgressBar();
        mcur = null;
    }


    private void invisibleProgressBar() {
        getall = true;
        mHandler.sendEmptyMessage(SystemConsts.ACT_HIDEPROGRESS);
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
            int visibleItemCount, int totalItemCount) {
        if (mAdapter == null || loadtask != null) {
            return;
        }
        if (isReadFooter) {
            if (!getall && totalItemCount > 0
                    && totalItemCount == (firstVisibleItem + visibleItemCount)) {
                final int current = mAdapter.getCount();
                if (current > 0) {
                    loadtask = new AsyncTask<Void, Void, Void>() {

                        @Override
                        protected Void doInBackground(Void... params) {
                            Cursor cursor = null;
                            try {
                                mHandler.sendEmptyMessage(SystemConsts.ACT_SHOWPROGRESS);
                                cursor = getActivity()
                                        .getContentResolver()
                                        .query(MediaConsts.GENRES_CONTENT_URI,
                                                null,
                                                "LIMIT ? AND OFFSET ?",
                                                new String[] {
                                                        Integer.toString(SystemConsts.DEFAULT_FETCH_LIMIT),
                                                        Long.toString(current) },
                                                null);
                                int count = cursor.getCount();
                                if (mcur != null && count > 0) {
                                    cursor.moveToFirst();
                                    do {
                                        ArrayList<Object> values = new ArrayList<Object>();
                                        for (int i = 0; i < mcur
                                                .getColumnCount(); i++) {
                                            String colname = mcur
                                                    .getColumnName(i);
                                            int colid = cursor
                                                    .getColumnIndex(colname);
                                            if (colid != -1) {
                                                values.add(cursor
                                                        .getString(colid));
                                            }
                                        }
                                        mHandler.sendMessage(mHandler
                                                .obtainMessage(
                                                        SystemConsts.ACT_ADDROW,
                                                        values.toArray(new Object[values
                                                                .size()])));
                                    } while (cursor.moveToNext());

                                    if (count < SystemConsts.DEFAULT_FETCH_LIMIT) {
                                        invisibleProgressBar();
                                    }
                                } else {
                                    if (count < 1) {
                                        invisibleProgressBar();
                                    }
                                }
                            } finally {
                                mHandler.sendEmptyMessage(SystemConsts.ACT_HIDEPROGRESS);
                                if (cursor != null) {
                                    cursor.close();
                                }
                                mHandler.sendEmptyMessage(SystemConsts.ACT_NOTIFYDATASETCHANGED);
                                loadtask = null;
                            }

                            return null;
                        }
                    };
                    loadtask.execute((Void) null);
                }
            }
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        Logger.d("onScrollStateChanged:" + scrollState);
        long time = Funcs.getHideTime(mPref);
        if (time > 0) {
            if (scrollState == 1) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                if (getFragmentManager() != null) {
                    ControlFragment control = (ControlFragment) getFragmentManager()
                            .findFragmentByTag(SystemConsts.TAG_CONTROL);
                    if (control != null) {
                        control.hideControl(false);
                    }
                }
            } else if (scrollState == 0) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                mTask = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (getFragmentManager() != null) {
                                ControlFragment control = (ControlFragment) getFragmentManager()
                                        .findFragmentByTag(
                                                SystemConsts.TAG_CONTROL);
                                if (control != null) {
                                    control.showControl(false);
                                }
                            }
                        } finally {
                            mTask = null;
                        }
                    }
                };
                mHandler.postDelayed(mTask, time);
            }
        }
    }

    @Override
    public boolean onBackPressed() {
        if (mMode != null) {
            mMode.finish();
            mMode = null;
            return true;
        }
        return false;
    }

    @Override
    public String selectSort() {
        return null;
    }

    @Override
    protected void datasetChanged() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void addRow(Object[] values) {
        if (mcur != null) {
            mcur.addRow(values);
        }
    }

    @Override
    protected void hideMenu() {
        if (mMode != null) {
            mMode.finish();
            mMode = null;
        }
    }
    
    @Override
    public String getName(Context context) {
        return context.getString(R.string.lb_tab_genres);
    }

    @Override
    public void doSearchQuery(String queryString) {
        boolean dirty = mQueryString==null || !mQueryString.equals(queryString);
        if(dirty){
            mQueryString = queryString; 
            reload();
        }
    }
}
