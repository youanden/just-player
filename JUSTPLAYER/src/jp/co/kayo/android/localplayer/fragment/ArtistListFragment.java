
package jp.co.kayo.android.localplayer.fragment;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Random;

import jp.co.kayo.android.localplayer.BaseActivity;
import jp.co.kayo.android.localplayer.BaseListFragment;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.TagEditActivity;
import jp.co.kayo.android.localplayer.adapter.ArtistListAdapter;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.core.ContentManager;
import jp.co.kayo.android.localplayer.core.ContextMenuFragment;
import jp.co.kayo.android.localplayer.core.IProgressView;
import jp.co.kayo.android.localplayer.core.ServiceBinderHolder;
import jp.co.kayo.android.localplayer.dialog.RatingDialog;
import jp.co.kayo.android.localplayer.menu.MediaContentActionCallback;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.service.IMediaPlayerService;
import jp.co.kayo.android.localplayer.task.AsyncAddPlaylistTask;
import jp.co.kayo.android.localplayer.util.AnimationHelper;
import jp.co.kayo.android.localplayer.util.BaseAnimationListener;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.ViewCache;
import jp.co.kayo.android.localplayer.util.bean.MediaData;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class ArtistListFragment extends BaseListFragment implements
        ContentManager, ContextMenuFragment, OnItemLongClickListener,
        LoaderCallbacks<Cursor>, OnScrollListener, IProgressView {
    private String artist_key;
    private String artist_title;
    private ViewCache viewcache;
    private ListView mListView;
    private ArtistListAdapter mAdapter;
    SharedPreferences mPref;
    boolean getall = false;
    MatrixCursor mcur;
    long lastQueryTime = 0;
    int mListMax = -1;
    private ActionMode mMode;
    private int mLoaderId = -1;
    boolean isReadFooter;

    String[] fetchcols = new String[] {
            MediaConsts.AudioMedia._ID,
            MediaConsts.AudioMedia.TITLE, MediaConsts.AudioMedia.ALBUM,
            MediaConsts.AudioMedia.ALBUM_KEY, MediaConsts.AudioMedia.ARTIST,
            MediaConsts.AudioMedia.DURATION, MediaConsts.AudioMedia.DATA
    };

    private IMediaPlayerService getBinder() {
        if (getActivity() instanceof ServiceBinderHolder) {
            return ((ServiceBinderHolder) getActivity()).getBinder();
        } else {
            return null;
        }
    }

    Runnable mTask = null;

    public static ArtistListFragment createFragment(String selectedkey, String selectedtitle,
            int max) {
        ArtistListFragment f = new ArtistListFragment();
        Bundle args = new Bundle();
        args.putString("selectedkey", selectedkey);
        args.putString("selectedtitle", selectedtitle);
        args.putInt("listmax", max);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mLoaderId = new Random(System.currentTimeMillis()).nextInt(99999999);

        getLoaderManager().initLoader(mLoaderId, null, this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            CharSequence key = args.getCharSequence("selectedkey");
            if (key != null && key.length() > 0) {
                artist_key = key.toString();
            }
            CharSequence title = args.getCharSequence("selectedtitle");
            if (title != null) {
                artist_title = title.toString();
            }

            Integer max = args.getInt("listmax");
            if (max != null) {
                mListMax = max;
            }
        }
        if (savedInstanceState != null) {
            getall = savedInstanceState.getBoolean("getall");
        }
        mPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        viewcache = (ViewCache) getFragmentManager().findFragmentByTag(
                SystemConsts.TAG_CACHE);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("getall", getall);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.listview_body, container, false);

        mListView = (ListView) root
                .findViewById(android.R.id.list);
        mListView.setOnItemLongClickListener(this);
        mListView.setOnScrollListener(this);
        isReadFooter = ContentsUtils.isNoCacheAction(mPref);

        return root;
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> arg0, View view,
            int position, long arg3) {
        Cursor selectedCursor = (Cursor) getListAdapter().getItem(position);
        if (selectedCursor != null) {
            BaseActivity act = (BaseActivity) getActivity();
            mMode = act.startActionMode(new AnActionModeOfEpicProportions(
                    getActivity(), position, mHandler));
            mMode.setTitle(selectedCursor.getString(selectedCursor
                    .getColumnIndex(AudioMedia.TITLE)));
            mMode.setSubtitle(getString(R.string.lb_tab_media));
            return true;
        }

        return false;
    }

    private final class AnActionModeOfEpicProportions extends
            MediaContentActionCallback {

        public AnActionModeOfEpicProportions(Context context,
                int selectedIndex, Handler handler) {
            super(context, selectedIndex, handler);
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mMode = null;
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            menu.add(getString(R.string.sub_mnu_order))
                    .setIcon(R.drawable.ic_menu_btn_add)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.sub_mnu_rating))
                    .setIcon(R.drawable.ic_menu_star)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

//            menu.add(getString(R.string.sub_mnu_album))
//                    .setIcon(R.drawable.ic_menu_album)
//                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.txt_web_more))
                    .setIcon(R.drawable.ic_menu_wikipedia)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.sub_mnu_edit))
                    .setIcon(R.drawable.ic_menu_strenc)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            boolean b = mPref.getBoolean("key.useLastFM", false);
            if (b) {
                menu.add(getString(R.string.sub_mnu_love)).setShowAsAction(
                        MenuItem.SHOW_AS_ACTION_IF_ROOM);
                menu.add(getString(R.string.sub_mnu_ban)).setShowAsAction(
                        MenuItem.SHOW_AS_ACTION_IF_ROOM);
            }

            if (!ContentsUtils.isSDCard(mPref)) {
                menu.add(getString(R.string.sub_mnu_clearcache))
                        .setIcon(R.drawable.ic_menu_refresh)
                        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
            }
            return true;
        }

    };

    @Override
    protected void messageHandle(int what, int selectedPosition) {
        Cursor selectedCursor = (Cursor) mAdapter.getItem(selectedPosition);
        if (selectedCursor != null) {
            long mediaId = selectedCursor.getLong(selectedCursor
                    .getColumnIndex(AudioMedia._ID));
            String title = selectedCursor.getString(selectedCursor
                    .getColumnIndex(MediaConsts.AudioMedia.TITLE));
            String album = selectedCursor.getString(selectedCursor
                    .getColumnIndex(MediaConsts.AudioMedia.ALBUM));
            String artist = selectedCursor.getString(selectedCursor
                    .getColumnIndex(MediaConsts.AudioMedia.ARTIST));
            String data = selectedCursor.getString(selectedCursor
                    .getColumnIndex(MediaConsts.AudioMedia.DATA));
            long duration = selectedCursor.getLong(selectedCursor
                    .getColumnIndex(MediaConsts.AudioMedia.DURATION));

            switch (what) {
                case SystemConsts.EVT_SELECT_RATING: {
                    RatingDialog dlg = new RatingDialog(getActivity(),
                            TableConsts.FAVORITE_TYPE_SONG, mediaId, mHandler);
                    dlg.show();
                }
                    break;
                case SystemConsts.EVT_SELECT_ADD: {
                    AsyncAddPlaylistTask task = new AsyncAddPlaylistTask(getActivity(),
                            getFragmentManager(), AudioMedia._ID + " = ?", new String[] {
                                    Long.toString(mediaId)
                            });
                    task.execute();
                }
                    break;
                case SystemConsts.EVT_SELECT_OPENALBUM: {
                    Hashtable<String, String> tbl = ContentsUtils.getMedia(
                            getActivity(), new String[] {
                                AudioMedia.ALBUM_KEY
                            },
                            mediaId);
                    String album_key = tbl.get(AudioMedia.ALBUM_KEY);
                    if (album_key != null) {
                        FragmentTransaction t = getFragmentManager().beginTransaction();
                        AnimationHelper.setFragmentToPlayBack(t);
                        t.replace(R.id.fragment_main, AlbumSongsFragment.createFragment(album_key));
                        t.addToBackStack(SystemConsts.TAG_SUBFRAGMENT);
                        t.commit();
                    }
                }
                    break;
                case SystemConsts.EVT_SELECT_MORE: {
                    showInfoDialog(album, artist, title);
                }
                    break;
                case SystemConsts.EVT_SELECT_CLEARCACHE: {
                    ContentsUtils.clearMediaCache(getActivity(), data);
                    ContentValues values = new ContentValues();
                    values.put(TableConsts.AUDIO_CACHE_FILE, (String) null);
                    getActivity().getContentResolver().update(
                            ContentUris.withAppendedId(
                                    MediaConsts.MEDIA_CONTENT_URI, mediaId),
                            values, null, null);
                    datasetChanged();
                }
                    break;
                case SystemConsts.EVT_SELECT_LOVE: {
                    ContentsUtils.lastfmLove(getActivity(), title, artist, album,
                            Long.toString(duration));
                }
                    break;
                case SystemConsts.EVT_SELECT_BAN: {
                    ContentsUtils.lastfmBan(getActivity(), title, artist, album,
                            Long.toString(duration));
                }
                    break;
                case SystemConsts.EVT_SELECT_EDIT: {
                    // 文字化け修正
                    Intent intent = new Intent(getActivity(), TagEditActivity.class);
                    intent.putExtra(SystemConsts.KEY_EDITTYPE,
                            TagEditActivity.MEDIA);
                    intent.putExtra(SystemConsts.KEY_EDITKEY,
                            Long.toString(mediaId));
                    getActivity().startActivityForResult(intent, SystemConsts.REQUEST_TAGEDIT);
                }
                    break;
                default: {
                }
            }
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, final int position, long id) {
        BaseActivity base = (BaseActivity)getActivity();
        final IMediaPlayerService binder = base.getBinder();
        if (binder == null) {
            return;
        }
        final ArrayList<MediaData> ids = getIds();
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                playMediaReplace(SystemConsts.CONTENTSKEY_ARTIST
                        + lastQueryTime, binder, ids, position);
                return null;
            }
        };
        task.execute();
        
        base.getSupportFragmentManager().popBackStack();
        Funcs.setResumeReloadFlag(mPref, true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getLoaderManager().destroyLoader(mLoaderId);
        mLoaderId = -1;
    }

    @Override
    public void onResume() {
        super.onResume();
        
    }

    @Override
    public void onPause() {
        super.onPause();
        Logger.d("ArtistList.onPause");
    }

    @Override
    public void reload() {
        getLoaderManager().restartLoader(mLoaderId, null, this);
    }

    @Override
    public void changedMedia() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void release() {
    }

    public void play() {
        if (artist_key != null) {
            ContentsUtils.playArtist(getBinder(), (BaseActivity) getActivity(),
                    artist_key);
        } else {
            ContentsUtils.playArtistTitle(getBinder(),
                    (BaseActivity) getActivity(), artist_title);
        }

        Toast.makeText(getActivity(), getString(R.string.txt_action_addartist),
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (mAdapter == null) {
            mAdapter = new ArtistListAdapter(getActivity(), null, viewcache);
            setListAdapter(mAdapter);
        } else {
            Cursor cur = mAdapter.swapCursor(null);
            if (cur != null) {
                cur.close();
            }
        }

        getall = false;
        showProgressBar();
        if (artist_key != null) {
            return new CursorLoader(getActivity(),
                    MediaConsts.MEDIA_CONTENT_URI, fetchcols,
                    MediaConsts.AudioMedia.ARTIST_KEY + " = ?",
                    new String[] {
                        artist_key
                    }, AudioMedia.YEAR + ","
                            + AudioMedia.ALBUM + "," + AudioMedia.TRACK);
        } else {
            return new CursorLoader(getActivity(),
                    MediaConsts.MEDIA_CONTENT_URI, fetchcols,
                    MediaConsts.AudioMedia.ARTIST + " like '%' || ? || '%'",
                    new String[] {
                        artist_title
                    }, AudioMedia.YEAR + ","
                            + AudioMedia.ALBUM + "," + AudioMedia.TRACK);
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        hideProgressBar();
        try {
            lastQueryTime = Calendar.getInstance().getTimeInMillis();
            if (mAdapter != null) {
                if (data == null || data.isClosed()) {
                    if(data==null) mAdapter.swapCursor(null);
                    return;
                }
                if (ContentsUtils.isNoCacheAction(mPref)) {
                    try {
                        mcur = new MatrixCursor(data.getColumnNames(),
                                data.getCount());
                        int count = data.getCount();
                        if (count > 0) {
                            data.moveToFirst();
                            do {
                                ArrayList<Object> values = new ArrayList<Object>();
                                for (int i = 0; i < mcur.getColumnCount(); i++) {
                                    if (AudioMedia.DURATION.equals(mcur
                                            .getColumnName(i))) {
                                        values.add(data.getLong(i));
                                    } else {
                                        values.add(data.getString(i));
                                    }
                                }
                                mcur.addRow(values
                                        .toArray(new Object[values.size()]));
                            } while (data.moveToNext());
                            Cursor cur = mAdapter.swapCursor(mcur);
                        }

                    } finally {
                        if (data != null) {
                            data.close();
                        }
                    }
                } else {
                    Cursor cur = mAdapter.swapCursor(data);
                    getall = true;
                }
            }
        } finally {
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        hideProgressBar();
        mcur = null;
    }

    public ArrayList<MediaData> getIds(){
        ArrayList<MediaData> ret = new ArrayList<MediaData>();
        int count = mAdapter.getCount();
        for(int i=0; i<count; i++){
            Cursor cursor = (Cursor)mAdapter.getItem(i);
            long id = cursor.getLong(cursor
                    .getColumnIndex(AudioMedia._ID));
            String data = cursor.getString(cursor
                    .getColumnIndex(AudioMedia.DATA));
            ret.add(new MediaData(id, data));
        }
        return ret;
    }
    
    public boolean playMediaReplace(String contentkey,
            IMediaPlayerService binder, ArrayList<MediaData> list, int position) {
        try {
            if (!binder.setContentsKey(contentkey)) {
                // 既に設定中のリストだった
                int pos = binder.getPosition();
                if (pos != position) {// 同じ位置をクリックした
                    binder.lockUpdateToPlay();
                    try {
                        binder.stop();
                        binder.setPosition(position);
                    } finally {
                        binder.play();
                    }
                    return true;
                }
            } else {
                binder.lockUpdateToPlay();
                try {
                    binder.clear();
                    int size = list.size();
                    for(int i=0; i<size; i++){
                        MediaData data = list.get(i);
                        binder.addMedia(data.mediaId, data.data);
                    }
                    binder.setPosition(position);
                } catch (Exception e) {
                    Logger.e("cursor closed Exception", e);
                } finally {
                    binder.commit();
                    binder.play();
                }
                return true;
            }
        } catch (RemoteException e) {
        }
        return false;
    }

    @Override
    public void startProgress(final long max) {
        if (mAdapter != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    viewcache.startProgress(max);
                    IMediaPlayerService binder = getBinder();
                    if (binder != null) {
                        try {
                            viewcache.setPrefetchId(binder.getPrefetchId());
                        } catch (RemoteException e) {
                        }
                    }
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public void stopProgress() {
        if (mAdapter != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    viewcache.stopProgress();
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public void progress(final long pos, final long max) {
        if (mAdapter != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    viewcache.progress(pos, max);
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    AsyncTask<Void, Void, Void> loadtask = null;

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
            int visibleItemCount, int totalItemCount) {
        if (mAdapter == null || loadtask != null) {
            return;
        }
        if (isReadFooter) {
            if (!getall && totalItemCount > 0
                    && totalItemCount == (firstVisibleItem + visibleItemCount)) {
                final int current = mAdapter.getCount();
                if (current > 0) {
                    loadtask = new AsyncTask<Void, Void, Void>() {

                        @Override
                        protected Void doInBackground(Void... params) {
                            Cursor cursor = null;
                            try {
                                mHandler.sendEmptyMessage(SystemConsts.ACT_SHOWPROGRESS);
                                if (artist_key != null) {
                                    cursor = getActivity()
                                            .getContentResolver()
                                            .query(MediaConsts.MEDIA_CONTENT_URI,
                                                    fetchcols,
                                                    MediaConsts.AudioMedia.ARTIST_KEY
                                                            + " = ? AND LIMIT ? AND OFFSET ?",
                                                    new String[] {
                                                            artist_key,
                                                            Integer.toString(SystemConsts.DEFAULT_LIST_LIMIT),
                                                            Long.toString(current)
                                                    },
                                                    null);
                                } else {
                                    cursor = getActivity()
                                            .getContentResolver()
                                            .query(MediaConsts.MEDIA_CONTENT_URI,
                                                    fetchcols,
                                                    MediaConsts.AudioMedia.ARTIST
                                                            + " like '%' || ? || '%'"
                                                            + " AND LIMIT ? AND OFFSET ?",
                                                    new String[] {
                                                            artist_title,
                                                            Integer.toString(SystemConsts.DEFAULT_LIST_LIMIT),
                                                            Long.toString(current)
                                                    },
                                                    null);
                                }
                                int count = cursor.getCount();
                                if (mcur != null && count > 0) {
                                    cursor.moveToFirst();
                                    do {
                                        ArrayList<Object> values = new ArrayList<Object>();
                                        for (int i = 0; i < mcur
                                                .getColumnCount(); i++) {
                                            String colname = mcur
                                                    .getColumnName(i);
                                            int colid = cursor
                                                    .getColumnIndex(colname);
                                            if (colid != -1) {
                                                if (AudioMedia.DURATION
                                                        .equals(colname)) {
                                                    values.add(cursor
                                                            .getLong(colid));
                                                } else {
                                                    values.add(cursor
                                                            .getString(colid));
                                                }
                                            }
                                        }
                                        mHandler.sendMessage(mHandler
                                                .obtainMessage(
                                                        SystemConsts.ACT_ADDROW,
                                                        values.toArray(new Object[values
                                                                .size()])));
                                    } while (cursor.moveToNext());
                                }
                            } finally {
                                getall = true;
                                mHandler.sendEmptyMessage(SystemConsts.ACT_HIDEPROGRESS);
                                if (cursor != null) {
                                    cursor.close();
                                }
                                mHandler.sendEmptyMessage(SystemConsts.ACT_NOTIFYDATASETCHANGED);
                                loadtask = null;
                            }

                            return null;
                        }
                    };
                    loadtask.execute((Void) null);
                }
            }
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        Logger.d("onScrollStateChanged:" + scrollState);
        long time = Funcs.getHideTime(mPref);
        if (time > 0) {
            if (scrollState == 1) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                if (getFragmentManager() != null) {
                    ControlFragment control = (ControlFragment) getFragmentManager()
                            .findFragmentByTag(SystemConsts.TAG_CONTROL);
                    if (control != null) {
                        control.hideControl(false);
                    }
                }
            } else if (scrollState == 0) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                mTask = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (getFragmentManager() != null) {
                                ControlFragment control = (ControlFragment) getFragmentManager()
                                        .findFragmentByTag(
                                                SystemConsts.TAG_CONTROL);
                                if (control != null) {
                                    control.showControl(false);
                                }
                            }
                        } finally {
                            mTask = null;
                        }
                    }
                };
                mHandler.postDelayed(mTask, time);
            }
        }
    }

    @Override
    public boolean onBackPressed() {
        if (mMode != null) {
            mMode.finish();
            mMode = null;
            return true;
        }
        return false;
    }

    @Override
    public String selectSort() {
        getFragmentManager().popBackStack();
        return null;
    }

    @Override
    protected void datasetChanged() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void addRow(Object[] values) {
        if(mcur!=null){
            mcur.addRow(values);
        }
    }

    @Override
    protected void hideMenu() {
        if (mMode != null) {
            mMode.finish();
            mMode = null;
        }
    }

    @Override
    public String getName(Context context) {
        return context.getString(R.string.lb_tab_artist_songs);
    }

    @Override
    public void doSearchQuery(String queryString) {
        // TODO Auto-generated method stub

    }
}
