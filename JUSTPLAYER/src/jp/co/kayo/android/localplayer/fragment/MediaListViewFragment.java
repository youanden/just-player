
package jp.co.kayo.android.localplayer.fragment;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.net.MalformedURLException;
import java.util.Hashtable;

import jp.co.kayo.android.localplayer.BaseActivity;
import jp.co.kayo.android.localplayer.BaseListFragment;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.TabFragment;
import jp.co.kayo.android.localplayer.TagEditActivity;
import jp.co.kayo.android.localplayer.adapter.MediaListViewAdapter;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.core.ContentManager;
import jp.co.kayo.android.localplayer.core.ContextMenuFragment;
import jp.co.kayo.android.localplayer.core.IProgressView;
import jp.co.kayo.android.localplayer.core.ServiceBinderHolder;
import jp.co.kayo.android.localplayer.dialog.LyricsDialog;
import jp.co.kayo.android.localplayer.dialog.RatingDialog;
import jp.co.kayo.android.localplayer.dialog.YoutubeSearchDialog;
import jp.co.kayo.android.localplayer.menu.MediaContentActionCallback;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.provider.DownloadHelper;
import jp.co.kayo.android.localplayer.service.IMediaPlayerService;
import jp.co.kayo.android.localplayer.task.AsyncAddPlaylistTask;
import jp.co.kayo.android.localplayer.util.AnimationHelper;
import jp.co.kayo.android.localplayer.util.BaseAnimationListener;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.ViewCache;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.support.v4.content.CursorLoader;
import android.content.Intent;
import android.support.v4.content.Loader;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class MediaListViewFragment extends BaseListFragment implements
        ContentManager, OnItemLongClickListener, ContextMenuFragment,
        TabFragment, LoaderCallbacks<Cursor>, OnScrollListener, IProgressView {
    private ViewCache viewcache;
    SharedPreferences mPref;
    private final String SORTKEY = "mediaview.sort";
    MediaListViewAdapter mAdapter;
    String sort_cname;
    private ActionMode mMode;
    private String contentKey;
    private String mQueryString;

    Runnable mTask = null;

    private IMediaPlayerService getBinder() {
        if (getActivity() instanceof ServiceBinderHolder) {
            return ((ServiceBinderHolder) getActivity()).getBinder();
        } else {
            return null;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        viewcache = (ViewCache) getFragmentManager().findFragmentByTag(
                SystemConsts.TAG_CACHE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater
                .inflate(R.layout.media_list_view, container, false);

        ListView list = (ListView) root
                .findViewById(android.R.id.list);
        list.setOnItemLongClickListener(this);
        list.setOnScrollListener(this);
        
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getLoaderManager().initLoader(getFragmentId(), null, this);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getLoaderManager().destroyLoader(getFragmentId());
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> arg0, View view,
            final int position, long arg3) {
        Cursor selectedCursor = (Cursor) getListAdapter().getItem(position);
        if (selectedCursor != null) {
            BaseActivity act = (BaseActivity) getActivity();
            mMode = act.startActionMode(new AnActionModeOfEpicProportions(
                    getActivity(), position, mHandler));
            mMode.setTitle(selectedCursor.getString(selectedCursor
                    .getColumnIndex(AudioMedia.TITLE)));
            mMode.setSubtitle(getString(R.string.lb_tab_media));
            return true;
        }

        return false;
    }

    private final class AnActionModeOfEpicProportions extends
            MediaContentActionCallback {

        public AnActionModeOfEpicProportions(Context context,
                int selectedIndex, Handler handler) {
            super(context, selectedIndex, handler);
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mMode = null;
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            menu.add(getString(R.string.sub_mnu_order))
                    .setIcon(R.drawable.ic_menu_btn_add)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.sub_mnu_rating))
                    .setIcon(R.drawable.ic_menu_star)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.sub_mnu_artist))
                    .setIcon(R.drawable.ic_menu_btn_artist)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.sub_mnu_album))
                    .setIcon(R.drawable.ic_menu_album)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.txt_web_more))
                    .setIcon(R.drawable.ic_menu_wikipedia)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
            menu.add(getString(R.string.sub_mnu_edit))
                    .setIcon(R.drawable.ic_menu_strenc)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            boolean b = mPref.getBoolean("key.useLastFM", false);
            if (b) {
                menu.add(getString(R.string.sub_mnu_love)).setShowAsAction(
                        MenuItem.SHOW_AS_ACTION_IF_ROOM);
                menu.add(getString(R.string.sub_mnu_ban)).setShowAsAction(
                        MenuItem.SHOW_AS_ACTION_IF_ROOM);
            }
            if (!ContentsUtils.isSDCard(mPref)) {
                menu.add(getString(R.string.sub_mnu_clearcache))
                        .setIcon(R.drawable.ic_menu_refresh)
                        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
            }
            return true;
        }

    };

    @Override
    protected void messageHandle(int what, int selectedPosition) {
        Cursor selectedCursor = (Cursor) mAdapter.getItem(selectedPosition);
        if (selectedCursor != null) {
            long mediaId = selectedCursor.getLong(selectedCursor
                    .getColumnIndex(AudioMedia._ID));
            String title = selectedCursor.getString(selectedCursor
                    .getColumnIndex(MediaConsts.AudioMedia.TITLE));
            String album = selectedCursor.getString(selectedCursor
                    .getColumnIndex(MediaConsts.AudioMedia.ALBUM));
            String artist = selectedCursor.getString(selectedCursor
                    .getColumnIndex(MediaConsts.AudioMedia.ARTIST));
            String data = selectedCursor.getString(selectedCursor
                    .getColumnIndex(MediaConsts.AudioMedia.DATA));
            long duration = selectedCursor.getLong(selectedCursor
                    .getColumnIndex(MediaConsts.AudioMedia.DURATION));

            switch (what) {
                case SystemConsts.EVT_SELECT_RATING: {
                    RatingDialog dlg = new RatingDialog(getActivity(),
                            TableConsts.FAVORITE_TYPE_SONG, mediaId, mHandler);
                    dlg.show();
                }
                    break;
                case SystemConsts.EVT_SELECT_ARTIST: {
                    FragmentTransaction t = getFragmentManager().beginTransaction();
                    AnimationHelper.setFragmentToPlayBack(t);
                    t.replace(R.id.fragment_main,
                            jp.co.kayo.android.localplayer.fragment.ArtistListFragment
                                    .createFragment(null, artist, -1));
                    t.addToBackStack(SystemConsts.TAG_SUBFRAGMENT);
                    t.commit();
                }
                    break;
                case SystemConsts.EVT_SELECT_ADD: {
                    AsyncAddPlaylistTask task = new AsyncAddPlaylistTask(getActivity(),
                            getFragmentManager(), AudioMedia._ID + " = ?", new String[] {
                                    Long.toString(mediaId)
                            });
                    task.execute();
                }
                    break;
                case SystemConsts.EVT_SELECT_OPENALBUM: {
                    String album_key = selectedCursor.getString(selectedCursor
                            .getColumnIndex(MediaConsts.AudioMedia.ALBUM_KEY));

                    FragmentTransaction t = getFragmentManager().beginTransaction();
                    AnimationHelper.setFragmentToPlayBack(t);
                    t.replace(R.id.fragment_main, AlbumSongsFragment.createFragment(album_key));
                    t.addToBackStack(SystemConsts.TAG_SUBFRAGMENT);
                    t.commit();
                }
                    break;
                case SystemConsts.EVT_SELECT_MORE: {
                    showInfoDialog(album, artist, title);
                }
                    break;
                case SystemConsts.EVT_SELECT_CLEARCACHE: {
                    ContentsUtils.clearMediaCache(getActivity(), data);
                    ContentValues values = new ContentValues();
                    values.put(TableConsts.AUDIO_CACHE_FILE, (String) null);
                    getActivity().getContentResolver().update(
                            ContentUris.withAppendedId(
                                    MediaConsts.MEDIA_CONTENT_URI, mediaId),
                            values, null, null);
                    datasetChanged();
                }
                    break;
                case SystemConsts.EVT_SELECT_LOVE: {
                    ContentsUtils.lastfmLove(getActivity(), title, artist, album,
                            Long.toString(duration));
                }
                    break;
                case SystemConsts.EVT_SELECT_BAN: {
                    ContentsUtils.lastfmBan(getActivity(), title, artist, album,
                            Long.toString(duration));
                }
                    break;
                case SystemConsts.EVT_SELECT_EDIT: {
                    // 文字化け修正
                    Intent intent = new Intent(getActivity(), TagEditActivity.class);
                    intent.putExtra(SystemConsts.KEY_EDITTYPE,
                            TagEditActivity.MEDIA);
                    intent.putExtra(SystemConsts.KEY_EDITKEY,
                            Long.toString(mediaId));
                    getActivity().startActivityForResult(intent, SystemConsts.REQUEST_TAGEDIT);
                }
                    break;
                default: {
                }
            }
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, final int position, long id) {
        final Cursor cursor = (Cursor) getListAdapter().getItem(position);
        final BaseActivity base = (BaseActivity)getActivity();
        final IMediaPlayerService binder = base.getBinder();
        
        AsyncTask<Void, Void, Void> animTask = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                playMediaReplace(contentKey, binder, cursor,
                        MediaConsts.AudioMedia._ID,
                        MediaConsts.AudioMedia.DURATION,
                        MediaConsts.AudioMedia.DATA, position);
                return null;
            }
        };
        animTask.execute();
        //AnimationHelper.setViewToPlayback(v, new BaseAnimationListener((BaseActivity)getActivity()) {});
    }

    @Override
    public void reload() {
        getLoaderManager().restartLoader(getFragmentId(), null, this);
    }

    @Override
    public void changedMedia() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void release() {
        // TODO Auto-generated method stub

    }

    @Override
    public String selectSort() {
        int sort = mPref.getInt(SORTKEY, 1);
        sort++;

        if (sort > 3) {
            sort = 0;
        }
        Editor editor = mPref.edit();
        editor.putInt(SORTKEY, sort);
        editor.commit();
        getLoaderManager().restartLoader(getFragmentId(), null, this);

        return getName(getActivity());
    }

    @Override
    public int getFragmentId() {
        return R.layout.media_list_view;
    }

    private String[] getSort() {
        int sort = mPref.getInt(SORTKEY, 1);
        String sortOrder = null;
        String cname = MediaConsts.AudioMedia.ALBUM;
        if (sort == 0) {
            sortOrder = MediaConsts.AudioMedia.ALBUM;
        } else if (sort == 1) {
            sortOrder = MediaConsts.AudioMedia.ARTIST + ","
                    + MediaConsts.AudioMedia.ALBUM + ","
                    + MediaConsts.AudioMedia.TITLE;
            cname = MediaConsts.AudioAlbum.ARTIST;
        } else if (sort == 2) {
            sortOrder = MediaConsts.AudioMedia.TITLE;
            cname = MediaConsts.AudioMedia.TITLE;
        } else if (sort == 3) {
            sortOrder = MediaConsts.AudioMedia.DATE_ADDED;
        }

        return new String[] {
                cname, sortOrder
        };
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String selection = null;
        String[] selectionArgs = null;
        String[] sortname = getSort();
        sort_cname = sortname[0];
        if (mAdapter == null) {
            mAdapter = new MediaListViewAdapter(getActivity(), null, viewcache,
                    sortname[0]);
            setListAdapter(mAdapter);
        } else {
            Cursor cur = mAdapter.swapCursor(null);
            if (cur != null) {
                cur.close();
            }
        }

        if (Funcs.isNotEmpty(mQueryString)) {
            StringBuilder buf = new StringBuilder();
            buf.append("( ").append(MediaConsts.AudioMedia.TITLE + " like '%' || ? || '%' ) OR (");
            // buf.append(MediaConsts.AudioMedia.ALBUM +
            // " like '%' || ? || '%' ) OR (");
            buf.append(MediaConsts.AudioMedia.ARTIST + " like '%' || ? || '%' )");
            selection = buf.toString();
            selectionArgs = new String[] {
                    mQueryString, mQueryString
            };// , mQueryString};
        }

        showProgressBar();
        contentKey = SystemConsts.CONTENTSKEY_MEDIA + System.currentTimeMillis();
        return new CursorLoader(getActivity(), MediaConsts.MEDIA_CONTENT_URI,
                null, selection, selectionArgs, sortname[1]);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        hideProgressBar();
        if (mAdapter != null) {
            if (data == null || data.isClosed()) {
                if(data==null) mAdapter.swapCursor(null);
                return;
            }
            if (isVisible()) {
                getListView().setFastScrollEnabled(false);
            }
            mAdapter.mCname = sort_cname;
            Cursor cur = mAdapter.swapCursor(data);
            if (isVisible()) {
                getListView().setFastScrollEnabled(true);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        hideProgressBar();
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
            int visibleItemCount, int totalItemCount) {
        // TODO Auto-generated method stub

    }

    public boolean playMediaReplace(String contentkey,
            IMediaPlayerService binder, Cursor cursor, String cname1,
            String cname2, String cname3, int position) {
        try {
            if (!binder.setContentsKey(contentkey)) {
                // 既に設定中のリストだった
                int pos = binder.getPosition();
                if (pos != position) {// 同じ位置をクリックした
                    binder.lockUpdateToPlay();
                    try {
                        binder.stop();
                        binder.setPosition(position);
                    } finally {
                        binder.play();
                    }
                    return true;
                }
            } else {
                binder.lockUpdateToPlay();
                try {
                    binder.clear();
                    cursor.moveToFirst();
                    do {
                        long id = cursor.getLong(cursor.getColumnIndex(cname1));
                        String data = cursor.getString(cursor.getColumnIndex(cname3));
                        binder.addMedia(id, data);
                    } while (cursor.moveToNext());
                    binder.setPosition(position);
                    cursor.moveToPosition(position);
                } finally {
                    binder.play();
                }
                return true;
            }
        } catch (RemoteException e) {
        }
        return false;
    }

    @Override
    public void startProgress(final long max) {
        if (mAdapter != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    viewcache.startProgress(max);
                    IMediaPlayerService binder = getBinder();
                    if (binder != null) {
                        try {
                            viewcache.setPrefetchId(binder.getPrefetchId());
                        } catch (RemoteException e) {
                        }
                    }
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public void stopProgress() {
        if (mAdapter != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    viewcache.stopProgress();
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public void progress(final long pos, final long max) {
        if (mAdapter != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    viewcache.progress(pos, max);
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        Logger.d("onScrollStateChanged:" + scrollState);
        long time = Funcs.getHideTime(mPref);
        if (time > 0) {
            if (scrollState == 1) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                if (getFragmentManager() != null) {
                    ControlFragment control = (ControlFragment) getFragmentManager()
                            .findFragmentByTag(SystemConsts.TAG_CONTROL);
                    if (control != null) {
                        control.hideControl(false);
                    }
                }
            } else if (scrollState == 0) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                mTask = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (getFragmentManager() != null) {
                                ControlFragment control = (ControlFragment) getFragmentManager()
                                        .findFragmentByTag(
                                                SystemConsts.TAG_CONTROL);
                                if (control != null) {
                                    control.showControl(false);
                                }
                            }
                        } finally {
                            mTask = null;
                        }
                    }
                };
                mHandler.postDelayed(mTask, time);
            }
        }
    }

    @Override
    public boolean onBackPressed() {
        if (mMode != null) {
            mMode.finish();
            mMode = null;
            return true;
        }
        return false;
    }

    @Override
    protected void datasetChanged() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void addRow(Object[] values) {

    }

    @Override
    protected void hideMenu() {
        if (mMode != null) {
            mMode.finish();
            mMode = null;
        }
    }

    @Override
    public String getName(Context context) {
        SharedPreferences pref = mPref;
        if(pref == null){
            pref = PreferenceManager.getDefaultSharedPreferences(context);
        }
        int sort = pref.getInt(SORTKEY, 1);
        if(sort == 1)
            return context.getString(R.string.lb_tab_media_artist);
        else if(sort == 2)
            return context.getString(R.string.lb_tab_media_title);
        else if(sort == 3)
            return context.getString(R.string.lb_tab_media_date);
        else
            return context.getString(R.string.lb_tab_media_album);
    }

    @Override
    public void doSearchQuery(String queryString) {
        boolean dirty = mQueryString == null || !mQueryString.equals(queryString);
        if (dirty) {
            mQueryString = queryString;
            reload();
        }
    }
}
