package jp.co.kayo.android.localplayer.adapter;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.Hashtable;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.appwidget.ColorSet;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.core.IndexCursorAdapter;
import jp.co.kayo.android.localplayer.core.ViewHolder;
import jp.co.kayo.android.localplayer.util.ViewCache;
import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class GenresListViewAdapter extends IndexCursorAdapter {
    LayoutInflater inflator;
    Context context;
    Handler handler = new Handler();
    java.text.DateFormat format;
    Hashtable<String, Integer> tbl = new Hashtable<String, Integer>();

    public GenresListViewAdapter(Context context, ViewCache cache, Cursor c) {
        super(context, c, true, cache, MediaConsts.AudioGenres.NAME);
        this.context = context;
        format = DateFormat.getMediumDateFormat(context);
    }

    public LayoutInflater getInflator(Context context) {
        if (inflator == null) {
            inflator = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        return inflator;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        ViewHolder holder = (ViewHolder) view.getTag();
        if (position % 2 == 1) {
            holder.getBackground().setVisibility(View.GONE);
        } else {
            holder.getBackground().setVisibility(View.VISIBLE);
        }

        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder holder = (ViewHolder) view.getTag();
        String name = cursor.getString(cursor
                .getColumnIndex(MediaConsts.AudioGenres.NAME));
        int colnum = cursor.getColumnIndex("number_of_tracks");
        if (colnum != -1) {
            name = name + "(" + cursor.getInt(colnum) + ")";
        }

        holder.getText2().setText(name);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View v = getInflator(context).inflate(R.layout.genres_grid_row, parent,
                false);
        ViewHolder holder = new ViewHolder();
        holder.setText2((TextView) v.findViewById(R.id.textName));
        getViewcache().getColorset().setColor(ColorSet.KEY_DEFAULT_PRI_COLOR, holder.getText2());
        holder.setBackground(v.findViewById(R.id.background));
        v.setTag(holder);
        return v;
    }
}
