package jp.co.kayo.android.localplayer.service;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.UnknownHostException;
import java.util.Calendar;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.appwidget.AppWidgetHelper;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.pref.CacheConfigPreference;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.SSLHelper;
import jp.co.kayo.android.localplayer.util.SoundManager;
import jp.co.kayo.android.localplayer.util.bean.MediaData;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.widget.Toast;

public class MediaPlayerService2 extends Service {
    private static final String CALL_RESUME = "MediaPlayerService2.Call.Resume";
    
    public static final int MSG_CONNECTEXCEPTION = 1;
    public static final int MSG_IOEXCEPTION = 2;
    public static final int MSG_FILENOTFOUNDEXCEPTION = 3;
    public static final int MSG_STREAMING_NEXT = 4;
    public static final int MSG_STREAMING_CACHE_NOTHAVESPACE = 5;

    private SwitchMediaPlayer mediaPlayer;
    private SharedPreferences pref;
    private JukeBox jukebox;
    private NotificationHelper notificationHelper;
    private WifiLock wifiLock = null;
    private SoundManager soundmanager = new SoundManager();
    private AudioManager audiomgr;
    private BroadcastReceiver headsetplug = null;
    private BroadcastReceiver earPhoneChecker = null;
    private BroadcastReceiver connectivityAction = null;
    private PhoneStateListener telephone;
    private TelephonyManager tm;
    private StreamCacherServer streamcacheserv;
    private AudioFocus audiofocus;
    private Handler mHandler;
    
    boolean firsttime = true;
    private final RemoteCallbackList<IMediaPlayerServiceCallback> callbackList = new RemoteCallbackList<IMediaPlayerServiceCallback>();
    
    enum RING_STATE { STATE_RINGING, STATE_OFFHOOK, STATE_NORMAL };

    @Override
    public void onCreate() {
        super.onCreate();
        
        //
        pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        mediaPlayer = new SwitchMediaPlayer(this);
        jukebox = new JukeBox(this);
        notificationHelper = new NotificationHelper(this);
        soundmanager.init(this);
        wifiLock = ((WifiManager) getSystemService(Context.WIFI_SERVICE))
                .createWifiLock(WifiManager.WIFI_MODE_FULL, "mylock");
        audiomgr = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        audiofocus = new AudioFocus(this, mHandler, audiomgr);
        
        //
        lockWifi();
        new SSLHelper().disableSSLVerifier();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        
        mediaPlayer.release();
        
        if (streamcacheserv != null) {
            streamcacheserv.release();
            streamcacheserv.stop();
            streamcacheserv = null;
        }
        
        notificationHelper.stopForegroundCompat(R.string.app_name);
        
        freeWifi();
        
        if (soundmanager != null) {
            soundmanager.release();
        }

        if (headsetplug != null) {
            unregisterReceiver(headsetplug);
        }
        if (earPhoneChecker != null) {
            unregisterReceiver(earPhoneChecker);
        }

        if (connectivityAction != null) {
            unregisterReceiver(connectivityAction);
        }

        if (audiofocus != null) {
            audiofocus.unregister(audiomgr);
            audiofocus = null;
        }
        
        if (telephone != null) {
            tm.listen(telephone, PhoneStateListener.LISTEN_NONE);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return bind;
    }
    
    void freeWifi() {
        if (wifiLock != null && wifiLock.isHeld()) {
            wifiLock.release();
        }
    }

    void lockWifi() {
        if (wifiLock != null) {
            wifiLock.acquire();
        }
    }
    
    public void safeVolume() {
        if (audiomgr != null && pref != null) {
            boolean b = pref.getBoolean(SystemConsts.KEY_AUTO_VOLUMEAJUST, true);
            if (b) {
                int volume = pref.getInt(SystemConsts.KEY_FIRST_VOLUME, 2);
                if (volume != 0) {
                    audiomgr.setStreamVolume(AudioManager.STREAM_MUSIC, volume,
                            AudioManager.FLAG_SHOW_UI);
                }
            }
        }
    }

    public void saveVolume() {
        if (audiomgr != null && pref != null) {
            if (HeadsetHelper.isWiredHeadsetOn(audiomgr)) {
                int volume = audiomgr.getStreamVolume(AudioManager.STREAM_MUSIC);
                Editor editor = pref.edit();
                editor.putInt(SystemConsts.KEY_FIRST_VOLUME, volume);
                editor.commit();
            }
        }
    }
    
    private void notifyOn() {
        if(jukebox!=null){
            MediaData media = jukebox.getCurrent();
            if(media!=null){
                //AsyncNotifyUpdate task = new AsyncNotifyUpdate(this, media);
                //task.execute();
            }
        }
    }

    private void notifyOff(boolean force) {
        //AsyncNotifyUpdate task = new AsyncNotifyUpdate(this, null);
        //task.execute();
    }
    
    private void setResumeStart(int delay, String action) {
        Intent intent = new Intent(this, MediaPlayerService2.class);
        intent.setAction(action);
        PendingIntent operation = PendingIntent.getService(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar cal = Calendar.getInstance();
        if (delay > 0) {
            cal.add(Calendar.SECOND, delay);
        }

        AlarmManager am = (AlarmManager) this
                .getSystemService(Activity.ALARM_SERVICE);
        am.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), operation);
    }

    private void setResumeStop(String action) {
        Intent intent = new Intent(this, MediaPlayerService2.class);
        intent.setAction(action);
        PendingIntent operation = PendingIntent.getService(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager am = (AlarmManager) this
                .getSystemService(Activity.ALARM_SERVICE);
        am.cancel(operation);
    }
    
    void init(){
        if (jukebox.isLoaded() != true) {
            jukebox.load();
        }

        if (streamcacheserv == null) {
            try {
                streamcacheserv = new StreamCacherServer(
                        getApplicationContext(), mHandler);
                streamcacheserv.start();
            } catch (UnknownHostException e) {
                Logger.e("StreamCacherServer.UnknownHostException", e);
                streamcacheserv = null;
            } catch (IOException e) {
                Logger.e("StreamCacherServer.IOException", e);
                streamcacheserv = null;
            }
        }
        
        if(headsetplug == null){
            headsetplug = new BroadcastReceiver() {
                boolean firstPluggd = true;
                @Override
                public void onReceive(Context context, Intent intent) {
                    if (intent.getAction().equalsIgnoreCase(Intent.ACTION_HEADSET_PLUG)) {
                        int state = intent.getIntExtra("state", 0);
                        Logger.v("Intent.ACTION_HEADSET_PLUG state is " + state);
                        // イヤホンを刺した
                        if (state == 1 && firstPluggd) {
                            firstPluggd = false;
                            // AutoVolume
                            safeVolume();
                            firsttime = false;
    
                            // AutoReplay
                            boolean autoreplay = pref.getBoolean("key.useAutoPlay",
                                    true);
                            if (autoreplay) {
                                if (mediaPlayer != null && jukebox.getList().size() > 0
                                        && !mediaPlayer.isPlaying()) {
                                    Toast.makeText(
                                            MediaPlayerService2.this,
                                            getString(R.string.txt_action_headset_plug),
                                            Toast.LENGTH_SHORT).show();
                                    try {
                                        bind.play();
                                    } catch (RemoteException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        } else if (state != 1) {
                            firstPluggd = true;
                        }
                    }
                }
            };
            registerReceiver(headsetplug, new IntentFilter(Intent.ACTION_HEADSET_PLUG));
        }
        
        if(earPhoneChecker == null){
            earPhoneChecker = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    // イヤホンを抜いた
                    Toast.makeText(MediaPlayerService2.this,
                            getString(R.string.txt_action_audio_becoming_noisy),
                            Toast.LENGTH_SHORT).show();
                    try {
                        bind.pause();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            };        
            registerReceiver(earPhoneChecker, new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY));
        }
        
        if(connectivityAction == null){
            connectivityAction = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if (intent.getAction().equalsIgnoreCase(
                            ConnectivityManager.CONNECTIVITY_ACTION)) {
                        getContentResolver().query(MediaConsts.CLEAR_CONTENT_URI, null,
                                null, null, null);
                        if (streamcacheserv != null) {
                            streamcacheserv.start();
                        }
                    }
                }
            };
            registerReceiver(connectivityAction, new IntentFilter(
                    ConnectivityManager.CONNECTIVITY_ACTION));
        }

        if(telephone == null){
            telephone = new PhoneStateListener() {
                boolean onhook = false;
                RING_STATE callstaet;

                public void onCallStateChanged(int state, String number) {
                    switch (state) {
                        case TelephonyManager.CALL_STATE_RINGING: {
                            callstaet = RING_STATE.STATE_RINGING;
                            if (mediaPlayer != null) {
                                if (mediaPlayer.isPlaying()) {
                                    try {
                                        bind.pause();
                                    } catch (RemoteException e) {
                                        e.printStackTrace();
                                    }
                                    onhook = true;
                                    setResumeStop(CALL_RESUME);
                                }
                            }
                        }
                            break;
                        case TelephonyManager.CALL_STATE_OFFHOOK: {
                            if (callstaet == RING_STATE.STATE_RINGING) {
                                callstaet = RING_STATE.STATE_OFFHOOK;
                            } else {
                                callstaet = RING_STATE.STATE_NORMAL;
                                if (mediaPlayer != null) {
                                    if (mediaPlayer.isPlaying()) {
                                        try {
                                            bind.pause();
                                        } catch (RemoteException e) {
                                            e.printStackTrace();
                                        }
                                        onhook = true;
                                        setResumeStop(CALL_RESUME);
                                    }
                                }
                            }
                        }
                            break;
                        case TelephonyManager.CALL_STATE_IDLE: {
                            if (onhook) {
                                onhook = false;
                                setResumeStart(5, CALL_RESUME);
                            }
                            callstaet = RING_STATE.STATE_NORMAL;
                        }
                            break;
                        default: {

                        }
                    }
                }
            };
            tm.listen(telephone, PhoneStateListener.LISTEN_CALL_STATE);
        }

    }
    
    //control 
    int stat(int forceState) {
        // Logger.d("stat");
        int shf = jukebox.isShaffle() ? AppWidgetHelper.FLG_SHUFFLE
                : 0;
        int next = 0;
        int back = 0;
        int list = 0;
        int repeat = jukebox.getRepeatMode() == SystemConsts.FLG_REPEAT_ONCE ? AppWidgetHelper.FLG_LOOP1
                : jukebox.getRepeatMode() == SystemConsts.FLG_REPEAT_ALL ? AppWidgetHelper.FLG_LOOP2
                        : SystemConsts.FLG_REPEAT_NO;
        list = jukebox.getList().size() > 0 ? AppWidgetHelper.FLG_HASLIST : 0;
        if (list != 0) {
            next = jukebox.hasNext() ? AppWidgetHelper.FLG_HASNEXT : 0;
            back = jukebox.getPosition() >= 0 ? AppWidgetHelper.FLG_HASFOWD : 0;
        }

        if (forceState == 0) {
            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                return AppWidgetHelper.FLG_PLAY | next | back | repeat | shf
                        | list;
            } else {
                if (list != 0) {
                    return AppWidgetHelper.FLG_PAUSE | next | back | repeat
                            | shf | list;
                } else {
                    return AppWidgetHelper.FLG_STOP | next | back | repeat
                            | shf;
                }
            }
        } else {
            return forceState | next | back | repeat | shf | list;
        }
    }
    
    public void progress(int percent) {
        // 再描画を通知する
        synchronized (callbackList) {
            int n = callbackList.beginBroadcast();
            for (int i = 0; i < n; i++) {
                try {
                    callbackList.getBroadcastItem(i).onBufferingUpdate(percent);
                } catch (RemoteException e) {
                }
            }
            callbackList.finishBroadcast();
        }
    }
    
    IMediaPlayerService.Stub bind = new IMediaPlayerService.Stub() {

        @Override
        public long getMediaId() throws RemoteException {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public int getPosition() throws RemoteException {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public int getPort() throws RemoteException {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public long getPrefetchId() throws RemoteException {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public int getCount() throws RemoteException {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public int stat() throws RemoteException {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public void stop() throws RemoteException {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void pause() throws RemoteException {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void play() throws RemoteException {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void ff() throws RemoteException {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void rew() throws RemoteException {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void shuffle() throws RemoteException {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void repeat() throws RemoteException {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void seek(int pos) throws RemoteException {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void clear() throws RemoteException {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void clearcut() throws RemoteException {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void setPosition(int pos) throws RemoteException {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void drop(int from, int to) throws RemoteException {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void remove(int pos) throws RemoteException {
            // TODO Auto-generated method stub
            
        }

        @Override
        public long getSeekPosition() throws RemoteException {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public long getDuration() throws RemoteException {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public void addMedia(long id, String data) throws RemoteException {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void addMediaD(long id, long duration, String title, String album, String artist,
                String data) throws RemoteException {
            // TODO Auto-generated method stub
            
        }

        @Override
        public String[] getMediaD(int pos) throws RemoteException {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long[] getList() throws RemoteException {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public void release() throws RemoteException {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void commit() throws RemoteException {
            // TODO Auto-generated method stub
            
        }

        @Override
        public int getAudioSessionId() throws RemoteException {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public boolean setContentsKey(String key) throws RemoteException {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public String getContentsKey() throws RemoteException {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public void lockUpdateToPlay() throws RemoteException {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void setEqEnabled(boolean enabled) throws RemoteException {
            // TODO Auto-generated method stub
            
        }

        @Override
        public boolean getEqEnabled() throws RemoteException {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public void setRvEnabled(boolean enabled) throws RemoteException {
            // TODO Auto-generated method stub
            
        }

        @Override
        public boolean getRvEnabled() throws RemoteException {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public void setBsEnabled(boolean enabled) throws RemoteException {
            // TODO Auto-generated method stub
            
        }

        @Override
        public boolean getBsEnabled() throws RemoteException {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public int getNumberOfPresets() throws RemoteException {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public String getPresetName(int n) throws RemoteException {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public int getBandLevel(int band) throws RemoteException {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public int getCenterFreq(int band) throws RemoteException {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public int getCurrentPreset() throws RemoteException {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public int getNumberOfBands() throws RemoteException {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public int getMinEQLevel() throws RemoteException {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public int getMaxEQLevel() throws RemoteException {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public void usePreset(int preset) throws RemoteException {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void setBandLevel(int band, int level) throws RemoteException {
            // TODO Auto-generated method stub
            
        }

        @Override
        public int getRvPreset() throws RemoteException {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public void setRvPreset(int preset) throws RemoteException {
            // TODO Auto-generated method stub
            
        }

        @Override
        public int getStrength() throws RemoteException {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public void setStrength(int strength) throws RemoteException {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void registerCallback(IMediaPlayerServiceCallback callback)
                throws RemoteException {
            callbackList.register(callback);
        }

        @Override
        public void unregisterCallback(IMediaPlayerServiceCallback callback)
                throws RemoteException {
            callbackList.unregister(callback);
        }

        @Override
        public void reload() throws RemoteException {
            // TODO Auto-generated method stub
            
        }
    };
}
