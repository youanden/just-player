package jp.co.kayo.android.localplayer.service;

import jp.co.kayo.android.localplayer.fx.AudioFx;
import android.annotation.TargetApi;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.audiofx.AudioEffect;
import android.os.Build;

public class SwitchMediaPlayer implements OnPreparedListener, OnCompletionListener, OnBufferingUpdateListener, OnErrorListener {
    private MediaPlayer[] mp;
    private AudioFx[] audiofx;
    private int pos = 0;
    private Context context;
    
    private OnPreparedListener mOnPreparedListener;
    private OnCompletionListener mOnCompletionListener;
    private OnBufferingUpdateListener mOnBufferingUpdateListener;
    private OnErrorListener mOnErrorListener;

    @TargetApi(16)
    public SwitchMediaPlayer(Context context){
        this.context = context;
        if(Build.VERSION.SDK_INT<16){
            mp = new MediaPlayer[1];
            audiofx = new AudioFx[1];
            mp[0] = new MediaPlayer();
            mp[0].setOnCompletionListener(this);
            mp[0].setAudioStreamType(AudioManager.STREAM_MUSIC);
        }
        else{
            mp = new MediaPlayer[2];
            audiofx = new AudioFx[2];
            
            mp[0] = new MediaPlayer();
            mp[0].setOnCompletionListener(this);
            mp[0].setAudioStreamType(AudioManager.STREAM_MUSIC);
            mp[1] = new MediaPlayer();
            mp[1].setOnCompletionListener(this);
            mp[1].setAudioStreamType(AudioManager.STREAM_MUSIC);
            
            mp[0].setNextMediaPlayer(mp[1]);
            mp[1].setNextMediaPlayer(mp[0]);
            
            pos = 0;
        }
    }
    
    public MediaPlayer getMediaPlayer(){
        if(Build.VERSION.SDK_INT<16){
            return mp[0];
        }
        else{
            return mp[pos];
        }
    }
    
    private void setListener(MediaPlayer mp, boolean b){
        if(b){
            mp.setOnPreparedListener(this);
            mp.setOnBufferingUpdateListener(this);
            mp.setOnErrorListener(this);
        }
        else{
            mp.setOnPreparedListener(null);
            mp.setOnBufferingUpdateListener(null);
            mp.setOnErrorListener(null);
        }
    }
    
    @Override
    public void onCompletion(MediaPlayer m) {
        if(mp.length>1){
            if(m.isLooping()!=true){
                if(mp[0] == m){
                    pos = 1;
                    setListener(mp[0], false);
                    setListener(mp[1], true);
                }
                else{
                    pos = 0;
                    setListener(mp[1], false);
                    setListener(mp[0], true);
                }
            }
        }
        
        if(mOnCompletionListener!=null){
            mOnCompletionListener.onCompletion(mp[pos]);
        }
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        if(mOnBufferingUpdateListener!=null){
            mOnBufferingUpdateListener.onBufferingUpdate(mp, percent);
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        if(mOnPreparedListener!=null){
            mOnPreparedListener.onPrepared(mp);
        }
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        if(mOnErrorListener!=null){
            return mOnErrorListener.onError(mp, what, extra);
        }
        return false;
    }

    public void release() {
        for(int i=0; i<mp.length; i++){
            mp[i].release();
            mp[i] = null;
        }
        for(int i=0; i<audiofx.length; i++){
            audiofx[i].release();
            audiofx[i] = null;
        }
    }

    public void setOnPreparedListener(OnPreparedListener l) {
        this.mOnPreparedListener = l;
    }

    public void setOnCompletionListener(OnCompletionListener l) {
        this.mOnCompletionListener = l;
    }

    public void setOnBufferingUpdateListener(OnBufferingUpdateListener l) {
        this.mOnBufferingUpdateListener = l;
    }

    public void setOnErrorListener(OnErrorListener l) {
        this.mOnErrorListener = l;
    }

    public boolean isPlaying() {
        return getMediaPlayer().isLooping();
    }

    
}
