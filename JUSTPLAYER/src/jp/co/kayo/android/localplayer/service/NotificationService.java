
package jp.co.kayo.android.localplayer.service;

import jp.co.kayo.android.localplayer.MainActivity2;
import jp.co.kayo.android.localplayer.appwidget.AppWidgetHelper;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

public class NotificationService extends Service {
    private float mX;
    private float mY;

    /** ビュー(画面全体に展開) */
    private View mView;
    private WindowManager mWindowManager;

    @Override
    public void onCreate() {
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        setTouchView();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && "Click".equals(intent.getAction())) {
            DisplayMetrics metrics = new DisplayMetrics();
            mWindowManager.getDefaultDisplay().getMetrics(metrics);

            float x1 = metrics.scaledDensity * 0;
            float x2 = metrics.scaledDensity * 64 + x1;
            float height = metrics.scaledDensity * 64;
            float freespace = (float) metrics.widthPixels - x2;
            float sepsize = freespace / 4;

            float x3 = x2;
            float x4 = sepsize + x3;

            float x5 = x4;
            float x6 = sepsize + x5;

            float x7 = x6;
            float x8 = sepsize + x7;

            float x9 = x8;
            float x10 = sepsize + x9;

            if (mX >= x1 && mX < x2) {
                // AlbumArt
                Intent i = new Intent(this, MainActivity2.class);
                i.setAction(SystemConsts.MAIN_ACITON_SHOWHOMW);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            } else if (mX >= x3 && mX < x4) {
                // rew
                Intent i = createIntent(this, AppWidgetHelper.CALL_REW);
                startService(i);
            } else if (mX >= x5 && mX < x6) {
                // pause play
                Intent i = createIntent(this, AppWidgetHelper.CALL_PLAY_PAUSE);
                startService(i);
            } else if (mX >= x7 && mX < x8) {
                // ff
                Intent i = createIntent(this, AppWidgetHelper.CALL_FF);
                startService(i);
            } else if (mX >= x9 && mX < x10) {
                // close
                Intent i = createIntent(this, AppWidgetHelper.CALL_STOP);
                startService(i);
            }

        }

        return START_NOT_STICKY;
    }

    public void onDestroy() {
        mWindowManager.removeView(mView);
        mView = null;
    }

    private Intent createIntent(Context context, String action) {
        Intent i = new Intent(context, MediaPlayerService.class);
        i.setAction(action);
        return i;
    }

    /**
     * タッチイベントの起動
     */
    private void setTouchView() {
        if (mView == null) {
            mView = new View(this) {
                public boolean onTouchEvent(MotionEvent motionevent) {
                    // タップされたX座標を代入
                    mX = motionevent.getX();
                    mY = motionevent.getY();
                    return super.onTouchEvent(motionevent);
                }
            };
        }

        // 面倒なので画面の横幅決め打ち
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
                WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, PixelFormat.TRANSLUCENT);
        mWindowManager.addView(mView, params);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
