package jp.co.kayo.android.localplayer.service;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;

import android.content.SharedPreferences;
import android.os.Handler;

public class PrefetchResponseHandler implements
        org.apache.http.client.ResponseHandler<String> {
    final long LIMIT_PROG_STACK = 1024 * 2000;
    public final String RFC1123_PATTERN = "EEE, dd MMM yyyyy HH:mm:ss z";
    public final DateFormat rfc1123Format = new SimpleDateFormat(
            RFC1123_PATTERN, Locale.US);

    SharedPreferences mPref;
    int streambuffsize;
    long mDownloaded;
    long mSeekSize;
    String mUri;
    OnPrefetchProgressListener mListener;
    boolean stopHandle = false;
    File mTempFile;
    BufferedOutputStream mSocketWriter;
    private Handler mHandler = null;

    public PrefetchResponseHandler(Handler handler, File tempFile,
            SharedPreferences pref, int bufsize, long downloaded,
            OnPrefetchProgressListener listener) {
        this.mHandler = handler;
        this.streambuffsize = bufsize;
        this.mTempFile = tempFile;
        this.mPref = pref;
        this.mDownloaded = downloaded;
        this.mListener = listener;
    }

    public PrefetchResponseHandler(BufferedOutputStream sos, File tempFile,
            SharedPreferences pref, int bufsize, String uri, long seekSize,
            long downloaded, OnPrefetchProgressListener listener) {
        this.streambuffsize = bufsize;
        this.mSocketWriter = sos;
        this.mTempFile = tempFile;
        this.mPref = pref;
        this.mUri = uri;
        this.mSeekSize = seekSize;
        this.mDownloaded = downloaded;
        this.mListener = listener;
    }

    @Override
    public String handleResponse(HttpResponse response)
            throws ClientProtocolException, IOException {
        int code = response.getStatusLine().getStatusCode();
        Logger.d("on handleResponse:" + code);
        switch (code) {
        case HttpStatus.SC_NON_AUTHORITATIVE_INFORMATION:
        case HttpStatus.SC_PARTIAL_CONTENT:
        case HttpStatus.SC_OK: {
            InputStream ris = null;
            BufferedOutputStream fos = null;

            long lastsize = response.getEntity().getContentLength();
            long totallength = mDownloaded + lastsize;
            int cacheState = Funcs.canUseCache(mPref, totallength);

            try {
                if (mSocketWriter != null) {
                    if (mSeekSize == 0) {
                        writeFullHeader(mSocketWriter, mUri, totallength);
                    } else {
                        writePartialHeader(mSocketWriter, mUri, totallength
                                - mSeekSize);
                    }

                    byte[] b = new byte[streambuffsize];
                    long nbBytesReadIncremental = 0;
                    int nbBytesRead = 0;

                    // すでにダウンロード済みのものを書きこむ
                    if (mDownloaded > mSeekSize) {
                        Logger.d("Write from Storage");
                        BufferedInputStream fis = null;
                        try {
                            fis = new BufferedInputStream(new FileInputStream(
                                    mTempFile));
                            if (mSeekSize > 0) {
                                nbBytesReadIncremental += fis.skip(mSeekSize);
                            }
                            while ((nbBytesRead = fis.read(b)) != -1
                                    && !stopHandle) {
                                nbBytesReadIncremental += nbBytesRead;
                                mSocketWriter.write(b, 0, nbBytesRead);
                            }
                        } finally {
                            if (fis != null) {
                                fis.close();
                            }
                        }
                    } else {
                        nbBytesReadIncremental = mDownloaded;
                    }

                    if (stopHandle) {
                        return "";
                    }

                    if ((cacheState == Funcs.CACHE_STATE_OK || cacheState == Funcs.CACHE_STATE_NOT_LIMIT_SPACE)
                            && mDownloaded >= mSeekSize) {
                        cacheState = Funcs.CACHE_STATE_DENGERUS;
                    }
                }

                if ((cacheState == Funcs.CACHE_STATE_OK || cacheState == Funcs.CACHE_STATE_NOT_LIMIT_SPACE) || mSocketWriter != null) {
                    if (cacheState == Funcs.CACHE_STATE_NOT_LIMIT_SPACE) {
                        mHandler.sendEmptyMessage(MediaPlayerService.MSG_STREAMING_CACHE_NOTHAVESPACE);
                    }
                    
                    if (mListener != null) {
                        mListener.startProgress(totallength);
                    }
                    if (cacheState == Funcs.CACHE_STATE_OK) {
                        fos = new BufferedOutputStream(new FileOutputStream(
                                mTempFile, mDownloaded > 0));
                    }

                    byte[] b = new byte[streambuffsize];
                    // Server側のサイトのコネクションを取得する
                    if (ris == null) {
                        ris = response.getEntity().getContent();
                    }
                    int nbBytesRead = 0;
                    long nbBytesReadIncremental = mDownloaded;
                    long progStack = 0;
                    while ((nbBytesRead = ris.read(b)) != -1 && !stopHandle) {
                        nbBytesReadIncremental += nbBytesRead;
                        if (mSocketWriter != null) {
                            if (nbBytesReadIncremental > mSeekSize) {
                                long since = nbBytesReadIncremental == 0 ? 0
                                        : nbBytesReadIncremental - nbBytesRead;
                                if (since >= mSeekSize) {
                                    mSocketWriter.write(b, 0, nbBytesRead);
                                } else {
                                    int d = (int) (nbBytesReadIncremental - mSeekSize);
                                    int size = nbBytesRead - d;
                                    mSocketWriter.write(b, size, nbBytesRead
                                            - size);
                                }
                            }
                        }
                        if (fos != null) {
                            progStack += nbBytesRead;
                            fos.write(b, 0, nbBytesRead);
                            if (progStack > LIMIT_PROG_STACK) {
                                if (mListener != null) {
                                    mListener.progress(nbBytesReadIncremental,
                                            totallength);
                                }
                                progStack = 0;
                            }
                        }
                    }
                    if (totallength == nbBytesReadIncremental) {
                        return "Complete";
                    }
                }
            } finally {
                if (fos != null) {
                    fos.close();
                }
                if (ris != null) {
                    ris.close();
                }
            }
        }
        default:
            return "HTTP/1.1 " + code + " "
                    + response.getStatusLine().getReasonPhrase() + "\n";
        }
    }

    private void writeFullHeader(BufferedOutputStream writer, String uri,
            long totallength) throws IOException {
        String fname = uri.replaceFirst(".*name=([^&]+).*", "$1");
        String dstr = rfc1123Format.format(new Date());
        // キャッシュからデータを読み込む
        writer.write("HTTP/1.1 200 OK\n".getBytes());
        writer.write("Accept-Ranges: bytes\n".getBytes());
        writer.write("Connection: Keep-Alive\n".getBytes());
        writer.write(("Content-Disposition: attachment; filename=\"" + fname + "\"\n")
                .getBytes());
        writer.write(("Content-Length: " + totallength + "\n").getBytes());
        writer.write("Content-Type: audio/mpeg\n".getBytes());
        writer.write((dstr + "\n").getBytes());
        writer.write("Keep-Alive: timeout=3, max=50\n".getBytes());
        writer.write("Server: localserver\n".getBytes());
        writer.write("X-Powered-By: streamcache\n".getBytes());
        writer.write("\n".getBytes());
        writer.flush();
    }

    private void writePartialHeader(BufferedOutputStream writer, String uri,
            long totallength) throws IOException {
        String fname = uri.replaceFirst(".*name=([^&]+).*", "$1");
        String dstr = rfc1123Format.format(new Date());
        // キャッシュからデータを読み込む
        writer.write("HTTP/1.1 206 Partial Content\n".getBytes());
        writer.write("Accept-Ranges: bytes\n".getBytes());
        writer.write("Connection: Keep-Alive\n".getBytes());
        writer.write(("Content-Disposition: attachment; filename=\"" + fname + "\"\n")
                .getBytes());
        writer.write(("Content-Length: " + totallength + "\n").getBytes());
        writer.write("Content-Type: audio/mpeg\n".getBytes());
        writer.write((dstr + "\n").getBytes());
        writer.write("Keep-Alive: timeout=3, max=50\n".getBytes());
        writer.write("Server: localserver\n".getBytes());
        writer.write("X-Powered-By: streamcache\n".getBytes());
        writer.write("\n".getBytes());
        writer.flush();
    }

    public void stop() {
        stopHandle = true;
    }
}
