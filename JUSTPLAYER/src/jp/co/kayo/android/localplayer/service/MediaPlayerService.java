
package jp.co.kayo.android.localplayer.service;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Hashtable;

import jp.co.kayo.android.localplayer.LockScreenActivity;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.appwidget.AppWidgetHelper;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.Auth;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.fx.AudioFx;
import jp.co.kayo.android.localplayer.pref.CacheConfigPreference;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.NetworkHelper;
import jp.co.kayo.android.localplayer.util.SSLHelper;
import jp.co.kayo.android.localplayer.util.SdCardAccessHelper;
import jp.co.kayo.android.localplayer.util.SoundManager;
import jp.co.kayo.android.localplayer.util.bean.MediaData;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.widget.Toast;

public class MediaPlayerService extends Service implements OnPreparedListener,
        OnCompletionListener, OnBufferingUpdateListener, OnErrorListener,
        OnPrefetchProgressListener {

    public static final String CALL_RESUME = "MediaPlayerService.Call.Resume";
    public static final String CALL_KEEPSESSION = "MediaPlayerService.Call.KeepSession";
    public static final int NFCSHARE_RUNNING = 999;
    private static final int KEEP_DELAY = 60 * 30;

    private final RemoteCallbackList<IMediaPlayerServiceCallback> callbackList = new RemoteCallbackList<IMediaPlayerServiceCallback>();
    JukeBox jukebox;
    private boolean prepared = false;
    private boolean firsttime = true;
    private boolean isInitService = false;
    SoundManager soundmanager = new SoundManager();
    TelephonyManager tm;
    AudioManager audiomgr;
    AudioFocus audiofocus;
    private StreamCacherServer streamcacheserv;
    SharedPreferences pref;
    private boolean error = false; // 再生エラーが発生した場合
    private boolean retryConnect = false;
    private boolean updatelist = false;
    private boolean updatelock = false;
    private long prefetchId = -1;
    private WifiLock mWifiLock = null;
    private boolean mFadingNow = false;
    private Notification mNotification;

    public static final int MSG_CONNECTEXCEPTION = 1;
    public static final int MSG_IOEXCEPTION = 2;
    public static final int MSG_FILENOTFOUNDEXCEPTION = 3;
    public static final int MSG_STREAMING_NEXT = 4;
    public static final int MSG_STREAMING_CACHE_NOTHAVESPACE = 5;
    public static final int MSG_FOCUSCHANGE = 6;
    public static final int MSG_FADEUP = 7;
    public static final int MSG_FADEDOWN = 8;
    public static final int MSG_GETAUDIOFOCUS = 9;
    public static final int MSG_SETNOTIFICATION = 10;
    public static final int MSG_RELEASEAUDIOFOCUS = 11;
    public static final int MSG_UPDATEWIDGET = 12;

    enum RING_STATE {
        STATE_RINGING, STATE_OFFHOOK, STATE_NORMAL
    };

    NotificationHelper notificationHelper;
    private Handler mHandler = new MyHandler(this);

    private static class MyHandler extends Handler {
        WeakReference<MediaPlayerService> refService;
        float mCurrentVolume = 1.0f;

        MyHandler(MediaPlayerService service) {
            refService = new WeakReference<MediaPlayerService>(service);
        }

        @Override
        public void handleMessage(Message msg) {
            MediaPlayerService service = refService.get();
            if (service != null) {
                switch (msg.what) {
                    case MSG_CONNECTEXCEPTION: {
                        Logger.d("MediaPlayerSerivce.MSG_CONNECTEXCEPTION");
                        service.stop();
                        service.notifyOff(false);
                    }
                        break;
                    case MSG_IOEXCEPTION: {
                        // MediaPlayerService.this.stop();
                    }
                        break;
                    case MSG_FILENOTFOUNDEXCEPTION: {
                        // getContentResolver().query(MediaConsts.CLEAR_CONTENT_URI,
                        // null, null, null, null);
                    }
                        break;
                    case MSG_STREAMING_NEXT: {
                        if (msg.obj != null) {
                            Object[] params = (Object[]) msg.obj;
                            if (params != null && params.length > 1 && params[0] instanceof Long) {
                                ContentValues values = new ContentValues();
                                values.put(AudioMedia.AUDIO_CACHE_FILE, (String) params[1]);
                                service.getContentResolver().update(
                                        ContentUris.withAppendedId(
                                                MediaConsts.MEDIA_CONTENT_URI, (Long) params[0]),
                                        values, null, null);
                            }
                        }
                        service.startPrefetch();
                    }
                        break;
                    case MSG_STREAMING_CACHE_NOTHAVESPACE: {
                        boolean b = service.makeFreeSpace();
                        if (!b) {
                            if (service.notificationHelper.getNotificationManager() != null) {
                                Notification notification;
                                notification = new Notification(R.drawable.status,
                                        service.getString(R.string.txt_notify_info),
                                        System.currentTimeMillis());

                                Intent intent = new Intent(service,
                                        CacheConfigPreference.class);

                                PendingIntent contentIntent = PendingIntent.getActivity(
                                        service.getContext(), 0, intent,
                                        Intent.FLAG_ACTIVITY_NEW_TASK);
                                notification.setLatestEventInfo(service,
                                        service.getString(R.string.app_name),
                                        service.getString(R.string.txt_notify_cache_limit_nospace),
                                        contentIntent);

                                service.notificationHelper.getNotificationManager().notify(
                                        R.string.txt_notify_info, notification);
                            }
                        }
                    }
                        break;
                    case MSG_GETAUDIOFOCUS: {
                        if (service.audiofocus == null && Build.VERSION.SDK_INT > 7) {
                            service.audiofocus = new AudioFocus(service, service.mHandler,
                                    service.audiomgr);
                        }
                        // ICSのロックスクリーンはここの中でやってるからね！いつも忘れてる
                        Object[] objs = (Object[]) msg.obj;
                        Hashtable<String, String> tbl1 = (Hashtable<String, String>) objs[0];
                        Hashtable<String, String> tbl2 = (Hashtable<String, String>) objs[1];
                        Bitmap bitmap = (Bitmap) objs[2];
                        if (service.audiofocus != null) {
                            service.audiofocus.register(service);
                            service.audiofocus.setLockScreen(service, service.pref,
                                    AppWidgetHelper.FLG_PLAY, tbl1,
                                    tbl2, bitmap);
                        }
                    }
                        break;
                    case MSG_RELEASEAUDIOFOCUS: {
                        boolean keepNotify = service.pref.getBoolean("key.useKeepNotification",
                                true);
                        if (!keepNotify) {
                            service.notificationHelper.stopForegroundCompat(R.string.app_name);
                            service.mNotification = null;
                        }
                        else {
                            Notification notification = (Notification) msg.obj;
                            service.notificationHelper.startForegroundCompat(R.string.app_name,
                                    notification);
                        }
                        if (service.audiofocus != null) {
                            service.audiofocus.setLockScreen(service, service.pref,
                                    AppWidgetHelper.FLG_PAUSE, null, null,
                                    null);
                        }
                    }
                        break;
                    case MSG_UPDATEWIDGET: {
                        Object[] objs = (Object[]) msg.obj;
                        String mediaId = (String) objs[0];
                        String title = (String) objs[1];
                        String artist = (String) objs[2];
                        String album = (String) objs[3];
                        long duration = (Long) objs[4];
                        String path = (String) objs[5];
                        int stat = (Integer) objs[6];
                        service.updateWidgetTitle(mediaId, artist, title, album, duration,
                                path,
                                service.stat(stat));
                    }
                        break;
                    case MSG_SETNOTIFICATION: {
                        Object[] objs = (Object[]) msg.obj;
                        Hashtable<String, String> tbl1 = (Hashtable<String, String>) objs[0];
                        Hashtable<String, String> tbl2 = (Hashtable<String, String>) objs[1];
                        Bitmap bitmap = (Bitmap) objs[2];
                        Notification notification = (Notification) objs[3];
                        service.mNotification = notification;
                        if (Build.VERSION.SDK_INT >= 11) {
                            MediaPlayerHelper.setCustomizedNotify(service, notification,
                                    tbl1, tbl2, bitmap);
                        }
                        service.notificationHelper.startForegroundCompat(R.string.app_name,
                                notification);
                    }
                        break;
                    case MSG_FADEDOWN:
                        mCurrentVolume -= .05f;
                        if (mCurrentVolume > .2f) {
                            service.mHandler.sendEmptyMessageDelayed(MSG_FADEDOWN, 10);
                        } else {
                            mCurrentVolume = .2f;
                        }
                        service.jukebox.setVolume(mCurrentVolume);
                        break;
                    case MSG_FADEUP:
                        mCurrentVolume += .01f;
                        if (mCurrentVolume < 1.0f) {
                            service.mHandler.sendEmptyMessageDelayed(MSG_FADEUP, 10);
                        } else {
                            mCurrentVolume = 1.0f;
                        }
                        service.jukebox.setVolume(mCurrentVolume);
                        break;
                    case MSG_FOCUSCHANGE:
                        switch (msg.arg1) {
                            case AudioManager.AUDIOFOCUS_LOSS:
                                if (service.jukebox.isPlaying()) {
                                    service.mFadingNow = false;
                                }
                                service.pause();

                                break;
                            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                                service.mHandler.removeMessages(MSG_FADEUP);
                                service.mHandler.sendEmptyMessage(MSG_FADEDOWN);
                                break;
                            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                                if (service.jukebox.isPlaying()) {
                                    service.mFadingNow = true;
                                }
                                service.pause();
                                break;
                            case AudioManager.AUDIOFOCUS_GAIN:
                                if (!service.jukebox.isPlaying() && service.mFadingNow) {
                                    service.mFadingNow = false;
                                    mCurrentVolume = 0f;
                                    service.jukebox.setVolume(mCurrentVolume);
                                    service.play();
                                } else {
                                    service.mHandler.removeMessages(MSG_FADEDOWN);
                                    service.mHandler.sendEmptyMessage(MSG_FADEUP);
                                }
                                break;
                            default:
                        }
                        break;
                }
            }
        }

    };

    IMediaPlayerService.Stub bind = new IMediaPlayerService.Stub() {
        private String contentkey = null;

        @Override
        public boolean setContentsKey(String key) throws RemoteException {
            if (contentkey == null) {
                contentkey = key;
                return true;
            } else {
                if (contentkey.equals(key)) {
                    contentkey = key;
                    return false;
                } else {
                    contentkey = key;
                    return true;
                }
            }
        }

        @Override
        public void registerCallback(IMediaPlayerServiceCallback callback)
                throws RemoteException {
            callbackList.register(callback);
        }

        @Override
        public void unregisterCallback(IMediaPlayerServiceCallback callback)
                throws RemoteException {
            callbackList.unregister(callback);
        }

        @Override
        public void addMedia(long id, String data)
                throws RemoteException {
            updatelist = true;
            jukebox.addMedia(id, data);
        }

        @Override
        public void addMediaD(long id, long duration, String title,
                String album, String artist, String data)
                throws RemoteException {
            updatelist = true;
            // Logger.d("MediaPlayerService.addMedia");
            if(id == 0){
                id = System.nanoTime()*-1;
            }
            jukebox.addMedia(id, MediaData.NOTPLAYED, duration, title, album, artist, data);
        }

        @Override
        public void clear() throws RemoteException {
            Logger.d("MediaPlayerService.clear");
            if (jukebox.clear()) {
                updatelist = true;
                prepared = false;
                // jukebox.setPlayPosition(0);
                if (jukebox.stop()) {
                    notifyOff(false);
                }

                Editor editor = pref.edit();
                editor.putString("lastSetMediaId", "");
                editor.putString("lastSetTitle", "");
                editor.putString("lastSetArtist", "");
                editor.putString("lastSetAlbum", "");
                editor.commit();

                updateView();
            }
            // instantSave();
        }

        @Override
        public void clearcut() throws RemoteException {
            Logger.d("MediaPlayerService.clearcut");
            if (jukebox.clearcut()) {
                updatelist = true;
                // jukebox.setPlayPosition(0);
                if (jukebox.stop()) {
                    prepared = false;
                    notifyOff(false);
                }
                updateView();
            }
        }

        @Override
        public void stop() throws RemoteException {
            MediaPlayerService.this.stop();
        }

        @Override
        public long[] getList() throws RemoteException {
            ArrayList<MediaData> list = jukebox.getList();
            long[] ret = new long[list.size()];
            for (int i = 0; i < list.size(); i++) {
                ret[i] = list.get(i).mediaId;
            }
            return ret;
        }

        @Override
        public String[] getMediaD(int pos) throws RemoteException {

            return null;
        }

        @Override
        public void play() throws RemoteException {
            MediaPlayerService.this.play();
        }

        @Override
        public void pause() throws RemoteException {
            mFadingNow = false;
            MediaPlayerService.this.pause();
        }

        @Override
        public void ff() throws RemoteException {
            MediaPlayerService.this.ff();
        }

        @Override
        public void rew() throws RemoteException {
            MediaPlayerService.this.rew();
        }

        @Override
        public void shuffle() throws RemoteException {
            MediaPlayerService.this.shuffle();
        }

        @Override
        public void repeat() throws RemoteException {
            MediaPlayerService.this.repeat();
        }

        @Override
        public void commit() throws RemoteException {
            updatelist = true;
            updatelock = false;
            updateView();

            // startPrefetch(false);
        }

        @Override
        public int stat() throws RemoteException {
            return MediaPlayerService.this.stat(0);
        }

        @Override
        public void setPosition(int pos) throws RemoteException {
            int ppos = jukebox.getPosition();
            if (ppos != pos) {
                // jukebox.setPlayPosition(0);
                jukebox.movePosition(pos);
            }
        }

        @Override
        public long getMediaId() throws RemoteException {
            MediaData inf = jukebox.getCurrent();
            if (inf != null) {
                return inf.mediaId;
            } else {
                return -1;
            }
        }

        @Override
        public long getSeekPosition() throws RemoteException {
            try {
                if (prepared) {
                    int pos = jukebox.getCurrentPosition();
                    // jukebox.setPlayPosition(pos);
                    return pos;
                } else {
                    return 0;
                }
            } catch (Exception e) {
                Logger.e("getSeekPosition", e);
                throw new RemoteException();
            }
        }

        @Override
        public long getDuration() throws RemoteException {
            return MediaPlayerService.this.getDuration();
        }

        @Override
        public void seek(int msec) throws RemoteException {
            if (prepared) {
                jukebox.seekTo(msec);
            }
        }

        @Override
        public int getCount() throws RemoteException {
            return jukebox.getList().size();
        }

        @Override
        public void remove(int pos) throws RemoteException {
            Logger.d("MediaPlayerService.remove");
            try {
                if (jukebox.removePosition(pos)) {
                    if (jukebox.isPlaying()) {
                        jukebox.stop();
                        prepared = false;
                        notifyOff(false);
                        if (jukebox.getList().size() > 0) {
                            MediaData inf = jukebox.getCurrent();
                            try {
                                // jukebox.setPlayPosition(0);
                                setDataSource(inf);
                                return;
                            } catch (Exception e) {
                                Logger.e("play", e);
                            }
                        }
                    }
                }
                if (jukebox.isPlaying()) {
                    startPrefetch();
                }
            } finally {
                updatelist = true;
                updateView();
            }
        }

        @Override
        public void drop(int from, int to) throws RemoteException {
            Logger.d("MediaPlayerService.drop");
            jukebox.drop(from, to);
            if (jukebox.isPlaying()) {
                startPrefetch();
            }
            updatelist = true;
            updateView();
        }

        @Override
        public int getPosition() throws RemoteException {
            return jukebox.getPosition();
        }

        @Override
        public long getPrefetchId() throws RemoteException {
            return prefetchId;
        }

        @Override
        public int getAudioSessionId() throws RemoteException {
            if (Build.VERSION.SDK_INT > 8) {
                return jukebox.getAudioSessionId();
            }
            return -1;
        }

        @Override
        public void lockUpdateToPlay() throws RemoteException {
            updatelock = true;
        }

        @Override
        public void setEqEnabled(boolean enabled) throws RemoteException {
            jukebox.setEqEnabled(enabled);
        }

        @Override
        public boolean getEqEnabled() throws RemoteException {
            return jukebox.getEqEnabled();
        }

        @Override
        public void setRvEnabled(boolean enabled) throws RemoteException {
            jukebox.setRvEnabled(enabled);
        }

        @Override
        public boolean getRvEnabled() throws RemoteException {
            return jukebox.getRvEnabled();
        }

        @Override
        public void setBsEnabled(boolean enabled) throws RemoteException {
            jukebox.setBsEnabled(enabled);
        }

        @Override
        public boolean getBsEnabled() throws RemoteException {
            return jukebox.getBsEnabled();
        }

        @Override
        public int getNumberOfPresets() throws RemoteException {
            return jukebox.getNumberOfPresets();
        }

        @Override
        public String getPresetName(int n) throws RemoteException {
            return jukebox.getPresetName((short) n);
        }

        @Override
        public int getBandLevel(int band) throws RemoteException {
            return jukebox.getBandLevel((short) band);
        }

        @Override
        public int getCenterFreq(int band) throws RemoteException {
            return jukebox.getCenterFreq((short) band);
        }

        @Override
        public int getCurrentPreset() throws RemoteException {
            return jukebox.getCurrentPreset();
        }

        @Override
        public int getNumberOfBands() throws RemoteException {
            return jukebox.getNumberOfBands();
        }

        @Override
        public int getMinEQLevel() throws RemoteException {
            return jukebox.getMinEQLevel();
        }

        @Override
        public int getMaxEQLevel() throws RemoteException {
            return jukebox.getMaxEQLevel();
        }

        @Override
        public void usePreset(int preset) throws RemoteException {
            jukebox.usePreset((short) preset);
        }

        @Override
        public void setBandLevel(int band, int level) throws RemoteException {
            jukebox.setBandLevel((short) band, (short) level);
        }

        @Override
        public int getRvPreset() throws RemoteException {
            return jukebox.getRvPreset();
        }

        @Override
        public void setRvPreset(int preset) throws RemoteException {
            jukebox.setRvPreset((short) preset);
        }

        @Override
        public int getStrength() throws RemoteException {
            return jukebox.getStrength();
        }

        @Override
        public void setStrength(int strength) throws RemoteException {
            jukebox.setStrength((short) strength);
        }

        @Override
        public String getContentsKey() throws RemoteException {
            return contentkey;
        }

        @Override
        public int getPort() throws RemoteException {
            if (streamcacheserv != null) {
                return streamcacheserv.getPort();
            }

            return -1;
        }

        @Override
        public void release() throws RemoteException {
        }

        @Override
        public void reload() throws RemoteException {
            jukebox.load();
        }
    };

    protected void repeat() {
        jukebox.incRepeat();
        setRepeatMode();
        updateView();
    }

    public boolean makeFreeSpace() {
        long limitsize = 1000 * 1000 * 10; // 10M
        SdCardAccessHelper sdHelper = new SdCardAccessHelper();
        long cacheSizeByte = sdHelper
                .calcDirSize(SdCardAccessHelper.cachedMusicDir);

        long deletesizeByte = cacheSizeByte / 2; // 50% delete
        if (limitsize > deletesizeByte) {
            return false;
        }

        File[] files = SdCardAccessHelper.cachedMusicDir.listFiles();
        Arrays.sort(files, new Comparator<File>() {
            @Override
            public int compare(File lhs, File rhs) {
                long d = lhs.lastModified() - rhs.lastModified();
                if (d > 0)
                    return 1;
                else if (d < 0)
                    return -1;
                return 0;
            }
        });

        for (File f : files) {
            deletesizeByte -= f.length();
            f.delete();
            if (deletesizeByte < 0) {
                break;
            }
        }
        return true;
    }

    protected void shuffle() {
        jukebox.shuffle();
        if (jukebox.isPlaying()) {
            startPrefetch();
        }
        // saveShuffleFlg();
        updateView();
    }

    static PendingIntent createIntent(Service service, String action) {
        Intent i = new Intent(service, MediaPlayerService.class);
        i.setAction(action);
        return PendingIntent.getService(service, 0, i, 0);
    }

    private void notifyOn() {
        if (jukebox != null) {
            MediaData media = jukebox.getMediaData(jukebox.getPosition());
            if (media != null) {
                AsyncNotifyUpdate task = new AsyncNotifyUpdate(this, mHandler, media, null);
                task.execute();
            }
        }
    }

    private void notifyOff(boolean force) {
        AsyncNotifyUpdate task = new AsyncNotifyUpdate(this, mHandler, null, mNotification);
        task.execute();
        mNotification = null;
    }

    private void setRepeatMode() {
        switch (jukebox.getRepeatMode()) {
            case SystemConsts.FLG_REPEAT_NO:
            case SystemConsts.FLG_REPEAT_ALL:
                jukebox.setLooping(false);
                break;
            case SystemConsts.FLG_REPEAT_ONCE:
                jukebox.setLooping(true);
                break;
        }
    }

    private boolean firstPluggd = true;
    BroadcastReceiver headsetplug = null;
    BroadcastReceiver earPhoneChecker = null;
    BroadcastReceiver connectivityAction = null;

    private PhoneStateListener telephone = new PhoneStateListener() {
        boolean onhook = false;
        RING_STATE callstaet;

        public void onCallStateChanged(int state, String number) {
            switch (state) {
                case TelephonyManager.CALL_STATE_RINGING: {
                    callstaet = RING_STATE.STATE_RINGING;
                    if (jukebox.isPlaying()) {
                        mFadingNow = false;
                        pause();
                        onhook = true;
                        setResumeStop(CALL_RESUME);
                    }
                }
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK: {
                    if (callstaet == RING_STATE.STATE_RINGING) {
                        callstaet = RING_STATE.STATE_OFFHOOK;
                    } else {
                        callstaet = RING_STATE.STATE_NORMAL;
                        if (jukebox.isPlaying()) {
                            mFadingNow = false;
                            pause();
                            onhook = true;
                            setResumeStop(CALL_RESUME);
                        }
                    }
                }
                    break;
                case TelephonyManager.CALL_STATE_IDLE: {
                    if (onhook) {
                        onhook = false;
                        setResumeStart(5, CALL_RESUME);
                    }
                    callstaet = RING_STATE.STATE_NORMAL;
                }
                    break;
                default: {

                }
            }
        }
    };

    BroadcastReceiver mScreenLock = null;

    private BroadcastReceiver createScreenLockReciever() {
        if (mScreenLock == null) {
            mScreenLock = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if (Intent.ACTION_SCREEN_ON.equals(intent.getAction())) {
                        // if(Intent.ACTION_USER_PRESENT.equals(intent.getAction())){
                        boolean b = pref.getBoolean("key.useLockscreen", true);
                        if (b) {
                            if (jukebox.isPlaying()) {
                                Intent i = new Intent(context,
                                        LockScreenActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                            }
                        }
                    }
                }
            };
        }
        return mScreenLock;
    }

    private void startPrefetch() {
        long cachesize = Funcs.getCachSize(pref);
        if (cachesize != 0 && streamcacheserv != null) {
            synchronized (jukebox) {
                int prefetchSize = Funcs.getInt(pref.getString("key.cachecount", "3"));
                ArrayList<Long> nextlist = jukebox.getNexList();
                ArrayList<MediaData> mediaList = jukebox.getList();
                boolean isAllcache = pref.getBoolean("key.useAllCache", false);
                if (isAllcache) {
                    boolean isWifi = NetworkHelper
                            .isWifiConnected(getApplicationContext());
                    if (isWifi) {
                        prefetchSize = mediaList.size();
                    }
                }
                if(nextlist.size()>0){
                    int size = prefetchSize;
                    if(size>nextlist.size()){
                        size = nextlist.size();
                    }
                    for(int i=0; i<size; i++){
                        long nextId = nextlist.get(i);
                        MediaData mediadata = jukebox.selectMediaData(nextId);
                        if (mediadata != null && mediadata.data!= null && !mediadata.data.startsWith("http")) {
                            File cacheFile = StreamCacherServer.getCacheFile(this, mediadata.data);
                            if (cacheFile != null && !cacheFile.exists()) {
                                streamcacheserv.prefetch(mediadata.mediaId, mediadata.data, true,
                                        this);
                                prefetchId = mediadata.mediaId;
                                return;
                            }
                        }
                    }
                    
                    if(jukebox.getRepeatMode() != SystemConsts.FLG_REPEAT_ALL){
                        return;
                    }
                }
                
                if(mediaList.size() > 0){
                    int size = prefetchSize;
                    if(size>mediaList.size()){
                        size = mediaList.size();
                    }
                    for(int i=0; i<size; i++){
                        MediaData data = mediaList.get(i);
                        if (data.data != null && !data.data.startsWith("http")) {
                            File cacheFile = StreamCacherServer.getCacheFile(this, data.data);
                            if (cacheFile != null && !cacheFile.exists()) {
                                streamcacheserv.prefetch(data.mediaId, data.data, true,
                                        this);
                                prefetchId = data.mediaId;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    private void setDataSource(MediaData inf) throws IllegalArgumentException,
            SecurityException, IllegalStateException, IOException {
        if (streamcacheserv == null) {
            streamcacheserv = new StreamCacherServer(this, mHandler);
        }
        streamcacheserv.start();
        // URLなら直で再生、そうでないならローカル再生
        if (inf.data.startsWith("http")) {
            // NofityLoading
            if (Build.VERSION.SDK_INT >= 14 || inf.data.startsWith("http:")) {
                jukebox.setDataSource(inf.data);
            }
            else {
                notifyOn();
                String geturi = "http://127.0.0.1:"
                        + streamcacheserv.getPort() + "/"
                        + URLEncoder.encode(inf.data);
                jukebox.setDataSource(geturi);
            }
        } else {
            File cacheFile = StreamCacherServer.getCacheFile(this, inf.data);
            if (cacheFile != null) {
                if (cacheFile.exists() && cacheFile.length() > 0) {
                    cacheFile.setLastModified(System.currentTimeMillis());
                    jukebox.setDataSource(getContext(),
                            Uri.parse(cacheFile.toString()));
                    startPrefetch();
                    if (inf.mediaId > 0) {
                        ContentValues values = new ContentValues();
                        values.put(AudioMedia.AUDIO_CACHE_FILE, cacheFile.getName());
                        getContentResolver().update(
                                ContentUris.withAppendedId(
                                        MediaConsts.MEDIA_CONTENT_URI, inf.mediaId),
                                values, null, null);
                    }
                } else if (streamcacheserv.isUseCache()) {
                    // NofityLoading
                    notifyOn();
                    Logger.v("cache uri: " + inf.data);
                    String geturi = "http://127.0.0.1:"
                            + streamcacheserv.getPort() + "/"
                            + URLEncoder.encode(inf.data);
                    jukebox.setDataSource(geturi);
                } else {
                    // CacheServerを利用できない
                    String geturi = StreamCacherServer.getContentUri(this,
                            inf.data);
                    Logger.d("nocache url: " + geturi);
                    if (geturi != null) {
                        // NofityLoading
                        notifyOn();
                        if (geturi.startsWith("http")) {
                            jukebox.setDataSource(geturi);
                        } else {
                            jukebox.setDataSource(getContext(), Uri.parse(geturi));
                        }
                    } else {
                        Logger.d("データがおかしい data=" + inf.data);
                        return;
                    }
                }
            } else {
                // ZIPファイルかどうかチェック
                if (inf.data.startsWith("zip://")) {
                    Logger.v("cache uri: " + inf.data);
                    String geturi = "http://127.0.0.1:"
                            + streamcacheserv.getPort() + "/"
                            + URLEncoder.encode(inf.data);
                    jukebox.setDataSource(geturi);
                } else {
                    jukebox.setDataSource(getContext(), Uri.parse(inf.data));
                }
            }
        }
        jukebox.prepareAsync();

        // SessionKeepAlive
        setKeepSession(-1);
    }

    @Override
    public IBinder onBind(Intent intent) {
        Logger.d("onBind");
        return bind;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Logger.d("onUnbind");
        instantSave();

        return true;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Logger.d("MediaPlayerSerivce.onCreate");
        pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        jukebox = new JukeBox(getApplicationContext());
        notificationHelper = new NotificationHelper(this);

        mWifiLock = ((WifiManager) getSystemService(Context.WIFI_SERVICE))
                .createWifiLock(WifiManager.WIFI_MODE_FULL, "mylock");
        lockWifi();

        if (Build.VERSION.SDK_INT < 8) {
        } else {
            if (Build.VERSION.SDK_INT > 7) {
                new SSLHelper().disableSSLVerifier();
            }
        }
    }

    private synchronized void initService() {
        if (jukebox.isInitialize()) {
            return;
        }

        soundmanager.init(this);

        // MediaPlayer生成
        jukebox.Initialize(this, this, this, this);
        prepared = false;

        // Ampache Session Keep
        SessionKeepTask task = new SessionKeepTask();
        task.execute((Void[]) null);

        if (streamcacheserv == null) {
            try {
                streamcacheserv = new StreamCacherServer(
                        getApplicationContext(), mHandler);
                streamcacheserv.start();
            } catch (UnknownHostException e) {
                Logger.e("StreamCacherServer.UnknownHostException", e);
                streamcacheserv = null;
            } catch (IOException e) {
                Logger.e("StreamCacherServer.IOException", e);
                streamcacheserv = null;
            }
        }

        headsetplug = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equalsIgnoreCase(Intent.ACTION_HEADSET_PLUG)) {
                    int state = intent.getIntExtra("state", 0);
                    Logger.v("Intent.ACTION_HEADSET_PLUG state is " + state);
                    // イヤホンを刺した
                    if (state == 1 && firstPluggd) {
                        firstPluggd = false;
                        // AutoVolume
                        safeVolume();
                        firsttime = false;

                        // AutoReplay
                        boolean autoreplay = pref.getBoolean("key.useAutoPlay",
                                true);
                        if (autoreplay) {
                            if (jukebox.isInitialize() && jukebox.getList().size() > 0
                                    && !jukebox.isPlaying()) {
                                Toast.makeText(
                                        MediaPlayerService.this,
                                        getString(R.string.txt_action_headset_plug),
                                        Toast.LENGTH_SHORT).show();
                                play();
                            }
                        }
                    } else if (state != 1) {
                        firstPluggd = true;
                    }
                }
            }
        };

        registerReceiver(headsetplug, new IntentFilter(Intent.ACTION_HEADSET_PLUG));

        earPhoneChecker = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // イヤホンを抜いた
                Toast.makeText(MediaPlayerService.this,
                        getString(R.string.txt_action_audio_becoming_noisy),
                        Toast.LENGTH_SHORT).show();
                mFadingNow = false;
                pause();
            }
        };
        registerReceiver(earPhoneChecker,
                new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY));

        if (Build.VERSION.SDK_INT < 14) {
            registerReceiver(createScreenLockReciever(), new IntentFilter(
                    Intent.ACTION_SCREEN_ON));
        }

        connectivityAction = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equalsIgnoreCase(
                        ConnectivityManager.CONNECTIVITY_ACTION)) {
                    getContentResolver().query(MediaConsts.CLEAR_CONTENT_URI, null,
                            null, null, null);
                    if (streamcacheserv != null) {
                        streamcacheserv.start();
                    }
                }
            }
        };
        registerReceiver(connectivityAction, new IntentFilter(
                ConnectivityManager.CONNECTIVITY_ACTION));

        tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        tm.listen(telephone, PhoneStateListener.LISTEN_CALL_STATE);

        audiomgr = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        // BlueTooth
        if (Build.VERSION.SDK_INT > 7) {
            audiofocus = new AudioFocus(this, mHandler, audiomgr);
        }

        // ステータスリセット
        Editor editor = pref.edit();
        editor.putBoolean("earPhoneChecker", false);
        editor.commit();

        // 初期化サービス
        Intent service = new Intent(this, SystemInitService.class);
        startService(service);
    }

    void instantSave() {
        if (pref != null) {

            if (jukebox.isLoaded()) {
                jukebox.save(false);
            }

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Logger.d("MediaPlayerSerivce.onDestroy");
        freeWifi();

        instantSave();

        jukebox.Release();

        if (streamcacheserv != null) {
            streamcacheserv.release();
            streamcacheserv.stop();
            streamcacheserv = null;
        }

        // nm.cancel(R.string.app_name);
        notificationHelper.stopForegroundCompat(R.string.app_name);

        setResumeStop(CALL_KEEPSESSION);
        setResumeStop(CALL_RESUME);

        if (soundmanager != null) {
            soundmanager.release();
        }

        if (headsetplug != null) {
            unregisterReceiver(headsetplug);
        }
        if (earPhoneChecker != null) {
            unregisterReceiver(earPhoneChecker);
        }
        if (mScreenLock != null) {
            unregisterReceiver(mScreenLock);
        }

        if (connectivityAction != null) {
            unregisterReceiver(connectivityAction);
        }

        if (audiofocus != null) {
            audiofocus.unregister(audiomgr);
            audiofocus = null;
        }

        if (tm != null) {
            tm.listen(telephone, PhoneStateListener.LISTEN_NONE);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Logger.d("onStartCommand " + flags + "," + startId);
        String action = intent != null ? intent.getAction() : "";

        if ((!AppWidgetHelper.CALL_UPDATEWIDGET.equals(action) && !CALL_KEEPSESSION.equals(action))
                && !isInitService) {
            isInitService = true;
            initService();
        }

        if (intent != null) {
            // PendinIntentもしくはBroadcastによってStartされた
            Logger.d("onStartCommand " + intent.getAction());
            if (CALL_KEEPSESSION.equals(intent.getAction())) {
                // KeepSession 継続するなら更新処理を呼んであげる
                SessionKeepTask task = new SessionKeepTask();
                task.execute((Void[]) null);
            } else {
                if (CALL_RESUME.equals(intent.getAction())) {
                    play();
                } else if (AppWidgetHelper.CALL_STOP.equals(intent.getAction())) {
                    Logger.d("MediaPlayerSerivce.CALL_STOP");
                    notificationHelper.stopForegroundCompat(R.string.app_name);
                    closeActivity();
                    // if (!jukebox.isPlaying()) {
                    stopSelf();
                    // }
                } else if (AppWidgetHelper.CALL_FF.equals(intent.getAction())) {
                    if (jukebox.isInitialize()) {
                        ff();
                    }
                } else if (AppWidgetHelper.CALL_REW.equals(intent.getAction())) {
                    if (jukebox.isInitialize()) {
                        rew();
                    }
                } else if (AppWidgetHelper.CALL_SHUFFLE.equals(intent
                        .getAction())) {
                    if (!jukebox.isLoaded()) {
                        jukebox.load();
                    }
                    shuffle();
                    if (jukebox.isInitialize()) {
                        stopSelf();
                    }
                } else if (AppWidgetHelper.CALL_LOOP.equals(intent.getAction())) {
                    if (!jukebox.isLoaded()) {
                        jukebox.load();
                    }
                    repeat();
                    if (jukebox.isInitialize()) {
                        stopSelf();
                    }
                } else if (AppWidgetHelper.CALL_PLAY_PAUSE.equals(intent
                        .getAction())) {
                    Logger.d("MediaPlayerSerivce.CALL_PLAY_PAUSE");
                    int stat = stat(0);
                    if ((stat & AppWidgetHelper.FLG_PLAY) > 0) {
                        mFadingNow = false;
                        pause();
                    } else {
                        play();
                    }
                } else if (AppWidgetHelper.CALL_UPDATEWIDGET.equals(intent
                        .getAction())) {
                    Logger.d("call CALL_UPDATEWIDGET");

                    SharedPreferences preference = PreferenceManager
                            .getDefaultSharedPreferences(this);
                    String mediaid = preference.getString("lastSetMediaId", "");
                    String title = preference.getString("lastSetTitle", "");
                    String artist = preference.getString("lastSetArtist", "");
                    String album = preference.getString("lastSetAlbum", "");
                    updateWidgetTitle(mediaid, artist, title, album, 0, null, stat(0));
                    if (!jukebox.isInitialize()) {
                        stopSelf();
                    }
                } else if ("jp.co.kayo.android.localplayer.MEDIA".equals(intent.getAction())) {
                    int eventType = intent.getIntExtra("key.eventType", 0);
                    if (eventType == 1) {
                        String contentsKey = intent.getStringExtra("key.contentsKey");
                        long duration = intent.getLongExtra("key.duration", 0);
                        String title = intent.getStringExtra("key.title");
                        String album = intent.getStringExtra("key.album");
                        String artist = intent.getStringExtra("key.artist");
                        String url = intent.getStringExtra("key.url");

                        try {
                            bind.setContentsKey(contentsKey);
                            bind.lockUpdateToPlay();
                            bind.clearcut();
                            bind.addMediaD(0, duration, title, album, artist, url);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        } finally {
                            play();
                        }
                    }
                }
            }
        }
        setResumeStop(AppWidgetHelper.CALL_STOP);
        return START_NOT_STICKY;
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        // 読み込みバッファが変わった場合に通知される。プログレスバーの更新に使用
        progress(percent);
        // if(jukebox.getSeekPosition()>0){
        // jukebox.setPlayPosition(mp.getCurrentPosition());
        // }
    }

    @Override
    public void onCompletion(MediaPlayer mpa) {
        Logger.d("onCompletion: error=" + hasError() + ", mp=" + jukebox.isInitialize());
        // 曲の再生が終わった
        // 曲の再生が終わったときに通知される。次の曲の読み込みに使用
        AsyncNotifyUpdate.stopScrobbler(getApplicationContext(), pref);
        if (hasError() != true) {
            int position = jukebox.getPosition();
            if (retryConnect || jukebox.moveNext()) {
                int pos = jukebox.getPosition();
                if (retryConnect != true && pos != -1 && pos == position) {
                    prepared = true;
                    jukebox.seekTo(0);
                    jukebox.play();
                } else {
                    retryConnect = false;
                    prepared = false;

                    jukebox.stop();

                    if (pos != -1) {
                        MediaData inf = jukebox.getCurrent();
                        try {
                            // jukebox.setPlayPosition(0);
                            setDataSource(inf);
                        } catch (Exception e) {
                            Logger.e("play", e);
                        } finally {
                            jukebox.save(true);
                        }
                    }
                    updateView();
                }
            } else {
                try {
                    saveVolume();
                    notifyOff(false);
                    // 曲の再生終了
                    boolean useendoflist = pref.getBoolean("key.useEndOfList",
                            true);
                    if (useendoflist && prepared) {
                        Thread.sleep(500);
                        soundmanager.play(R.raw.cassette_off, 1.0f);
                    }
                } catch (InterruptedException e) {
                } finally {
                    prepared = false;
                }
            }
        }
        updateView();
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        Logger.d("onPrepared");
        // 曲のバッファリングが終わったときに通知される。曲の再生開始に使用
        prepared = true;
        jukebox.play();
        mHandler.removeMessages(MSG_FADEDOWN);
        mHandler.sendEmptyMessage(MSG_FADEUP);

        int msec = jukebox.getSeekPosition();
        if (msec > 0) {
            jukebox.setSeekPosition(0);
            mp.seekTo(msec);
        }
        MediaData inf = jukebox.getCurrent();
        if (inf != null && inf.getDuration() <= 0) {
            inf.setDuration(mp.getDuration());
        }
        setRepeatMode();
        notifyOn();
        updateView();
    }

    public void progress(int percent) {
        // 再描画を通知する
        synchronized (callbackList) {
            int n = callbackList.beginBroadcast();
            for (int i = 0; i < n; i++) {
                try {
                    callbackList.getBroadcastItem(i).onBufferingUpdate(percent);
                } catch (RemoteException e) {
                }
            }
            callbackList.finishBroadcast();
        }
    }

    public void updateView() {
        if (!updatelock) {
            // 再描画を通知する
            boolean flg = updatelist;
            updatelist = false;
            synchronized (callbackList) {
                int n = callbackList.beginBroadcast();
                for (int i = 0; i < n; i++) {
                    Logger.d("callbackList.getBroadcastItem(i).updateView i="
                            + i);
                    try {
                        callbackList.getBroadcastItem(i).updateView(flg);
                    } catch (RemoteException e) {
                        Logger.e("updateView", e);
                    }
                }
                callbackList.finishBroadcast();
            }
        }
    }

    private Context getContext() {
        return this;
    }

    private long getDuration() {
        MediaData inf = jukebox.getMediaData(jukebox.getPosition());
        if (inf != null) {
            return inf.getDuration();
        } else {
            return 0;
        }
    }

    public void safeVolume() {
        if (audiomgr != null && pref != null) {
            boolean b = pref.getBoolean(SystemConsts.KEY_AUTO_VOLUMEAJUST, true);
            if (b) {
                int volume = pref.getInt(SystemConsts.KEY_FIRST_VOLUME, 2);
                if (volume != 0) {
                    audiomgr.setStreamVolume(AudioManager.STREAM_MUSIC, volume,
                            AudioManager.FLAG_SHOW_UI);
                }
            }
        }
    }

    public void saveVolume() {
        if (audiomgr != null && pref != null) {
            if (HeadsetHelper.isWiredHeadsetOn(audiomgr)) {
                int volume = audiomgr.getStreamVolume(AudioManager.STREAM_MUSIC);
                Editor editor = pref.edit();
                editor.putInt(SystemConsts.KEY_FIRST_VOLUME, volume);
                editor.commit();
            }
        }
    }

    public void updateWidgetTitle(String mediaId, String artist,
            String title, String album, long duration, String data,
            int stat) {
        Intent intent = new Intent(AppWidgetHelper.CALL_APPWIDGET_DISPLAY);
        intent.putExtra("mediaId", mediaId);
        intent.putExtra("artist", artist);
        intent.putExtra("title", title);
        intent.putExtra("album", album);
        intent.putExtra("duration", duration);
        intent.putExtra("data", data);
        intent.putExtra("stat", stat);
        sendBroadcast(intent);
    }

    int stat(int forceState) {
        // Logger.d("stat");
        int shf = jukebox.isShaffle() ? AppWidgetHelper.FLG_SHUFFLE : 0;
        int next = 0;
        int back = 0;
        int list = 0;
        int repeat = jukebox.getRepeatMode() == SystemConsts.FLG_REPEAT_ONCE ? AppWidgetHelper.FLG_LOOP1
                : jukebox.getRepeatMode() == SystemConsts.FLG_REPEAT_ALL ? AppWidgetHelper.FLG_LOOP2
                        : SystemConsts.FLG_REPEAT_NO;
        list = jukebox.getList().size() > 0 ? AppWidgetHelper.FLG_HASLIST : 0;
        if (list != 0) {
            next = jukebox.hasNext() ? AppWidgetHelper.FLG_HASNEXT : 0;
            back = jukebox.getPosition() >= 0 ? AppWidgetHelper.FLG_HASFOWD : 0;
        }

        if (forceState == 0) {
            if (jukebox.isPlaying()) {
                return AppWidgetHelper.FLG_PLAY | next | back | repeat | shf
                        | list;
            } else {
                if (list != 0) {
                    return AppWidgetHelper.FLG_PAUSE | next | back | repeat
                            | shf | list;
                } else {
                    return AppWidgetHelper.FLG_STOP | next | back | repeat
                            | shf;
                }
            }
        } else {
            return forceState | next | back | repeat | shf | list;
        }
    }

    private void play() {
        Logger.d("play");
        setError(false);
        retryConnect = false;
        updatelock = false;

        if (!jukebox.isInitialize()) {
            return;
        }

        if (firsttime) {
            if (HeadsetHelper.isWiredHeadsetOn(audiomgr)) {
                safeVolume();
                firsttime = false;
            }
        }

        if (prepared) {
            jukebox.play();
            mHandler.removeMessages(MSG_FADEDOWN);
            mHandler.sendEmptyMessage(MSG_FADEUP);

            notifyOn();
            updateView();
        } else {
            MediaData inf = jukebox.getCurrent();
            if (inf != null) {
                try {
                    jukebox.stop();
                    int playposition = jukebox.getRestorePosition();
                    if (playposition > 0) {
                        jukebox.setSeekPosition(playposition);
                    }
                    setDataSource(inf);
                    progress(playposition);
                } catch (Exception e) {
                    Logger.e("play", e);
                }
            }
        }
    }

    private void pause() {
        Logger.d("pause");
        if (jukebox.isInitialize()) {
            int pos = jukebox.getCurrentPosition();
            // jukebox.setPlayPosition(pos);
            saveVolume();
            jukebox.pause();
            notifyOff(false);
            updateView();
        }
    }

    private void ff() {
        Logger.d("ff");
        if (!jukebox.isInitialize()) {
            return;
        }

        int position = jukebox.getPosition();
        if (jukebox.moveNext() != true) {
            return;
        }
        int pos = jukebox.getPosition();
        if (pos != -1 && pos == position) {
            jukebox.seekTo(0);
        } else {
            prepared = false;

            if (jukebox.isPlaying()) {
                AsyncNotifyUpdate.stopScrobbler(getApplicationContext(), pref);
            }

            jukebox.stop();

            if (pos != -1) {
                MediaData inf = jukebox.getCurrent();
                try {
                    setDataSource(inf);
                } catch (Exception e) {
                    Logger.e("play", e);
                }
            }
            updateView();
        }
    }

    private void rew() {
        if (!jukebox.isInitialize()) {
            return;
        }
        if (jukebox.getList().size() == 0) {
            return;
        }
        int position = jukebox.getPosition();
        int current = jukebox.getCurrentPosition();
        int duration = jukebox.getDuration();
        int pos = jukebox.getBack(current, duration);
        if (pos == position) {
            jukebox.seekTo(0);
        } else {
            prepared = false;

            if (jukebox.isPlaying()) {
                AsyncNotifyUpdate.stopScrobbler(getApplicationContext(), pref);
            }

            jukebox.stop();
            MediaData inf = jukebox.getItem(pos);
            if (inf != null) {
                jukebox.moveBack(inf);
                try {
                    // jukebox.setPlayPosition(0);
                    setDataSource(inf);
                } catch (Exception e) {
                    Logger.e("play", e);
                }
            }
        }
        updateView();
    }

    public void stop() {
        Logger.d("stop");
        if (jukebox.isInitialize()) {
            if (jukebox.isPlaying()) {
                AsyncNotifyUpdate.stopScrobbler(getApplicationContext(), pref);
            }
            saveVolume();
            jukebox.stop();
            prepared = false;
            notifyOff(false);
            updateView();
        } else {
            prepared = false;
            notifyOff(false);
            updateView();
        }
    }

    private void setResumeStart(int delay, String action) {
        Intent intent = new Intent(this, MediaPlayerService.class);
        intent.setAction(action);
        PendingIntent operation = PendingIntent.getService(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar cal = Calendar.getInstance();
        if (delay > 0) {
            cal.add(Calendar.SECOND, delay);
        }

        AlarmManager am = (AlarmManager) this
                .getSystemService(Activity.ALARM_SERVICE);
        am.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), operation);
    }

    private void setResumeStop(String action) {
        Intent intent = new Intent(this, MediaPlayerService.class);
        intent.setAction(action);
        PendingIntent operation = PendingIntent.getService(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager am = (AlarmManager) this
                .getSystemService(Activity.ALARM_SERVICE);
        am.cancel(operation);
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Logger.d("MediaPlayer.onError--->   what:" + what + "    extra:"
                + extra);
        // MediaPlayer Error things: http://android.joao.jp/
        if (extra == -1002) { // -1002:
                              // HTTPStream::connect(pd.vog.sprintpcs.com, 8085)
                              // failed. ERROR_UNKNOWN_HOST
            Toast.makeText(getContext(),
                    getString(R.string.txt_error_disconnect),
                    Toast.LENGTH_SHORT).show();
            setError(true);
        } else if (extra == -1004 && what == 1) { // セッション切れ
            if (!hasError()) {
                Toast.makeText(getContext(),
                        getString(R.string.txt_error_disconnect),
                        Toast.LENGTH_SHORT).show();
                Cursor cur = null;
                try {
                    cur = getContentResolver().query(MediaConsts.PING_CONTENT_URI,
                            null, "force", null, null);
                    if (cur != null && cur.moveToFirst()) {
                        int col = cur.getColumnIndex(Auth.AUTH_KEY);
                        if (col != -1) {
                            String auth = cur.getString(col);
                            if (auth != null && auth.length() > 0) {
                                retryConnect = true;
                                return false;
                            }
                        }
                    }
                    setError(true);
                } finally {
                    if (cur != null) {
                        cur.close();
                    }
                }
            }
        } else if (extra == -1 && what == 1) { // ?
            Toast.makeText(getContext(),
                    getString(R.string.txt_error_disconnect),
                    Toast.LENGTH_SHORT).show();
            setError(true);
        } else if (what == 1) {
            // helper.clearAuth();
            // setError(true);
        }
        return false;
    }

    private synchronized boolean hasError() {
        return error;
    }

    private synchronized void setError(boolean b) {
        error = b;
    }

    private void setKeepSession(long expired) {
        // Serverを使うならセッションをKeepする
        // if (!ContentsUtils.isSDCard(pref)) {
        Intent intent = new Intent(getContext(), MediaPlayerService.class);
        intent.setAction(MediaPlayerService.CALL_KEEPSESSION);
        PendingIntent operation = PendingIntent.getService(getContext(), 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        long nextime = 0;
        if (expired > 0) {
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(expired);
            cal.add(Calendar.SECOND, 60);
            nextime = cal.getTimeInMillis();
        } else {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.SECOND, KEEP_DELAY);
            nextime = cal.getTimeInMillis();
        }

        AlarmManager am = (AlarmManager) getContext().getSystemService(
                Activity.ALARM_SERVICE);
        am.set(AlarmManager.RTC_WAKEUP, nextime, operation);
        // }
    }

    class SessionKeepTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            if (pref != null && !ContentsUtils.isSDCard(pref)) {
                getContentResolver().query(MediaConsts.PING_CONTENT_URI, null,
                        null, null, null);
            }

            if (jukebox.isInitialize()) {
                setKeepSession(-1);
            }
            return null;
        }

    }

    public void closeActivity() {
        synchronized (callbackList) {
            int n = callbackList.beginBroadcast();
            for (int i = 0; i < n; i++) {
                try {
                    callbackList.getBroadcastItem(i).close();
                } catch (RemoteException e) {
                }
            }
            callbackList.finishBroadcast();
        }
    }

    @Override
    public void startProgress(long max) {
        synchronized (callbackList) {
            int n = callbackList.beginBroadcast();
            for (int i = 0; i < n; i++) {
                try {
                    callbackList.getBroadcastItem(i).startProgress(max);
                } catch (RemoteException e) {
                }
            }
            callbackList.finishBroadcast();
        }
    }

    @Override
    public void stopProgress() {
        synchronized (callbackList) {
            int n = callbackList.beginBroadcast();
            for (int i = 0; i < n; i++) {
                try {
                    callbackList.getBroadcastItem(i).stopProgress();
                } catch (RemoteException e) {
                }
            }
            callbackList.finishBroadcast();
        }
    }

    @Override
    public void progress(long pos, long max) {
        synchronized (callbackList) {
            int n = callbackList.beginBroadcast();
            for (int i = 0; i < n; i++) {
                try {
                    callbackList.getBroadcastItem(i).progress(pos, max);
                } catch (RemoteException e) {
                }
            }
            callbackList.finishBroadcast();
        }
    }

    void freeWifi() {
        if (mWifiLock != null && mWifiLock.isHeld()) {
            mWifiLock.release();
        }
    }

    void lockWifi() {
        if (mWifiLock != null) {
            mWifiLock.acquire();
        }
    }
}
