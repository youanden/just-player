package jp.co.kayo.android.localplayer.service;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.params.ConnManagerPNames;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;

import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.SSLHelper;
import jp.co.kayo.android.localplayer.util.SdCardAccessHelper;
import jp.co.kayo.android.localplayer.util.bean.MediaData;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;

public class StreamCacherServer implements Runnable {
    final int STREAM_BUFFER = 49152;
    final int PLAY_STREAM_BUFFER = 49152;
    private ServerSocket mSocket;
    private Boolean mStopServer = false;
    private Context mContext;
    private Thread mThread;
    private WeakReference<Handler> refHandler = null;
    private Thread prefetchThread = null;
    private volatile PrefetchWorker mWorker = null;
    private SharedPreferences pref;
    private volatile SchemeRegistry mSchemeRegistry;
    private BasicHttpParams mParams = new BasicHttpParams();
    private BasicHttpContext mHttpContext;
    private final int CONNECTION_TIMEOUT = 10000;
    private final int SOCKET_TIMEOUT = 20000;
    private final int SOCK_BUFSIZE = 4096;
    private final long LIMIT_PROG_STACK = 1024 * 2000;
    Object _lock = new Object();

    /**
     * format for RFC 1123 date string -- "Sun, 06 Nov 1994 08:49:37 GMT"
     */
    public final static String RFC1123_PATTERN = "EEE, dd MMM yyyyy HH:mm:ss z";
    public final static DateFormat rfc1123Format = new SimpleDateFormat(
            RFC1123_PATTERN, Locale.US);

    public StreamCacherServer(Context context, Handler handler)
            throws UnknownHostException, IOException {
        mContext = context;
        pref = PreferenceManager.getDefaultSharedPreferences(mContext);
        if (Build.VERSION.SDK_INT > 7) {
            new SSLHelper().disableSSLVerifier();
        }
        Funcs.disableConnectionReuseIfNecessary();

        this.refHandler = new WeakReference<Handler>(handler);

        createServer();
    }

    public void createServer() throws IOException {
        mSocket = new ServerSocket(0, 0,
                InetAddress.getByAddress(new byte[] { 127, 0, 0, 1 }));
    }

    synchronized SchemeRegistry getRegistry() {
        if (mSchemeRegistry == null) {
            mSchemeRegistry = new SchemeRegistry();
            // http scheme
            mSchemeRegistry.register(new Scheme("http", PlainSocketFactory
                    .getSocketFactory(), 80));
            // https scheme
            mSchemeRegistry.register(new Scheme("https",
                    new EasySSLSocketFactory(), 443));
        }
        return mSchemeRegistry;
    }

    synchronized BasicHttpParams getHttpParams() {
        if (mParams == null) {
            mParams = new BasicHttpParams();
            mParams.setParameter(ConnManagerPNames.MAX_TOTAL_CONNECTIONS, 30);
            mParams.setParameter(HttpProtocolParams.USE_EXPECT_CONTINUE, false);
            HttpConnectionParams.setSocketBufferSize(mParams, SOCK_BUFSIZE); // ソケットバッファサイズ
                                                                             // 4KB
            HttpConnectionParams.setSoTimeout(mParams, SOCKET_TIMEOUT); // ソケット通信タイムアウト20秒
            HttpConnectionParams.setConnectionTimeout(mParams, SOCKET_TIMEOUT); // HTTP通信タイムアウト20秒
            HttpProtocolParams.setContentCharset(mParams, "UTF-8"); // 文字コードをUTF-8と明示
            HttpProtocolParams.setVersion(mParams, HttpVersion.HTTP_1_1);

            CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
            BasicScheme basicAuth = new BasicScheme();
            mHttpContext = new BasicHttpContext();
            mHttpContext.setAttribute("http.auth.credentials-provider",
                    credentialsProvider);
            mHttpContext.setAttribute("preemptive-auth", basicAuth);
        }
        return mParams;
    }

    public boolean isUseCache() {
        if (Build.VERSION.SDK_INT > 4) {
            boolean useCache = pref.getBoolean("key.useCacheServer", true);
            return useCache;
        }
        return false;
    }

    public int getPort() {
        return mSocket.getLocalPort();
    }

    public void start() {
        if (mThread == null) {
            mStopServer = false;
            mThread = new Thread(this);
            mThread.start();
        }
    }

    public boolean isRunning() {
        return mThread != null;
    }

    public void prefetch(final long id, String url, boolean deleteOnStart,
            OnPrefetchProgressListener listener) {
        PrefetchWorker worker = mWorker;
        if (worker != null) {
            if (worker.id == id) {
                return;
            }
            if (!deleteOnStart) {
                worker.stop();
            } else {
                return;
            }
        }
        mWorker = new PrefetchWorker(id, url, listener);
        prefetchThread = new Thread(mWorker);
        prefetchThread.start();
    }

    public void release() {
        mStopServer = true;
        if (mWorker != null) {
            mWorker.stop();
            mWorker = null;
        }
        if (mSocket != null) {
            try {
                mSocket.close();
            } catch (IOException e) {
            }
            mSocket = null;
        }
    }

    public long getCurrentWorkerId() {
        if (mWorker != null) {
            return mWorker.id;
        } else {
            return -1;
        }
    }

    public void stop() {
        if (isRunning()) {
            try {
                mThread.join(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
            }
        }
    }

    private static String getExtention(String fname) {
        return ".dat";
    }

    private static String getFilename(String uri) {
        if (uri != null) {
            String ret = uri.replaceAll("auth=[^&]+", "");
            if (ret.equals(uri)) {
                ret = uri.replaceAll("ssid=[^&]+", "");
            }

            String fname = "media" + Integer.toString(ret.hashCode())
                    + getExtention(uri);
            return fname;
        } else {
            return null;
        }
    }

    public static String getTempFilename(String uri) {
        if (uri != null) {
            String ret = uri.replaceAll("auth=[^&]+", "");
            if (ret.equals(uri)) {
                ret = uri.replaceAll("ssid=[^&]+", "");
            }
            return "temp" + Integer.toString(ret.hashCode())
                    + getExtention(uri);
        } else {
            return null;
        }
    }

    public static File getCacheFile(Context context, String uri) {
        if (!ContentsUtils.isSDCard(context)) {
            String s = getFilename(uri);
            if (s != null) {
                File cacheFile = new File(SdCardAccessHelper.cachedMusicDir, s);
                return cacheFile;
            }
        }
        return null;
    }

    public static String getContentUri(Context context, String data)
            throws MalformedURLException {
        if (data != null && data.length() > 0) {
            SharedPreferences pref = PreferenceManager
                    .getDefaultSharedPreferences(context);
            if (!ContentsUtils.isSDCard(pref)) {
                String url = null;
                Cursor cur = null;
                try {
                    cur = context.getContentResolver().query(
                            MediaConsts.URL_CONTENT_URI,
                            new String[] { MediaConsts.Auth.AUTH_URL },
                            MediaConsts.Auth.PARAM1 + " = ?",
                            new String[] { data }, null);
                    if (cur != null && cur.moveToFirst()) {
                        url = cur.getString(cur
                                .getColumnIndex(MediaConsts.Auth.AUTH_URL));
                    }
                } finally {
                    if (cur != null) {
                        cur.close();
                    }
                }
                if (url != null) {
                    String path = url;
                    if (path != null && path.length() > 0) {
                        Logger.d("URL=" + path);
                        return path;
                    }
                }
            } else {
                Logger.d("PATH=" + data);
                return data;
            }
        }
        return null;
    }

    @Override
    public void run() {
        // clean temp file
        Logger.d("###### Start Server");
        while (!mStopServer) {
            try {
                final Socket client = mSocket.accept();
                if (client == null) {
                    continue;
                }
                Logger.i("クライアントからの接続要求が発生しました");
                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        getStreaming(client);
                    }
                };
                new Thread(r).start();

            } catch (SocketTimeoutException e) {
                // Do nothing
                Logger.e("run SocketTimeoutException", e);
            } catch (IOException e) {
                Logger.e("Error connecting to client", e);
            }
        }
        Logger.d("###### Stopped Server");
        mThread = null;
    }

    private void writeFullHeader(BufferedOutputStream writer, String uri, String contentType,
            long totallength) throws IOException {
        String fname = uri.replaceFirst(".*name=([^&]+).*", "$1");
        String dstr = rfc1123Format.format(new Date());
        // キャッシュからデータを読み込む
        writer.write("HTTP/1.1 200 OK\n".getBytes());
        writer.write("Accept-Ranges: bytes\n".getBytes());
        writer.write("Connection: Keep-Alive\n".getBytes());
        writer.write(("Content-Disposition: attachment; filename=\"" + fname + "\"\n")
                .getBytes());
        writer.write(("Content-Length: " + totallength + "\n").getBytes());
        writer.write(("Content-Type: "+contentType+"\n").getBytes());
        writer.write((dstr + "\n").getBytes());
        writer.write("Keep-Alive: timeout=3, max=50\n".getBytes());
        writer.write("Server: localserver\n".getBytes());
        writer.write("X-Powered-By: streamcache\n".getBytes());
        writer.write("\n".getBytes());
        writer.flush();
    }

    private void writePartialHeader(BufferedOutputStream writer, String uri, String contentType,
            long totallength) throws IOException {
        String fname = uri.replaceFirst(".*name=([^&]+).*", "$1");
        String dstr = rfc1123Format.format(new Date());
        // キャッシュからデータを読み込む
        writer.write("HTTP/1.1 206 Partial Content\n".getBytes());
        writer.write("Accept-Ranges: bytes\n".getBytes());
        writer.write("Connection: Keep-Alive\n".getBytes());
        writer.write(("Content-Disposition: attachment; filename=\"" + fname + "\"\n")
                .getBytes());
        writer.write(("Content-Length: " + totallength + "\n").getBytes());
        writer.write(("Content-Type: "+contentType+"\n").getBytes());
        writer.write((dstr + "\n").getBytes());
        writer.write("Keep-Alive: timeout=3, max=50\n".getBytes());
        writer.write("Server: localserver\n".getBytes());
        writer.write("X-Powered-By: streamcache\n".getBytes());
        writer.write("\n".getBytes());
        writer.flush();
    }
    
    public static String getSuffix(String fileName) {
        if (fileName == null)
            return null;
        int point = fileName.lastIndexOf(".");
        if (point != -1) {
            return fileName.substring(point + 1);
        }
        return fileName;
    }
    private String getContentType(String fileName){
        String ext = getSuffix(fileName);
        if(ext == null || ext.length() == 0){
            return "audio/mpeg";
        } else {
            String lower = ext.toLowerCase();
            if(lower.equals("mp3")){
                return "audio/x-wav";
            } else if(lower.equals("m4a")){
                return "audio/mp4";
            } else if(lower.equals("wav")){
                return "audio/x-wav";
            } else if(lower.equals("mid") || lower.equals("midi")){
                return "audio/midi";
            } else if(lower.equals("mmf")){
                return "application/x-smaf";
            } else if(lower.equals("mpg")||lower.equals("mpeg")){
                return "video/mpeg";
            } else if(lower.equals("wmv")){
                return "video/x-ms-wmv";
            } else if(lower.equals("swf")){
                return "application/x-shockwave-flash";
            } else if(lower.equals("3g2")){
                return "video/3gpp2";
            } else {
                return "audio/mpeg";
            }
        }
    }

    private void getStreaming(Socket client) {
        Logger.d("###### start Streaming");

        InputStream serverInput = null;
        InputStream socketInput = null;
        OutputStream socketOutput = null;
        ThreadSafeClientConnManager connManager = null;
        HttpURLConnection connection = null;
        File cacheFile= null;
        File tempFile = null;
        String firstLine;
        String uri;
        long seekSize = 0;
        long existFileSize = 0;

        boolean bComplete = false;
        boolean useHttpClient = pref.getBoolean("key.useHttpClient", false);

        try {
            // 入出力ストリームを初期化
            socketInput = client.getInputStream();
            socketOutput = client.getOutputStream();

            // リクエストからヘッダを取得し、曲のURLを取得する
            BufferedReader socketReader = new BufferedReader(
                    new InputStreamReader(socketInput));
            BufferedOutputStream socketWriter = new BufferedOutputStream(
                    socketOutput);
            firstLine = socketReader.readLine();

            if (firstLine == null) {
                Logger.i("リクエストが妥当でないため処理を終了します");
                return;
            }

            // レジュームリクエストかどうか判断する
            String check = firstLine;
            do {
                Logger.d("readLine=" + check);
                // Range: bytes=9664771-
                if (check.toLowerCase().startsWith("range:")) {
                    String value = check.replaceFirst(".*bytes=([^&]+-).*",
                            "$1");
                    Logger.d("Range=" + value);
                    seekSize = Funcs.parseLong(value.replace("-", ""));
                    break;
                }
                check = socketReader.readLine();
            } while (check != null && check.length() > 0);

            StringTokenizer st = new StringTokenizer(firstLine);

            // 空読み
            String method = st.nextToken();
            // １行目に曲のURLがあるので、これを取得する
            uri = URLDecoder.decode(st.nextToken().substring(1));
            Logger.d("media uri=" + uri);
            if (uri.startsWith("zip://")) {
                // zip file play
                String fname = uri.replaceAll("(.+?)\\.zip/", "");
                String zipfname = uri.substring("zip://".length(), uri.length()
                        - fname.length() - 1);

                ZipFile zipfile = new ZipFile(zipfname);
                ZipEntry entry = zipfile.getEntry(fname);
                existFileSize = entry.getSize();
                if (seekSize == 0) {
                    writeFullHeader(socketWriter, uri, getContentType(uri), existFileSize);
                } else {
                    writePartialHeader(socketWriter, uri, getContentType(uri), existFileSize
                            - seekSize);
                }
                BufferedInputStream tmpis = null;
                byte[] b = new byte[STREAM_BUFFER];
                int nbBytesRead = 0;
                try {
                    tmpis = new BufferedInputStream(
                            zipfile.getInputStream(entry));
                    if (seekSize > 0) {
                        tmpis.skip(seekSize);
                    }
                    while ((nbBytesRead = tmpis.read(b)) != -1 && !mStopServer) {
                        socketWriter.write(b, 0, nbBytesRead);
                    }
                } finally {
                    if (tmpis != null) {
                        tmpis.close();
                    }
                    if (zipfile != null) {
                        zipfile.close();
                    }
                }
            } else if (!uri.startsWith("/mnt")) {
                // URL処理
                // キャッシュフォルダの作成
                SdCardAccessHelper.cachedMusicDir.mkdirs();
                // テンポラリキャッシュファイル（途中までダウンロードが終わったファイル）
                tempFile = new File(SdCardAccessHelper.cachedMusicDir,
                        getTempFilename(uri));
                // フルキャッシュファイル(完全にダウンロードが終わったファイル）
                cacheFile = new File(SdCardAccessHelper.cachedMusicDir,
                        getFilename(uri));

                // フルキャッシュファイルがあるならテンポラリキャッシュファイルを使用する
                long ftime = System.nanoTime();
                if (mWorker != null && mWorker.url.equals(uri)) {
                    mWorker.stop();
                    synchronized (_lock) {
                        // Lock待ち
                    }
                }
                long etime = System.nanoTime();
                Logger.d("LockTime nano : " + (etime - ftime));

                if (cacheFile.exists() && cacheFile.length()>0) {
                    existFileSize = cacheFile.length();
                    // 基本的には存在しないはず、だけど念のためフルキャッシュがあるならテンポラリのキャッシュファイルは削除する
                    if (tempFile.exists()) {
                        tempFile.delete();
                    }
                    tempFile = null;
                } else {
                    //サイズ０のファイルがあるのでしょう
                    if (cacheFile.exists()) {
                        cacheFile.delete();
                    }
                    // テンポラリキャッシュファイルがあるならそれをつかう
                    if (tempFile.exists()) {
                        existFileSize = tempFile.length();
                    } else {
                        // はじめて
                        existFileSize = 0;
                    }
                }

                String geturl = null;
                if (tempFile == null && cacheFile!=null && cacheFile.exists()) {
                    // フルキャッシュ処理
                    if (seekSize == 0) {
                        writeFullHeader(socketWriter, uri, getContentType(uri), existFileSize);
                    } else {
                        writePartialHeader(socketWriter, uri, getContentType(uri), existFileSize
                                - seekSize);
                    }
                    BufferedInputStream tmpis = null;
                    byte[] b = new byte[STREAM_BUFFER];
                    int nbBytesRead = 0;
                    try {
                        tmpis = new BufferedInputStream(new FileInputStream(
                                cacheFile));
                        if (seekSize > 0) {
                            tmpis.skip(seekSize);
                        }
                        while ((nbBytesRead = tmpis.read(b)) != -1
                                && !mStopServer) {
                            socketWriter.write(b, 0, nbBytesRead);
                        }
                    } finally {
                        if (tmpis != null) {
                            tmpis.close();
                        }
                    }
                    return;
                } else {
                    // テンポラリキャッシュを使用する場合は、URLチェックを行う。
                    geturl = getContentUri(mContext, uri);
                    if (geturl == null) {
                        Logger.i("取得するURLが不正なので処理を終了します");
                        return;
                    }
                }
                
                Logger.d("request url+"+geturl);

                if (useHttpClient){//Build.VERSION.SDK_INT <= 8) {
                    URL url = new URL(geturl);
                    connManager = new ThreadSafeClientConnManager(
                            getHttpParams(), getRegistry());

                    HttpGet request = new HttpGet(geturl);
                    request.setHeader("Host", url.getHost());
                    request.setHeader("User-Agent", "Android-JustPlayer");
                    DefaultHttpClient httpClient = new DefaultHttpClient(
                            connManager, getHttpParams());

                    if (existFileSize > 0) {
                        request.addHeader("Range", "bytes=" + existFileSize
                                + "-");
                    }

                    PrefetchResponseHandler prefetchHandler = new PrefetchResponseHandler(
                            socketWriter, tempFile, pref, PLAY_STREAM_BUFFER,
                            uri, seekSize, existFileSize, null);
                    HttpResponse response = httpClient.execute(request,
                            mHttpContext);
                    String result = prefetchHandler.handleResponse(response);
                    Logger.v("--- Proxy Header :" + result);
                    if (result == null || result.startsWith("HTTP")) {
                        // エラー処理
                        String status = result;
                        if (status == null) {
                            status = "HTTP/1.1 " + HttpStatus.SC_FORBIDDEN
                                    + " unkonwn error\n";
                        }
                        Logger.v(status);
                        try {
                            socketWriter.write(status.getBytes());
                        } catch (Exception e) {
                            Logger.e(result, e);
                        }
                        return;
                    }

                    if (result != null && result.startsWith("Complete")) {
                        bComplete = true;
                    }
                } else {
                    long lastFileSize = 0;
                    HttpEntity httpEntity = null;
                    // remoteからデータを読み込む
                    connection = (HttpURLConnection) (new URL(geturl))
                            .openConnection();

                    // 既にダウンロード済みなら要求するデータはレジュームする
                    if (existFileSize > 0) {
                        connection.setRequestProperty("Range", "bytes="
                                + existFileSize + "-");
                    }
                    connection.setDoOutput(true);
                    connection.setRequestProperty("Connection", "close");

                    connection.connect();
                    // ここから下はクライアントに対するHTTPレスポンスを作成
                    Logger.v("--- Proxy Header");
                    int responseCode = connection.getResponseCode();

                    // クライアントにHTTPレスポンスを書きこむ
                    if (responseCode == 200 || responseCode == 206
                            || responseCode == 203) {
                        // 正常処理へ
                    } else {
                        String status = "HTTP/1.1 " + responseCode + " "
                                + connection.getResponseMessage() + "\n";
                        Logger.v(status);
                        try {
                            socketWriter.write(status.getBytes());
                        } catch (Exception e) {
                            Logger.e("request faile responseCode="
                                    + responseCode, e);
                        }
                        return;
                    }

                    // 残りのHTTPヘッダからファイルのサイズを取得する
                    for (Entry<String, List<String>> e : connection
                            .getHeaderFields().entrySet()) {
                        Logger.d(e.getKey() + ":" + e.getValue());
                        if (e.getKey() == null || e.getValue() == null) {
                            continue;
                        }
                        if ("content-length".equals(e.getKey().toLowerCase())) {
                            if (e.getValue().size() > 0) {
                                // ダウンロードがある場合とない場合で大きくわける
                                lastFileSize = Long
                                        .valueOf(e.getValue().get(0));
                                break;
                            }
                        }
                    }

                    long totallength = existFileSize + lastFileSize;
                    if (seekSize == 0) {
                        writeFullHeader(socketWriter, uri, getContentType(geturl), totallength);
                    } else {
                        writePartialHeader(socketWriter, uri, getContentType(geturl), totallength
                                - seekSize);
                    }

                    byte[] b = new byte[STREAM_BUFFER];
                    long nbBytesReadIncremental = 0;
                    int nbBytesRead = 0;

                    // すでにダウンロード済みのものを書きこむ
                    if (existFileSize > seekSize) {
                        Logger.d("Write from Storage");
                        BufferedInputStream tmpis = null;
                        try {
                            tmpis = new BufferedInputStream(
                                    new FileInputStream(tempFile));
                            if (seekSize > 0) {
                                nbBytesReadIncremental += tmpis.skip(seekSize);
                            }
                            while ((nbBytesRead = tmpis.read(b)) != -1
                                    && !mStopServer) {
                                nbBytesReadIncremental += nbBytesRead;
                                socketWriter.write(b, 0, nbBytesRead);
                            }
                        } finally {
                            if (tmpis != null) {
                                tmpis.close();
                            }
                        }
                    } else {
                        nbBytesReadIncremental = existFileSize;
                    }

                    if (mStopServer) {
                        return;
                    }

                    // キャッシュファイルよりもシーク箇所が低い場合は、キャッシュの生成を行う
                    BufferedOutputStream fileOutput = null;
                    if (existFileSize >= seekSize) {
                        int cachestate = Funcs.canUseCache(pref, lastFileSize);
                        if (cachestate == Funcs.CACHE_STATE_OK || cachestate == Funcs.CACHE_STATE_NOT_LIMIT_SPACE) {
                            if(cachestate == Funcs.CACHE_STATE_NOT_LIMIT_SPACE){
                                Handler handler = refHandler.get();
                                if(handler!=null){
                                    handler.sendEmptyMessage(MediaPlayerService.MSG_STREAMING_CACHE_NOTHAVESPACE);
                                }
                            }
                            fileOutput = new BufferedOutputStream(
                                    new FileOutputStream(tempFile,
                                            existFileSize > 0));
                        } else if(cachestate == Funcs.CACHE_STATE_DENGERUS){
                            Handler handler = refHandler.get();
                            if(handler!=null){
                                handler.sendEmptyMessage(MediaPlayerService.MSG_STREAMING_CACHE_NOTHAVESPACE);
                            }
                        }
                    }

                    Logger.d("Write from Stream");
                    // Server側のサイトのコネクションを取得する
                    // if(connection!=null){
                    serverInput = connection.getInputStream();
                    /*
                     * } else{ serverInput = httpEntity.getContent(); }
                     */

                    // streamから書きこむ
                    nbBytesReadIncremental = writeSocket(serverInput,
                            socketWriter, fileOutput, b,
                            nbBytesReadIncremental, seekSize);

                    socketWriter.flush();

                    bComplete = !mStopServer && fileOutput != null
                            && nbBytesReadIncremental == totallength;
                }

                Logger.d("###### fine Streaming:" + bComplete);
            }
        } catch (FileNotFoundException e) {
            Logger.e("getStreaming.FileNotFoundException", e);
            Handler handler = refHandler.get();
            if(handler!=null){
                handler.sendEmptyMessage(MediaPlayerService.MSG_FILENOTFOUNDEXCEPTION);
            }
        } catch (java.net.ConnectException e) {
            Logger.e("getStreaming.ConnectException", e);
            Handler handler = refHandler.get();
            if(handler!=null){
                handler.sendEmptyMessage(MediaPlayerService.MSG_CONNECTEXCEPTION);
            }
        } catch (IOException e) {
            Logger.e("getStreaming.IOException", e);
            Handler handler = refHandler.get();
            if(handler!=null){
                handler.sendEmptyMessage(MediaPlayerService.MSG_IOEXCEPTION);
            }
        } catch (Exception e) {
            Logger.e("getStreaming.Exception", e);
            // handler.sendEmptyMessage(MediaPlayerService.MSG_IOEXCEPTION);
        } finally {
            Logger.d("###### finish Streaming");
            // mPrefetchHandler = null;
            // キャッシュファイルの保存
            if (socketOutput != null) {
                try {
                    socketOutput.close();
                } catch (IOException e) {
                }
                socketOutput = null;
            }
            if (socketInput != null) {
                try {
                    socketInput.close();
                } catch (IOException e) {
                }
            }
            if (connection != null) {
                try {
                    connection.disconnect();
                    connection = null;
                } catch (Exception e) {
                    Logger.e("connection.disconnect", e);
                }
                connection = null;
            }
            if (client != null) {
                try {
                    client.close();
                } catch (IOException e) {
                }
            }
            if (serverInput != null) {
                try {
                    serverInput.close();
                } catch (IOException e) {
                }
            }
            if (connManager != null) {
                connManager.shutdown();
                connManager = null;
            }

            // 中断している場合はファイルを消す
            if (bComplete && (tempFile != null && tempFile.exists())
                    && (cacheFile != null && !cacheFile.exists())) {
                tempFile.renameTo(cacheFile);
                Logger.d("prefetch rename:" + cacheFile.getName());
            } else if ((tempFile != null && tempFile.exists() && tempFile
                    .length() == 0)) {
                // tempFile.delete();
            }
            if (cacheFile != null && cacheFile.exists()) {
                // readNext
                Handler handler = refHandler.get();
                if(handler!=null){
                    Message msg = handler
                            .obtainMessage(MediaPlayerService.MSG_STREAMING_NEXT);
                    msg.obj = null;//new Object[]{null, cacheFile.getName()};
                    handler.sendMessage(msg);
                }
            }
        }
    }

    class PrefetchWorker implements Runnable {
        long id;
        String url;
        OnPrefetchProgressListener listener;
        PrefetchResponseHandler mHandler;

        PrefetchWorker(long id, String url, OnPrefetchProgressListener listener) {
            this.id = id;
            this.url = url;
            this.listener = listener;
        }

        public void stop() {
            if (mHandler != null) {
                mHandler.stop();
            }
        }

        @Override
        public void run() {
            Logger.d("prefetch start:" + url);
            boolean useHttpClient = pref.getBoolean("key.useHttpClient", false);

            synchronized (_lock) {
                File cacheFile = null;
                File tempFile = null;
                long downloaded = 0;
                boolean bComplete = false;
                ThreadSafeClientConnManager connManager = null;
                DefaultHttpClient httpClient = null;
                InputStream ris = null;
                BufferedOutputStream fos = null;
                HttpURLConnection connection = null;
                try {
                    // キャッシュフォルダの作成
                    SdCardAccessHelper.cachedMusicDir.mkdirs();
                    // テンポラリキャッシュファイル（途中までダウンロードが終わったファイル）
                    tempFile = new File(SdCardAccessHelper.cachedMusicDir,
                            getTempFilename(url));
                    // フルキャッシュファイル(完全にダウンロードが終わったファイル）
                    cacheFile = new File(SdCardAccessHelper.cachedMusicDir,
                            getFilename(url));

                    // フルキャッシュファイルがあるならキャッシュファイルの作成は不要
                    if (cacheFile.exists()) {
                        return;
                    } else {
                        // テンポラリキャッシュファイルがあるならそれをつかう
                        if (tempFile.exists()) {
                            downloaded = tempFile.length();
                        } else {
                            // はじめて
                            downloaded = 0;
                        }
                    }
                    String geturl = null;
                    // テンポラリキャッシュを使用する場合は、URLチェックを行う。
                    geturl = getContentUri(mContext, url);
                    if (geturl == null) {
                        Logger.i("取得するURLが不正なので処理を終了します");
                        return;
                    }

                    if (useHttpClient){//Build.VERSION.SDK_INT <= 8) {
                        connManager = new ThreadSafeClientConnManager(
                                getHttpParams(), getRegistry());
                        httpClient = new DefaultHttpClient(connManager,
                                getHttpParams());
                        HttpGet request = new HttpGet(geturl);
                        request.setHeader("Host", new URL(geturl).getHost());
                        request.setHeader("User-Agent", "Android-JustPlayer");
                        if (downloaded > 0) {
                            request.addHeader("Range", "bytes=" + downloaded
                                    + "-");
                        }
                        Handler handler = refHandler.get();
                        if(handler!=null){
                            mHandler = new PrefetchResponseHandler(handler,
                                    tempFile, pref, STREAM_BUFFER, downloaded,
                                    listener);
                            HttpResponse response = httpClient.execute(request,
                                    mHttpContext);
                            String result = mHandler.handleResponse(response);
                            Logger.d("result:" + result);
                            if (result != null && result.startsWith("Complete")) {
                                bComplete = true;
                            }
                        }
                    } else {

                        fos = new BufferedOutputStream(new FileOutputStream(
                                tempFile, downloaded > 0));

                        // 以降フルキャッシュファイルがない場合の処理
                        // remoteからデータを読み込む
                        connection = (HttpURLConnection) (new URL(geturl))
                                .openConnection();

                        // 既にダウンロード済みなら要求するデータはレジュームする
                        if (downloaded > 0) {
                            connection.setRequestProperty("Range", "bytes="
                                    + downloaded + "-");
                        }
                        connection.setDoOutput(true);
                        connection.setRequestProperty("Connection", "close");

                        connection.connect();

                        // ここから下はクライアントに対するHTTPレスポンスを作成
                        Logger.v("--- Proxy Header");
                        int responseCode = connection.getResponseCode();

                        // クライアントにHTTPレスポンスを書きこむ
                        if (responseCode == 200 || responseCode == 206
                                || responseCode == 203) {
                            // 正常処理へ
                        } else {
                            return;
                        }

                        // 残りのHTTPヘッダを横流し
                        long lastsize = 0;
                        for (Entry<String, List<String>> e : connection
                                .getHeaderFields().entrySet()) {
                            if (e.getKey() == null || e.getValue() == null) {
                                continue;
                            }
                            if ("content-length".equals(e.getKey()
                                    .toLowerCase())) {
                                if (e.getValue().size() > 0) {
                                    // ダウンロードがある場合とない場合で大きくわける
                                    lastsize = Long
                                            .valueOf(e.getValue().get(0));
                                }
                            }
                        }

                        long totallength = downloaded + lastsize;

                        if (listener != null) {
                            listener.startProgress(totallength);
                        }

                        byte[] b = new byte[STREAM_BUFFER];
                        // Server側のサイトのコネクションを取得する
                        ris = connection.getInputStream();
                        int nbBytesRead = 0;
                        long nbBytesReadIncremental = downloaded;
                        long progStack = 0;
                        while ((nbBytesRead = ris.read(b)) != -1
                                && !mStopServer) {
                            nbBytesReadIncremental += nbBytesRead;
                            progStack += nbBytesRead;
                            fos.write(b, 0, nbBytesRead);
                            if (progStack > LIMIT_PROG_STACK) {
                                if (listener != null) {
                                    listener.progress(nbBytesReadIncremental,
                                            totallength);
                                    // Logger.d("progStack is "+progStack);
                                }
                                progStack = 0;
                            }
                        }
                        Logger.d("prefetch complete:" + url);
                        bComplete = totallength == nbBytesReadIncremental;
                    }
                } catch (FileNotFoundException e) {
                    Logger.e("prefetch.FileNotFoundException", e);
                } catch (java.net.ConnectException e) {
                    Logger.e("prefetch.ConnectException", e);
                } catch (IOException e) {
                    Logger.e("prefetch.IOException", e);
                } catch (Exception e) {
                    Logger.e("prefetch.Exception", e);
                } finally {
                    Logger.d("prefetch end:" + url);
                    mHandler = null;
                    // キャッシュファイルの保存
                    if (bComplete && (tempFile != null && tempFile.exists())
                            && (cacheFile != null && !cacheFile.exists())) {
                        tempFile.renameTo(cacheFile);
                        Logger.d("prefetch rename:" + cacheFile.getName());
                    } else if ((tempFile != null && tempFile.exists() && tempFile
                            .length() == 0)) {
                        // tempFile.delete();
                    }
                    if (fos != null) {
                        try {
                            fos.close();
                        } catch (IOException e) {
                        }
                        fos = null;
                    }
                    if (ris != null) {
                        try {
                            ris.close();
                        } catch (IOException e) {
                        }
                        ris = null;
                    }
                    if (connection != null) {
                        try {
                            connection.disconnect();
                            connection = null;
                        } catch (Exception e) {
                            Logger.e("connection.disconnect", e);
                        }
                        connection = null;
                    }
                    if (connManager != null) {
                        connManager.shutdown();
                        connManager = null;
                    }
                    if (mWorker != null && cacheFile != null
                            && cacheFile.exists()) {
                        // readNext
                        Handler handler = refHandler.get();
                        if(handler!=null){
                            Message msg = handler
                                    .obtainMessage(MediaPlayerService.MSG_STREAMING_NEXT);
                            msg.obj = new Object[]{id, cacheFile.getName()};
                            handler.sendMessage(msg);
                        }
                    }

                    prefetchThread = null;
                    if (listener != null) {
                        listener.stopProgress();
                    }
                    mWorker = null;
                }
            }
        }
    }

    private long writeSocket(InputStream serverInput,
            BufferedOutputStream socketWriter, BufferedOutputStream fileOutput,
            byte[] b, long nbBytesReadIncremental, long seekSize)
            throws IOException {
        Logger.d("Write from Stream");
        int nbBytesRead = 0;
        while ((nbBytesRead = serverInput.read(b)) != -1 && !mStopServer) {
            nbBytesReadIncremental += nbBytesRead;
            // リクエスト元への書込
            if (nbBytesReadIncremental > seekSize) {
                // Logger.d("Progress "+nbBytesReadIncremental+"/"+writesize);
                long since = nbBytesReadIncremental == 0 ? 0
                        : nbBytesReadIncremental - nbBytesRead;
                if (since >= seekSize) {
                    socketWriter.write(b, 0, nbBytesRead);
                } else {
                    int d = (int) (nbBytesReadIncremental - seekSize);
                    int size = nbBytesRead - d;
                    socketWriter.write(b, size, nbBytesRead - size);
                }
            } else {
                // Logger.d("Skip Progress "+nbBytesReadIncremental+"/"+writesize);
            }
            // キャッシュファイルへの書込処理
            if (fileOutput != null) {
                try {
                    fileOutput.write(b, 0, nbBytesRead);
                } catch (IOException e) {
                    Logger.e("os.write", e);
                    try {
                        fileOutput.close();
                    } catch (IOException e1) {
                    }
                    fileOutput = null;
                }
            }
        }

        return nbBytesReadIncremental;
    }
}
