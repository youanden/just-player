package jp.co.kayo.android.localplayer.service;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import android.annotation.TargetApi;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Build;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.provider.BaseColumns;

import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.fx.AudioFx;
import jp.co.kayo.android.localplayer.provider.DeviceContentProvider;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.bean.MediaData;

public class JukeBox {
    public static final String PREF_DS = "pref.ds";
    private static final String PREF_POSITION = "pref.position";
    private static final String PREF_PLAYPOSITION = "pref.playposition";
    private Uri orderuri = Uri.parse(DeviceContentProvider.MEDIA_CONTENT_AUTHORITY_SLASH+"order/audio");

    private MediaPlayer mp;
    private AudioFx audiofx;
    private int repeatmode;
    private ArrayList<MediaData> playlist = new ArrayList<MediaData>();
    private ArrayList<Long> nextlist = new ArrayList<Long>();
    private boolean isShuffle;
    private int seekPosition = 0;
    private Random rand = new Random(System.nanoTime());
    private boolean isLoaded = false;
    private SharedPreferences pref;
    private Context mContext;
    private int position;
    private int playposition = 0;
    private boolean prepare = false;

    public JukeBox(Context context) {
        mContext = context;
        pref = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public boolean isLoaded() {
        return isLoaded;
    }
    
    private void loadPlaylist(String cur_contenturi){
        playlist = new ArrayList<MediaData>();
        nextlist = new ArrayList<Long>();
        
        Cursor cursor = null;
        try{
            cursor = mContext.getContentResolver().query(orderuri, new String[]{BaseColumns._ID, TableConsts.PLAYBACK_MEDIA_ID, TableConsts.PLAYBACK_STATE, TableConsts.PLAYBACK_DATA}, TableConsts.PLAYBACK_URI + " = ?", new String[]{cur_contenturi}, TableConsts.PLAYBACK_ORDER);

            if(cursor != null && cursor.moveToFirst()){
                do{
                    long id = cursor.getInt(cursor.getColumnIndex(BaseColumns._ID));
                    long media_id = cursor.getInt(cursor.getColumnIndex(TableConsts.PLAYBACK_MEDIA_ID));
                    int state = cursor.getInt(cursor.getColumnIndex(TableConsts.PLAYBACK_STATE));
                    String data = cursor.getString(cursor.getColumnIndex(TableConsts.PLAYBACK_DATA));
                    if(state!=MediaData.PLAYED){
                        nextlist.add(id);
                    }
                    playlist.add(new MediaData(id, media_id, state, data));
                }while(cursor.moveToNext());
            }
            if(isShuffle){
                makeShuffle();
            }
        }
        finally{
            if(cursor!=null){
                cursor.close();
            }
        }
    }
    
    public void load() {
        isLoaded = true;
        playlist = new ArrayList<MediaData>();
        nextlist = new ArrayList<Long>();
        isShuffle = pref.getBoolean(SystemConsts.PREF_SHUFFLEFLG, false);
        setRepeatMode(pref.getInt(SystemConsts.PREF_REPEATFLG, SystemConsts.FLG_REPEAT_NO));

        String cur_contenturi = pref.getString(SystemConsts.PREF_CONTENTURI,
                DeviceContentProvider.MEDIA_AUTHORITY);
        loadPlaylist(cur_contenturi);
        
        String save_contenturi = pref.getString(PREF_DS, "");
        if(save_contenturi.equals(cur_contenturi)) {
            position = pref.getInt(PREF_POSITION, -1);
            playposition = pref.getInt(PREF_PLAYPOSITION, 0);
        }else{
            position = -1;
            playposition = 0;
        }

        if(playlist.size()<=position){
            position = -1;
            playposition = 0;
        }
    }

    public void save(boolean prepareBefore) {
        if (audiofx != null) {
            try {
                audiofx.save();
            } catch (Exception e) {
            }
        }
        
        Editor editor = pref.edit();

        editor.putString(PREF_DS, pref.getString(SystemConsts.PREF_CONTENTURI,
                DeviceContentProvider.MEDIA_AUTHORITY));
        editor.putInt(PREF_POSITION, position);
        if(!prepareBefore && mp!=null && mp.isPlaying()){
            playposition = mp.getCurrentPosition();
        }
        else{
            playposition = 0;
        }
        editor.putInt(PREF_PLAYPOSITION, playposition);
        editor.putBoolean(SystemConsts.PREF_SHUFFLEFLG, isShuffle);
        editor.putInt(SystemConsts.PREF_REPEATFLG, getRepeatMode());

        editor.commit();
    }

    public int getSeekPosition() {
        return seekPosition;
    }

    public void setSeekPosition(int seekPosition) {
        this.seekPosition = seekPosition;
    }

    public boolean isShaffle() {
        return isShuffle;
    }

    public int getRepeatMode() {
        return repeatmode;
    }

    public void setRepeatMode(int mode) {
        this.repeatmode = mode;
    }

    private final String[] FETCH = new String[]{ AudioMedia._ID, AudioMedia.TITLE, AudioMedia.ARTIST, AudioMedia.ALBUM, AudioMedia.ALBUM_KEY, AudioMedia.DATA, AudioMedia.DURATION};
    
    public void addMedia(long id, int notplayed, long duration, String title, String album,
            String artist, String data) {
        
        String cur_contenturi = pref.getString(SystemConsts.PREF_CONTENTURI,
                DeviceContentProvider.MEDIA_AUTHORITY);
        ContentValues values = new ContentValues();
        values.put(TableConsts.PLAYBACK_URI, cur_contenturi);
        values.put(TableConsts.PLAYBACK_TITLE, title);
        values.put(TableConsts.PLAYBACK_ARTIST, artist);
        values.put(TableConsts.PLAYBACK_ALBUM, album);
        values.put(TableConsts.PLAYBACK_MEDIA_ID, id);
        //values.put(TableConsts.PLAYBACK_ALBUM_KEY, null);
        //values.put(TableConsts.PLAYBACK_ARTIST_KEY, null);
        values.put(TableConsts.PLAYBACK_DATA, data);
        values.put(TableConsts.PLAYBACK_DURATION, duration);
        values.put(TableConsts.PLAYBACK_ORDER, System.currentTimeMillis());
        values.put(TableConsts.PLAYBACK_STATE, MediaData.NOTPLAYED);
        
        Uri uri = mContext.getContentResolver().insert(orderuri, values);
        long createid = ContentUris.parseId(uri);
        playlist.add(new MediaData(createid, id, MediaData.NOTPLAYED, duration, title, album, artist, data));
        nextlist.add(createid);
    }
    
    public void addMedia(long id, String data) {
        String cur_contenturi = pref.getString(SystemConsts.PREF_CONTENTURI,
                DeviceContentProvider.MEDIA_AUTHORITY);
        ContentValues values = new ContentValues();
        values.put(TableConsts.PLAYBACK_URI, cur_contenturi);
        values.put(TableConsts.PLAYBACK_MEDIA_ID, id);
        values.put(TableConsts.PLAYBACK_DATA, data);
        values.put(TableConsts.PLAYBACK_ORDER, System.currentTimeMillis());
        values.put(TableConsts.PLAYBACK_STATE, MediaData.NOTPLAYED);
        
        Uri uri = mContext.getContentResolver().insert(orderuri, values);
        long createid = ContentUris.parseId(uri);
        playlist.add(new MediaData(createid, id, MediaData.NOTPLAYED, data));
        nextlist.add(createid);
    }

    public ArrayList<MediaData> getList() {
        return playlist;
    }
    
    public ArrayList<Long> getNexList() {
        return nextlist;
    }

    public int getPosition() {
        return position;
    }
    
    public int getRestorePosition() {
        int ret = playposition;
        playposition = 0;
        Editor editor = pref.edit();
        editor.putInt(PREF_PLAYPOSITION, playposition);
        editor.commit();
        
        return ret;
    }

    public void incRepeat() {
        if (repeatmode < 2) {
            repeatmode++;
        } else {
            repeatmode = 0;
        }

        Editor editor = pref.edit();
        editor.putInt(SystemConsts.PREF_REPEATFLG, getRepeatMode());
        editor.commit();
    }

    public void shuffle() {
        isShuffle = !isShuffle;
        if(isShuffle){
            makeShuffle();
        }else{
            makeSort();
        }

        save(false);
    }

    public boolean clear() {
        int count = playlist.size();
        position = -1;
        playposition = 0;
        
        String cur_contenturi = pref.getString(SystemConsts.PREF_CONTENTURI,
                DeviceContentProvider.MEDIA_AUTHORITY);
        mContext.getContentResolver().delete(orderuri, TableConsts.PLAYBACK_URI + " = ?", new String[]{cur_contenturi});
        playlist = new ArrayList<MediaData>();
        nextlist = new ArrayList<Long>();
        
        save(false);
        
        return count > 0;
    }

    public boolean clearcut() {
        int befor_count = playlist.size();
        String cur_contenturi = pref.getString(SystemConsts.PREF_CONTENTURI,
                DeviceContentProvider.MEDIA_AUTHORITY);
        mContext.getContentResolver().delete(orderuri, TableConsts.PLAYBACK_URI + " = ? AND "+TableConsts.PLAYBACK_STATE + " = ?", new String[]{cur_contenturi, Integer.toString(MediaData.NOTPLAYED)});
        loadPlaylist(cur_contenturi);
        
        int count = playlist.size();
        return befor_count != count;
    }
    
    public MediaData getItem(int pos){
        //指定の位置のIDを取得するない場合はnull
        if(pos >= 0 && playlist.size()>pos){
            return playlist.get(pos);
        }
        return null;
    }
    
    private long getNext(int pos){
        if(pos >= 0 && nextlist.size()>pos){
            return nextlist.get(pos);
        }
        return -1;
    }
    
    private void setPosition(MediaData id){
        if(id != null){
            if(!isShuffle){
                //シャッフルモードでない場合は、指定のポジションより前の曲は再生済にする
                for(MediaData p : playlist){
                    nextlist.remove(Long.valueOf(p.id));
                    setPlayed(p.id);
                    if(p.id == id.id){
                        break;
                    }
                }
            }else{
                nextlist.remove(Long.valueOf(id.id));
                setPlayed(id.id);
            }
            setPosition(id.id);
        }else{
            position = -1;
            playposition = 0;
        }
    }
    
    private void setPosition(long id){
        //指定IDの位置にポジションを設定する、ない場合は−１
        if(id!=-1){
            for(int i=0; i<playlist.size(); i++){
                MediaData p = playlist.get(i);
                if(p.id == id){
                    position = i;
                    playposition = 0;
                    return;
                }
            }
        }
        position = -1;
        playposition = 0;
    }

    public boolean removePosition(int pos) {
        MediaData id = getItem(pos);
        MediaData cur = getItem(position);
        MediaData ncur = getItem(position+1);
        if(id != null){
            
            String cur_contenturi = pref.getString(SystemConsts.PREF_CONTENTURI,
                    DeviceContentProvider.MEDIA_AUTHORITY);
            
            int n = mContext.getContentResolver().delete(orderuri, TableConsts.PLAYBACK_URI + " = ? AND "+BaseColumns._ID + " = ?", new String[]{cur_contenturi, Long.toString(id.id)});
            if(n>0){
                loadPlaylist(cur_contenturi);
                
                if(cur == null || id.id != cur.id){
                    setPosition(cur);
                }else {
                    setPosition(ncur);
                }
                
                return true;
            }
            else{
                return false;
            }
        }
        return false;
    }
    
    private boolean setPlayed(long id){
        if(id!=-1){
            ContentValues values = new ContentValues();
            values.put(TableConsts.PLAYBACK_STATE, MediaData.PLAYED);
            mContext.getContentResolver().update(ContentUris.withAppendedId(orderuri, id), values, null, null);
            
            return true;
        }
        return false;
    }
    
    private boolean setResetPlayed(String cur_contenturi){
        ContentValues values = new ContentValues();
        values.put(TableConsts.PLAYBACK_STATE, MediaData.NOTPLAYED);
        int n = mContext.getContentResolver().update(orderuri, values, TableConsts.PLAYBACK_URI + " = ? ", new String[]{cur_contenturi});
        
        return n>0;
    }

    public void movePosition(int pos) {
        String cur_contenturi = pref.getString(SystemConsts.PREF_CONTENTURI,
                DeviceContentProvider.MEDIA_AUTHORITY);
        MediaData next = getItem(pos);
        setResetPlayed(cur_contenturi);
        loadPlaylist(cur_contenturi);
        setPosition(next);
    }

    public boolean moveNext() {
        if (repeatmode == SystemConsts.FLG_REPEAT_ONCE) {
            return position != -1;
        }
        
        String cur_contenturi = pref.getString(SystemConsts.PREF_CONTENTURI,
                DeviceContentProvider.MEDIA_AUTHORITY);
        
        if(nextlist.size()>0){
            long next = nextlist.remove(0);
            setPlayed(next);
            setPosition(next);
            playposition = 0;
            return true;
        }else if(repeatmode == SystemConsts.FLG_REPEAT_ALL){
            setResetPlayed(cur_contenturi);
            loadPlaylist(cur_contenturi);
            if(playlist.size()>0){
                long next = nextlist.remove(0);
                setPlayed(next);
                setPosition(next);
            }
            else{
                position = -1;
            }
            playposition = 0;
            return true;
        }else{
            setResetPlayed(cur_contenturi);
            loadPlaylist(cur_contenturi);
            position = -1;
            playposition = 0;
        }
        
        return false;
    }
    
    public void moveBack(MediaData id) {
        if(id != null){
            setPosition(id.id);
        }else{
            position = -1;
            playposition = 0;
        }
    }

    public MediaData getCurrent() {
        MediaData id = getItem(position);
        return id;
    }
    
    public MediaData selectMediaData(long id){
        for(MediaData data : playlist){
            if(data.id == id){
                return data;
            }
        }
        return null;
    }
    
    public MediaData getMediaData(int pos) {
        MediaData data = getItem(pos);
        if(data != null){
            if(data.getDuration() <= 0 || data.getAlbum() == null){
                Cursor cursor = null;
                try{
                    if(data.getDuration() <= 0 && data.mediaId>0){
                        cursor = mContext.getContentResolver().query(
                                ContentUris.withAppendedId(MediaConsts.MEDIA_CONTENT_URI, data.mediaId), FETCH, null,
                                null, null);
                        if(cursor!=null && cursor.moveToFirst()){
                            String cur_contenturi = pref.getString(SystemConsts.PREF_CONTENTURI,
                                    DeviceContentProvider.MEDIA_AUTHORITY);
                            long duration = cursor.getLong(cursor.getColumnIndex(AudioMedia.DURATION));
                            data.setTitle(cursor.getString(cursor.getColumnIndex(AudioMedia.TITLE)));
                            data.setAlbum(cursor.getString(cursor.getColumnIndex(AudioMedia.ALBUM)));
                            data.setArtist(cursor.getString(cursor.getColumnIndex(AudioMedia.ARTIST)));
                            String albumKey = cursor.getString(cursor.getColumnIndex(AudioMedia.ALBUM_KEY));
                            data.setDuration(duration);
                            if(duration>0){
                                ContentValues values = new ContentValues();
                                values.put(TableConsts.PLAYBACK_ALBUM_KEY, albumKey);
                                values.put(TableConsts.PLAYBACK_DURATION, duration);
                                mContext.getContentResolver().update(orderuri, values, TableConsts.PLAYBACK_URI + " = ? AND "+BaseColumns._ID + " = ?", new String[]{cur_contenturi, Long.toString(data.id)});
                            }
                        }
                    }else{
                        cursor = mContext.getContentResolver().query(ContentUris.withAppendedId(orderuri, data.id), null, null, null, null);

                        if(cursor != null && cursor.moveToFirst()){
                            data.setTitle(cursor.getString(cursor.getColumnIndex(TableConsts.PLAYBACK_TITLE)));
                            data.setAlbum(cursor.getString(cursor.getColumnIndex(TableConsts.PLAYBACK_ALBUM)));
                            data.setArtist(cursor.getString(cursor.getColumnIndex(TableConsts.PLAYBACK_ARTIST)));
                            data.setDuration(cursor.getLong(cursor.getColumnIndex(TableConsts.PLAYBACK_DURATION)));
                        }
                    }
                    return data;
                }
                finally{
                    if(cursor!=null){
                        cursor.close();
                    }
                }
            }
            else{
                return data;
            }
        }
        return null;
    }

    public MediaData getNextPrefetch(int next) {
        long id = getNext(next);
        if(id != -1){
            Cursor cursor = null;
            try{
                cursor = mContext.getContentResolver().query(orderuri, null, BaseColumns._ID + " = ?", new String[]{Long.toString(id)}, null);
    
                if(cursor.moveToFirst()){
                    MediaData data = new MediaData(
                            cursor.getLong(cursor.getColumnIndex(BaseColumns._ID)), 
                            cursor.getLong(cursor.getColumnIndex(TableConsts.PLAYBACK_MEDIA_ID)), 
                            cursor.getInt(cursor.getColumnIndex(TableConsts.PLAYBACK_STATE)), 
                            cursor.getLong(cursor.getColumnIndex(TableConsts.PLAYBACK_DURATION)), 
                            cursor.getString(cursor.getColumnIndex(TableConsts.PLAYBACK_TITLE)), 
                            cursor.getString(cursor.getColumnIndex(TableConsts.PLAYBACK_ALBUM)), 
                            cursor.getString(cursor.getColumnIndex(TableConsts.PLAYBACK_ARTIST)), 
                            cursor.getString(cursor.getColumnIndex(TableConsts.PLAYBACK_DATA)));
                    return data;
                }
            }
            finally{
                if(cursor!=null){
                    cursor.close();
                }
            }
        }
        return null;
    }

    public int getBack(int pos, long duration) {
        // 全体の１０％以下なら頭出し、そうでなければ前の曲
        if (duration > 0 && pos > 0) {
            float pt = ((float) pos / duration) * 100.0f;
            if (pt > 10) {
                return position;
            } else {
                if (position > 0) {
                    return position - 1;
                } else {
                    return position;
                }
            }
        } else {
            if (position > 0) {
                return position - 1;
            } else {
                return position;
            }
        }
    }

    public boolean hasNext() {
        if(nextlist.size()>0){
            return true;
        }
        else if(repeatmode == SystemConsts.FLG_REPEAT_ALL){
            return playlist.size()>0;
        }
        return false;
    }

    public boolean drop(int from, int to) {
        return false;
    }

    private void makeShuffle() {
        int count = nextlist.size();
        ArrayList<Long> shuffle = new ArrayList<Long>(count);
        for(Long id : nextlist){
            int index = shuffle.size() > 0 ? rand.nextInt(shuffle.size()) : 0;
            if (shuffle.size() > index) {
                shuffle.add(index, id);
            } else {
                shuffle.add(id);
            }
        }
        nextlist = shuffle;
    }
    
    private void makeSort() {
        Collections.sort(nextlist);
    }
    
    public MediaPlayer getMediaPlayer(){
        return mp;
    }
    
    public boolean isInitialize(){
        return mp != null;
    }
    
    public boolean Initialize(OnPreparedListener prepare, OnCompletionListener completion, OnBufferingUpdateListener bufering, OnErrorListener error){
        //MediaPlayer
        mp = new MediaPlayer();
        mp.setOnPreparedListener(prepare);
        mp.setOnCompletionListener(completion);
        mp.setOnBufferingUpdateListener(bufering);
        mp.setLooping(false);
        mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mp.setOnErrorListener(error);
        
        if (Build.VERSION.SDK_INT > 8) {
            try {
                audiofx = new AudioFx(this);
                audiofx.load();
            } catch (Exception e) {
                audiofx = null;
            }
        }
        
        // load playlist
        load();
        
        //autoPlay
        return true;
    }
    
    public SharedPreferences getPreferences(){
        return pref;
    }
    
    public boolean Release(){
        if (audiofx != null) {
            try {
                audiofx.release();
            } catch (Exception e) {
                audiofx = null;
            }
        }
        if (mp != null) {
            mp.release();
            mp = null;
            return true;
        }
        return false;
    }
    
    public boolean isPlaying(){
        if(mp!=null){
            return mp.isPlaying();
        }
        return false;
    }

    public boolean play(){
        if (mp != null && !mp.isPlaying()) {
            mp.start();
            return true;
        }
        return false;
    }
    
    public boolean pause(){
        if (mp != null && mp.isPlaying()) {
            mp.pause();
            return true;
        }
        return false;
    }
    
    public boolean stop(){
        if(mp!=null){
            if (mp.isPlaying()) {
                mp.stop();
            }
            mp.reset();
            return true;
        }
        return false;
    }
    
    @TargetApi(9)
    public int getAudioSessionId(){
        if(mp!=null){
            return mp.getAudioSessionId();
        }
        return -1;
    }
    
    public boolean prepareAsync(){
        if(mp!=null){
            mp.prepareAsync();
            return true;
        }
        return false;
    }

    public boolean setLooping(boolean b){
        if(mp!=null){
            mp.setLooping(b);
            return true;
        }
        return false;
    }
    
    public boolean setDataSource(String path) throws IllegalArgumentException, SecurityException, IllegalStateException, IOException{
        if(mp!=null){
            mp.setDataSource(path);
            return true;
        }
        return false;
    }
    
    public boolean setDataSource(Context context, Uri uri) throws IllegalArgumentException, SecurityException, IllegalStateException, IOException{
        if(mp!=null){
            mp.setDataSource(context, uri);
            return true;
        }
        return false;
    }
    
    public int getCurrentPosition(){
        if(mp!=null){
            return mp.getCurrentPosition();
        }
        return 0;
    }
    
    public int getDuration(){
        if(mp!=null){
            return mp.getDuration();
        }
        return 0;
    }
    
    public boolean seekTo(int msec){
        if(mp!=null){
            mp.seekTo(msec);
            return true;
        }
        return false;
    }

    public boolean setVolume(float vol) {
        if(mp!=null){
            mp.setVolume(vol, vol);
            return true;
        }
        
        return false;
    }

    public void setEqEnabled(boolean enabled) {
        if (audiofx != null) {
            audiofx.setEqEnabled(enabled);
            try {
                audiofx.save();
            } catch (Exception e) {
            }
        }
    }

    public boolean getEqEnabled() {
        if (audiofx != null) {
            return audiofx.getEqEnabled();
        }
        return false;
    }

    public void setRvEnabled(boolean enabled) {
        if (audiofx != null) {
            audiofx.setRvEnabled(enabled);
            try {
                audiofx.save();
            } catch (Exception e) {
            }
        }
    }

    public boolean getRvEnabled() {
        if (audiofx != null) {
            return audiofx.getRvEnabled();
        }
        return false;
    }

    public void setBsEnabled(boolean enabled) {
        if (audiofx != null) {
            audiofx.setBsEnabled(enabled);
            try {
                audiofx.save();
            } catch (Exception e) {
            }
        }
    }

    public boolean getBsEnabled() {
        if (audiofx != null) {
            return audiofx.getBsEnabled();
        }
        return false;
    }

    public int getNumberOfPresets() {
        if (audiofx != null) {
            return audiofx.getNumberOfPresets();
        }
        return 0;
    }

    public String getPresetName(short n) {
        if (audiofx != null) {
            return audiofx.getPresetName(n);
        }
        return null;
    }

    public int getBandLevel(short band) {
        if (audiofx != null) {
            return audiofx.getBandLevel(band);
        }
        return 0;
    }

    public int getCenterFreq(short band) {
        if (audiofx != null) {
            return audiofx.getCenterFreq(band);
        }
        return 0;
    }

    public int getCurrentPreset() {
        if (audiofx != null) {
            return audiofx.getCurrentPreset();
        }
        return 0;
    }

    public int getNumberOfBands() {
        if (audiofx != null) {
            return audiofx.getNumberOfBands();
        }
        return 0;
    }

    public int getMinEQLevel() {
        if (audiofx != null) {
            return audiofx.getMinEQLevel();
        }
        return 0;
    }

    public int getMaxEQLevel() {
        if (audiofx != null) {
            return audiofx.getMaxEQLevel();
        }
        return 0;
    }

    public void usePreset(short preset) {
        if (audiofx != null) {
            audiofx.usePreset(preset);
        }
    }

    public void setBandLevel(short band, short level) {
        if (audiofx != null) {
            audiofx.setBandLevel(band, level);
        }
    }

    public int getRvPreset() {
        if (audiofx != null) {
            return audiofx.getRvPreset();
        }
        return 0;
    }

    public void setRvPreset(short preset) {
        if (audiofx != null) {
            audiofx.setRvPreset(preset);
        }
    }

    public int getStrength() {
        if (audiofx != null) {
            return audiofx.getStrength();
        }
        return 0;
    }

    public void setStrength(short strength) {
        if (audiofx != null) {
            audiofx.setStrength(strength);
        }
    }

}
