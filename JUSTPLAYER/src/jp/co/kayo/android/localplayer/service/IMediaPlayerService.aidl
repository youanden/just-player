package jp.co.kayo.android.localplayer.service;

import jp.co.kayo.android.localplayer.service.IMediaPlayerServiceCallback;

interface IMediaPlayerService {
	long getMediaId();
	int getPosition();
	int getPort();
	long getPrefetchId();
	int getCount();
	int  stat();
	void stop();
	oneway void pause();
	oneway void play();
	oneway void ff();
	oneway void rew();
	oneway void shuffle();
	oneway void repeat();
	oneway void reload();
	void seek(int pos);
	void clear();
	void clearcut();
	void setPosition(int pos);
	oneway void drop(int from, int to);
	oneway void remove(int pos);
	long getSeekPosition();
	long getDuration();
	void addMedia(long id, String data);
	void addMediaD(long id, long duration, String title, String album, String artist, String data);
	String[] getMediaD(int pos);
	long[] getList();
	oneway void release();
	oneway void commit();
	int getAudioSessionId();
	boolean setContentsKey(String key);
	String getContentsKey();
	void lockUpdateToPlay();
	
	void setEqEnabled(boolean enabled);
	boolean getEqEnabled();
	void setRvEnabled(boolean enabled);
	boolean getRvEnabled();
	void setBsEnabled(boolean enabled);
	boolean getBsEnabled();
	
	
	int getNumberOfPresets();
	String getPresetName(int n);
	int getBandLevel(int band);
	int getCenterFreq(int band);
	int getCurrentPreset();
	int getNumberOfBands();
	int getMinEQLevel();
	int getMaxEQLevel();
	void usePreset(int preset);
	void setBandLevel(int band, int level);
	
	int getRvPreset();
	void setRvPreset(int preset);
	
	int getStrength();
    void setStrength(int strength);
	
	//サービスにコールバックを登録します
	void registerCallback(IMediaPlayerServiceCallback callback);
	//サービスからコールバックを解除します
	void unregisterCallback(IMediaPlayerServiceCallback callback);
}