
package jp.co.kayo.android.localplayer.service;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.Hashtable;

import jp.co.kayo.android.localplayer.MainActivity2;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.appwidget.AppWidgetHelper;
import jp.co.kayo.android.localplayer.appwidget.ColorSet;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.util.Funcs;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.widget.RemoteViews;

public class MediaPlayerHelper {

    public static void setCustomizedNotify(Service service,
            Notification notification, Hashtable<String, String> tbl1,
            Hashtable<String, String> tbl2, Bitmap bitmap) {
        try {

            ColorSet colorset = new ColorSet();
            colorset.load(service);

            RemoteViews remoteView = new RemoteViews(service.getPackageName(),
                    R.layout.statusbar);
            if(bitmap!=null){
                remoteView.setBitmap(R.id.imageView1, "setImageBitmap", bitmap);
            }
            else{
                remoteView.setImageViewResource(R.id.imageView1, R.drawable.albumart_mp_unknown);
            }

            PendingIntent contentIntent = PendingIntent.getActivity(service, 0, new Intent(service,
                    MainActivity2.class), Intent.FLAG_ACTIVITY_NEW_TASK);
            remoteView.setOnClickPendingIntent(R.id.imageView1, contentIntent);

            remoteView.setOnClickPendingIntent(R.id.btnPlay,
                    MediaPlayerService.createIntent(service, AppWidgetHelper.CALL_PLAY_PAUSE));
            remoteView.setOnClickPendingIntent(R.id.btnFf,
                    MediaPlayerService.createIntent(service, AppWidgetHelper.CALL_FF));
            remoteView.setOnClickPendingIntent(R.id.btnRew,
                    MediaPlayerService.createIntent(service, AppWidgetHelper.CALL_REW));
            remoteView.setOnClickPendingIntent(R.id.btnClose,
                    MediaPlayerService.createIntent(service, AppWidgetHelper.CALL_STOP));

            remoteView.setInt(R.id.btnPlay, "setImageResource",
                    R.drawable.widget_pause);

            int pricolor = colorset.getColor(ColorSet.KEY_NOTIFI_PRI_COLOR);
            int seccolor = colorset.getColor(ColorSet.KEY_NOTIFI_SEC_COLOR);
            if (pricolor != -1) {
                remoteView.setTextColor(R.id.textTitle, pricolor);
            }
            if (seccolor != -1) {
                remoteView.setTextColor(R.id.textArtist, seccolor);
                remoteView.setTextColor(R.id.textTime, seccolor);
            }

            remoteView.setTextViewText(R.id.textTitle,
                    Funcs.trimString(tbl1.get(AudioMedia.TITLE)));
            remoteView.setTextViewText(R.id.textArtist,
                    Funcs.trimString(tbl1.get(AudioMedia.ARTIST)));
            remoteView.setTextViewText(R.id.textTime, Funcs
                    .makeTimeString(Funcs.parseLong(tbl1
                            .get(AudioMedia.DURATION))));

            notification.contentView = remoteView;
        } finally {
            // (bitmap!=null){
            // bitmap.recycle();
            bitmap = null;
            // }
        }
    }

}
