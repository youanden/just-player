package jp.co.kayo.android.localplayer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.FontMetrics;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Window;
import android.view.WindowManager;

public class CreditActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(new CreditView(this));
    }

    static class CreditItem{
        int type;
        String text;
        CreditItem(int type, String text){
            this.type = type;
            this.text = text;
        }
    }
    
    static class CreditView extends SurfaceView implements SurfaceHolder.Callback, Runnable {
        private static final int FRAME_RATE = 20;
        private SurfaceHolder mHolder;
        private Thread mThread;
        private boolean mAttached;
        private ArrayList<CreditItem> mCredits= new ArrayList<CreditItem>();
        private float mSpeed = 2;
        private float mPos = 0;
        private float mWidth=0;
        private float mHeight=0;
        private float mSpace=0;
        private Paint mTextPaint1 = new Paint();
        private Paint mTextPaint2 = new Paint();
        
        public CreditView(Context context) {
            super(context);
            mHolder = getHolder();
            mHolder.addCallback(this);
        }

        @Override
        public void run() {
            load();
            WindowManager wm = (WindowManager)getContext().getSystemService(Context.WINDOW_SERVICE);
            Display disp = wm.getDefaultDisplay();
            mWidth = disp.getWidth();
            mHeight = disp.getHeight();
            
            while (mAttached) {
                long starttime = System.currentTimeMillis();
                
                doDraw();
                
                long sleeptime = 1000 / FRAME_RATE;
                long amounttime = System.currentTimeMillis() - starttime;
                sleeptime -= amounttime;
                if (sleeptime > 0) {
                    try {
                        Thread.sleep(sleeptime);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        
        private void load(){
            mCredits.clear();
            AssetManager as = getResources().getAssets(); 
            InputStream in = null;
            try {
                in = as.open("credit.txt");
                BufferedReader br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
                while(true){
                    String line = br.readLine();
                    if(line==null){
                        break;
                    }
                    if(line.length()>0){
                        String[] split = line.split(",");
                        if(split!=null && split.length>0){
                            String type = split[0];
                            String text = "";
                            if(split.length>1){
                                text = split[1];
                            }
                            mCredits.add(new CreditItem(Integer.parseInt(type), text));
                        }
                    }
                }
                
                PackageInfo packageInfo = null;
                try {
                        if(mCredits.size()>0){
                            mCredits.add(new CreditItem(0, ""));
                            mCredits.add(new CreditItem(0, ""));
                            mCredits.add(new CreditItem(0, ""));
                        }
                        mCredits.add(new CreditItem(0, "BUILD VERSION"));
                        packageInfo = getContext().getPackageManager().getPackageInfo("jp.co.kayo.android.localplayer", PackageManager.GET_META_DATA);
                        String credit = getContext().getString(R.string.app_name)+" "+ packageInfo.versionName;
                                
                        mCredits.add(new CreditItem(1, credit));
                } catch (NameNotFoundException e) {
                        e.printStackTrace();
                }
                
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally{
                if(in!=null){
                    try {
                        in.close();
                    } catch (IOException e) {}
                }
            }
        }
        
        private void doDraw(){
            Canvas canvas = mHolder.lockCanvas();
            if (canvas != null) {
                try {
                    canvas.drawColor(Color.BLACK);

                    if(mHeight>0){
                        float base = mHeight - mPos;
                        for(Iterator<CreditItem> ite=mCredits.iterator(); ite.hasNext(); ){
                            CreditItem item = ite.next();
                            Paint paint = item.type == 0?mTextPaint1:mTextPaint2;
                            FontMetrics metrics = paint.getFontMetrics();
                            float textWidth = paint.measureText(item.text);
                            float textHeight = Math.abs(metrics.ascent+metrics.descent)+mSpace;
                            float x = mWidth/2 - textWidth/2;
                            float y = base;
                            
                            canvas.drawText(item.text, x, y, paint);
                            base += textHeight;
                        }
                        mPos += mSpeed;
                        if(base<-50){
                            mPos = 0;
                        }
                    }
                } finally {
                    mHolder.unlockCanvasAndPost(canvas);
                }
            }
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        }

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            mAttached = true;
            mThread = new Thread(this);
            mThread.start();
            
            mSpace = getContext().getResources().getDimensionPixelSize(R.dimen.credit_space);
            mTextPaint1.setColor(Color.WHITE);
            mTextPaint1.setTypeface(Typeface.DEFAULT_BOLD);
            mTextPaint1.setTextSize(getContext().getResources().getDimensionPixelSize(R.dimen.credit_text1));
            mTextPaint1.setAntiAlias(true);
            
            mTextPaint2.setColor(Color.WHITE);
            mTextPaint2.setTextSize(getContext().getResources().getDimensionPixelSize(R.dimen.credit_text2));
            mTextPaint2.setAntiAlias(true);
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            mAttached = false;
            while (mThread.isAlive()){
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                }
            }
        }
        
    }
}
